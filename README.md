# mt-ui
Flutter Frontend

# Releasing iOS
## Option A
    flutter clean
    flutter build ios --release

##if Option A does not work, then:

### Remove leftover xcode compilation files
    rm -rf $HOME/Library/Developer/Xcode/DerivedData/*
    rm -rf $HOME/Library/Developer/Xcode/DerivedData/*
    rm -rf $HOME/Library/Developer/Xcode/DerivedData/*

### Destroy entire flutter cache (will be redownloaded and rebuilt)
    rm -rf $HOME/Library/Flutter/bin/cache/*

### Sometimes Flutter doesn't recompile the frameworks
    rm -rf ios/Flutter/App.framework ios/Flutter/Flutter.framework

### Remove the entire pub-cache
    rm -rf ~/.pub-cache/*

### Remove the build directory
    rm -rf build

### Remove the .packages file
    rm -f .packages

### Remove the plugins directory
    rm -rf .flutter-plugins

    pushd ios
    pod deintegrate
    rm -rf Pods Podfile.lock
    rm -rf .symlinks/*
    popd

    flutter packages get
