import 'dart:convert';
import 'dart:math';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;


class Utils{

  static String get googleApiKey => "AIzaSyB_kIX5UrOzY9KC14LVNRAIsZCkx3xBXeA";

  static String convertToTwoDecimals(double val){

    // Round double to 2 decimals
    double mod = pow(10.0, 2);
    double value = ((val * mod).round().toDouble() / mod);

    // Return number format that always has 2 decimals
    return NumberFormat("####.00").format(value);
  }

  static String capitalize(String s) { 
      var i;
      if(s != null && s.length > 0) {
        i = s[0].toUpperCase() + s.substring(1);
      } else {
        i = "";
      }
      return i;
    }

  static launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  static share(String message,{String subject}) {
   Share.share(message,subject:subject);
  }

  static bool isIOS (context) => Theme.of(context).platform == TargetPlatform.iOS;

  // works at least for Youtube and SoundCloud but
  // quite a few others as well. Check www.noembed.com
  static Future<MediaInfo> getMediaInfo (String videoUrl) async {
    if(videoUrl == null || videoUrl.isEmpty){
      return null;
    }
    return await http.Client()
        .get("https://noembed.com/embed?url=" + videoUrl)
        .then((result) => result.body)
        .then(json.decode)
        .then((json) => MediaInfo(
      title: json['title'],
      thumbnailUrl: json['thumbnail_url'],
    ));
  }

  static bool isValidEmail(String value) {
    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    return RegExp(pattern).hasMatch(value);
  }

  Future<bool> isOnline() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile|| connectivityResult == ConnectivityResult.wifi) {
      return true;
    }else{
      return false;
    }
  }
  
  static shareToSocial(String text){
      print('Share Link:- $text');
    Share.share(text);
  }

  static bool isTablet() {
    final data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
    return data.size.shortestSide < 600 ? false : true;
  }

}

class MediaInfo{
  final String thumbnailUrl;
  final String title;
  MediaInfo({this.thumbnailUrl, this.title});
}

