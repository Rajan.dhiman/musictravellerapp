import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mt_ui/data/Invoices.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:rxdart/rxdart.dart';

import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'dart:io';

// fetching data from API following the BloC pattern as
// explained here: https://www.youtube.com/watch?v=ALcbTxz3bUw&t=868s

class BookingBloc {

  // List<Booking> _cachedBookings;

  // Stream<List<Booking>> _results = Stream.empty();

  static const String QUERY_GET_BOOKINGS = '''query{
    reservations {
      id
      roomId
      roomPrice
      roomPriceCurrency
      price
      start
      end
      isApproved
      isPaid
      room{
        id
        name
        coverImage{
          id
          objectId
          attachmentFile
        }
      }
    }
  }''';
  static const String QUERY_GET_INVOICE = '''query
    { invoices{
          id
          userId
          roomId
          isPaid
          isApproved
          insuranceRevenueCurrency
          insuranceRevenueLoc
          price
          timezone
          partySize
          status
          priceCurrency
          isRated
          reservations{
            id
            roomId
            roomPrice
            roomPriceCurrency
            price
            start
            end
            isApproved
            isPaid
            room{
              id
              name
                place{
                  id
                  type
                  name
                  location
                  city
                  address
                  country
                  description
                  coordinates{
                    latitude
                    longitude
                  }
                }
              coverImage{
                id
                objectId
                attachmentFile
              }
               user{
                name
                email
                phone
              }
            }
          }
          user{
            name
          }
          room{
            name
          }    
    
        }
  }''';
  
  ValueNotifier<List<Booking>> bookings = ValueNotifier([]);
  ValueNotifier<List<Reservation>> invoices = ValueNotifier([]);
  ValueNotifier<List<Invoice>> invoiceList = ValueNotifier([]);
  ValueNotifier<bool> loading = ValueNotifier(false);
  static const String defaultQuery = QUERY_GET_INVOICE;// QUERY_GET_BOOKINGS;

  Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});

  BehaviorSubject<String> _query = BehaviorSubject<String>.seeded(defaultQuery);

  // Stream<List<Booking>> get results => _results;

  Sink<String> get query => _query;

  // BookingBloc(){
  //   _results = _query
  //       .asyncMap((query) => getBookings(query: query))
  //       .asBroadcastStream();
  // }
   
  // close the stream
  void dispose(){
    _query.close();
  }

  final Client _client = Client();

  Future<void> getBookings({
    String query = BookingBloc.defaultQuery,
  }) async {
    loading.value = true;
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};

   try{
      await _client
        .get(getUrlForQuery(query), headers: headers)
        .then((result) => result.body)
        .then(json.decode)
        .then((json){
          
          var error = json['errors'];
          if (error != null) {
            var errorRes = error[0]['message'];
            loading.value = false;
            return errorRes;
          }
      
          List<Booking> bookingslist = (json["data"]["reservations"] as List)
              .map<Booking>((json) => Booking.fromAPIJson(json))
              .toList();
          bookings.value = bookingslist;
          loading.value = false;
    });
   }
   catch(error){
      loading.value = false;
      print("[ERROR] on booking_bloc while fetching booking");
   }
  }

  Future<void> getInvoices({
    String query = BookingBloc.defaultQuery,
  }) async {
    loading.value = true;
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};

   try{
      await _client
        .get(getUrlForQuery(query), headers: headers)
        .then((result) {
          return result.body;
        })
        .then(json.decode)
        .then((json)async{

          
          var error = json['errors'];
          if (error != null) {
            var errorRes = error[0]['message'];
            loading.value = false;
            return errorRes;
          }
        /// [Currencyconversion]
        var currencyPrice = await SharedPreferencesHelper.getCurrencyRate();
        var currencyPref = await SharedPreferencesHelper.getCurrencyPreference();
        var bookingslist = InvoiceResponse.fromJson(json, rate:currencyPrice, curencyPrefrence: currencyPref).data.invoices;
        invoiceList.value = bookingslist;
        loading.value = false;
    });
   }
   catch(error){
      loading.value = false;
      print("[ERROR] on booking_bloc while fetching booking:- $error");
   }
  }

  Future<dynamic> submitReview({int invoiceId, int rating, String text, String title})async{

    String query = '''mutation {
      roomReview(
        invoiceId : $invoiceId,
        rating:  $rating,
        text :   "$text",
        title:   "$title"
      	
      ){
        	success,    
	        message
        }
      }''';
     loading.value = true;
     print("Request Rating $query");
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};
     
   try{
     var status = await _client
        .post(getUrlForQuery(query), headers: headers)
        .then((result) {
          return result.body;
        })
        .then(json.decode)
        .then((json)async{
          print("Response Rating $json");
          
          var error = json['errors'];
          if (error != null) {
            var errorRes = error[0]['message'];
            return errorRes;
          }
          else{
            return json['data']['roomReview'];//['message'];
          }
       
    });
     loading.value = false;
    return status;
   }
   catch(error){
      loading.value = false;
      print("[ERROR] on booking_bloc while submitting review on booking:- $error");
   }
  }
}

  
final bookingBloc = BookingBloc();

