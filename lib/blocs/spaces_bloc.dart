import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:rxdart/rxdart.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'dart:io';

import 'package:mt_ui/data/models.dart';
// import 'package:mt_ui/blocs/data_mock.dart';

// fetching data from API following the BloC pattern as
// explained here: https://www.youtube.com/watch?v=ALcbTxz3bUw&t=868s

class SpaceBloc {

  List<Space> _cachedSpaces;
  static DateTime _dateFilter;

   DateTime get dateFilter => _dateFilter;
  set  setdateFilter (DateTime date){
     _dateFilter = date;
  }

  Stream<List<Space>> _results = Stream.empty();

  static const String QUERY_GET_SPACES = '''query {
    places {
      id
      type
      name
      description
      rating
      address
      city
      country
      rooms{
        id
        name
        description
        rating
        sizeMeasurement
        placeId
        userId
        capacity
        title
        hasAc
        hasWindow
        cancellationPolicy
        rules
        soundproof
        size
        listingPrice
        staticSchedule{
          price
          priceCurrency
          day
          hour
          timeSlots
          priceLoc
        }
        instruments{
          id
          manufacturer
          instrumentModel
          productionYear
          instrumentId
          roomId
          additionalInfo
          instrument {
            id
            name
          }
        }
        reviews{
          id
          created
          rating
          text
          title
          userId
          user{
            id
            email
            username
            isHost
            occupation
            bio
            isPublic
          }
        }
        images{
          id
          objectId
          attachmentFile
          contentTypeId
          order
        }
      }
    }
  }''';

  static const String defaultQuery = QUERY_GET_SPACES;

  Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});

  BehaviorSubject<String> _query = BehaviorSubject<String>.seeded(defaultQuery);

  Stream<List<Space>> get results => _results;

  Sink<String> get query => _query;

  SpaceBloc(){
    _results = _query
        .asyncMap((query) => getSpaces(query: query))
        .asBroadcastStream();
  }

  // close the stream
  void dispose(){
    _query.close();
  }

  final Client _client = Client();

  Future<List<Space>> getSpaces({
    String query = SpaceBloc.defaultQuery,
  }) async {

    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};

    if(_cachedSpaces != null){
      return _cachedSpaces;
    }

    List<Space> items = [];

    await _client
        // .get(getUrlForQuery(query))
        .get(getUrlForQuery(query), headers: headers)
        .then((result) => result.body)
        .then(json.decode)
        .then((json){

        json["data"]["places"].forEach((place) {
          if(place['rooms'] is List && place['rooms'].length > 0){
            place['rooms'].forEach((room) {
              Map temp = place;
              temp['rooms'] = room;
              temp['location'] = {
                "type": "POINT",
                "location": [
                    13.049217699999986,
                    52.3992161
                  ]
              };
              // if (temp['todaySchedule'].length == 0) {
              //   temp['todaySchedule'] = {
              //     'price': 0,
              //     'priceCurrency': '',
              //     'day': 0,
              //     'hour': 0
              //   };
              // }

              // temp['rooms']['images'] = [
              //   {
              //     'attachmentFile': 'https://imgplaceholder.com/420x320/ff7f7f/333333/fa-image',
              //     'order': 0
              //   }
              // ];

              // TODO take from api
              temp['rooms']['schedule'] = [
                {
                  'id': 1,
                  'created': new DateTime.now(),
                  'rating': 4,
                  'text': 'test comment',
                  'roomId': 1,
                  'userId': 1,
                  'title': 'test title',
                  'price': 1.1,
                  'priceCurrency': 'EUR',
                  'user': {
                    'id': 0,
                    'email':'email',
                    'username': 'username',
                    'occupation': 'occupation',
                    'bio': 'bio'
                  }
                }
              ];

              temp['rooms']['reviews'] = [
                {
                  'id': 1,
                  'created': new DateTime.now(),
                  'rating': 4,
                  'text': 'test comment',
                  'roomId': 1,
                  'userId': 1,
                  'title': 'test title',
                  'user': {
                    'id': 0,
                    'email':'email',
                    'username': 'username',
                    'occupation': 'occupation',
                    'bio': 'bio'
                  }
                },
                {
                  'id': 1,
                  'created': new DateTime.now(),
                  'rating': 4,
                  'text': 'test comment',
                  'roomId': 1,
                  'userId': 1,
                  'title': 'test title',
                  'user': {
                    'id': 0,
                    'email':'email',
                    'username': 'username',
                    'occupation': 'occupation',
                    'bio': 'bio'
                  }
                },
              ];
              items.add(Space.fromAPIJson(temp));
            });
          }
        });
      
        // List<Space> spaces = (jsonNew["data"]["places"] as List)
        //     .map<Space>((json) => Space.fromAPIJson(json))
        //     .toList();

        // List<Map<String, dynamic>> list = jsonNew["data"]["places"][0]["rooms"].toList().cast<Map<String, dynamic>>();

        // spaces.forEach((space){
        //   items.add(space);

        //   // TODO here you can also filter results, e.g.
        //   // if(s.instruments.contains("piano")){
        //   //    items.add(s);
        //   // }

        // });
    });

    // cache items
    _cachedSpaces = items;
    return items;
  }
}
final spaceBLoc = SpaceBloc();

