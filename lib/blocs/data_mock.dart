  Map<String, dynamic> jsonExample = {"data": {"spaces": [{
        "id": "1BQdpocRLBXHm5Ia8Yn6",
        "title": "Space 1a",
        "location": {
          "lon": 13.049217699999986,
          "lat": 52.3992161,
          "country": "14467",
          "city": "P",
          "address": "4 Brandenburger Str. Innenstadt",
          "isActive": false,
          "currency": "EUR"
        },
        "images": [
          {
            "url": "https://storage.googleapis.com/mt-staging-da47c.appspot.com/images/room/1BQdpocRLBXHm5Ia8Yn6/99c527fc-b886-4ce9-85ae-19d53f1fe872",
            "order": 0
          }
        ],
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "rules": "Rules are rules",
        "size": 24,
        "capacity": 5,
        "amenities": [],
        "rating": 5,
        "priceDefault": 13,
        "cancellationPolicy": 1,
        "cancellationPolicyUrl": null,
        "instruments": [
          {
            "id": "SW5zdHJ1bWVudFR5cGU6dTZvTERES2RzYUJoc0twcDREckk=",
            "type": "Piano",
            "manufacturer": "Steinway",
            "model": "2000",
            "year": 0,
            "detail": ""
          }
        ]
      }]}};

  Map<String, dynamic> jsonNew = {"data": {"places": [{
  // String jsonNew = '''{"data": {"places": [{
        "id": "5",
        "name": "Vienna Rehearsal Studios",
        "country": "",
        "city": "Vienna",
        "address": "Geusaugasse 2, 1030 Wien",
        "isActive": false,
        "description": "Für die perfekte Probe stehen 4 Drumkits, jede Menge Gitarren Amps (Marshal, Hughes&Kettner, VOX, Epiphone etc.), 4 Bass-amps (2x Hartke, Marschall, Gallien Kruger), Mischpulte (Allen & Heath, Soundcraft), Tasteninstrumente (E-Piano, Pianino, Konzertflügel) etc. zur Verfügung.",
        "rating": 5,
        "location": {
            "type": "POINT",
            "location": [
                13.049217699999986,
                52.3992161
              ]
          },
        "rooms": [
          {
            "id": 73,
            "name": "Ben's practice room",
            "description": "A half upright in a light room. Great for practicing with a pianist accompaniment, especially singers.",
            "rating": 5,
            "sizeMeasurement": "m",
            "placeId": 43,
            "userId": 110,
            "capacity": 2,
            "title": "Cheap room for practice",
            "hasAc": false,
            "hasWindow": true,
            "cancellationPolicy": 24,
            "rules": "Please take care of the doors being closed whilst you practice.",
            "soundproof": 1,
            "size": 20.1,
            "images": [
              {
                "attachmentFile": "https://imgplaceholder.com/420x320/ff7f7f/333333/fa-image",
                "order": 0
              }
            ],
            "instruments": [
              {
                "id": "99",
                "manufacturer": null,
                "instrumentModel": null,
                "productionYear": null,
                "instrument": {
                  "name": "Cello"
                },
                "room": {
                  "id": "49"
                }
              },
              {
                "id": "99",
                "manufacturer": null,
                "instrumentModel": null,
                "productionYear": null,
                "instrument": {
                  "name": "Guitar"
                },
                "room": {
                  "id": "49"
                }
              }
            ],
            "schedule": [
              {
                "id": "802",
                "roomId": 49,
                "price": 0.0,
                "priceCurrency": "EUR",
                "priceLoc": 0,
                "priceLocCurrency": "EUR",
                "date": "2019-06-20T22:00:00+00:00",
              }
            ]
          },
          {
            "id": 73,
            "name": "Ben's practice roomasdfasdfasdf sad fasdf s",
            "description": "A half upright in a light room. Great for practicing with a pianist accompaniment, especially singers.",
            "rating": 5,
            "sizeMeasurement": "m",
            "placeId": 43,
            "userId": 110,
            "capacity": 2,
            "title": "Cheap room for practice",
            "hasAc": false,
            "hasWindow": true,
            "cancellationPolicy": 24,
            "rules": "Please take care of the doors being closed whilst you practice.",
            "soundproof": 1,
            "size": 20.0,
            "images": [
              {
                "attachmentFile": "https://imgplaceholder.com/420x320/ff7f7f/333333/fa-image",
                "order": 0
              }
            ],
            "instruments": [
              {
                "id": "99",
                "manufacturer": null,
                "instrumentModel": null,
                "productionYear": null,
                "instrument": {
                  "name": "Cello"
                },
                "room": {
                  "id": "49"
                }
              }
            ],
            "schedule": [
              {
                "id": "802",
                "roomId": 49,
                "price": 0.0,
                "priceCurrency": "EUR",
                "priceLoc": 0,
                "priceLocCurrency": "EUR",
                "date": "2019-06-24T22:00:00+00:00",
              }
            ]
          },
        ],
        "instruments": [
          {
            "id": "SW5zdHJ1bWVudFR5cGU6dTZvTERES2RzYUJoc0twcDREckk=",
            "type": "Piano",
            "manufacturer": "Steinway",
            "model": "2000",
            "year": 0,
            "detail": ""
          }
        ]
      }
      ]
      }
      };
      // ''';