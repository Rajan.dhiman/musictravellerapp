import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mt_ui/data/Response/profile_pic_upload_response.dart';
import 'package:mt_ui/data/Response/user_modia_link_response.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:rxdart/rxdart.dart';

import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:http/http.dart' as http;

// fetching data from API following the BloC pattern as
// explained here: https://www.youtube.com/watch?v=ALcbTxz3bUw&t=868s

class UserBloc {
//  DD. Commented line because variable was unused
//  List<User> _cachedUsers;

  Stream<List<Map<String, dynamic>>> _results = Stream.empty();

  static const String QUERY_GET_USERS = '''query {
    user{
       id
      email
      username
      name
      isHost
      occupation
      bio
      isPublic
      city
      school
      gender
      phone
      dob
      soundcloud
      youtube
      facebook
      website
      avatar
      instruction
      country
      skillLevel
      graduated
      instrumentsPlayed
      instrumentsOwned
      instruction
      phone
      address
      state
      postalCode
    }
  }''';
  //
  static const String defaultQuery = QUERY_GET_USERS;
  UserMediaResponse mediaResponse;

  Uri getUrlForQuery(String query) =>
      Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});

  BehaviorSubject<String> _query = BehaviorSubject<String>.seeded(defaultQuery);

  Stream<List<Map<String, dynamic>>>  get results => _results;

  Sink<String> get query => _query;

  UserBloc() {
    _results =
        _query.asyncMap((query) => getMediaLinks()).asBroadcastStream();
  }

  // close the stream
  void dispose() {
    _query.close();
  }

  final Client _client = Client();

  Future<List<User>> getUsers({
    String query = UserBloc.defaultQuery,
  }) async {
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
    };

    List<User> items = [];
    User user;

    await _client
        .get(getUrlForQuery(query), headers: headers)
        .then((result) => result.body)
        .then(json.decode)
        .then((json) {
      try {
        print(json);
        user = User.fromAPIJson(json["data"]["user"]);
        items.add(user);
      } catch (error) {
        print(error);
      }
    });
    if (items != null && items.length > 0) {
      user = items.first;
      SharedPreferencesHelper.setUserData(
          "true",
          user.id,
          user.email,
          user.userName,
          user.name,
          user.isHost,
          user.occupation,
          user.bio,
          user.avatar);
    }

    return items;
  }

  Future<String> submitReview({String link, String type}) async {
    String query = '''mutation{
      userMediaLinks(
         link: "$link",
         section: "$type"){
           success
           error
         }
      }
     
     
     ''';
    //  loading.value = true;
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
    };

    try {
      var status = await _client
          .post(getUrlForQuery(query), headers: headers)
          .then((result) {
            var body = result.body;
            return body;
          })
          .then(json.decode)
          .then((json) async {
            var error = json['errors'];
            if (error != null) {
              var errorRes = error[0]['message'];
              return errorRes;
            } else {
              return json['data']['userMediaLinks'];
            }
          });
      return status;
    } catch (error) {
      print("[ERROR] on user_bloc while submitting link:- $error");
    }
  }

  Future<List<Map<String, dynamic>>> getMediaLinks({
    String query = '''  query{
     user{
       id
      email
      username
      name
      isHost
      occupation
      bio
      isPublic
      city
      school
      gender
      phone
      dob
      soundcloud
      youtube
      facebook
      website
      avatar
      instruction
      country
      skillLevel
      graduated
      instrumentsPlayed
      instrumentsOwned
      instruction
      phone
      address
      state
      postalCode
    },
      userMedia{
        id,
        type,
        section,
        url,
        isPrivate,
      }
    }''',
  }) async {
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
    };

   var map = await _client
        .get(getUrlForQuery(query), headers: headers)
        .then((result) {
          var body = result.body;
          return body;
        })
        .then(json.decode)
        .then((json) {
          try {
            print(json);
            var list = json["data"];
            var map = [ 
              {"user": User.fromAPIJson(json["data"]["user"],)},
              {"links":UserMediaResponse.fromJson(json).data.userMedia},
            ];
           updateUserInLocalDb(map[0]["user"]);
           return map;
          } catch (error) {
            print(error);
          }
        });

    return map;
  }
 
 Future<void> updateUserInLocalDb(User user){
    SharedPreferencesHelper.setUserData(
          "true",
          user.id,
          user.email,
          user.userName,
          user.name,
          user.isHost,
          user.occupation,
          user.bio,
          user.avatarUrl);
 }

  Future<bool> uploadImage(File imageFile, {String userId, BuildContext context})async{
      log("Profile upload initiate");
      var stream = new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
      var length = await imageFile.length();
      ProfilePicResponse profileModel;
      var uri = Uri.parse(MtConstants.baseUploadProfilePicUrl);
      

     var request = new http.MultipartRequest("POST", uri)  ..fields['user_id'] = userId;
     String imagePath ;
     if(imageFile.path.length > 20){
       imagePath = imageFile.path.substring(imageFile.path.length  - 12, imageFile.path.length);
     }
     else{
       imagePath = imageFile.path;
     }
     
     
      var multipartFile = new http.MultipartFile('image', stream, length,
          // filename: basename(imageFile.path));
          filename: basename(imagePath));
          //contentType: new MediaType('image', 'png'));

      request.files.add(multipartFile);
      var response = await request.send();
      print(response.statusCode);
      if(response.statusCode == 200){
        response.stream.transform(utf8.decoder).listen((value) {
        print(value);
        profileModel = ProfilePicResponse.fromRawJson(value);
        showSnack(context,profileModel.message);
        return profileModel;
        // map.add()
      });
      log("Profile upload finished");
      return true;
      }
      else{
        response.stream.transform(utf8.decoder).listen((value) {
          // profileModel = ProfilePicResponse.fromRawJson(value);
          print('Status : ${response.statusCode} Image Not saved on server');
          print(value);
          showSnack(context,"Status : ${response.statusCode} Image not saved on server");
      });
      log("Profile upload finished");
      return false;
      }
      
  }
}
void showSnack(BuildContext context, String message){
  Scaffold.of(context).showSnackBar(SnackBar(content: Text(message)),);
}
final userBloc = UserBloc();
