import 'dart:convert';
import 'package:mt_ui/helpers/constants.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;

class VersionBloc  {
      //get Card List

  Future<AppVersion> fetchVersion() async {
  final response = await http.get(MtConstants.appVersionList);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.

    print(json.decode(response.body));
    return AppVersion.fromJson(json.decode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

  // close the stream
  void dispose() {
  }

}


class AppVersion {
  final List android;
   final List ios;
 
  AppVersion({this.android, this.ios});

  factory AppVersion.fromJson(Map<String, dynamic> json) {
    return AppVersion(
      ios: json["app_versions"][0]['ios'] as List,
      android: json["app_versions"][1]['android'] as List,
    );
  }
}






final versionBloc = VersionBloc();