import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:mt_ui/utils.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;

class CitySuggestionsBloc {

  Stream<List<String>> _results = Stream.empty();
  ReplaySubject<String> _query = ReplaySubject<String>();
  Stream<List<String>> get results => _results;
  Sink<String> get query => _query;

  CitySuggestionsBloc(){
    _results = _query
          .distinct() // don't return the same results twice
          .asyncMap((query) => getSuggestions(constructQuery(query)))
          .asBroadcastStream();
  }

  // close the stream
  void dispose(){
    _query.close();
  }

  String constructQuery(String input) {
    return "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&types=(cities)&language=en&key=${Utils.googleApiKey}";
  }
  String nearByCities(double lat, double long){
    return ('http://api.geonames.org/findNearbyPlaceNameJSON?lat=$lat&lng=$long&radius=10&maxRows=200');
  }
  Future<List<String>> getSuggestions(String query) async {

    List<String> items = [];
    // log(query);
    await http.Client()
        .get(query)
        .then((result) => result.body)
        .then(json.decode)
        .then((json){

      (json["predictions"] as List)
          .map<String>((json) => json['description'] as String)
          .toList().forEach((city) => items.add(city));
    });

    return items;
  }

}


