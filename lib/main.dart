import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mt_ui/blocs/spaces_bloc.dart';
import 'package:mt_ui/providers/bottom_navigation_index_provider.dart';
import 'package:mt_ui/providers/spaces_provider.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/pages/home_page.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:mt_ui/pages/welcome_page.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';

import 'data/stripe.dart';
import 'helpers/constants.dart';
import 'pages/Auth_page/email_login_page.dart';
import 'widgets/testWidget.dart';

void main() async{
  //  WidgetsFlutterBinding.ensureInitialized();
   Crashlytics.instance.enableInDevMode = true;
  FlutterError.onError = Crashlytics.instance.recordFlutterError;
  runApp(MyApp());
  //  SharedPreferencesHelper.setLogout(false);
}

class MyApp extends StatelessWidget{

  // Firebase analytics
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =  FirebaseAnalyticsObserver(analytics: analytics);

  final GlobalKey<HomePageState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
  
  
  
    //added getToken here to because otherwise setToken doesn't write data to SharedPref on first run
    // SharedPreferencesHelper.getAccessToken();

    // Change android system bar colors
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: MtColors.F5F5F5,
      systemNavigationBarIconBrightness: Brightness.dark,
      statusBarColor: Colors.white,
    ));

    FlutterStatusbarcolor.setStatusBarColor(Colors.white);
  
    return SpacesProvider(
      spaceBloc: SpaceBloc(),
      child: BottomNavigationIndexProvider(
        goToPage: (index) => _key.currentState.goToPage(index),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Music Traveler',
            builder: (context, child) =>
              MediaQuery(data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: Platform.isIOS), child: child),
      
          navigatorObservers: <NavigatorObserver>[observer],
          // home: HomePage(
          //   key: _key,  EmailLoginPage(signUp: false),
          // ),
          // home: WelcomePage(),
          routes: {
            '/': (BuildContext context) =>   WelcomePage(isAppStartApp: true,),
            '/login': (BuildContext context) => EmailLoginPage(signUp: false),
          },
          theme: getTheme(),
        ),
      ),
    );
  }

  ThemeData getTheme(){
    return ThemeData(

      // Default colors
        brightness: Brightness.light,
        primaryColor: Colors.white,
        accentColor: MtColors.blue,
        scaffoldBackgroundColor: Colors.white,

        // Default font
        fontFamily: 'Lato',

        // Default text theme
        textTheme: TextTheme(
          body1: TextStyle(fontSize: 16.0, fontFamily: 'Lato'),
        )
    );
  }

}
