import 'package:flutter/material.dart';
import 'widget/update_widget.dart';

class ForceUpdatePage extends StatefulWidget {
  ForceUpdatePage({Key key}) : super(key: key);

  @override
  _ForceUpdatePageState createState() => _ForceUpdatePageState();
}

class _ForceUpdatePageState extends State<ForceUpdatePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title:Text('MUSIC TRAVELER')
      ),
      body: UpdateWidget(),
    );
  }
}