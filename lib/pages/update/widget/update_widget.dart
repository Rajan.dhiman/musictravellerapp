import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:mt_ui/data/auth_helper.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/Auth_page/widget/enterCode.dart';
import 'package:mt_ui/pages/Auth_page/widget/submitButton.dart';
import 'package:mt_ui/res/images.dart';
import 'package:mt_ui/res/mt_logos_icons.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/utils.dart';

import 'package:mt_ui/pages/update/widget/updateButton.dart';

class UpdateWidget extends StatefulWidget {
  final bool isEmailVerification;
  UpdateWidget({Key key, this.isEmailVerification = false});

  @override
  _UpdateWidgetState createState() => _UpdateWidgetState();
}

class _UpdateWidgetState extends State<UpdateWidget> {
  
   String error = '';
    bool loading = false;
     TextEditingController email = TextEditingController();
   final _formKey = GlobalKey<FormState>();
   Widget _emailField() {
      double screenHeight = MediaQuery.of(context).size.height;
      double mPadding = (screenHeight > 600) ? 32.0 : 16.0;
    return Padding(
      padding: const EdgeInsets.all(16.0),

      
      child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
          
            Image(image: AssetImage(MtImages.holdOn),height: 200, width: 200,),
              Text('Please Update',style:TextStyle(fontSize: 22,fontWeight: FontWeight.bold)),
              SizedBox(height: 10),

            Padding(
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: Text('You\'re using an outdated app',style:TextStyle(fontSize: 20,color: Colors.black), textAlign: TextAlign.center,),
              ),
               SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: Text('Please update app to newer version to improve your experience.',style:TextStyle(fontSize: 17,color: Colors.black54), textAlign: TextAlign.center,),
              ),
              SizedBox(height: 100),
              // 

            
              
              _submitButton(),
            ],
          ),
    );
  }
  Widget _errorMessage() {
    return Container(
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.centerLeft,
      child: (error == "")
          ? Container()
          : Container(
              decoration: BoxDecoration(
                  color: Colors.red.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(4)),
              padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
              margin: EdgeInsets.only(bottom: 8),
              child: Text(error, style: TextStyle(color: Colors.red)),
            ),
    );
  }

  Widget _submitButton() {
    return UpdateButton(
          loading: loading,
          formKey: _formKey,
          buttonText: 'Update',
          onPressed: () async {
         redirectToSpecificApp();
          
      },
    );
    
  }


  void redirectToSpecificApp(){
     if (Platform.isIOS) {
          Utils.launchURL('https://apps.apple.com/us/app/music-traveler/id1256078265');
     }
      if (Platform.isAndroid) {
            Utils.launchURL('https://play.google.com/store/apps/details?id=com.musictraveler.mtui');
      }
  }
  

  InputDecoration _inputDecoration(String hint,{bool showPassword = false,int index = -1}) {
    return InputDecoration(
      contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      hintText: hint,
      labelStyle: TextStyle(color: Colors.black26),
      suffixIcon: index == -1 ? null 
        :  Container(
            width: 30,
            alignment: Alignment.center,
            child: InkWell(
              onTap: (){
               
              },
              child: Image.asset(
                !showPassword ?   MtImages.hidePAssword : MtImages.viewPAssword,
                width: 25,
                fit: BoxFit.cover,),
            ),
        ),
        // IconButton(
        //   onPressed: (){
        //     setState(() {
        //       _showPassword = !_showPassword;
        //     });
        //   },
        //   icon:Icon(!_showPassword? Icons.cancel : Icons.remove_red_eye, color: Colors.black,)
        // ),
        
      border: OutlineInputBorder(
        borderSide: BorderSide(width: 1, style: BorderStyle.none),
        borderRadius: const BorderRadius.all(
          const Radius.circular(25.0),
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(width:1, style: BorderStyle.solid,color: Colors.blue),
        borderRadius: const BorderRadius.all(
          const Radius.circular(25.0),
        ),
      ) ,
      filled: true,
      fillColor: MtColors.F9F9F9,
    );
  
  }
  

  @override
  Widget build(BuildContext context) {
    return _emailField();
  }

}