import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mt_ui/res/mt_styles.dart';

class UpdateButton extends StatefulWidget {
  final bool loading;
  final String buttonText;
  final Function onPressed;
  final GlobalKey<FormState> formKey; 
  UpdateButton({Key key, this.loading, this.onPressed, this.formKey, this.buttonText}) : super(key: key);

  @override
  _UpdateButtonState createState() => _UpdateButtonState();
}

class _UpdateButtonState extends State<UpdateButton> {

   Widget _submitButton() {
    return AnimatedContainer(
      duration: Duration(milliseconds: 350),
      curve: Curves.easeInOut,
      // height: loading ? 48 : 48,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20))
      ),
      child: RaisedButton(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                  color: Colors.green,
                  borderRadius: new BorderRadius.all(const Radius.circular(25.0))),
            
              constraints: BoxConstraints(maxWidth: 100),
              child: widget.loading
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(25),
                      child: AnimatedOpacity(
                          duration: Duration(milliseconds: 350),
                          opacity: widget.loading ? 1 : 0,
                          child: LinearProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white),
                          )),
                    )
                  : null,
            ),
            AnimatedOpacity(
              duration: Duration(milliseconds: 350),
              opacity: widget.loading ? 0 : 1,
              child: Center(
                child: Text(widget.buttonText,
                    style: MtTextStyle.button),
              ),
            ),
          ],
        ),
        padding: EdgeInsets.symmetric(vertical: 16),
        textColor: Colors.white,
        color: MtColors.blue,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        onPressed: widget.onPressed
        // async {
        //   // don't allow click if already loading
        //   if (widget.loading) return;

        //   // Validate will return true if the form is valid
        //   if (widget.formKey.currentState.validate()) {
        //        setState(() {
        //       // widget.loading = true;
        //       // error = "";
        //     });

        //     // hide keyboard
        //     SystemChannels.textInput.invokeMethod('TextInput.hide');

        //     // remove focus from text fields
        //     FocusScope.of(context).requestFocus(new FocusNode());

        //     // run logic
            

        //     setState(() {
        //       loading = false;
        //     });
        //   }

           
        //   }
        
      
    ));
  }
  @override
  Widget build(BuildContext context) {
    return _submitButton();
  }
}