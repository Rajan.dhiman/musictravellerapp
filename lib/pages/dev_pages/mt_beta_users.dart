import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/res/mt_styles.dart';

///
/// This page pulls down a list of users from mt-beta
///

class MtBetaUsers extends StatefulWidget {
  final String title;
  MtBetaUsers({Key key, this.title}) : super(key: key);

  @override
  MtBetaUsersState createState() {
    return new MtBetaUsersState();
  }
}

class MtBetaUsersState extends State<MtBetaUsers>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Users from mt-beta db", style: MtTextStyle.appBarTitle),
        centerTitle: true,
      ),
      body: FutureBuilder<List<User>>(
        future: fetchUsers(http.Client()),
        builder: (context, snapshot) {
          return snapshot.hasData
              ? UsersList(users: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

Future<List<User>> fetchUsers(http.Client client) async {

  var uri = new Uri.http(MtConstants.baseUrl, "/graphql",
      {"query": "query { users {id email username} }"});

  var response = await http.get(uri);

  // Use the compute function to run parseUsers in a separate isolate
  return compute(parseUsers, response.body);
}

// A function that will convert a response body into a List<Photo>
List<User> parseUsers(String responseBody) {
  final parsed = json.decode(responseBody);

  return (parsed["data"]["users"] as List)
      .map<User>((json) => User.fromJson(json))
      .toList();
}

class UsersList extends StatelessWidget {
  final List<User> users;

  UsersList({Key key, this.users}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: users.length,
      itemBuilder: (context, index) {

        // header
        if (index == 0) {
          return ListTile(
              title: Text("Found ${users.length} users",
                  style: Theme.of(context).textTheme.title));
        }

        // List item
        return ListTile(
          leading: Icon(Icons.person),
          title: Text(users[index].username, style: TextStyle(fontSize: 20.0)),
          // subtitle: Text(users[index].email),
        );
      },
    );
  }
}

class User {
  final String username;
  final String email;

  User({
    this.username,
    this.email,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      username: json['username'] as String,
      email: json['email'] as String,
    );
  }
}
