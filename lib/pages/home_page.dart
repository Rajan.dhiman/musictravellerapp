import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/favourites_page.dart';
import 'package:mt_ui/pages/search_page/search_page.dart';
import 'package:mt_ui/pages/user_page/user_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/pages/bookings_page/bookings_page.dart';
import 'package:mt_ui/pages/welcome_page.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';

import 'user_page/profile_edit_page.dart';
import 'user_page/profile_view_page.dart';
enum ProfilPageType{
 ProfilePage,
 ProfileView,
 ProfileEdit
}
class HomePage extends StatefulWidget {
  static const INIT_PAGE = PAGE_SEARCH;
  final bool isFromPaymentPage ;
  final bool isFromCheckBookingPage;
  static const PAGE_SEARCH = 0;

//  static const PAGE_EXPLORE = 0;
  static const PAGE_FAVOURITES = 1;
  static const PAGE_BOOKINGS = 2;
  static const PAGE_PROFILE = 3;
  
  // static const PAGE_WELCOME = 4;

  const HomePage({Key key, this.isFromPaymentPage = false, this.isFromCheckBookingPage = false}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  List<Widget> pages;
  Widget currentPage;
  int currentPageIndex = HomePage.INIT_PAGE;
  PageController _pageController;
  bool isViewProfilePageopened = false;
  ProfilPageType profilePageType = ProfilPageType.ProfilePage;
  User _user;
  bool _islocationFilterOpen = false;
  @override
  void initState() {
    
    super.initState();

    MtConstants.changeUrl();

    print("baseUrl  ${MtConstants.baseUrl}");

    // create pages (if you change the order, please update BottomNavigationTabHelper)
    _pageController = new PageController();
//     pages = [
//       SearchPage(),
// //       ExplorePage(),
//       FavouritesPage(),
//       BookingsOverviewPage(),
//       profilePageType == ProfilPageType.ProfilePage ?  UserPage(openProfilePage: openProfileViewPage)
//       :  profilePageType == ProfilPageType.ProfileView ?  ProfileViewPage(openProfilePage: openProfileViewPage,)
//       : ProfileEditPage()
//       // WelcomePage(), 
//     ];
   refreshPageList();

   WidgetsBinding.instance
        .addPostFrameCallback((_)  { 
          if(widget.isFromPaymentPage){
            goToPage(2) ;
          }
        });
   
   
  }
 
  void openProfileViewPage(ProfilPageType value, User user){
    print('openProfilePageCalled $value');
    setState(() {
      profilePageType = value;
      _user = user;
      refreshPageList();
      // goToPage(3);
    });
  }
  void refreshPageList(){
     pages = [
         SearchPage(
           isFromCheckBookingPage: widget.isFromCheckBookingPage,
           onBottomNavIconPressed: (index){
           goToPage(index);
         }),
//          ExplorePage(),
         FavouritesPage(),
         BookingsOverviewPage(),
         _getProfilePage(profilePageType)
         // WelcomePage(),
       ];
  }
  Widget _getProfilePage(ProfilPageType type){
   //  print(_user.email + type.toString());
    switch (type) {
      case ProfilPageType.ProfilePage : return  UserPage(openProfilePage: openProfileViewPage) ;
      case ProfilPageType.ProfileView : return  ProfileViewPage(openProfilePage: openProfileViewPage) ;
      case ProfilPageType.ProfileEdit : return  ProfileEditPage(openProfilePage: openProfileViewPage,user: _user,) ;
        
        break;
      default:return  UserPage(openProfilePage: openProfileViewPage) ;
    }
  }
  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  /// called from main.dart when a widget somewhere in the app
  /// uses the BottomNavigationIndexProvider to change page
  goToPage(int index) {
    setState(() {
      currentPageIndex = index;
      if(index != 3){
       profilePageType = ProfilPageType.ProfilePage;
      }
      refreshPageList();
    });
    _pageController.jumpToPage(index);
  }

  @override
  Widget build(BuildContext context) {
    // Bottom Navigation Bar Theme
    ThemeData bottomNavBarTheme = Theme.of(context).copyWith(
        canvasColor: Colors.white,
        primaryColor: MtColors.blue,
        // sets the inactive color of the `BottomNavigationBar`
        textTheme: Theme.of(context).textTheme.copyWith(
                caption: TextStyle(
              color: Colors.black54,
            )));

    TextStyle tabLabelStyle = TextStyle(
      fontWeight: FontWeight.bold,
    );

    // set current page (init page is set in main.dart)
    // currentPage = pages[currentPageIndex];

    // build layout
    return Scaffold(
      bottomNavigationBar: Theme(
        data: bottomNavBarTheme,
        child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: currentPageIndex,
            onTap: (int index) {
              if (index == HomePage.PAGE_FAVOURITES ||
                  index == HomePage.PAGE_BOOKINGS ||
                  index == HomePage.PAGE_PROFILE) {
                SharedPreferencesHelper.getAccessToken().then((String token) {
                  if (token != 'null') {
                    setState(() {
                      currentPageIndex = index;
                    });
                    goToPage(index);
                  } else {
                    Navigator.pop(context);
                    // Navigator.of(context).pushReplacement(MaterialPageRoute<Null>(
                    //   builder: (BuildContext context) {
                    //     return WelcomePage();
                    //   },
                    // ));
                  }
                });
              } else {
                setState(() {
                  currentPageIndex = index;
                });
                goToPage(index);
              }
            },
            items: [
              BottomNavigationBarItem(
                icon: Icon(FeatherIcons.search),
                title: Text("Search", style: tabLabelStyle),
              ),
              // TODO uncomment (DD)
//               BottomNavigationBarItem(
//                 icon: Icon(FeatherIcons.compass),
//                 title: Text("Explore", style: tabLabelStyle),
//               ),
              BottomNavigationBarItem(
                icon: Icon(FeatherIcons.heart),
                title: Text("Favourites", style: tabLabelStyle),
              ),
              BottomNavigationBarItem(
                icon: Icon(FeatherIcons.bookmark),
                title: Text("Bookings", style: tabLabelStyle),
              ),
              BottomNavigationBarItem(
                icon: Icon(FeatherIcons.user),
                title: Text("Profile", style: tabLabelStyle),
              )
            ]),
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: (index) {
          setState(() {
            currentPageIndex = index;
          });
        },
        controller: _pageController,
        children: pages,
      ),
    );
  }
}
