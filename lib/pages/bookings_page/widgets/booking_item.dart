import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/data/Invoices.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/pages/bookings_page/widgets/booking_detail_page.dart';
import 'package:mt_ui/pages/bookings_page/widgets/booking_status_widget.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/my_card.dart';

class BookingItem extends StatefulWidget {
  final Invoice invoce;
  final GlobalKey<ScaffoldState> snakKey;
  BookingItem({Key key, this.invoce, this.snakKey}) : super(key: key);

  @override
  _BookingItemState createState() => _BookingItemState();
}

class _BookingItemState extends State<BookingItem> {
  Invoice invoce;
  @override
  void initState() {
    invoce = widget.invoce;
    super.initState();
  }
  Widget _timeWidget(){
    return Row(
        children: <Widget>[
          Row(children: <Widget>[
            Text(BookingHour.getHour(invoce.reservations.first.start, context),
                style: TextStyle(color: Colors.black87,fontWeight: FontWeight.bold)),
          invoce.reservations.length > 1 ?
          Text(", ${BookingHour.getHour(invoce.reservations[1].start, context)}",
                style: TextStyle(color: Colors.black87,fontWeight: FontWeight.bold))
              : Container(),
          invoce.reservations.length > 2 ?
          Text(", ${BookingHour.getHour(invoce.reservations[2].start, context)}",
                style: TextStyle(color: Colors.black87,fontWeight: FontWeight.bold))
              : Container(),
          ],),
          // Expanded(

          //   child:Container(
          //     width: 80,
          //     padding: EdgeInsets.only(right:60),
          //     alignment: Alignment.centerRight,
          //     child:Text(invoce.getTotalPriceDisplayTwoDecimals(),
          //               style: TextStyle(color: Color.fromRGBO(0, 0, 0, 0.54))),
          //   )
          // )
        ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 16),
      child: MyCard(
        padding: EdgeInsets.all(16.0),
        onPressed: () => Navigator.of(context).push(MaterialPageRoute<Null>(
            builder: (BuildContext context) {
              return BookingDetailPage(
                invoice: invoce,
                snakKey:widget.snakKey
              );
            },
            fullscreenDialog: true)),
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _timeWidget(),
                // Wrap(
                //   children: <Widget>[
                //     SizedBox(width: 8),
                //     Text(invoce.getTotalPriceDisplayTwoDecimals(),
                //         style: TextStyle(color: Color.fromRGBO(0, 0, 0, 0.54))),
                //   ],
                // ),
                SizedBox(height: 8.0),
                Padding(
                  padding: const EdgeInsets.only(right: 64.0),
                  child: Text(invoce.reservations.first.space.name,
                      style: Theme.of(context).textTheme.title.copyWith(
                          fontWeight: FontWeight.bold, color: Colors.black87)),
                ),
                SizedBox(height: 12.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    BookingStatusWidget(
                      booking: Booking(
                          bookingHours: invoce.bookingHours,
                          currency: invoce.priceCurrency,
                          isApprovedApi: invoce.reservations.first.status,
                          isPaid: invoce.isPaid,
                          price: invoce.price.toDouble(),
                          roomId: invoce.roomId,
                          space: Room(
                            name: invoce.reservations.first.space.name,
                            // price: booking..
                          ),
                          status: invoce.reservations.first.status),
                    ),
                    Row(
                      children: <Widget>[
                        Text(invoce.getTotalPriceDisplayTwoDecimals(),
                        style: TextStyle(color: Color.fromRGBO(0, 0, 0, 0.54))),
                        SizedBox(width: 20),
                        Icon(
                          Icons.people,
                          color: Colors.black54,
                          size: 18,
                        ),
                        SizedBox(width: 8),
                        Text(invoce.partySize.toString(),
                            style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Positioned(
              right: 0,
              top: 0,
              child: ClipOval(
                child: Container(
                  height: 48,
                  width: 48,
                  color: Colors.black26,
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    // imageUrl: "https://s3.amazonaws.com/musictraveler.herokuapp.com/media/attachments/room_room/373/thumbs/thumb_K3_71db.jpg.368x207_q70.jpg",
                    imageUrl:  invoce.reservations.first.space.coverImage == null ||  invoce.reservations.first.space.coverImage.attachmentFile == null ?
                       '' : invoce.reservations.first.space.coverImage.attachmentFile,
                    placeholder: (context, url) => Container(
                      color: MtColors.EEEEEE,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
