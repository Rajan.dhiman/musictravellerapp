import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mt_ui/blocs/booking_bloc.dart';
import 'package:mt_ui/data/Invoices.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/space_view_compact.dart';
import 'package:mt_ui/widgets/star_rating.dart';

class BookingRatePage extends StatefulWidget {
  final Booking booking;
  final String invoiceId;
  const BookingRatePage({Key key, this.booking, this.invoiceId}) : super(key: key);

  @override
  _BookingRatePageState createState() => _BookingRatePageState();
}

class _BookingRatePageState extends State<BookingRatePage> {
  final key = new GlobalKey<ScaffoldState>();
  double rating = 0;
  bool postButtonEnabled = false;
  TextEditingController _textFieldController;
  TextEditingController _title;
  @override
  void initState() {
    _textFieldController = TextEditingController();
     _title = TextEditingController();
    super.initState();
  }
  @override
  void dispose() {
     _title.dispose();
    _textFieldController.dispose();
       super.dispose();
  }
  // TODO make editing and deleting existing reviews possible

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: false,
            floating: true,
            centerTitle: true,
            title: Text("Rate this space", style: MtTextStyle.appBarTitle),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              Column(
                children: <Widget>[
                  Container(
                    constraints: BoxConstraints(
                      maxWidth: 600,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(32.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          MyCard(
                            child: SpaceViewCompact(
                              space: ReservationRoom(
                                name: widget.booking.space.name,
                                id: int.parse(widget.booking.space.spaceId),
                                coverImage: CoverImage(
                                  attachmentFile: widget.booking.space.images != null ? widget.booking.space.images.first.url: '',
                                ),
                                location: widget.booking.space.location
                              ),
                            ),
                          ),
                          SizedBox(height: 32),
                          SizedBox(
                            width: double.infinity,
                            child: StarRating(
                              alignment: WrapAlignment.spaceEvenly,
                              rating: rating,
                              allowHalfRating: false,
                              filledEmptyStar: false,
                              size: 40,
                              onRatingChanged: (double newRating) {
                                setState(() {
                                  rating = newRating;

                                  // enable post button
                                  postButtonEnabled = true;
                                });
                              },
                            ),
                          ),
                          SizedBox(height: 32),

                         
                           Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: MtColors.EEEEEE,
                            ),
                            height: 50,
                            child: TextField(
                              focusNode: FocusNode(),
                              controller: _title,
                              // onChanged: _onTextChanged,
                              // onSubmitted: _onEnterClicked, // called when user presses ENTER on the keyboard
                              style: TextStyle(
                                  fontSize: 16, color: Colors.black54),
                              textCapitalization: TextCapitalization.sentences,
                              maxLines: 5,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 16),
                                hintText: 'Title',
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          SizedBox(height: 32),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: MtColors.EEEEEE,
                            ),
                            height: 150,
                            child: TextField(
                              focusNode: FocusNode(),
                              controller: _textFieldController,
                              // onChanged: _onTextChanged,
                              // onSubmitted: _onEnterClicked, // called when user presses ENTER on the keyboard
                              style: TextStyle(
                                  fontSize: 16, color: Colors.black54),
                              textCapitalization: TextCapitalization.sentences,
                              maxLines: 5,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 16),
                                hintText: 'Describe your experience (optional)',
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          SizedBox(height: 16),
                          Text(
                            "Reviews are public and appear with your name",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.black45,
                            ),
                          ),
                          SizedBox(height: 32),
                          RaisedButton(
                            onPressed:
                                postButtonEnabled ? _onPostPressed : null,
                            disabledTextColor: Colors.white,
                            color: MtColors.blue,
                            textColor: Colors.white,
                            elevation: 0,
                            padding: EdgeInsets.symmetric(
                                vertical: 16.0, horizontal: 40.0),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(100)),
                            child: Text(
                              "POST".toUpperCase(),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 0.9),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ]),
          )
        ],
      ),
    );
  }



 
  Widget _textFieldHolder(Widget child){
    return Container(
      // margin: EdgeInsets.only(bottom: 8),
      // decoration: BoxDecoration(
      //   borderRadius: BorderRadius.circular(8),
      //   color: Color(0xFFf9f9f9),
      //   // border: Border.all(color: MtColors.EEEEEE),
      // ),
      child: child
    );
  }


  // TODO post review
  void _onPostPressed() async{
    String message;
   var submitReview = bookingBloc.submitReview(
      invoiceId:int.parse( widget.invoiceId),
      rating: rating.toInt(), 
      text: _textFieldController.text,
      title: _title.text
    );
   var status =await  submitReview;
      message = status["message"];
      key.currentState
        .showSnackBar(SnackBar(content: Text(message)));
        Timer(Duration(seconds: 1), () {
                print('Redirect to booking page');
                var isReviewd = false;
                if(status["success"] != null && status["success"] == true){
                      isReviewd = true;
                }
                Navigator.pop(context, isReviewd);
      });
    // if(status["success"] != true){
    //   Timer(Duration(seconds: 1), () {
    //             print('Redirect to booking page');
    //             Navigator.pop(context);
    //   });
    // }
  
  }
}
