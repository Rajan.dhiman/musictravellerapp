import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mt_ui/data/Invoices.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/pages/bookings_page/widgets/booking_review_page.dart';
import 'package:mt_ui/pages/bookings_page/widgets/booking_status_widget.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/booking_hours_overview.dart';
import 'package:mt_ui/widgets/booking_price_overview.dart';
import 'package:mt_ui/widgets/location_view_dialog.dart';
import 'package:mt_ui/widgets/space_location_widget.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/space_view_compact.dart';
import 'package:url_launcher/url_launcher.dart';

class BookingDetailPage extends StatefulWidget {
  // final Reservation booking;
  final Invoice invoice;
  final GlobalKey<ScaffoldState> snakKey;

  BookingDetailPage({Key key, this.invoice, this.snakKey}) : super(key: key);
  @override
  BookingDetailPageState createState() {
    return new BookingDetailPageState();
  }
}

class BookingDetailPageState extends State<BookingDetailPage> {
  Invoice invoice;
  GlobalKey<ScaffoldState> snakKey;
  String bookingStatus = "*You have already reviewed this space";

  @override
  void initState() {
    invoice = widget.invoice;
    snakKey = widget.snakKey;
    super.initState();
  }

  final key = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var booking = invoice.reservations.first;
    // TODO fetch host info from API
    Host dummyHost = invoice.reservations.first.space.host;
    print("Booking detail page initilised");
    return Scaffold(
      key: snakKey,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text("Booking details", style: MtTextStyle.appBarTitle),
            centerTitle: true,
            snap: true,
            floating: true,
            pinned: false,
            backgroundColor: Color(0xffFEFEFE),
          ),

          // Divider
          SliverToBoxAdapter(
            child: Container(
              height: 1,
              width: double.infinity,
              color: MtColors.EEEEEE,
            ),
          ),

          SliverPadding(
            padding: EdgeInsets.all(16),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                _statusSection(context),
                SizedBox(height: 32),
                _bookingSection(context),
                SizedBox(height: 32),
                _locationSection(context),
                SizedBox(height: 32),
                _hostSection(context, dummyHost),
              ]),
            ),
          ),
        ],
      ),
    );
  }

  Widget _bookingSection(BuildContext context) {
    var booking = invoice.reservations;

    TextStyle pricingStyle = TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Booking".toUpperCase(),
          style: Theme.of(context).textTheme.overline,
        ),
        SizedBox(height: 8),
        MyCard(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              SpaceViewCompact(
                space: booking.first.space,
              ),
              BookingHoursOverview(
                booking: Booking(
                    bookingHours: invoice.bookingHours,
                    currency: invoice.priceCurrency,
                    isApprovedApi: invoice.reservations.first.status,
                    isPaid: invoice.isPaid,
                    price: invoice.price,
                    roomId: booking.first.roomId,
                    space: Room(
                      name: booking.first.space.name,
                      // price: booking..
                    ),
                    status: booking.first.status),
              ),
              Divider(height: 16),
              ListTile(
                contentPadding: EdgeInsets.zero,
                title: Text(
                  "Number of guests",
                  style: pricingStyle,
                ),
                trailing: Text(
                  invoice.partySize.toString(),
                  style: pricingStyle,
                ),
              ),
              Divider(),
              BookingPriceOverview(
                booking: Booking(
                    bookingHours: invoice.bookingHours,
                    currency: invoice.priceCurrency,
                    isApprovedApi: invoice.reservations.first.status,
                    isPaid: invoice.isPaid,
                    price: invoice.price,
                    roomId: booking.first.roomId,
                    space: Room(
                        name: booking.first.space.name,
                        location: booking.first.space.location
                        // price: booking..
                        ),
                    status: booking.first.status),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _cancelButton() {
    return Container(
      width: double.infinity,
      height: 48,
      child: RaisedButton(
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
            side: BorderSide(color: Colors.red)),
        textColor: Colors.red,
        elevation: 0,
        onPressed: () {
          // key.currentState
          //     .showSnackBar(SnackBar(content: Text("To be implemented")));
        },
        child: Text("Cancel booking",
            style: TextStyle(fontWeight: FontWeight.bold)),
      ),
    );
  }

  // TODO change to "EDIT REVIEW" or smt if user already rated
  Widget _reviewButton(BuildContext context) {
    var booking = invoice.reservations;
    return Container(
      width: double.infinity,
      height: 48,
      child: RaisedButton(
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
            side: BorderSide(color: MtColors.blue)),
        textColor: MtColors.blue,
        elevation: 0,
        onPressed: () async {
          final reviewStatus =
              await Navigator.of(context).push(MaterialPageRoute<bool>(
            fullscreenDialog: true,
            builder: (BuildContext context) => BookingRatePage(
              invoiceId: invoice.id,
              booking: Booking(
                  bookingHours: invoice.bookingHours,
                  currency: invoice.priceCurrency,
                  isApprovedApi: invoice.reservations.first.status,
                  isPaid: invoice.isPaid,
                  price: invoice.price,
                  roomId: booking.first.roomId,
                  space: Room(
                      spaceId: booking.first.space.id.toString(),
                      name: booking.first.space.name,
                      location: booking.first.space.location,
                      images: [
                        SpaceImage(
                            url: booking.first.space.coverImage != null
                                ? booking.first.space.coverImage.attachmentFile
                                : null)
                      ]
                      // price: booking..
                      ),
                  status: booking.first.status),
            ),
          ));
          if (reviewStatus != null && reviewStatus == true) {
            setState(() {
              invoice.isRated = "true";
              bookingStatus = "*Thank you for rating this space";
            });
            print("Space reviewed");
          } else {
            print("Space is not reviewed");
          }
        },
        child: Text("Rate this Space",
            style: TextStyle(fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget _statusSection(BuildContext context) {
    var booking = invoice.reservations.first;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Status".toUpperCase(),
          style: Theme.of(context).textTheme.overline,
        ),
        SizedBox(height: 8),
        MyCard(
          child: Column(
            children: <Widget>[
              BookingStatusWidgetLarge(
                booking: Booking(
                    bookingHours: booking.bookingHours,
                    currency: booking.roomPriceCurrency,
                    isApprovedApi: booking.status,
                    isPaid: booking.isPaid,
                    price: booking.price.toDouble(),
                    roomId: booking.roomId,
                    space: Room(
                      name: booking.space.name,
                      // price: booking..
                    ),
                    status: booking.status),
              ),
              _actions(context),
            ],
          ),
        ),
      ],
    );
  }

  Widget _actions(BuildContext context) {
    var booking = invoice.reservations.first;
    // Actions:
    // - Cancel booking
    // - Review Space

    // Return empty container if not action are
    // shown (which is when booking is not approved)
    if (!booking.isApproved()) {
      return Container();
    }

    return Column(
      children: <Widget>[
        Divider(height: 0),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              // Show cancel button if booking is approved and in the future
              Container(
                width: MediaQuery.of(context).size.width - 60,
                child: booking.isApproved() && !booking.isPast()
                    ? //_cancelButton()
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Note:",
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                                child: Text(
                              "Please login to website for doing cancelation of this booking",
                              style: TextStyle(fontSize: 14),
                            ))
                          ],
                        ))
                    : Container(),
              ),

              // Show review button if booking is approved and in the past
              Container(
                child: booking.isPast() && invoice.isRated == "false"
                    ? _reviewButton(context)
                    : Container(
                        child: Text(bookingStatus),
                      ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _hostSection(BuildContext context, Host host) {
    var booking = invoice.reservations.first;
    Widget content;

    if (booking.isApproved() || booking.isCancelled()) {
      content = MyCard(
        padding: EdgeInsets.all(8),
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.person),
              title: Text(host.name),
              trailing: IconButton(
                icon: Icon(Icons.content_copy),
                onPressed: () {
                  Clipboard.setData(ClipboardData(text: host.name));
                  snakKey.currentState.showSnackBar(SnackBar(
                    content: Text("${host.name} copied to clipboard"),
                  ));
                },
              ),
            ),
            ListTile(
              leading: Icon(Icons.phone),
              title: Text(host.phoneNumber),
              onTap: () =>
                  _launchURL("tel://${host.phoneNumber.replaceAll(" ", "")}"),
              trailing: IconButton(
                icon: Icon(Icons.content_copy),
                onPressed: () {
                  _copy(host.phoneNumber);
                },
              ),
            ),
            ListTile(
              leading: Icon(Icons.alternate_email),
              title: Text(host.email),
              onTap: () => _launchURL(Uri.encodeFull(
                  "mailto:${host.email}?subject=${booking.space.name}&body=Hello ${host.name}")),
              trailing: IconButton(
                icon: Icon(Icons.content_copy),
                onPressed: () {
                  _copy(host.email);
                },
              ),
            ),
          ],
        ),
      );
    } else {
      content = MyCard(
        padding: EdgeInsets.all(8),
        child: ListTile(
          leading: Icon(Icons.person),
          title: Text(
              "Host information will be able available once your booking has been approved"),
        ),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Your Host".toUpperCase(),
          style: Theme.of(context).textTheme.overline,
        ),
        SizedBox(height: 8),
        content,
      ],
    );
  }

  Widget _locationSection(BuildContext context) {
    var booking = invoice.reservations.first;
    Widget content;
    if (booking.isApproved() || booking.isCancelled()) {
      content = MyCard(
        child: Column(
          children: <Widget>[
            // Map
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 16, right: 16),
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute<Null>(
                      builder: (BuildContext context) {
                        return LocationViewDialog(
                          showFullAddress: true,
                          space: Room(
                            spaceId: booking.space.id.toString(),
                            name: booking.space.name,
                            location: booking.space.location,
                            price: booking.price,
                            priceDefault: booking.price,
                            currency: booking.roomPriceCurrency,
                            images: [
                              SpaceImage(
                                url: booking.space.coverImage.attachmentFile,
                              )
                            ],
                          ),
                          enableControls: booking.isPaid,
                        );
                      },
                      fullscreenDialog: true));
                },
                child: AbsorbPointer(
                  child: Container(
                    height: MediaQuery.of(context).size.height / 3,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: _googleMapWidget(
                            context,
                            Room(
                              spaceId: booking.space.id.toString(),
                              name: booking.space.name,
                              location: booking.space.location,
                              price: booking.price,
                              priceDefault: booking.price,
                              currency: booking.roomPriceCurrency,
                              images: [
                                SpaceImage(
                                  url: booking.space.coverImage.attachmentFile,
                                )
                              ],
                            ))),
                  ),
                ),
              ),
            ),

            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
              child: ListTile(
                onTap: () {
                  String lat = booking.space.location.latitude.toString();
                  String long = booking.space.location.longitude.toString();
                  _launchURL(
                      "https://www.google.com/maps/search/?api=1&query=$lat,$long");
                },
                leading: Icon(Icons.place),
                title: Text(booking.space.location.getFullAddressDisplay()),
                trailing: IconButton(
                  icon: Icon(Icons.content_copy),
                  onPressed: () {
                    _copy(booking.space.location.getFullAddressDisplay());
                  },
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      content = MyCard(
        padding: EdgeInsets.all(8),
        child: ListTile(
          leading: Icon(Icons.place),
          title: Text(
              "Address will be available once your booking has been approved"),
        ),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Location".toUpperCase(),
          style: Theme.of(context).textTheme.overline,
        ),
        SizedBox(height: 8),
        content,
      ],
    );
  }

  Widget _googleMapWidget(BuildContext context, Room space) {
    if (Platform.isAndroid) {
      return SpaceLocationWidget(
        space: space,
        enableControls: false,
      );
    } else if (Platform.isIOS) {
      return Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: Image.network(_buildUrl(context, space), fit: BoxFit.fill),
          ),
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: MtColors.blue75),
                color: MtColors.blue75.withAlpha(50)),
          )
        ],
      );
    } else {
      return Container();
    }
  }

  String _buildUrl(
    BuildContext context,
    Room space,
  ) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    var isMobileLayout = shortestSide < 600;

    int defaultWidth = (MediaQuery.of(context).size.width * 2.5).toInt();
    defaultWidth = isMobileLayout ? 620 : 1900;
    print(defaultWidth);
    int defaultHeight = isMobileLayout ? 400 : 350;
    Map<String, String> defaultLocation = {
      "latitude": space.location.latitude.toString(),
      "longitude": space.location.longitude.toString()
    };
    var baseUri = new Uri(
        scheme: 'https',
        host: 'maps.googleapis.com',
        port: 443,
        path: '/maps/api/staticmap',
        queryParameters: {
          'size': '${defaultWidth}x$defaultHeight',
          'center':
              '${defaultLocation['latitude']},${defaultLocation['longitude']}',
          'zoom': '12',
          'key': 'AIzaSyB_kIX5UrOzY9KC14LVNRAIsZCkx3xBXeA',
        });
    var url = baseUri.toString() +
        '&markers=color:blue%7Clabel:SS%7c${defaultLocation['latitude']},${defaultLocation['longitude']}';
    print(url);
    return url;
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      // key.currentState.showSnackBar(
      //     SnackBar(content: new Text("Woops, something went wrong")));
      throw 'Could not launch $url';
    }
  }

  void _copy(String text) {
    snakKey.currentState.hideCurrentSnackBar();
    Clipboard.setData(ClipboardData(text: text));
    snakKey.currentState.showSnackBar(SnackBar(
      content: Text("$text copied to clipboard"),
    ));
  }
}
