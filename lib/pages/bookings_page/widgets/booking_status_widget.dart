import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/res/mt_styles.dart';

class BookingStatusWidget extends StatelessWidget{
  final Booking booking;

  const BookingStatusWidget({Key key, @required this.booking}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    String statusString;
    Color txtColor;
    IconData icon;

    switch(booking.status){

      case Booking.STATUS_APPROVED:
        statusString = "Approved";
        txtColor = Colors.green;
        icon = Icons.check;
        break;

      case Booking.STATUS_PENDING:
        statusString = "Pending";
        txtColor = MtColors.blue;
        icon = Icons.access_time;
        break;

      case Booking.STATUS_CANCELLED:
        statusString = "Cancelled";
        txtColor = Colors.red;
        icon = Icons.cancel;
        break;

      case Booking.STATUS_REJECTED:
        statusString = "Rejected";
        txtColor = Colors.red;
        icon = Icons.cancel;
        break;

      default:
        return Container();
        break;
    }

    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8)
      ),
      child: Row(
        children: <Widget>[
          Icon(icon, size: 16, color: txtColor),
          SizedBox(width: 6),
          Text(statusString.toUpperCase(), style: TextStyle(
              color: txtColor,
              fontSize: 12,
              fontWeight: FontWeight.bold
          ),),
        ],
      ),
    );
  }
}

class BookingStatusWidgetLarge extends StatelessWidget{
  final Booking booking;

  const BookingStatusWidgetLarge({Key key, @required this.booking}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    String statusString;
    Color foregroundColor;
    IconData icon;

    switch(booking.status){

      case Booking.STATUS_APPROVED:
        statusString = "Approved";
        foregroundColor = Colors.green;
        icon = Icons.check;
        break;

      case Booking.STATUS_PENDING:
        statusString = "Pending";
        foregroundColor = MtColors.blue;
        icon = Icons.access_time;
        break;

      case Booking.STATUS_CANCELLED:
        statusString = "Cancelled";
        foregroundColor = Colors.red;
        icon = Icons.cancel;
        break;

      case Booking.STATUS_REJECTED:
        statusString = "Rejected";
        foregroundColor = Colors.red;
        icon = Icons.cancel;
        break;

      default:
        return Container();
        break;
    }

    return Container(
      padding: EdgeInsets.all(12),
      child: Row(
        children: <Widget>[
          Icon(icon, size: 24, color: foregroundColor),
          SizedBox(width: 16),
          Text(statusString.toUpperCase(), style: TextStyle(
              color: foregroundColor,
              fontSize: 16,
              fontWeight: FontWeight.bold
          ),),
        ],
      ),
    );
  }
}