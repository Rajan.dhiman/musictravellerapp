import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/blocs/user_bloc.dart';
import 'package:mt_ui/data/Invoices.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/pages/bookings_page/widgets/booking_detail_page.dart';
import 'package:mt_ui/pages/bookings_page/widgets/booking_status_widget.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:flutter/foundation.dart';
import 'package:mt_ui/providers/bookings_provider.dart';
import 'package:mt_ui/blocs/booking_bloc.dart';

import 'widgets/booking_item.dart';

class BookingsOverviewPage extends StatefulWidget {
  BookingsOverviewPage({Key key}) : super(key: key);

  @override
  _BookingsOverviewPageState createState() => _BookingsOverviewPageState();
}

class _BookingsOverviewPageState extends State<BookingsOverviewPage>
    with
        SingleTickerProviderStateMixin,
        AutomaticKeepAliveClientMixin<BookingsOverviewPage> {
  @override
  bool get wantKeepAlive => true;

  TabController _tabController;
  ScrollController _scrollController;
  GlobalKey<ScaffoldState> snakKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    getBookings();
    super.initState();

    userBloc.getMediaLinks();
    
    _tabController = TabController(length: 2, vsync: this);
    _scrollController = ScrollController();
  }
  void getBookings()async{
    await bookingBloc.getInvoices();
  }


  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
  
    getBookings();
    super.build(context);
    print("Booking TAB page initilised");
    // BookingsProvider.of(context).query.add(BookingBloc.defaultQuery);
    return Scaffold(
      
      body: NestedScrollView(
            controller: _scrollController,
            headerSliverBuilder: (BuildContext context, bool boxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  centerTitle: true,
                  title: Text("Bookings", style: MtTextStyle.appBarTitle),
                  pinned: true,
                  floating: true,
                  forceElevated: boxIsScrolled,
                  bottom: TabBar(
                    controller: _tabController,
                    labelColor: MtColors.blue,
                    indicatorColor: MtColors.blue,
                    unselectedLabelColor: Colors.black54,
                    isScrollable: true,
                    labelStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                    onTap: (index) {
                      // animate to top when switching pages
                      _scrollController.animateTo(0,
                          duration: Duration(milliseconds: 350),
                          curve: Curves.ease);
                    },
                    tabs: <Widget>[
                      Tab(text: "UPCOMING"),
                      Tab(text: "PAST"),
                    ],
                  ),
                ),
              ];
            },
            body:ValueListenableBuilder(
              valueListenable: bookingBloc.invoiceList,
               builder: (context,List<Invoice>list,_){
                 List<Invoice> list1 =[];
                 list.forEach((x){
                   x.reservations.forEach((y){
                     y.partySize = x.partySize;
                     y.insuranceRevenueCurrency = x.insuranceRevenueCurrency;
                     y.insuranceRevenueLoc = x.insuranceRevenueLoc;
                     y.priceCurrency = x.priceCurrency;
                    //  y.space.location = x.reservations.first.space.location;
                     
                   });
                 });
                 /// remove booking whosse status is Approved or Pending
                 /// !x.isPaid || 
                list.removeWhere((x)=> !( x.status == "Approved" || x.status == "Pending"));

                List<Invoice> pastBookings = List.from(list);
                List<Invoice> upcommingBookings = List.from(list);
                pastBookings.removeWhere((s)=> !s.reservations.first.isPast() );
                upcommingBookings.removeWhere((s)=> s.reservations.first.isPast() );
               return 
                 TabBarView(
                      controller: _tabController,
                      children: <Widget>[
                        BookingsPage(
                            showPastBookings: false, bookingsData: upcommingBookings,refreshPage:_refreshpcomming,snakKey: snakKey),
                            // Container()
                        BookingsPage(
                            showPastBookings: true, bookingsData:pastBookings,refreshPage:_refreshpcomming, snakKey: snakKey,),
                      ],
                    );
               }
              )
      )
    );
      
  }
  void _refreshpcomming(){
    print('PageREfresh');
    setState(() {
      
    });
  }
}

class BookingsPage extends StatelessWidget {
  // pass in true to only show past bookings
  final bool showPastBookings;
  // final data;
  // final List bookings = new List();
  final GlobalKey<ScaffoldState> snakKey;
  final Function refreshPage;
  final List<Invoice> bookingsData;
  // final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =new GlobalKey<RefreshIndicatorState>();
  BookingsPage({Key key,this.showPastBookings = false,this.bookingsData,this.refreshPage, this.snakKey}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // still we be our content list
    List<Widget> slivers = List();

    // top border
    slivers.add(SliverToBoxAdapter(
      child: Container(
        width: double.infinity,
        height: 1,
        color: MtColors.EEEEEE,
      ),
    ));

    // top padding
    slivers.add(SliverToBoxAdapter(
      child: SizedBox(height: 16),
    ));

    // Group bookings by date
    

    //todo uncomment
    // show upcoming or past bookings
    // if (showPastBookings) {
    //   // remove future bookings
    //   bookings.addAll(bookingsData);
    //   bookings.removeWhere((booking) => !booking.isPast());
    // } else {
    //   // remove past bookings
    //   bookings.addAll(bookingsData);
    //   bookings.removeWhere((booking) => booking.isPast());
    // }

    // show empty screen if we have no bookings
    return ValueListenableBuilder(
      valueListenable: bookingBloc.loading,
       builder: (context,bool value,_){
         if(value){
           return Container(
             alignment: Alignment.center,
             child: CircularProgressIndicator(),
           );
         }
         else{
           if (bookingsData.length == 0) {
           TextStyle style = TextStyle(
             color: Colors.black38,
             fontWeight: FontWeight.bold,
           );
           
    
           if (showPastBookings) {
             return Center(
               child: Text("Past bookings will appear here", style: style),
             );
           } else {
             return Center(
               child: Text("Upcoming bookings will appear here", style: style),
             );
           }
         }
         // Group bookings by date
        //  Map<String, List<Invoice>> bookingGroups = Map();
        //  for (Invoice booking in bookingsData) {
        //    // Get date of first hour
        //    String date =
        //        BookingHour.getDateAndMonth(booking.bookingHours[0].dateTime);
    
        //    if (!bookingGroups.containsKey(date)) {
        //      List<Invoice> list = List();
        //      list.add(booking);
        //      bookingGroups.putIfAbsent(date, () => list);
        //    } else {
        //      bookingGroups.update(date, (list) {
        //        list.add(booking);
        //        return list;
        //      });
        //    }
        //  }
    
        //  // Draw bookings with sticky headers on the side for each group
        //  bookingGroups.forEach((String date, List<Invoice> bookings) {
        //    List<Widget> bookingItems = List();
    
        //    for (Invoice booking in bookings) {
        //     //  bookingItems.add(_bookingItem(context, booking));
        //     bookingItems.add(BookingItem(
        //       invoce: booking,
        //     ));
        //    }
    
        //    BookingHour firstBookingHour = bookings[0].bookingHours[0];
        //    slivers.add(_buildListForDate(firstBookingHour.dateTime, bookingItems));
        //  });


         bookingsData.forEach((x){
            List<Widget> bookingItems = List();
             bookingItems.add(BookingItem(
              invoce: x,
              snakKey:snakKey ,
            ));

            BookingHour firstBookingHour = x.reservations[0].bookingHours[0];
           slivers.add(_buildListForDate(firstBookingHour.dateTime, bookingItems));
         });

         return  RefreshIndicator(
           onRefresh: ()async{
               refreshPage();
           },
           child:CustomScrollView(shrinkWrap: true, slivers: slivers)
           );
         }

       }
       );
    
   
  }

  // Space makeDummySpace(String name){
  //   return Space(
  //     spaceId: "1",
  //     name: name,
  //     images: [
  //       SpaceImage(
  //         order: 0,
  //         url: "https://s3.amazonaws.com/musictraveler.herokuapp.com/media/attachments/room_room/373/thumbs/thumb_K3_71db.jpg.368x207_q70.jpg",
  //       ),
  //     ],
  //     location: Location(
  //       city: "Vienna",
  //       country: "Austria",
  //       streetAddress1: "Annagasse 1/10",
  //       zip: "1010",
  //       latitude: 48.204321,
  //       longitude: 16.3719837,
  //       currency: "EUR",
  //     ),
  //   );
  // }

  Widget _buildListForDate(DateTime dateTime, List<Widget> bookingItems) {
    return SliverStickyHeaderBuilder(
      overlapsContent: true,
      builder: (context, state) => _buildSideHeader(dateTime),
      sliver: SliverPadding(
        padding: EdgeInsets.only(
          left: 88,
          right: 16,
        ),
        sliver: SliverList(delegate: SliverChildListDelegate(bookingItems)),
      ),
    );
  }

  Widget _buildSideHeader(DateTime dateTime) {
    String day = BookingHour.getDayShort(dateTime);
    String date = BookingHour.getDateAndMonth(dateTime);
    bool isToday = (BookingHour.getDateAndMonth(dateTime) ==
        BookingHour.getDateAndMonth(DateTime.now()));

    TextStyle _dateStyle1 = TextStyle(
      fontSize: 14,
      letterSpacing: 1.2,
      color: Colors.black38,
    );

    TextStyle _dateStyle2 = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 16,
      color: Colors.black54,
    );

    return Container(
      width: 72,
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: isToday
                ? Text("Today",
                    style: _dateStyle2.copyWith(color: MtColors.blue))
                : Text(day.toUpperCase(), style: _dateStyle1),
          ),
          Text(
            date.toUpperCase(),
            style: _dateStyle2,
          ),
        ],
      ),
    );
  }

  Widget _bookingItem(BuildContext context, Reservation booking) {
    return Container(
      padding: EdgeInsets.only(bottom: 16),
      child: MyCard(
        padding: EdgeInsets.all(16.0),
        onPressed: () => Navigator.of(context).push(MaterialPageRoute<Null>(
            builder: (BuildContext context) {
              return BookingDetailPage(
                // booking: booking,
              );
            },
            fullscreenDialog: true)),
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Wrap(
                  children: <Widget>[
                    Text(
                        BookingHour.getHourSpanString(
                            booking.bookingHours, context),
                        style: TextStyle(
                            color: Colors.black87,
                            fontWeight: FontWeight.bold)),
                    SizedBox(width: 8),
                    Text(booking.getTotalPriceDisplayTwoDecimals(),
                        style: TextStyle(color: Color.fromRGBO(0, 0, 0, 0.54))),
                  ],
                ),
                SizedBox(height: 8.0),
                Padding(
                  padding: const EdgeInsets.only(right: 64.0),
                  child: Text(booking.space.name,
                      style: Theme.of(context).textTheme.title.copyWith(
                          fontWeight: FontWeight.bold, color: Colors.black87)),
                ),
                SizedBox(height: 12.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    BookingStatusWidget(
                      booking:  Booking(
                  bookingHours: booking.bookingHours,
                  currency: booking.roomPriceCurrency,
                  isApprovedApi: booking.status,
                  isPaid: booking.isPaid,
                  price: booking.price.toDouble(),
                  roomId: booking.roomId,
                  space: Room(
                    name: booking.space.name,
                    // price: booking..
                  ),
                  status: booking.status

                ),
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.people,
                          color: Colors.black54,
                          size: 18,
                        ),
                        SizedBox(width: 8),
                        // Text(booking.toString(),
                        //     style: TextStyle(
                        //         color: Colors.black54,
                        //         fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Positioned(
              right: 0,
              top: 0,
              child: ClipOval(
                child: Container(
                  height: 48,
                  width: 48,
                  color: Colors.black26,
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    // imageUrl: "https://s3.amazonaws.com/musictraveler.herokuapp.com/media/attachments/room_room/373/thumbs/thumb_K3_71db.jpg.368x207_q70.jpg",
                    imageUrl: booking.space.coverImage.attachmentFile,
                    placeholder: (context, url) => Container(
                      color: MtColors.EEEEEE,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

// // / create dummy booking objects to show
// List<Booking> _getDummyBookings(){

//   DateTime now = DateTime.now();
//   now = now.subtract(Duration(minutes: now.minute)); // round down to whole hour

//   List<Booking> bookings = List();
//   bookings.add(Booking(
//     space: makeDummySpace("Practice space 2a"),
//     partySize: 1,
//     bookingHours: _getDummyBookingHours(now, 8),
//     status: Booking.STATUS_APPROVED,
//   ));
//   bookings.add(Booking(
//     space: makeDummySpace("Practice space 2a"),
//     partySize: 1,
//     bookingHours: _getDummyBookingHours(now.add(Duration(days: 2)), 8),
//     status: Booking.STATUS_APPROVED,
//   ));
//   bookings.add(Booking(
//     space: makeDummySpace("Practice space 2a"),
//     partySize: 1,
//     bookingHours: _getDummyBookingHours(now.subtract(Duration(days: 2)), 8),
//     status: Booking.STATUS_APPROVED,
//   ));
//   bookings.add(Booking(
//     space: makeDummySpace("Practice space 5b"),
//     partySize: 1,
//     bookingHours: _getDummyBookingHours(now.add(Duration(days: 5)), 16),
//     status: Booking.STATUS_REJECTED,
//   ));
//   bookings.add(Booking(
//     space: makeDummySpace("Practice space 2b"),
//     partySize: 1,
//     bookingHours: _getDummyBookingHours(now.add(Duration(days: 5)).add(Duration(hours: 6)), 16),
//     status: Booking.STATUS_APPROVED,
//   ));
//   bookings.add(Booking(
//     space: makeDummySpace("Downtown Blues Bar"),
//     partySize: 40,
//     bookingHours: _getDummyBookingHours(now.add(Duration(days: 10)), 50),
//     status: Booking.STATUS_CANCELLED,
//   ));
//   bookings.add(Booking(
//     space: makeDummySpace("Chamber Hall 2"),
//     partySize: 40,
//     bookingHours: _getDummyBookingHours(now.add(Duration(days: 30)), 20),
//     status: Booking.STATUS_PENDING,
//   ));

// //     // sort bookings by date
//   bookings.sort();

//   return bookings;
// }

// List<BookingHour> _getDummyBookingHours(DateTime startDate, double price){
//   List<BookingHour> hours = List();
//   hours.add(BookingHour(
//     dateTime: startDate.add(Duration(hours: 1)),
//     price: price,
//     isAvailable: true,
//   ));
//   hours.add(BookingHour(
//     dateTime: startDate.add(Duration(hours: 2)),
//     price: price,
//     isAvailable: true,
//   ));
//   hours.add(BookingHour(
//     dateTime: startDate.add(Duration(hours: 3)),
//     price: price,
//     isAvailable: true,
//   ));

//   return hours;
// }
