import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mt_ui/data/auth_helper.dart';
import 'package:mt_ui/data/currencyConversionHelper.dart';
import 'package:mt_ui/data/rates.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/update/force_update.dart';
import 'package:mt_ui/res/mt_logos_icons.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/utils.dart';
import 'package:mt_ui/widgets/icon_with_text.dart';
import 'package:mt_ui/widgets/loading_indicators.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/pages/home_page.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mt_ui/widgets/progress_overlay.dart';
import 'package:mt_ui/blocs/version_block.dart';

import 'Auth_page/email_login_page.dart';
import 'package:package_info/package_info.dart';

class WelcomePage extends StatefulWidget {
  final bool isAppStartApp;

  const WelcomePage({Key key, this.isAppStartApp = false}) : super(key: key);
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  bool _loading = false;
  int _currentIndex = 1;
   bool productionSelected = false;
  bool stagingSelected = false;
  var service = new CurrencyConvertService();
  final _key = new GlobalKey<ScaffoldState>();
  var keyIndices = new List();
  int lastTap = DateTime.now().millisecondsSinceEpoch;
  int consecutiveTaps = 0;
  String appVersion = '';
   String appEnviornment = '';
  @override
  void initState() { 
    _initCurrencyPreferences();
    getAppBuildNumber();
    checkForForceUpdate();
    //autoLoginUsingAccessToken();
   //  checRememerMeData();
    super.initState();

     print("Enviornment $_currentIndex");

     checkEnviormnent();
     
    resetcityFilter();
   
  }
  void resetcityFilter(){
    /// Fetch user preference filter from local storage
    /// Check if filter is not empty
    /// Set `Vienna At` as initial city filter.
    SharedPreferencesHelper.getSearchFilters().then((List<String> filters) {
      if(filters != null && filters.length >0){
        filters[0] = "Vienna, At";
        SharedPreferencesHelper.setSearchFilters(filters);
      }
    });
  } 
  
  void isBusy(bool value){
    setState(() {
     _loading = value;
    });
  }

  checkEnviormnent() async {
    String sessionEnv =  await SharedPreferencesHelper.getBaseUrl();
    print("sessionEnv  $sessionEnv");
      if(sessionEnv == "staging"){
         appEnviornment = 'Staging';
      }else{
         appEnviornment = '';
      }
  }
  /// initilise currency prfrence
  void _initCurrencyPreferences() async {
    SharedPreferencesHelper.initCurrencyPreferences().then((_)async{
        await service.getRates();
    });
  }

  Future<void> autoLoginUsingAccessToken()async{
    isBusy(true);
    var isLoginSucess = await AuthHelper().getAccessToken();
    if(isLoginSucess){
      var userdata = await SharedPreferencesHelper.getUserData();
      if(userdata == null){
         await AuthHelper().processUserData();
      }
       print("Login sucess using autoLoginUsingAccessToken");
        Navigator.pop(context);
              Navigator.pushAndRemoveUntil( 
                context, MaterialPageRoute(builder: (context) => HomePage()),
                 (x) => false
                );
        isBusy(false);
    }
    else{
      isBusy(false);
      print("AutoLogin Failed: Login first to access app");
    }
  }


   Future<void> checkForForceUpdate()async{
    isBusy(true);

    
    var versionList =  await versionBloc.fetchVersion();

    print(versionList);
    var deviceType = "";

    var list;

   if (Platform.isIOS) {
      deviceType = "ios";
       list = versionList.ios;
    }
    if (Platform.isAndroid) {
      deviceType = "android";
      list = versionList.android;
    }

     print(" Device Type $appVersion");

    print(" Device Type  $deviceType" + " List $list");

    

    if(!list.contains(appVersion)){
        Navigator.pop(context);
              Navigator.pushAndRemoveUntil( 
                context, MaterialPageRoute(builder: (context) => ForceUpdatePage()),
                 (x) => false
                );
        isBusy(false);
    } else{
      isBusy(false);
      autoLoginUsingAccessToken();
    }
  }

  void getAppBuildNumber() async{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    appVersion = "${packageInfo.version}";
  }
  void checRememerMeData()async{
      if(widget.isAppStartApp){
        var isFacebookLogin = await SharedPreferencesHelper.isLoginThroughFacebook();
        if(isFacebookLogin){
          // await signInWithFacebook();
          await autoLoginForFacebook();
        }
         else{
           var isLogOut =  await SharedPreferencesHelper.getLogout();
           if(isLogOut != null && isLogOut != true){
             SharedPreferencesHelper.getCredential().then((credential){
               if(credential != null){
                 print('Remember me is true');
                 signIn(credential[0],credential[1],remember:isLogOut);
                 setState(() {
                   _loading = true;
                 });
                }
               else{
                 print('Credentials are not saved');
               }
             });
           }
           else{
              print('Need to login again');
           }
         }
       }
       else{
         print('Auto login falied');
       }
  }

   Future signIn(String email,String password, {bool remember = false}) async {
      String errorMessage;
      // if(rememberme){
      //   SharedPreferencesHelper.saveCredential(email: email, password:password);
      // }
      // AuthResult result = await AuthHelper().createAccountWithEmail(_data.email, _data.password);
      String result = await AuthHelper().signIn(email, password,remember:remember);
   
      if (result == 'success') {
        await AuthHelper().processUserData();
        // Navigator.pop(context, true);
        // setState(() {
        //     _loading = false;
        //   });
        Navigator.pop(context);
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));
            
      } else {
        errorMessage = "Invalid Sign In credentials";
        setState(() {
          var error = errorMessage;
           _loading = false;
        });
    }
  }


   Widget envRadio(String text, bool selected, int index){
    return  InkWell(
      onTap: (){
        Navigator.pop(context);
        setState(() {
           productionSelected = index == 1 ? true : false;
           stagingSelected = index == 2 ? true : false;
        });
       changeEnviornment();
      },
     
      child:  Container(
        width: (MediaQuery.of(context).size.width -50)/2,
        height: 50,
        padding: EdgeInsets.symmetric(horizontal: 10,vertical:5),
        decoration: BoxDecoration(
          
          borderRadius: BorderRadius.circular(10),
         border: Border.all(color: selected ?  Colors.transparent : Colors.black54),
         color: selected ? MtColors.blue : Colors.transparent
        ),
         child: Text(
            text,
              style: TextStyle(
              color: Colors.black,
              decorationStyle: TextDecorationStyle.wavy,
            ),
            textAlign: TextAlign.center,
          ),
      ),
     
    
     );
  }


   void changeEnviornment() async{
     if(productionSelected){ 
       SharedPreferencesHelper.initBaseUrlPreferences(url:'production');
       print('Production is selected');
     }
     else if(stagingSelected){
       SharedPreferencesHelper.initBaseUrlPreferences(url:'staging');
       print('Staging is selected');
    }
     MtConstants.changeUrl();
     checkEnviormnent();
  }


   void _displayDialog() async {
    
         await showDialog(
              context: context,
              useRootNavigator: true,
              builder: (BuildContext context) {
                return Dialog(
                  elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  child:
                  Container(
                     decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(40))
                    ),
                    height: 180,
                    width:  MediaQuery.of(context).size.width ,
                    child:  Container(
                       padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20))
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          MyCard(
                              shadow: Shadow.none,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(16, 0, 16, 8),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Container(),
                                      ),
                                      Expanded(
                                        flex: 7,
                                        child:   Center(child:  Text('Switch Enviornemt', style: MtTextStyle.appBarTitle.copyWith(fontSize: 18),),)
                                      ),
                                       
                                      Expanded(
                                        flex:
                                         1,
                                        child:  InkWell(
                                          borderRadius: BorderRadius.all(Radius.circular(5)),
                                          onTap: (){ Navigator.pop(context, List<String>());},
                                          child: Container(
                                            height: 25,
                                            width: 25,
                                            decoration: BoxDecoration(
                                             border: Border.all(color: Colors.black87),
                                             shape: BoxShape.circle
                                            ),
                                            child:  Icon(Icons.close),
                                          ),
                                        ),
                                      ),
                                      
                                    ],),
                                  ],
                                ),
                              ),
                            ),
                         Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              envRadio("Production",productionSelected,1),
                      
                            ],
                          ),
                           Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                          
                              envRadio("Staging",stagingSelected,2),
                            ],
                          ),
                  
                        ],
                      ),
                    ),
                  )
                );
              }
         );
            
   }
  
  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double mPadding = (screenHeight > 600) ? 32.0 : 16.0;

    Color bgColor = (appEnviornment == 'Staging' ) ? Colors.red : Colors.white38;

    return Scaffold(
      key: _key,
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.white, Color(0xffF7F7F7)])),
        child: Stack(
          children: <Widget>[

            
            Positioned(
              bottom: -MediaQuery.of(context).size.height / 2 - 100,
              left: -MediaQuery.of(context).size.height / 2,
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100000),
                  color: MtColors.blue.withOpacity(0.02),
                ),
              ),
            ),

            Positioned(
              top: -MediaQuery.of(context).size.height / 2 - 100,
              right: -MediaQuery.of(context).size.height / 2,
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100000),
                  color: MtColors.blue.withOpacity(0.01),
                ),
              ),
            ),

            
            
            SafeArea(
              top: true,
              bottom: true,
              child: Padding(
                padding: EdgeInsets.only(
                    left: mPadding,
                    top: 0,
                    right: mPadding,
                    bottom: 16.0),
                child: Column(
                  children: <Widget>[


                    Container(
                        height: 75,
                        width: 75,
                        color: Colors.white38,
                        alignment: Alignment(1.0, -1.0),
                        child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child:  GestureDetector(
                                      onTap: () {
                                        int now = DateTime.now().millisecondsSinceEpoch;
                                        if (now - lastTap < 1000) {
                                          print("Consecutive tap");
                                          consecutiveTaps ++;
                                          print("taps = " + consecutiveTaps.toString());
                                          if (consecutiveTaps == 5){
                                            _displayDialog();
                                          }
                                        } else {
                                          consecutiveTaps = 0;
                                        }
                                        lastTap = now;
                                      },
                    
                                  ),
                        )
                    ),


        

                    Container(
                          constraints: BoxConstraints.expand(
                              height: Theme.of(context).textTheme.display1.fontSize * 1.0 + 10.0,
                            ),
                            padding: EdgeInsets.symmetric(horizontal:20,vertical:10),
                            alignment: Alignment.center,
                            color: bgColor,
                            child: Text(appEnviornment,style: TextStyle(color:Colors.white),),
                          ),
                  
                   
                    // Logo
                    Expanded(
                      flex: 4,
                      child: Container(
                          width: double.infinity,
                          child: (screenHeight > 500)
                              ? Icon(MtLogos.vertical,
                                  color: MtColors.blue, size: 160)
                              : (screenHeight > 400 && screenHeight < 500)
                                  ? Icon(MtLogos.horizontal,
                                      color: MtColors.blue, size: 64)
                                  : Container()),
                    ),

                    // Continue with Google
                    // TODO (DD) Uncomment in next release (FB login)
//                     MyCard(
//                       onPressed: (){
//                         signInWithGoogle();
//                       },
//                       borderRadiusValue: 100,
//                       shadow: Shadow.large,
//                       padding: EdgeInsets.symmetric(vertical: 20),
//                       child: Center(
//                         child: IconWithText(
//                           shrinkWrap: true,
//                           icon: FontAwesomeIcons.google,
//                           text: "Continue with Google",
//                           textColor: MtColors.blue,
//                           fontWeight: FontWeight.bold,
//                           iconSize: 22,
//                           fontSize: 18,
//                           iconSpacing: 16,
//                         ),
//                       ),
//                     ),
//
//                     SizedBox(height: 8),

                  

                    // Continue with Facebook
                     _loading ? Center(
                       child: InstrumentsIndicator(color: Colors.black54,)
                     )
                    : MyCard(
                      onPressed: () {
                        signInWithFacebook();
                      },
                      borderRadiusValue: 100,
                      shadow: Shadow.large,
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Center(
                        child: IconWithText(
                          shrinkWrap: true,
                          icon: FontAwesomeIcons.facebookSquare,
                          text: "Continue with Facebook",
                          textColor: MtColors.blue,
                          fontWeight: FontWeight.bold,
                          iconSize: 22,
                          fontSize: 18,
                          iconSpacing: 16,
                        ),
                      ),
                    ),

                    SizedBox(height: 8),

                    // Sign up
                    // MyCard(
                    //   onPressed: () async {
                    //     bool loginSuccess = await Navigator.push(
                    //       context,
                    //       MaterialPageRoute(
                    //         builder: (context) => EmailLoginPage(signUp: true),
                    //       ),
                    //     );

                    //     // pop welcome screen if user logged in
                    //     if (loginSuccess != null && loginSuccess) {
                    //       Navigator.pop(context);
                    //     }
                    //   },
                    //   borderRadiusValue: 100,
                    //   shadow: Shadow.large,
                    //   padding: EdgeInsets.symmetric(vertical: 20),
                    //   child: Center(
                    //     child: IconWithText(
                    //       shrinkWrap: true,
                    //       // todo !change icon
                    //       // icon: FontAwesomeIcons.facebookSquare,
                    //       icon: FontAwesomeIcons.userPlus,
                    //       text: "Sign up with email",
                    //       textColor: MtColors.blue,
                    //       fontWeight: FontWeight.bold,
                    //       iconSize: 22,
                    //       fontSize: 18,
                    //       iconSpacing: 16,
                    //     ),
                    //   ),
                    // ),

                    SizedBox(height: 8),
                     _loading ? SizedBox()
                    : 
                    // Sign in
                    MyCard(
                      onPressed: () async {
                        // bool loginSuccess =
                         await Navigator.pushNamed(context, '/login');
                        //  await Navigator.push(
                        //   context,
                        //   MaterialPageRoute(
                        //     builder: (context) => EmailLoginPage(signUp: false),
                        //   ),
                        // );

                        // pop welcome screen if user logged in
                        ///[Comment on 3 FEB]
                        // if (loginSuccess != null && loginSuccess) {
                        //   Navigator.pop(context);
                        // }
                      },
                      borderRadiusValue: 100,
                      shadow: Shadow.large,
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Center(
                        child: IconWithText(
                          shrinkWrap: true,
                          // todo !change icon
                          // icon: FontAwesomeIcons.facebookSquare,
                          icon: FontAwesomeIcons.signInAlt,
                          text: "Continue with Email",
                          textColor: MtColors.blue,
                          fontWeight: FontWeight.bold,
                          iconSize: 22,
                          fontSize: 18,
                          iconSpacing: 16,
                        ),
                      ),
                    ),

                    Row(
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        // FlatButton(
                        //   // onPressed: () async {

                        //   //   bool loginSuccess = await Navigator.push(context,
                        //   //     MaterialPageRoute(
                        //   //       builder: (context) => EmailLoginPage(),
                        //   //     ),
                        //   //   );

                        //   //   // pop welcome screen if user logged in
                        //   //   if(loginSuccess != null && loginSuccess){
                        //   //     Navigator.pop(context);
                        //   //   }

                        //   // },
                        //   onPressed: () {},
                        //   // child: Text("Sign in with email", style: MtTextStyle.bold),
                        //   textColor: Colors.black54,
                        // ),

                        // todo navigate to search page
                         _loading ? SizedBox()
                        : 
                        FlatButton(
                          onPressed: () async {
                            SharedPreferencesHelper.setUserLogin(
                                'null', 'null');
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomePage()));
                          },
                          child: Text("Sign in later", style: MtTextStyle.bold),
                          textColor: Colors.black54,
                        ),
                      ],
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(),
                    ),
                        // legal notice
                    Container(
                      constraints: BoxConstraints(maxWidth: 240),
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(children: [
                          TextSpan(
                              text: "By using this app you agree to our ",
                              style: _topNoticeStyle()),
                          TextSpan(
                            text: "Terms of Service",
                            style: _topNoticeStyleLink().copyWith(color:MtColors.blue),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () {
                                Utils.launchURL(
                                    'https://musictraveler.com/terms');
                              },
                          ),
                          TextSpan(text: " and ", style: _topNoticeStyle()),
                          TextSpan(
                            text: "Privacy Policy",
                            style: _topNoticeStyleLink().copyWith(color:MtColors.blue),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () {
                                Utils.launchURL(
                                    'https://musictraveler.com/privacy');
                              },
                          ),
                        ]),
                      ),
                    ),

                    // SizedBox(height: 16),
                  ],
                ),
              ),
            ),

            // loading indicator
            // Container(
            //   child: _loading ? ProgressOverlay() : Container(),
            // ),
          ],
        ),
      ),
    );
  }

  

  Future<Null> signInWithGoogle() async {
    this.setState(() {
      _loading = true;
    });

    await AuthHelper().connectWithGoogle().then((result) async {
      if (result.user != null) {
//        showWelcomeDialog(result.user.displayName);
        setState(() {
          _loading = false;
        });
        await AuthHelper().processUserData();
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      } else {
//        showErrorSnackBar(context, result.errorMessage);
        setState(() {
          _loading = false;
        });
      }
    });
  }
  
  Future<void> autoLoginForFacebook()async{
        try{
             this.setState(() {
           _loading = true;
         });
         await AuthHelper().processUserData().then((status){
           switch (status) {
             case "success": 
             Navigator.pop(context);
             Navigator.pushAndRemoveUntil( 
               context, MaterialPageRoute(builder: (context) => HomePage()),
                (x) => false
               );
               break;
             case "fail" : showErrorSnackBar("Server error please try again"); break;
             default: showErrorSnackBar(  "Server error please try again" ); break;

           }
            setState(() {
              _loading = false;
            });
       });
     }catch(error){
       showErrorSnackBar("Server error please try again");
         print('Error while facebook autologin :- $error');
         this.setState(() {
          _loading = false;
       });
     }
  }

 

  Future<Null> signInWithFacebook() async {
    this.setState(() {
      _loading = true;
    });

    await AuthHelper().connectWithFacebook().then((result) async {
      if (result.user != null) {
//        showWelcomeDialog(resul t.user.displayName);
       
        // AuthResult result = await AuthHelper().createAccountWithEmail(result.user.uid, _data.password);
        await AuthHelper().processUserData().then((status){
            switch (status) {
              case "success": 
              Navigator.pop(context);
              Navigator.pushAndRemoveUntil( 
                context, MaterialPageRoute(builder: (context) => HomePage()),
                 (x) => false
                );
                break;
              case "fail" : showErrorSnackBar("Server error please try again"); break;
              default: showErrorSnackBar(  "Server error please try again" ); break;

            }
             setState(() {
               _loading = false;
             });
        });
      } 
      else {
       showErrorSnackBar( result.errorMessage);
        setState(() {
          _loading = false;
        });
      }
    });
  }
  void showErrorSnackBar(String message){
     _key.currentState.showSnackBar(SnackBar(
      content: ListTile(
        leading: Icon(Icons.error_outline, color: Colors.red),
        title: Text(message, style: TextStyle(color: Colors.red)),
      ),
      duration: Duration(seconds: 10),
      backgroundColor: Colors.red[100],
    ));
  }

  

  void showWelcomeDialog(String name) {
    showDialog(
        context: context,
        builder: (context) {
          // dismiss dialog after 2 seconds
          Future.delayed(Duration(seconds: 2)).then((nothing) {
            Navigator.pop(context);
          });

          return SimpleDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            contentPadding: EdgeInsets.zero,
            children: <Widget>[
              // Header
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: MtColors.blue10),
                padding: EdgeInsets.only(
                    left: 48.0, top: 48.0, right: 48.0, bottom: 32.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      child: Image.asset(
                          'assets/graphics/illustrations/undraw_message_sent_1030.png'),
                    ),

                    SizedBox(
                      height: 32,
                    ),

                    // Welcome, first name
                    Text(
                      "Welcome, $name",
                      style: TextStyle(
                          fontSize: 20,
                          color: MtColors.blue,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          );
        });
  }

  TextStyle _topNoticeStyle() {
    return TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.black26,
        fontFamily: "lato" // need to specify font when used in RichText field
        );
  }

  TextStyle _topNoticeStyleLink() {
    return _topNoticeStyle().copyWith(
      decoration: TextDecoration.underline,
    );
  }
}
