import 'package:flutter/material.dart';
import 'package:mt_ui/pages/search_page/search_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/widgets/booking_hours_overview.dart';
import 'package:mt_ui/widgets/booking_price_overview.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/space_view_compact.dart';

class BookingSuccessPage extends StatelessWidget{
  final Booking booking;

  const BookingSuccessPage({Key key, this.booking}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    TextStyle pricingStyle = TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );

    return WillPopScope(
      onWillPop: () async {
        // pop back to space page when pressing back
        Navigator.popUntil(context, ModalRoute.withName('/space-page'));
        return false;
      },
      child: Scaffold(
        backgroundColor: MtColors.FEFEFE,
        body: CustomScrollView(

          slivers: <Widget>[
            SliverAppBar(
              floating: true,
              snap: true,
              title: Text("Booking request sent", style: MtTextStyle.appBarTitle),
              centerTitle: true,
            ),

            SliverList(
              delegate: SliverChildListDelegate([

                Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      _headerWidget(context),

                      SizedBox(height: 16),

                      MyCard(
                        onPressed: (){
                          // Go to bookings tab
                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => SearchPage()));

                        },
                        child: ListTile(
                          leading: Icon(Icons.playlist_add_check),
                          title: Text("Go back to search page", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),),
                          trailing: Icon(Icons.arrow_forward),
                        ),
                      ),

                      SizedBox(height: 32),

                      Text("Booking".toUpperCase(), style: Theme.of(context).textTheme.overline,),

                      SizedBox(height: 8),

                      MyCard(
                        padding: EdgeInsets.all(16.0),
                        child: Column(
                          children: <Widget>[
                            // SpaceViewCompact(
                            //   space: booking.space,
                            // ),

                            SizedBox(height: 16),

                            BookingHoursOverview(
                              booking: booking,
                            ),

                            Divider(height: 16),

                            ListTile(
                              contentPadding: EdgeInsets.zero,
                              title: Text("Number of guests", style: pricingStyle,),
                              trailing: Text(booking.partySize.toString(), style: pricingStyle,),
                            ),

                            Divider(height: 16),

                            BookingPriceOverview(
                              booking: booking,
                            ),
                          ],
                        ),
                      ),


                    ],
                  ),
                )
              ]),
            ),
          ],
        ),
      ),
    );
  }

  Widget _headerWidget(context){

    TextStyle infoStyle = Theme.of(context).textTheme.body1.copyWith(
      color: Colors.black87,
    );

    return MyCard(
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(32),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  colors: [MtColors.blue, MtColors.blue90],
                ),
            ),
            child: Center(
              child: Column(
                children: <Widget>[
                  Text("Thank you".toUpperCase(), style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                    color: Colors.white,
                  ),),
                ],
              ),
            ),
          ),

          Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[

                ListTile(
                  leading: Icon(Icons.access_time, color: MtColors.blue,),
                  title: Text(
                      "Your booking is pending approval.",
                      style: infoStyle),
                ),

                SizedBox(height: 16),

                ListTile(
                  leading: Icon(Icons.place, color: MtColors.blue,),
                  title: Text(
                      "Contact information and exact address will be available once approved",
                      style: infoStyle),
                ),

              ],
            ),
          ),

        ],
      ),
    );
  }
}
