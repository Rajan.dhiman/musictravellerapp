import 'package:flutter/material.dart';
import 'package:mt_ui/pages/home_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/widgets/my_card.dart';

class BookingFailedPage extends StatelessWidget{
  final Booking booking;

  const BookingFailedPage({Key key, this.booking}) : super(key: key);

  @override
  Widget build(BuildContext context) {

//    TextStyle pricingStyle = TextStyle(
//      color: Colors.black87,
//      fontWeight: FontWeight.bold,
//      fontSize: 15,
//    );

    return WillPopScope(
      onWillPop: () async {
        // pop back to space page when pressing back
        Navigator.popUntil(context, ModalRoute.withName('/space-page'));
        return false;
      },
      child: Scaffold(
        backgroundColor: MtColors.FEFEFE,
        body: CustomScrollView(

          slivers: <Widget>[
            SliverAppBar(
              floating: true,
              snap: true,
              title: Text("Booking failed", style: MtTextStyle.appBarTitle),
              centerTitle: true,
            ),

            SliverList(
              delegate: SliverChildListDelegate([

                Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      _headerWidget(context),
                      SizedBox(height: 30),
                      _goBackButton(context)
                    ],
                  ),
                )
              ]),
            ),
          ],
        ),
      ),
    );
  }

  Widget _headerWidget(context){

    TextStyle infoStyle = Theme.of(context).textTheme.body1.copyWith(
      color: Colors.black87,
    );

    return MyCard(
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(32),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  colors: [MtColors.blue, MtColors.blue90],
                ),
            ),
            child: Center(
              child: Column(
                children: <Widget>[
                  Text("Your card has not been charged".toUpperCase(), style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.white,
                  ),),
                ],
              ),
            ),
          ),

          Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: Icon(Icons.card_travel, color: MtColors.blue,),
                  title: Text(
                      "Please contact our customer service at support@musictraveler.com‍‍‍‍‍‍",
                      style: infoStyle),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  Widget _goBackButton(BuildContext context) {
    return RaisedButton(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              constraints: BoxConstraints(maxWidth: 100),
            ),
            Center(
              child: Text(
                "Go back to search page".toUpperCase(),
                style: MtTextStyle.button),
            ),
          ],
        ),
        padding: EdgeInsets.symmetric(vertical: 16),
        textColor: Colors.white,
        color: MtColors.blue,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()))
      );
  }
}
