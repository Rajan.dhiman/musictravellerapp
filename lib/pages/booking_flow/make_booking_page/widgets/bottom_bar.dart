import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/Auth_page/email_login_page.dart';
import 'package:mt_ui/pages/welcome_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/pages/booking_flow/booking_confirm_page.dart';
import 'package:mt_ui/pages/booking_flow/make_booking_page/widgets/booking_state_provider.dart';
import 'package:mt_ui/data/models.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'dart:io';

class BottomBar extends StatelessWidget {
  final int numberOfHoursSelected;
  final String roomId;
  final String userToken;
  final int maxGuest;
  // final GlobalKey<ScaffoldState>  key;
  

  const BottomBar({ Key  key, this.numberOfHoursSelected, this.roomId, this.userToken, this.maxGuest}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          offset: Offset(0, -3),
          color: MtColors.black7,
          blurRadius: 9,
        ),
      ]),
      child: SafeArea(
        top: false,
        bottom: true,
        child: _content(context),
      ),
    );
  }

  Widget _content(context){
    Booking booking = BookingState.of(context).booking;
    bool selected = booking.bookingHours.length > 0;
    bool loggedIn = userToken != 'null';

    return Container(
      height: 64,
      child: Stack(
        children: <Widget>[

          _bookingPadding(context, booking, selected, loggedIn),

          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.start,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: <Widget>[

          //       // price
          //       Text("${booking.getTotalSpaceFeeDisplay()}",
          //           style: TextStyle(
          //             fontWeight: FontWeight.bold,
          //             fontSize: 20.0,
          //           )
          //       ),

          //       // Spacing
          //       Expanded(
          //         flex: 1,
          //         child: SizedBox(),
          //       ),

          //       // Button
          //       RaisedButton(
          //         elevation: 0,
          //         color: MtColors.blue,
          //         child: Text(
          //           "Make booking".toUpperCase(),
          //           style: Theme.of(context).textTheme.button.copyWith(
          //               color: Colors.white,
          //               letterSpacing: 0.8,
          //               fontWeight: FontWeight.bold),
          //         ),
          //         onPressed: () {

          //           // Halt if no hours are selected
          //           if(BookingState.of(context).booking.bookingHours.length == 0){
          //             Scaffold.of(context).showSnackBar(SnackBar(
          //               content: Text("No hours selected"),
          //             ));
          //             return false;
          //           }

          //           // TODO make api call here

          //           Booking booking = BookingState.of(context).booking;

          //           _continueToPaymentMutation(booking, roomId).then((res) {
          //             // get error here if user is not authed
          //             if (res['errors'] == null) {
          //               var stripePk = res['data']['reserveRoom']['stripePk'];
          //               var price = res['data']['reserveRoom']['price'];
          //               var currency = res['data']['reserveRoom']['priceCurrency'];
          //               var bookingId = res['data']['reserveRoom']['id'];

          //               Navigator.of(context).push(MaterialPageRoute<Null>(
          //                 builder: (BuildContext context) {
          //                   return BookingConfirmPage(
          //                     booking: booking,
          //                     stripePk: stripePk,
          //                     price: price,
          //                     currency: currency,
          //                     bookingId: bookingId,
          //                   );
          //                 },
          //               ));
          //             }
          //           });

          //         },
          //         padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 20.0),
          //         shape: new RoundedRectangleBorder(
          //             borderRadius: new BorderRadius.circular(30.0)),
          //       ),
          //     ],
          //   ),
          // ),

          // // "Please select hours" text (shown when no hours are selected.
          // // slide-fade animation in and out
          // AnimatedOpacity(
          //   opacity: 1,
          //   // opacity: (selected && loggedIn) ? 0 : 1,
          //   duration: Duration(milliseconds: 350),
          //   child: AnimatedContainer(
          //       duration: Duration(milliseconds: 350),
          //       curve: Curves.easeInOut,
          //       transform: Matrix4.translationValues(0, selected ? 64 : 0, 0), // slide to bottom
          //       color: Colors.white,
          //       child: Center(
          //         child: Text(
          //           // (userToken != Null) ? "Please select hours" : "Please Login",
          //           loggedIn ? "Please select hours" : "Please Login",
          //           style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black54),
          //         ),
          //       ),
          //   ),
          // ),

        ],
      ),
    );
  }

  Future<Map<String, dynamic>> _continueToPaymentMutation(Booking booking, String roomId) async {

    var formatter = new DateFormat('yyyy-MM-dd');
    var dt = booking.bookingHours[0].dateTime;
    var hour =  Iterable.generate(booking.bookingHours.length, (x) => booking.bookingHours[x].dateTime.hour).toList();// DateTime.parse(dt.toString());
    String formatted = formatter.format(booking.bookingHours[0].dateTime);

    final Client _client = Client();

    Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});

    
    // String query = '''mutation{
    //   reserveRoom(date:"$formatted", hourStart:${moonLanding.hour}, hourEnd:${moonLanding.hour + 1}, roomId:$roomId, region:"EU") {
    //     id
    //     roomId
    //     price
    //     priceCurrency
    //     stripePk
    //   }
    // }''';
   String query = '''mutation{
      multipleReservations(
        date: "$formatted",
      	hours: $hour, 
        region: "US",
        roomId: $roomId,
        partySize: 1
      ){
      	invoiceId
        roomId
        userId
        reservations
        price
        priceCurrency
        stripePk
        roomPrice
    	}
    }''';

    var jsonOut = {};
    print(query);
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};
    await _client
    .post(getUrlForQuery(query), headers: headers)
    .then((result) => result.body)
    .then(json.decode)
    .then((json){
      jsonOut = json;
      print(jsonOut);
    });
    return jsonOut;
  }

  Widget _bookingPadding(context, booking, selected, loggedIn) {
    // if (selected && loggedIn) {
    if (loggedIn) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            // price
            Container(
              child: Text("${booking.getTotalSpaceFeeDisplay()}",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                )
              ),
            ),
 
             SizedBox(width: 10,),
             numberOfHoursSelected == 0 || numberOfHoursSelected ==null ?
              Text("for 0  hour")
              : numberOfHoursSelected == 1 ?  Text("for $numberOfHoursSelected  hour",)
              :   Text("for $numberOfHoursSelected  hours",),
            // Spacing
            Expanded(
              flex: 1,
              child: SizedBox(),
            ),

            // Button
            RaisedButton(
              elevation: 0,
              color: MtColors.blue,
              child: Text(
                selected ? "NEXT".toUpperCase() : "No hours selected".toUpperCase(),
                style: Theme.of(context).textTheme.button.copyWith(
                    color: Colors.white,
                    letterSpacing: 0.8,
                    fontWeight: FontWeight.bold),
              ),
              onPressed: () async{

                // Halt if no hours are selected
                if(BookingState.of(context).booking.bookingHours.length == 0){
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("No hours selected"),
                  ));
                  return false;
                }

                // TODO make api call here

                // Booking booking = BookingState.of(context).booking;
                var currencyPrice = await SharedPreferencesHelper.getCurrencyRate();
                var currencyPref = await SharedPreferencesHelper.getCurrencyPreference();

                 Navigator.of(context).push(MaterialPageRoute<Null>(
                      builder: (BuildContext context) {
                        return BookingConfirmPage(
                          booking: booking,
                          roomId: roomId,
                          // stripePk: stripePk,
                          price: booking.price,
                          // currency: currency,
                          // bookingId: bookingId,
                          // invoiceId: invoiceId,
                          maxGuest: maxGuest,
                        );
                      },
                    ));
                // _continueToPaymentMutation(booking, roomId).then((res) {
                //   if(res['data']['multipleReservations'] == null){
                  
                //      return;
                //   }
                //   // get error here if user is not authed
                //   var apiPrice = res['data']['multipleReservations']['price'];
                //   var apiCurrency = res['data']['multipleReservations']['priceCurrency'];
                //   if(currencyPrice != null && currencyPref != null){
                //     double mod = pow(10.0, 2); 
                //     apiPrice =  ((apiPrice * currencyPrice * mod).round().toDouble() / mod); 
                //     apiCurrency = currencyPref;
                //   }
                //   /// [Currencyconversion]
                
                //     var stripePk = res['data']['multipleReservations']['stripePk'];
                //     var price = apiPrice;
                //     var currency = apiCurrency;//
                //     var bookingId = res['data']['multipleReservations']['id'];
                //     var invoiceId = res['data']['multipleReservations']['invoiceId'];

                //     Navigator.of(context).push(MaterialPageRoute<Null>(
                //       builder: (BuildContext context) {
                //         return BookingConfirmPage(
                //           booking: booking,
                //           stripePk: stripePk,
                //           price: price,
                //           currency: currency,
                //           bookingId: bookingId,
                //           invoiceId: invoiceId,
                //           maxGuest: maxGuest,
                //         );
                //       },
                //     ));
                
                // });

                return true;
              },
              padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 20.0),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
            ),
          ],
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            // price
            Text("${booking.getTotalSpaceFeeDisplay()}",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                )
            ),

            // Spacing
            Expanded(
              flex: 1,
              child: SizedBox(),
            ),

            // Button
            RaisedButton(
              elevation: 0,
              color: MtColors.blue,
              child: Text(
                "Please Sign In".toUpperCase(),
                style: Theme.of(context).textTheme.button.copyWith(
                    color: Colors.white,
                    letterSpacing: 0.8,
                    fontWeight: FontWeight.bold),
              ),
              onPressed: ()async {
                await SharedPreferencesHelper.saveRoomId(int.parse(roomId));
                Navigator.of(context).pushAndRemoveUntil(
                  
                  MaterialPageRoute<Null>(
                       builder: (BuildContext context) {
                         return EmailLoginPage(signUp: false,isFromCheckBookingPage: true,);
                       },
                     ),
                    ModalRoute.withName('/')
                     
                );
                // Navigator.pushReplacement(context,
                //     MaterialPageRoute(builder: (context) => EmailLoginPage(signUp: false,isFromCheckBookingPage: true,))
                // );
              },
              padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 20.0),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
            ),
          ],
        ),
      );
    }
  }

}
