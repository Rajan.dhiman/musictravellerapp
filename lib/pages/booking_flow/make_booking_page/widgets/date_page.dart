import 'package:flutter/material.dart';
import 'package:mt_ui/pages/booking_flow/make_booking_page/widgets/booking_state_provider.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/data/models.dart';

class DatePage extends StatefulWidget {
  final List<BookingHour> bookingHours;

  const DatePage({Key key, this.bookingHours}) : super(key: key);

  @override
  _MyPageState createState() => _MyPageState();
}

class _MyPageState extends State<DatePage> {
  @override
  Widget build(BuildContext context) {

    if(widget.bookingHours.length == 0){
      return Container(
        child: Center(
          child: Text("No hours available on this day", style: TextStyle(color: Colors.black54)),
        ),
      );
    }

    return Container(
        child: ListView.builder(

          // 00:00 is in the list twice to get the last hour 23:00 - 00:00.
          // itemCount: widget.bookingHours.length - 1,
            itemCount: widget.bookingHours.length,

            // Create list item
            itemBuilder: (context, index) {
            
              var isDisable = widget.bookingHours[index].dateTime.toLocal().isBefore(DateTime.now().toLocal());
            
              return Container(
              // Blue background when selected
              decoration: BoxDecoration(
                color: isSelected(widget.bookingHours[index])
                    ? MtColors.blue.withOpacity(0.10)
                    : null,
              ),
              // List item with checkbox
              child: CheckboxListTile(
                // text
                title: DefaultTextStyle(
                  // Blue text color when selected
                  style: TextStyle(
                      color: isSelected(widget.bookingHours[index])
                          ? MtColors.blue
                          : Colors.black87,
                      fontSize: 18),

                  // e.g. 11:00 - 12:00.
                  child: Wrap(
                    children: <Widget>[
                      Text(BookingHour.getHour(
                          widget.bookingHours[index].dateTime, context),
                           style:TextStyle(
                            color: isDisable ? Colors.black38 : Colors.black
                           ),
                          ),
                      Text(
                        " - ",
                        style: TextStyle(
                            color: Colors.black26,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(BookingHour.getHour(widget.bookingHours[index].dateTime.add(Duration(hours: 1)),
                          context),
                          style:TextStyle(
                            color:  isDisable  ? Colors.black38 : Colors.black
                          ),
                      ),
                    ],
                  ),
                ),

                // Price container
                secondary: _priceContainer(widget.bookingHours[index],isDisable:isDisable),

                // value
                value: isSelected(widget.bookingHours[index]),

                // on check changed
                onChanged: (bool value) {
                  if(isDisable){
                    return;
                  }
                  // add/remove hour to Booking object
                  if (value) {
                    // BookingState.of(context).booking.bookingsHours.add(widget.bookingHours[index]);
                    BookingState.of(context).addBookingHour(widget.bookingHours[index],);
                  } else {
                    // BookingState.of(context).booking.bookingsHours.remove(widget.bookingHours[index]);
                    BookingState.of(context).removeBookingHour(widget.bookingHours[index]);
                  }
                },
                activeColor: MtColors.blue,
              ),
            );}
        )
      );
         
  }


  // returns true if bookingHour is selected
  bool isSelected(BookingHour bookingHour) {
    for(BookingHour b in BookingState.of(context).booking.bookingHours){
      if(b.dateTime == bookingHour.dateTime){
        return true;
      }
    }
    return false;
  }

  Widget _priceContainer(BookingHour bookingHour, {bool isDisable}){

    // remove trailing .0 from price
    String price = bookingHour.price.toString();
    if(price.endsWith(".0")){
      price = price.substring(0, price.length - 2);
    }

    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: isDisable ? Colors.black12 : isSelected(bookingHour)
            ? MtColors.blue
            : MtColors.blue10,
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: Text(
        BookingState.of(context).booking.space.location.getCurrencySymbol() + price,
        style: TextStyle(
          color: isDisable ? Colors.white : isSelected(bookingHour)
              ? Colors.white
              : MtColors.blue,
        ),
      ),
    );
  }
}