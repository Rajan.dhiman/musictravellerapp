import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';

class BookingState extends InheritedWidget {
  final Function addBookingHour;
  final Function removeBookingHour;
  // final List<BookingHour> bookingHours;
  final Booking booking;

  BookingState({
    Key key,
    Widget child,
    this.booking,
    // this.bookingHours,
    this.addBookingHour,
    this.removeBookingHour,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(BookingState oldWidget) {
    return true;
  }

  static BookingState of(BuildContext context){
    return context.inheritFromWidgetOfExactType(BookingState);
  }
}
