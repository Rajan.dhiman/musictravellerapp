import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:intl/intl.dart';
import 'package:mt_ui/blocs/spaces_bloc.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/booking_flow/make_booking_page/widgets/booking_state_provider.dart';
import 'package:mt_ui/pages/booking_flow/make_booking_page/widgets/bottom_bar.dart';
import 'package:mt_ui/pages/booking_flow/make_booking_page/widgets/date_page.dart';
import 'package:mt_ui/providers/spaces_provider.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';

import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mt_ui/widgets/my_card.dart';

/*make_booking_page
Maybe use this for dates instead: https://pub.dartlang.org/packages/flutter_calendar
 */

class CheckBookingAvailabilityPage extends StatefulWidget {
  final Room space;
  final Future<RoomBookingHour> bookingHours;

  const CheckBookingAvailabilityPage({Key key, @required this.space, this.bookingHours}) : super(key: key);

  @override
  CheckBookingAvailabilityPageState createState() => new CheckBookingAvailabilityPageState();
}

class CheckBookingAvailabilityPageState extends State<CheckBookingAvailabilityPage> with TickerProviderStateMixin{
  List<BookingHour> selectedBookingHours = List<BookingHour>();
  List<DatePage> _pagesList;
  // List<DateTime> _dates;
  List<DateTab> _tabsList;
  TabController _tabController;
  String appBarTitle = "";
  String userToken;
  DateTime dateFilter;
  static DateTime today = DateTime.now();
  static DateFormat formatter = new DateFormat('yyyy-MM-dd');
  String todayFormatted = formatter.format(today);
  static int amountOfDays = 7;
  final _key = new GlobalKey<ScaffoldState>();
  int selectedDate;
  TabBar tabbar ;

  List<BookingDate> _bookingDates;
  Future<RoomBookingHour> bookingHours;

  void addBookingHour(BookingHour bookingHour){
    setState(() {
      if(selectedBookingHours.length >0 && selectedDate != bookingHour.dateTime.day){
        //  selectedBookingHours.clear();
        //  selectedBookingHours.add(bookingHour);
        //  selectedDate = bookingHour.dateTime.day;
           _key.currentState.hideCurrentSnackBar();
           _key.currentState.showSnackBar(SnackBar(content: Text("You can book space for single day only !!")));
          return ;
      }
      else{
         selectedBookingHours.add(bookingHour);
         selectedDate = bookingHour.dateTime.day;
      }
    });
  }

  void removeBookingHour(BookingHour bookingHour){
    setState(() {
      selectedBookingHours.removeWhere((b) => b.dateTime == bookingHour.dateTime);
    });
  }

  void updateAppBarTitle(){
    setState(() {
      appBarTitle = DateFormat("MMMM").format(_tabsList[_tabController.index].date.date);
    });
  }

  @override
  void initState() {
    super.initState();
   
      var dat = spaceBLoc.dateFilter ?? today;
      dateFilter = dat;
     todayFormatted = formatter.format(dat);
    _bookingDates = initBookingDates([], []);

    _pagesList = getDatePages(context, _bookingDates);
    _tabsList = getDatesTabs(context, _bookingDates);

    _tabController = TabController(vsync: this, length: _tabsList.length)
      ..addListener(() {
        setState(() {
          updateAppBarTitle();
        });
      });
    initTimeSlots(widget.space.roomId, todayFormatted, amountOfDays);
    updateAppBarTitle();

    

    SharedPreferencesHelper.getAccessToken().then((String res){
      setState(() {
        userToken = res;
      });
    });
  }
   void initTimeSlots(String roomId, String todayFormatted, int amountOfDays, {bool upateUi = false}){
     if(upateUi){
       setState(() {
           selectedBookingHours.clear();  
          _bookingDates = initBookingDates([], []);
          _tabController = TabController(vsync: this, length: 0);
           _pagesList = getDatePages(context, _bookingDates);
         _tabsList = getDatesTabs(context, _bookingDates);
          _tabController = TabController(vsync: this, length: _tabsList.length)
              ..addListener(() {
                setState(() {
                  updateAppBarTitle();
                });
              });
         
       });
     }

     fetchHours(roomId, todayFormatted, amountOfDays).then((res) {
       if(upateUi){
         setState(() {
            _bookingDates = initBookingDates(res[0], res[1], startFromDate: dateFilter);
            _pagesList = getDatePages(context, _bookingDates);
            _tabsList = getDatesTabs(context, _bookingDates);

             _tabController = TabController(vsync: this, length: _tabsList.length)
              ..addListener(() {
                setState(() {
                  updateAppBarTitle();
                });
              });
             _tabController.animateTo(0);
            updateAppBarTitle();
                print('Tabbar updated');
            tabbar = setTabBar(false); 
         });
       }
       else{
          _bookingDates = initBookingDates(res[0], res[1]);
          _pagesList = getDatePages(context, _bookingDates);
          _tabsList = getDatesTabs(context, _bookingDates);

          _tabController = TabController(vsync: this, length: _tabsList.length)
            ..addListener(() {
              setState(() {
                updateAppBarTitle();
              });
            });
          tabbar = setTabBar(false);
          updateAppBarTitle();
       }
       
    });
   }
  void skiptoData(int index, bool fetchData, {bool fromCalender = false}){
    
     if(fetchData){
       if(selectedBookingHours.length > 0){
         showSnack((){
            selectedBookingHours.clear();
             initTimeSlots(widget.space.roomId, formatter.format(dateFilter), amountOfDays,upateUi: true);
         });
       }else{
         initTimeSlots(widget.space.roomId, formatter.format(dateFilter), amountOfDays,upateUi: true);
       }
       return;
     }
     else if(index == null && !fetchData){
       initTimeSlots(widget.space.roomId, formatter.format(dateFilter), amountOfDays,upateUi: true);
       return;
     }
    if(selectedBookingHours.length >0 ){
      if(!fromCalender){
        _tabController.animateTo(_tabController.previousIndex);
      }
           
            showSnack((){
               selectedBookingHours.clear();
               _tabController.animateTo(index);
            });
          return ;
    }
    else{
        _tabController.animateTo(index);
     }
   
  }
  void showSnack(Function onPreseed){
    _key.currentState.hideCurrentSnackBar();
    _key.currentState.showSnackBar(
             SnackBar(
               elevation: 5,
               content: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[
                   Expanded(child: Text("You can book a space for a single day only. By choosing a different date your current selection will expire"),),
                   InkWell(
                      onTap: (){
                       onPreseed();
                     },
                     child: Container(
                       width: 60,
                       height: 40,
                       alignment: Alignment.center,
                       decoration: BoxDecoration(
                         color: Colors.white,
                         borderRadius: BorderRadius.all(Radius.circular(25))
                       ),
                       child:Text("OK", style: TextStyle(color:Colors.black),)
                     ),
                   ),
                  
                 ],
               ),
             ));
  }
  Widget setTabBar(bool refresh){
   return  TabBar(
              controller: _tabController,
              isScrollable: true,
              tabs: _tabsList ?? 0,
              // onTap: (index){
              //   skiptoData(index, false);
                
              //   print(index);
              // },
          );
  }
  List<BookingDate> initBookingDates(List res, List resBooked, {DateTime startFromDate}){

    // Get a list of dates
    List<DateTime> dates = _getDates(res, amountOfDays, startFromDate:  dateFilter);
    List<BookingDate> bookingDates = List();

    dates.forEach((date){
      List<BookingHour> hours = _getDummyBookingsHours(date, res, resBooked);
      bookingDates.add(BookingDate(date, hours));
    });

    return bookingDates;
  }

  @override
  void dispose() {
    super.dispose();
    _tabController?.dispose();
  }

  @override
  Widget build(BuildContext context) {

  
    return BookingState(
      addBookingHour: addBookingHour,
      removeBookingHour: removeBookingHour,
      booking: Booking(
          bookingHours: selectedBookingHours,
          space: widget.space,
          partySize: 1
      ),
      child: Scaffold(
        key: _key,
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Text(appBarTitle, style: MtTextStyle.appBarTitle),
            IconButton(
              padding: EdgeInsets.only(top:5,right: 20,left:10),
              onPressed:_openCalender,
              icon: Icon(Icons.keyboard_arrow_down),
            )
          ],),
          centerTitle: true,
          // days
          bottom: tabbar
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          //hours + bottom button
          children: <Widget>[
            // hours
            SizedBox(height: 10,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text('AVAILABLE TIME SLOTS',style:TextStyle(color: Colors.black45)),
            ),
           
            Expanded(
              child: _pagesList == null || _pagesList.length == 0 ? Container() :
              TabBarView(
                  // physics: NeverScrollableScrollPhysics(),
                  controller: _tabController,
                  children: _pagesList,
              ),
            ),
            BottomBar(numberOfHoursSelected: selectedBookingHours?.length ?? 0, roomId: widget.space.roomId, userToken: userToken, maxGuest: widget.space.capacity,),
          ],
        ),
      ),
    );
  }
  void _openCalender(){
    print('open calender');
    showDialog(
                 context: context,
                 builder: (context)=> _calender()
               );
}
 
 Widget _calender(){
    //   var height = MediaQuery.of(context).size.height;
    // print('Height:- $height');
    // if(height > 800){
    //   height  = height * .62;
    // }
    // else{
    //   height  = height * .69;
    // }
    return Container(
      height:500,
      alignment: Alignment.center,
       child: Container(
       width: MediaQuery.of(context).size.width * .95,
       height: 500,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15),)
        ),
          child:Container(
            child: Stack(
              children: <Widget>[
                 Column(
                    children: <Widget>[
                    SizedBox(height: 30),
                       Material(
                          color: Colors.transparent,
                          child:  CalendarCarousel<Event>(
                           onDayPressed: (DateTime date, List<Event> events) {
                            setState(() {
                               dateFilter = date;
                            });
                           },
                           onDayLongPressed: (DateTime date, ) {
                            setState(() {
                               dateFilter = date;
                            });
                           },
                           weekendTextStyle: TextStyle(
                             color: Colors.black,
                           ),
                           weekdayTextStyle: TextStyle(
                             color: Colors.grey,
                             fontWeight: FontWeight.bold
                           ),
                           headerTextStyle:  TextStyle(
                             color: Colors.black,
                             fontSize: 20,
                             fontWeight: FontWeight.bold
                           ),
                           thisMonthDayBorderColor: Colors.transparent,
                             customDayBuilder: (
                             bool isSelectable,
                             int index,
                             bool isSelectedDay,
                             bool isToday,
                             bool isPrevMonthDay,
                             TextStyle textStyle,
                             bool isNextMonthDay,
                             bool isThisMonthDay,
                             DateTime day,
                           ) {
                             if(isToday && dateFilter != day){
                                return Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.black)
                                  ),
                                  child: Center(
                                    child: Text(day.day.toString()),
                                  ),
                               );
                             }
                             else if(dateFilter != null && dateFilter == day){
                              
                                return Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    // border: Border.all(color: Colors.black),
                                    color: MtColors.blue 
                                  ),
                                  child: Center(
                                    child: Text(day.day.toString(),style: TextStyle(color: Colors.white ),),
                                  ),
                               );
                             }
                             else{
                               Color disable  = Colors.grey;
                              //  if( _bookingDates.any((x)=> x.date.month  == day.month && x.date.day == day.day)){
                              //     disable =Colors.black;
                              //  }
                               if(day.isAfter(DateTime.now().add(Duration(days:90))))
                               return  Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    // border: Border.all(color: Colors.black),
                                    color:  Colors.white,
                                  ),
                                  child: Center(
                                    child: Text(day.day.toString(),style: TextStyle(color:disable),),
                                  ),
                               );
                             }
                           },
                          selectedDayBorderColor:  Colors.transparent,
                          selectedDayButtonColor:  Colors.transparent,
                          todayButtonColor: Colors.transparent,
                          todayBorderColor: Colors.transparent,
                           weekFormat: false,
                           height: 420.0,
                           minSelectedDate: DateTime.now().add(Duration(days: -1)),//_bookingDates.first.date.add(Duration(days: -1)),
                           maxSelectedDate:  DateTime.now().add(Duration(days: 90)),// _bookingDates.last.date,
                           selectedDateTime: _bookingDates.first.date,
                           daysHaveCircularBorder: false, /// null for not rendering any border, true for circular border, false for rectangular border
                         ),
                      ),
                    ],
                  ),
                  Align(
                        alignment: Alignment.topCenter,
                        child: Material(
                          color: Colors.white,
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            child:Text('SELECT DATE',style: TextStyle(color: Colors.black54,fontSize: 18,   ),),
                          )
                        )
                  ),
                 Align(
                   alignment: Alignment.topRight,
                   child: Material(
                     color: Colors.transparent,
                     borderRadius: BorderRadius.circular(30),
                     child: InkWell(
                       onTap: (){
                         Navigator.pop(context);
                       },
                       child: Container(
                         margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                         decoration: BoxDecoration(
                           shape: BoxShape.circle,
                           border: Border.all(color: Colors.black,width: 2)
                         ),
                         child: CircleAvatar(
                           radius: 12,
                           backgroundColor: Colors.transparent,
                           child: Icon(Icons.close,color: Colors.black,),
                         ),
                       )
                     ),
                   )
                 ),
                 Positioned(
                   bottom: 20,
                   left: MediaQuery.of(context).size.width * .2,
                   child:   Container(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            MyCard(
                              padding: EdgeInsets.only(left: 20),
                              borderRadiusValue: 40,
                              child: Row(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      MaterialButton(
                                        // color:MtColors.blue,
                                        padding: EdgeInsets.only(bottom: 5),
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                                        onPressed: (){
                                          setState(() {
                                              dateFilter = DateTime.now();
                                              Navigator.pop(context);
                                            });
                                             
                                        },
                                        child:Center(child:Text('TODAY',style: Theme.of(context).textTheme.headline.copyWith(fontSize: 20,color: MtColors.blue),textAlign:TextAlign.center,),)
                                      ),
                                     GestureDetector(
                                       onTap: (){
                                          if(dateFilter != null){
                                               Navigator.pop(context);
                                               if(_bookingDates.any((x)=> x.date.month == dateFilter.month && x.date.day == dateFilter.day)){
                                                   var dat = _bookingDates.firstWhere((x)=>  x.date.month == dateFilter.month && x.date.day == dateFilter.day);
                                                   var index = _bookingDates.indexOf(dat);
                                                   _tabController.animateTo(index);
                                                  //  skiptoData(index,false, fromCalender: true);
                                               }
                                               else{
                                                //  skiptoData(null,true);
                                                 initTimeSlots(widget.space.roomId, formatter.format(dateFilter), amountOfDays,upateUi: true);
                                                 return;
                                               }
                                               
                                             }
                                          // setState(() {
                                              
                                          //    if(dateFilter != null){
                                          //      Navigator.pop(context);
                                          //      if(_bookingDates.any((x)=> x.date.month == dateFilter.month && x.date.day == dateFilter.day)){
                                          //          var dat = _bookingDates.firstWhere((x)=>  x.date.month == dateFilter.month && x.date.day == dateFilter.day);
                                          //          var index = _bookingDates.indexOf(dat);
                                          //          _tabController.animateTo(index);
                                          //      }
                                          //      else{
                                          //        initTimeSlots(widget.space.roomId, formatter.format(dateFilter), amountOfDays,upateUi: true);
                                          //       //   _key.currentState.hideCurrentSnackBar();
                                          //       //  _key.currentState.showSnackBar(SnackBar(content: Text("Booking is not available on this date !!")));
                                          //        return;
                                          //      }
                                               
                                          //    }
                                            
                                          //   });
                                        },
                                       child: Container(
                                        padding: EdgeInsets.symmetric(vertical: 12,horizontal: 20),
                                        decoration: BoxDecoration(
                                          color: MtColors.blue,
                                          borderRadius: BorderRadius.circular(40)
                                        ),
                                        child: Center(child:Text('APPLY',style: Theme.of(context).textTheme.headline.copyWith(fontSize: 20,color:Colors.white),textAlign:TextAlign.center,),)
                                      ,)
                                     )
                                   ],
                              ),
                            ),
                           ],
                        ),
                      )
                  ,
                 )
              ],
            ),
          )
         ),
    );
  }
 
  List<DateTab> getDatesTabs(context, List<BookingDate> dates) {
    _tabsList = List<DateTab>();

    // Loop through dates
    for (BookingDate date in dates) {
      // Construct tab widget and add to list
      _tabsList.add((DateTab(date: date)));
    }

    return _tabsList;
  }

  List<DatePage> getDatePages(context, List<BookingDate> dates) {
    _pagesList = List<DatePage>();
   
    // Loop through dates
    for (BookingDate date in dates) {
      // Construct tab widget and add to list
      _pagesList.add((DatePage(
        bookingHours: date.hours,
        key: PageStorageKey(date.date.toString()), // use date as page key, to maintain scroll position when swiping across pages
      )));
    }

    return _pagesList;
  }

  /// Create a list of dates to show
  List<DateTime> _getDates(List res, int amountOfDays, {DateTime startFromDate}) {
    DateTime date = startFromDate ?? DateTime.now();
    List<DateTime> datesList = List<DateTime>();
    List<int> availableDays = [];
//    int weekdayNow = date.weekday - 1;

    // How far in the future can you book
    int numberOfDatesBookable = amountOfDays; // days
    // int numberOfDatesBookable = 1; // days

    if (res.length > 0) {
      res.forEach((el) {
        if (!availableDays.contains(el.day)) {
          availableDays.add(el.day);
        }
      });
    }

    if (availableDays.length > 0) {
      for (int i = 0; i < numberOfDatesBookable; i++) {
        // add date to the list
        // if (availableDays.contains(date.weekday)) {
          datesList.add(date);

          // go to next day
          date = date.add(new Duration(days: 1));
        // }
      }
    } else {
      datesList.add(date);
    }

    // availableDays.forEach((day) {});

    return datesList;
  }

  // TODO get space calendar/available hours from API
  /// testing: this will create a list of available hours on weekdays
  List<BookingHour> _getDummyBookingsHours(DateTime date, List res, List resBooked) {
    List<BookingHour> hours = List<BookingHour>();
    // DateTime day = DateTime(date.year, date.month, date.day, 0, 0, 0, 0);

    // if(date.weekday == 6 || date.weekday == 7){
    //   return hours;
    // }

    // for (int i = 0; i < 24; i++) {

    //   double minPrice = widget.space.priceDefault;
    //   BookingHour bookingHour = BookingHour(
    //     dateTime: hour,
    //     isAvailable: (i > 7 && i < 22 && i != 15 && i != 16),
    //     price: (i < 14) ? minPrice : minPrice * 2,
    //   );

    //   // add hour to list
    //   if(bookingHour.isAvailable) {
    //     hours.add(bookingHour);
    //   }

    //   // go to next hour
    //   hour = hour.add(Duration(hours: 1));
    // }

    var bookings = {};

    if (resBooked.length > 0) {
      resBooked.forEach((entry){
        bookings[entry.date] = entry.reservedHours;
      });
    }

    if (res.length > 0) {
      res.forEach((hour) {
        var formatter = new DateFormat('yyyy-MM-dd');
        String formatted = formatter.format(date);
          if (hour.day == date.weekday - 1) {
            if (!(bookings.containsKey(formatted) && bookings[formatted].contains(hour.hour))) {
              BookingHour bookingHour = BookingHour(
                // dateTime: DateTime(hour.hour),
                dateTime: DateTime(date.year, date.month, date.day, hour.hour, 0, 0, 0),
                price: hour.price,
              );
              hours.add(bookingHour);
            }
          }
      });
    } else {
      return hours;
    }
    return hours;
  }
}

class DateTab extends StatelessWidget{
  final BookingDate date;

  const DateTab({Key key, this.date}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String getDay(DateTime date) =>
        new DateFormat("EEEE").format(date).substring(0, 3);
    String getDate(DateTime date) => new DateFormat("d").format(date);

    // Check if booking hours on this day is selected
    bool selected = false;
    List<BookingHour> selectedHours = BookingState.of(context).booking.bookingHours;
    for (BookingHour hour in selectedHours) {
      if(DateFormat("yMMMMd").format(hour.dateTime) == DateFormat("yMMMMd").format(date.date)){
        selected = true;
      }
    }

    // Check if this date doesn't have any BookingHours
    bool noBookingHours = false;
    if(date.hours.length == 0){
      noBookingHours = true;
    }

    TextStyle style = TextStyle(
        fontSize: 15,
        fontWeight: selected ? FontWeight.bold : FontWeight.normal,
        color: selected ? MtColors.blue : Colors.black54
    );

    // If no booking hours, overwrite style
    if(noBookingHours){
      style = TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.normal,
          color: Colors.black12
      );
    }

    return Container(
      padding: EdgeInsets.only(bottom: 8),
      child: Column(
        children: <Widget>[
          Text(
            getDay(date.date),
            style: style,
          ),
          SizedBox(height: 4),
          Text(
            getDate(date.date),
            style: style.copyWith(fontSize: 16),
          ),
        ],
      ),
    );
  }
}

class BookingDate{
  final DateTime date;
  final List<BookingHour> hours;
  BookingDate(this.date, this.hours);
}

Future<List> fetchHours(String id, String todayFormatted, int amountOfDays) async {

  var query = '''query{
    # room(id: $id) {
    roomSchedule (roomId: $id, dateFrom: "$todayFormatted", numDays: $amountOfDays) {
      staticSchedule{
        price
        priceCurrency
        day
        hour
      }
      unavailableSlots {
        day
        date
        reservedHours
      }
    }
  }''';

  log('query: $query');

  var currencyPrice = await SharedPreferencesHelper.getCurrencyRate();
  var currencyPref = await SharedPreferencesHelper.getCurrencyPreference();
  
  Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});
  final response = await http.get(getUrlForQuery(query));
  if (response.statusCode == 200) {
    var hours = [];
    var hoursBooked = [];
    var parsedJson = json.decode(response.body);
    // print(parsedJson);
    var todaySchedule = parsedJson['data']['roomSchedule']['staticSchedule'];
    
    todaySchedule.forEach((time) {
      hours.add(RoomBookingHour.fromAPIJson(time, curencyPrefrence: currencyPref,rate:currencyPrice));
    });
    var todayScheduleBooked = parsedJson['data']['roomSchedule']['unavailableSlots'];
    todayScheduleBooked.forEach((timeBooked) {
      hoursBooked.add(RoomBookingHourBooked.fromAPIJson(timeBooked));
    });
    return [hours, hoursBooked];
  } else {
    throw Exception('Failed to load');
  }
}
