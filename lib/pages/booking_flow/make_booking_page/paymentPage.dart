import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/home_page.dart';
import 'package:mt_ui/pages/search_page/search_page.dart';
import 'package:mt_ui/res/images.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/my_card.dart';

class PaymentPage extends StatefulWidget {
  final Booking booking;
  final String currency;
  final double price;
  final int partySize ;
  final String roomId;
  PaymentPage({Key key, this.booking,this.price,  this.partySize, this.currency, this.roomId,}) : super(key: key);

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  bool isCreditSelected = true;
  bool isSofortSelected = false;
  bool rememberMe = false;
  String currencyPref = 'EUR';
  TextEditingController cardNumber;
  TextEditingController month;
  TextEditingController year;
  TextEditingController cvc;

  final _key = new GlobalKey<ScaffoldState>();
  final Client _client = Client();
  Booking booking;
  String stripePk;
  String currency;
  int bookingId;
  double price;
  int partySize = 1;
  int mm,yy;
  bool loading = false;
  String invoiceId;
  

  @override
  void initState() { 
    booking = widget.booking;
    currency = widget.currency;
    price = widget.price;
    partySize = widget.partySize ?? 1;
    cardNumber = TextEditingController();
    month = TextEditingController();
    year = TextEditingController();
    cvc = TextEditingController();
    super.initState();
  }
  Widget _body(){
   return Container(
     padding: EdgeInsets.symmetric(horizontal:10),
       child: Column(
         crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
           _rowWidget('Room Fee',widget.booking.getTotalSpaceFeeDisplay()),
           _rowWidget('Insurance Fee*',widget.booking.getInsuranceFeeDisplay()),
          Divider(thickness: 1,height: 25,),
           _rowWidget('TOTAL',widget.booking.getTotalPriceDisplayTwoDecimals()),
            Padding(
              padding: EdgeInsets.only(top:10,bottom: 30),
              child: Text('Pay via Credit Card',style: TextStyle(fontWeight: FontWeight.w500),),
            ),
           _selectPaymentMethod(),
           Divider(thickness: 1,height: 30,),
           _paymentEntries(),
          //  Padding(
          //    padding: EdgeInsets.only(top: 20,bottom: 10),
          //    child: Row(
          //       children: <Widget>[
          //         Expanded(
          //           flex: 1,
          //           child: _checkBox(rememberMe,
          //            size: 25,
          //            onPressed: (){
          //              setState(() {
          //                 rememberMe = !rememberMe;
          //              });
          //            }),
          //         ),
          //         SizedBox(width: 10,),
          //         Expanded(
          //           flex: 8,
          //           child: Text('Remember me',style:TextStyle(color: Colors.black54)),
          //         ),
          //       ],
          //     ),
          //  ),
           Divider(thickness: 1,height: 25,),
            _submitButton()
        ],
       ),
   );
  }
  Widget _rowWidget(String title,String trailing){
    return Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child:  Text(title,style:TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),
                  ),
                 Padding(
                    padding: EdgeInsets.only(left: 10),
                    child:  Text(trailing,style: TextStyle(fontSize:20,fontWeight: FontWeight.w600,color: Colors.black)),
                  ),
              ],
              
           );
  }
 
  Widget _selectPaymentMethod(){
    var width = MediaQuery.of(context).size.width - 50;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
             _paymentGenericContainer(
                onPressed: (){
                //  setState(() {
                //    isCreditSelected = true;
                //    isSofortSelected = false;
                //  });
               },
              borderColor:  Color.fromRGBO(98, 107, 128, 1),
              child:AspectRatio(
                aspectRatio: 16/7,
                child:  Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Image.asset(MtImages.visa_card, width:width*.15,),
                      Image.asset(MtImages.master_card,width:width*.15),
                      Image.asset(MtImages.express_card,width:width*.15),
                    ],
                  ),
                  SizedBox(height: 10,),
                  Text('CREDIT CARD',style: TextStyle(fontWeight: FontWeight.bold,color:Color.fromRGBO(98,107,128,1),))
                ],
              ),
              ),
            ),
            SizedBox(width: 10,),
            // _paymentGenericContainer(
            //   onPressed: (){
            //      setState(() {
            //        isCreditSelected = false;
            //        isSofortSelected = true;
            //      });
            //    },
            //   borderColor:  Color.fromRGBO(244, 114, 21, 1),
            //   child:AspectRatio(
            //     aspectRatio: 16/7,
            //     child:  Image.asset(MtImages.sofort_card,fit:BoxFit.contain),
            //   ),
            // )
        ],),
        // Padding(
        //   padding: EdgeInsets.only(top: 15),
        //   child:  Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //   children: <Widget>[
        //       _checkBox(isCreditSelected,),
        //       _checkBox(isSofortSelected,),
        //   ],
        // ),
        // )
      ],
    );
  }
 
  Widget _paymentGenericContainer({Widget child,Color borderColor,Function onPressed}){
     var width = MediaQuery.of(context).size.width - 50;
    return InkWell(
      onTap: onPressed,
      borderRadius:  BorderRadius.all(Radius.circular(5)),
      child:  Container(
              width:width*.5,
              padding: EdgeInsets.symmetric(vertical: 20,horizontal: 5),
              decoration: BoxDecoration(
              border: Border.all(color:borderColor,width: 1),
              borderRadius: BorderRadius.all(Radius.circular(5))
              ),
              child: AspectRatio(
                aspectRatio: 16/7,
                child:child
              )
            ),
    );
  }
 
  Widget _checkBox(bool isChecked,{Function onPressed, double size = 29}){
    return  InkWell(
      onTap: (){
        if(onPressed != null){
          onPressed();
        }
      },
      child: Container(
        alignment: Alignment.center,
        child: Container(
          child: isChecked ? FittedBox(
            alignment:Alignment.center,
            fit: BoxFit.contain,
            child:  Icon(Icons.check_circle,color:MtColors.blue,size: size ),
          ) : Container(
           height: size ,
           width: size - 5,
           decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color:isChecked ? Colors.transparent : Color.fromRGBO(112,112,112,1),width: 2 )
          ),
        ),
        )
      ),
    );
  }
  
  Widget _paymentEntries(){
    return Column(
      children: <Widget>[
        _entry('Card number',icon: Icons.credit_card,controller: cardNumber),
        SizedBox(height: 10,),
         Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
           SizedBox(
             width: (MediaQuery.of(context).size.width-20) *.45,
             child:  _entry('MM/YY',icon: Icons.calendar_today,keyboardType: TextInputType.number,controller: month,isExpiredate: true),
           ),
          SizedBox(
             width: (MediaQuery.of(context).size.width-20) *.45,
             child:  _entry('CVC',icon: Icons.lock,keyboardType: TextInputType.number,controller: cvc,isTextHidden:true),
           ),
          ],
        ),
      ],
    );
  }
  
  Widget _entry(String hint,{TextEditingController controller, IconData icon,TextInputType keyboardType = TextInputType.text, bool isExpiredate = false, bool isTextHidden = false}){
    return MyCard(
      shadow: Shadow.none,
      padding: EdgeInsets.only(left: 10),
      boxBorder: Border.all(color: Colors.black38),
      borderRadiusValue: 40,
       child: TextField(
         controller: controller,
         keyboardType: keyboardType,
         obscureText: isTextHidden,
         onChanged: isExpiredate ? _onExpireChange : (val){},
         decoration: InputDecoration(
           hintText: hint,
           border: InputBorder.none,
           prefixIcon: Icon(icon,color: Colors.grey,)
         ),
         cursorWidth: isExpiredate ? 0.0 : 1,
       ),
    );
  }
 
  Widget _submitButton2(){
    return  Container(
         width: MediaQuery.of(context).size.width,
         padding: EdgeInsets.symmetric(vertical: 10),
         alignment: Alignment.center,
          child: MyCard(
            onPressed: onSubmitButtonClick,
           backgroundColor: MtColors.blue,
           padding: EdgeInsets.symmetric(horizontal: 30,vertical: 15),
           borderRadiusValue: 25,
            child: Text('COMPLETE PAYMENT',style: TextStyle(color: Colors.white),),
         )
    );
  }
  Widget _submitButton() {
    return Container(
      alignment: Alignment.center,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 350),
        curve: Curves.easeInOut,
        // height: loading ? 48 : 48,
        width: MediaQuery.of(context).size.width * .7,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        child: RaisedButton(
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                decoration: new BoxDecoration(
                    color: Colors.green,
                    borderRadius: new BorderRadius.all(const Radius.circular(25.0))),
               
                constraints: BoxConstraints(maxWidth: 100),
                child: loading
                    ? ClipRRect( 
                        borderRadius: BorderRadius.circular(25),
                        child: AnimatedOpacity(
                            duration: Duration(milliseconds: 350),
                            opacity: loading ? 1 : 0,
                            child: LinearProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(Colors.white),
                            )),
                      )
                    : null,
              ),
              AnimatedOpacity(
                duration: Duration(milliseconds: 350),
                opacity: loading ? 0 : 1,
                child: Center(
                  child: Text("COMPLETE PAYMENT",style: MtTextStyle.button),
                ),
              ),
            ],
          ),
          padding: EdgeInsets.symmetric(vertical: 16),
          textColor: Colors.white,
          color: MtColors.blue,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
          onPressed: () async {
            // don't allow click if already loading
            if (loading) return;
            // Validate will return true if the form is valid
              setState(() {
                loading = true;
                
              });
              // hide keyboard
              SystemChannels.textInput.invokeMethod('TextInput.hide');
              // remove focus from text fields
              FocusScope.of(context).requestFocus(new FocusNode());
              onSubmitButtonClick();
          },
        ),
      ),
    );
    
  }
  void onSubmitButtonClick()async{
    if(!validation()){
      setState(() {
        loading = false;
      });
        return ;
    }
     
    
    // if(dob.text == null || dob.text.isEmpty){
    //   dob.text = DateTime.now().toUtc().toString();
    // }
    var currencyPrice = await SharedPreferencesHelper.getCurrencyRate();
     currencyPref = await SharedPreferencesHelper.getCurrencyPreference();
  try{
      _continueToPaymentMutation(booking, widget.roomId).then((res) {

        print(res);
       if(res['data']['multipleReservations'] == null){
         //  key.currentState.showSnackBar(SnackBar(content: Text(res["res"]["error"])));
          return;
       }
       print(res['data']['multipleReservations'] );
       // get error here if user is not authed
       var apiPrice = res['data']['multipleReservations']['price'];
      //  var apiCurrency = res['data']['multipleReservations']['priceCurrency'];
       if(currencyPrice != null && currencyPref != null){
         double mod = pow(10.0, 2); 
         apiPrice =  ((apiPrice * currencyPrice * mod).round().toDouble() / mod); 
        //  apiCurrency = currencyPref;
       }
       /// [Currencyconversion]
     
           price = apiPrice;
          //  currency = apiCurrency;//
          bookingId = res['data']['multipleReservations']['id'];
          invoiceId = res['data']['multipleReservations']['invoiceId'].toString();
           
       }).then((value)async{
         await _makePayment();
       });
  }
  catch(error){
    print("Error:- $error");
      setState(() {
        loading = false;
      });
    }
  }
  Future<void> _makePayment()async{
    String res;
    //  var currency = widget.currency;
    var amount = widget.booking.getTotalSpaceFee() + widget.booking.getInsuranceFee();
    var query = '''mutation {
      payment (
        amount:            "$amount", 
        cardNumber:        "${cardNumber.text}", 
        currency:          "$currencyPref", 
        cvv:               "${cvc.text}",
        expiryDate:        $mm,
        expiryYear:        $yy,
        partySize:         $partySize, 
        
        invoiceId:         $invoiceId
        ) {
        success
        error
      }
      
    }''';
   
    print('query: $query');
    Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};

   try{
         await _client
          .post(getUrlForQuery(query), headers: headers)
          .then((result) => result.body)
          .then(json.decode)
          .then((json) {
            print(json);
            // if(json['errors'] != null){
            //   var txt = json['errors']['message'].toString().split(' ')[];
            //    _key.currentState.showSnackBar(SnackBar(content: Text(txt)));
            // }
            if (json['data']['payment']['success'] == false) {
              res = 'error';
            } else {
              res = 'success';
            }
            return res;
          }).then((res) {
            print(res);
            var txt;
            switch(res) {
              case 'error': txt = 'Payment failed'; 
              break;
              case 'success': txt = 'Payment success'; 
              break;
            }
            _key.currentState.showSnackBar(SnackBar(content: Text(txt)));
            if(res == 'success'){
              print('payment is success');
              Timer(Duration(seconds: 1), () {
                print('Redirect to home page');
                 Navigator.pushAndRemoveUntil(
                   context,
                    MaterialPageRoute(builder: (context) => HomePage(isFromPaymentPage: true,)),
                     (x) => false
                 );
              });
            }
          }).catchError((err){
             print('Error[inner] in payment api:- $err');
              _key.currentState.showSnackBar(SnackBar(content: Text('Payment failed!!')));
          });
      }
      catch (error){
        print('Error in payment api- $error');
         _key.currentState.showSnackBar(SnackBar(content: Text('Payment failed!!')));
      }
       setState(() {
        loading = false;
      });
  }
  Future<Map<String, dynamic>> _continueToPaymentMutation(Booking booking, String roomId) async {

    var formatter = new DateFormat('yyyy-MM-dd');
    var dt = booking.bookingHours[0].dateTime;
    var hour =  Iterable.generate(booking.bookingHours.length, (x) => booking.bookingHours[x].dateTime.hour).toList();// DateTime.parse(dt.toString());
    String formatted = formatter.format(booking.bookingHours[0].dateTime);

    final Client _client = Client();

    Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});

    var userAgent = "";

    if (Platform.isIOS) {
        userAgent = "ios";
      }
      if (Platform.isAndroid) {
        userAgent = "android";
      }

   String query = '''mutation{
      multipleReservations(
        date: "$formatted",
      	hours: $hour, 
        region: "US",
        roomId: $roomId,
        partySize: $partySize,
        bookingPlatform : "$userAgent"
      ){
      	invoiceId
        roomId
        userId
        reservations
        price
        priceCurrency
        stripePk
        roomPrice
    	}
    }''';

    var jsonOut = {};
    print(query);
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};
    await _client
    .post(getUrlForQuery(query), headers: headers)
    .then((result) => result.body)
    .then(json.decode)
    .then((json){
      jsonOut = json;
      print(jsonOut);
    });
    return jsonOut;
  }
  void _onExpireChange(String value){
        setState(() {
          value = value.replaceAll(RegExp(r"\D"), "");
          switch (value.length) {
            case 0:
              month.text = "MM/YY";
              month.selection = TextSelection.collapsed(offset: 0);
              break;
            case 1:
              month.text = "${value}M/YY";
              month.selection = TextSelection.collapsed(offset: 1);
              break;
            case 2:
              month.text = "$value/YY";
              month.selection = TextSelection.collapsed(offset: 2);
              break;
            case 3:
              month.text =
                  "${value.substring(0, 2)}/${value.substring(2)}Y";
              month.selection = TextSelection.collapsed(offset: 4);
              break;
            case 4:
              month.text =
                  "${value.substring(0, 2)}/${value.substring(2, 4)}";
              month.selection = TextSelection.collapsed(offset: 5);
              mm = int.parse(month.text.split('/')[0]);
              yy = int.parse(month.text.split('/')[1]);
              break;
          }
          if (value.length > 4) {
            month.text =
                "${value.substring(0, 2)}/${value.substring(2, 4)}";
            month.selection = TextSelection.collapsed(offset: 5);
          }
        });
  }
  
  bool validation(){
    if(cardNumber.text == null || cardNumber.text.isEmpty ){
      showSnack('Card number is required');
      return false;
    }
    if(month.text == null || month.text.isEmpty){
      showSnack('Year and month is required');
      return false;
    }
    if(month.text.length < 5){
      showSnack('Year and month is required');
      return false;
    }
    if(cvc.text == null || cvc.text.isEmpty){
      showSnack('CVC is required');
      return false;
    }
    if(cvc.text.length > 3 || cvc.text.length < 3){
      showSnack('CVC must be of 3 digit number');
      return false;
    }
    if(mm == null){
      showSnack('Please enter expiry month');
      return false;
    }
    if(mm > 12 || mm < 1){
      showSnack('Invalid expiry month');
      return false;
    }
    if(yy == null){
      showSnack('Please enter expiry year');
      return false;
    }
    if( int.parse('20$yy') < DateTime.now().year){
      showSnack('Invalid expiry year ${DateTime.now().year}');
      return false;
    }
    return true;
  }
  showSnack(String txt){
     _key.currentState.showSnackBar(SnackBar(content: Text(txt)));
  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _key,
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: true,
                centerTitle: true,
                pinned: false,
                title: Text('Payment'),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                [
                  _body(),
                ]
               ),
              ),
           ],
          ),
        );
  }
}
