import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:intl/intl.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/booking_flow/make_booking_page/paymentPage.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/booking_price_overview.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/progress_overlay.dart';
import 'package:mt_ui/widgets/space_card_room.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '.././booking_failed_page.dart';
import '.././booking_success_page.dart';

class BookingReviewPage extends StatefulWidget {
  final Booking booking;
  final double price;
  final int partySize;
  final String roomId;
  // final Map<String, dynamic> myJson;

  const BookingReviewPage({
    Key key,
    this.booking,
    this.price, 
    this.partySize, this.roomId,
  }) : super(key: key);

  @override
  BookingReviewPageState createState() => new BookingReviewPageState();
}

class BookingReviewPageState extends State<BookingReviewPage> {
  String token;
  String name = '';
  List<String> time = [];
  bool loading = false;
  final Client _client = Client();

  @override
  void initState() {
     _setUserName();
    super.initState();
  }
 _setUserName()async{
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var userDataMap = prefs.getString("userDataMap");
         var data = json.decode(userDataMap);
        setState(() {
           name =data['name'];
           print(userDataMap);
           print('Username: $name');
        });
  }
  @override
  Widget build(BuildContext context) {
    if(name == null){
      _setUserName();
   }
    return Stack(
      children: <Widget>[

        Scaffold(
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: true,
                centerTitle: true,
                pinned: false,
                title: Text('Review booking details'),
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  _body(context),
                ]),
              )
            ],
          ),
        ),

        AnimatedOpacity(
          duration: Duration(milliseconds: 350),
          opacity: loading ? 1 : 0,
          child: loading ? ProgressOverlay() : Container(),
        ),
       Align(
         alignment: Alignment.bottomCenter,
         child:  MyCard(
               padding: EdgeInsets.only(top:10,left: 20,right: 20,bottom: 5),
               child: Container(
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.centerRight,
                height: 50,
                child: Row(
                  children: <Widget>[
                    FlatButton(
                       padding: EdgeInsets.symmetric(horizontal: 0),
                       onPressed: (){},
                       child:  Text('${widget.booking.getTotalPriceDisplayTwoDecimals()} in total',style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold),),
                     ),
                    Expanded(
                     child:Container()
                    ),
                    RaisedButton(
                      onPressed: () => _continueToPayment(),
                      color: MtColors.blue,
                      textColor: Colors.white,
                      elevation: 0,
                      padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 20.0),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25)),
                      child: Text("Pay", style: TextStyle(fontWeight: FontWeight.bold,),),
                    ),
                  ],
                )
              )
            )
          ),
      ],
    );
  }

  Widget _body(context){
     var imagePath = widget.booking.space.images != null && widget.booking.space.images.length >0 ? widget.booking.space.images[0].url : null;
     var imageOrder = widget.booking.space.images != null && widget.booking.space.images.length >0 ? widget.booking.space.images[0].order : null;
    var room = ListingRoom(
     currency:  widget.booking.space.currency,
     image: ListingSpaceImage(url: imagePath, order:  imageOrder),
     instruments: [ListingRoomInstrument(name:  widget.booking.space.instruments[0].model,id :widget.booking.space.instruments[0].id),],
     location: ListingRoomLocation(city:  widget.booking.space.location.city, country:  widget.booking.space.location.country),
     name:  widget.booking.space.name,
     price:  widget.booking.space.price
    );
    return  Container(
         padding: EdgeInsets.only(left:20,right: 20, bottom: 50),
         child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
           Text('Please confirm your booking details.',style:TextStyle(color: Colors.black,fontWeight: FontWeight.w400,),),
           ListTile(
             contentPadding: EdgeInsets.only(bottom: 5),
             title:  Text('NAME',style:TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),
             subtitle: Text(name,style: TextStyle(fontSize:20,fontWeight: FontWeight.w600,color: Colors.black)),
           ),
          Text('SPACE',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),
         
          SizedBox(height: 16),
          // Space section
          SpaceCardRoom(
              space: room,
               istappable: false,
            listViewMode: true,
          ),
      
          SizedBox(height: 10),

          // Show booking hours
          bookingHoursOverview(
            booking: widget.booking,
          ),
          
          Divider(height:10),

          // [DropDownButton] Number of guests
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
           children: <Widget>[
             Text("PARTY SIZE", style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,)),
            SizedBox(height: 10,),
            Text('${widget.partySize} Person'),
           ],
         ),
         Divider(height: 5,),
          // [button] Add coupon code
        //     ListTile(
        //      contentPadding: EdgeInsets.symmetric(vertical: 0),
        //      title:   Text('COUPON CODE',style:TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),
        //      subtitle:Padding(
        //        padding: EdgeInsets.symmetric(vertical: 5),
        //        child: Text('None'),
        //      )
        //    ),
      
        //  Divider(height: 0),

          BookingPriceOverview(
            booking: widget.booking,
          ),

          SizedBox(height: 16),
        ],
      ),
    );
  }

  void _continueToPayment() {

    // setState(() {
    //   loading = true;
    // });
   
    Navigator.of(context).push(MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        // return LoadHTMLFileToWEbView(this.widget.bookingId);
        return PaymentPage(
           booking: widget.booking,
            price: widget.price,
            partySize: widget.partySize,
            roomId:widget.roomId
            );
      }
    ));

  }


  // US_REGION
//  static const String QUERY_MAKE_BOOKINGS = '''mutation{
//    pay(partySize: 0, reservationId: reserId, stripeToken: "stptoken", region: "US"){
//      success
//      error
//    }
//  }''';
  static const String QUERY_MAKE_BOOKINGS = '''mutation{
    pay(partySize: 0, reservationId: reserId, stripeToken: "stptoken", region: "EU"){
      success
      error
    }
  }''';
//  static const String QUERY_MAKE_BOOKINGS = '''mutation{
//    pay(partySize: 0, reservationId: reserId, stripeToken: "stptoken"){
//      success
//      error
//    }
//  }''';

  Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});

  

   Widget bookingHoursOverview({Booking booking}) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: getDateTime(widget.booking),
    );
  }

  Widget getDateTime(Booking booking){
    Map<String, List<BookingHour>> map = BookingHour.groupByDate(booking.bookingHours);
     List<Widget> widgetList = [];
     widgetList.add(Text('DATE',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,)));
        widgetList.add(Text(DateFormat('EEEE, MMMM, dd , yyyy').format(booking.bookingHours.first.dateTime),style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.w400),));
        widgetList.add( SizedBox(height: 10,));
        widgetList.add(Text('TIME(S)',style:TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),);
      booking.bookingHours.forEach((x) {
      var tm = BookingHour.getHour(x.dateTime, context);
      var tm2 = BookingHour.getHour(x.dateTime.add(Duration(hours:1)), context);
        
        
       widgetList.add( 
           Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
                 SizedBox(height: 3,),
                 Container(
                      constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width - 100),
                      child: Text("$tm - $tm2",style:TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.w400, ),),
                    ),
                  SizedBox(height: 3,)
             ],
            
       ));
       
    });
   
    widgetList.add( SizedBox(height: 10,));
        var total =    booking.bookingHours.length > 1 
            ?Text('Total ${booking.bookingHours.length} hours',style:TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),)
              :Text('Total ${booking.bookingHours.length} hour',style:TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),);
        widgetList.add(total);    
  return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widgetList == null ? [Container()] : widgetList
    );
 }
}

class DateRange{
  final DateTime start;
  final DateTime end;

  DateRange(this.start, this.end);
}
