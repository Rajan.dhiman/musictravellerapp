import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/booking_flow/booking_failed_page.dart';
import 'package:mt_ui/pages/booking_flow/booking_success_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/widgets/booking_hours_overview.dart';
import 'package:mt_ui/widgets/booking_price_overview.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/progress_overlay.dart';
import 'package:mt_ui/widgets/space_card.dart';
import 'package:mt_ui/widgets/space_card_room.dart';
import 'package:mt_ui/widgets/space_view_compact.dart';

// import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'load_html.dart';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'dart:io';

import 'make_booking_page/bookingReviewPage.dart';

class BookingConfirmPage extends StatefulWidget {
  final Booking booking;
  final double price;
  final int maxGuest;
  final String roomId;
  
  // final Map<String, dynamic> myJson;

  const BookingConfirmPage({
    Key key,
    this.booking,
    this.price,
    this.maxGuest, this.roomId,
  }) : super(key: key);

  @override
  BookingConfirmPageState createState() => new BookingConfirmPageState();
}

class BookingConfirmPageState extends State<BookingConfirmPage> {
  // final flutterWebviewPlugin = new FlutterWebviewPlugin();

  // StreamSubscription _onDestroy;
  // StreamSubscription<String> _onUrlChanged;
  // StreamSubscription<WebViewStateChanged> _onStateChanged;
  String token;
  String name = '';
  TextEditingController couponController;
  String partySizeString = "1";
  int partySizeint = 1;
  @override
  void dispose() {
    // Every listener should be canceled, the same should be done with this stream.
    // _onDestroy.cancel();
    // _onUrlChanged.cancel();
    // _onStateChanged.cancel();
    // flutterWebviewPlugin.dispose();
    super.dispose();
  }

  bool loading = false;
  // final Client _client = Client();

  @override
  void initState() {
    //  _setUserName();
    couponController = TextEditingController();
    super.initState();
    // flutterWebviewPlugin.close();
    
     
    // Add a listener to on destroy WebView, so you can make came actions.
    // _onDestroy = flutterWebviewPlugin.onDestroy.listen((_) {
    // });

    // _onStateChanged =
    //     flutterWebviewPlugin.onStateChanged.listen((WebViewStateChanged state) {
    // });

    // // Add a listener to on url changed
    // _onUrlChanged = flutterWebviewPlugin.onUrlChanged.listen((String url) {
    //   if (mounted) {
    //     setState(() {
    //         // Navigator.of(context).pushNamedAndRemoveUntil(
    //         //     "/home", (Route<dynamic> route) => false);
    //         // flutterWebviewPlugin.close();

    //         if (url.startsWith('http://www.example.com')) {
    //           RegExp regExp = new RegExp(".*options=(tok_.*)");
    //           token = regExp.firstMatch(url)?.group(1);

    //           // Navigator.of(context).push(MaterialPageRoute<Null>(
    //           //   builder: (BuildContext context) {
    //           //     flutterWebviewPlugin.close();
    //           //     return BookingSuccessPage(booking: widget.booking);
    //           //   },));
    //           flutterWebviewPlugin.close();
    //           _continueToPaymentMutation();
    //         }

    //     });
    //   }
    // });
  }
   _setUserName()async{
      final SharedPreferences prefs = await SharedPreferences.getInstance();
       var userDataMap = prefs.getString("userDataMap");
         var data = json.decode(userDataMap);
        setState(() {
           name =data['name'];
           print(userDataMap);
           print('Username: $name');
        });
  }
  @override
  Widget build(BuildContext context) {
   if(name == null || name.isEmpty){
      _setUserName();
   }
    return Stack(
      children: <Widget>[

        Scaffold(
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: true,
                centerTitle: true,
                pinned: false,
                title: Text('Booking details'),
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  _body(context),
                ]),
              )
            ],
          ),
        ),

        AnimatedOpacity(
          duration: Duration(milliseconds: 350),
          opacity: loading ? 1 : 0,
          child: loading ? ProgressOverlay() : Container(),
        ),
       Align(
         alignment: Alignment.bottomCenter,
         child:  MyCard(
               padding: EdgeInsets.only(top:10,left: 20,right: 20,bottom: 5),
               child: Container(
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.centerRight,
                height: 50,
                child:   RaisedButton(
                   onPressed: () => _continueToPayment(),
                   color: MtColors.blue,
                   textColor: Colors.white,
                   elevation: 0,
                   padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 20.0),
                   shape: new RoundedRectangleBorder(
                       borderRadius: new BorderRadius.circular(25)),
                   child: Text("MAKE BOOKING", style: TextStyle(fontWeight: FontWeight.bold,),),
                ),
              )
            )
          ),
      ],
    );
  }

  Widget _body(context){
     var imagePath = widget.booking.space.images != null && widget.booking.space.images.length >0 ? widget.booking.space.images[0].url : null;
     var imageOrder = widget.booking.space.images != null && widget.booking.space.images.length >0 ? widget.booking.space.images[0].order : null;
    var room = ListingRoom(
     currency:  widget.booking.space.currency,
     image: ListingSpaceImage(url:  imagePath, order:  imageOrder),
     instruments: widget.booking.space.instruments.map((x){
        return ListingRoomInstrument(name:  x.type,id :x.id);
     }).toList(),
    //  [ListingRoomInstrument(name:  widget.booking.space.instruments[0].type,id :widget.booking.space.instruments[0].id),],
     location: ListingRoomLocation(city:  widget.booking.space.location.city, country:  widget.booking.space.location.country),
     name:  widget.booking.space.name,
     price:  widget.booking.space.price
    );
    return  Container(
         padding: EdgeInsets.only(left:20,right: 20, bottom: 50),
         child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
           ListTile(
             contentPadding: EdgeInsets.symmetric(vertical: 5),
             title:  Text('NAME',style:TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),
             subtitle: Text(name,style: TextStyle(fontSize:20,fontWeight: FontWeight.w600,color: Colors.black)),
           ),
          Text('SPACE',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),
         
          SizedBox(height: 16),
          // Space section
          SpaceCardRoom(
              space: room,
            listViewMode: true,
            istappable: false,
          ),
      
          SizedBox(height: 10),

          // Show booking hours
          bookingHoursOverview(
            booking: widget.booking,
          ),
          
          Divider(height:10),

          // [DropDownButton] Number of guests
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
           children: <Widget>[
             Text("PARTY SIZE", style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,)),
             Row(
               crossAxisAlignment: CrossAxisAlignment.center,
               children: <Widget>[
                  Container(
                    height: 40,
                    width:110,
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                      border: Border.all(color: Colors.black54,width: 1)
                    ),
                    // child: 
                    // Text('1'),
                    child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      child: DropdownButton<String>(
                        isDense: true,
                        value:partySizeString,
                        items: _getGuestNo(),
                        onChanged: (String value) {
                          setState(() {
                            partySizeString = value;
                            var val = _getPartySize(value);
                            partySizeint = val;
                          });
                        },
                      ),
                    ),
                  ),  
                 ),
                 SizedBox(width: 10,),
                 Padding(
                   padding: EdgeInsets.only(top: 10),
                   child: Text('Person(s)',style:TextStyle(color: Colors.black45)),
                 )
               ],
             )
           ],
         ),
         Divider(height: 20,),
          // [button] Add coupon code
          //   ListTile(
          //    contentPadding: EdgeInsets.symmetric(vertical: 5),
          //    title:  Row(
          //      children: <Widget>[
          //        Text('COUPON CODE',style:TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),
          //        Text('(empty if none)',style:TextStyle(color: Colors.black45,),),
          //      ],
          //    ),
          //    subtitle:Padding(
          //      padding: EdgeInsets.symmetric(vertical: 5),
          //      child:  Row(
          //      crossAxisAlignment: CrossAxisAlignment.center,
          //     children: <Widget>[
          //       Expanded(
          //         child: Container(
          //           height: 40,
          //          alignment: Alignment.center,
          //           padding: EdgeInsets.symmetric(horizontal: 20),
          //           decoration: BoxDecoration(
          //             borderRadius: BorderRadius.all(Radius.circular(25)),
          //             border: Border.all(color: Colors.black54,width: 1)
          //           ),
          //           child: TextField(
          //             onChanged: (val){
          //               setState(() {
                          
          //               });
          //             },
          //             controller: couponController,
          //             decoration: InputDecoration(
          //               border:InputBorder.none,
          //               contentPadding: EdgeInsets.only(bottom: 10),
          //               // hintText: 'Coupon code'
          //             ),
          //           ),
          //         ),
          //       ),
          //       SizedBox(width: 10,),
          //       RaisedButton(
          //         padding: EdgeInsets.symmetric(vertical: 5),
          //         onPressed: (){},
          //         elevation: 0,
          //         color: couponController.text != null && couponController.text.isNotEmpty ? MtColors.blue : Colors.grey.shade400,
          //         shape: RoundedRectangleBorder(
          //           borderRadius: BorderRadius.all(Radius.circular(20))
          //         ),
          //         child: Text('Apply',style: TextStyle(color: Colors.white),),
          //       )
          //     ],
          //    ),
          //    )
          //  ),
      
        //  Divider(height: 20),

          // BookingPriceOverview(
          //   booking: widget.booking,
          // ),

          // SizedBox(height: 16),

          // SizedBox(height: 16),

        ],
      ),
    );
  }
  int _getPartySize(String value){
    switch (value) {
      case "30+": return 30;
      case "21-30": return 20;
      case "11-20": return 30;
      default: return int.parse(value);
    }
  }
  List<DropdownMenuItem<String>> _getGuestNo(){
    var no = widget.maxGuest;
    List<String> list = [];
    switch (widget.maxGuest){
      case 1:list = ["1",];break;
      case 2:list = ["1", "2",];break;
      case 3:list = ["1", "2","3",];break;
      case 4:list = ["1", "2","3","4",];break;
      case 5: list = ["1", "2","3","4","5",];break;
      case 6: list = ["1", "2","3","4","5","6",];break;
      case 7: list = ["1", "2","3","4","5","6","7",];break;
      case 8: list = ["1", "2","3","4","5","6","7","8",];break;
      case 9: list = ["1", "2","3","4","5","6","7","8","9",];break;
      case 10: list = ["1", "2","3","4","5","6","7","8","9","10"];break;
      
        
       
      default: if(no>10 && no <=20){
                list = ["1", "2","3","4","5","6","7","8","9","10", "11-20"];
                }
               else if(no>=20 && no <=30){
                list = ["1", "2","3","4","5","6","7","8","9","10", "11-20", "21-30"];
                }
              else if(no  > 30){
                list = ["1", "2","3","4","5","6","7","8","9","10", "11-20", "21-30", "30+"];
              }
              else{
                list = ["1", "2","3","4","5",];
              }
         }
    
    return list.map((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value.toString()),
        );
      }).toList();
  }
  void _continueToPayment() {
    Navigator.of(context).push(MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return BookingReviewPage(
          booking: widget.booking,
          key: widget.key,
          price: widget.price,
          partySize: partySizeint ,
          roomId:widget.roomId
          
        );// LoadHTMLFileToWEbView(this.widget.bookingId);
      }
    ));

  }


  // US_REGION
//  static const String QUERY_MAKE_BOOKINGS = '''mutation{
//    pay(partySize: 0, reservationId: reserId, stripeToken: "stptoken", region: "US"){
//      success
//      error
//    }
//  }''';
  static const String QUERY_MAKE_BOOKINGS = '''mutation{
    pay(partySize: 0, reservationId: reserId, stripeToken: "stptoken", region: "EU"){
      success
      error
    }
  }''';
//  static const String QUERY_MAKE_BOOKINGS = '''mutation{
//    pay(partySize: 0, reservationId: reserId, stripeToken: "stptoken"){
//      success
//      error
//    }
//  }''';

  Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});

  // Future<void> _continueToPaymentMutation({String query = QUERY_MAKE_BOOKINGS}) async {

  //   setState(() {
  //     loading = true;
  //   });

  //   query = query.replaceAll('reserId', widget.bookingId.toString());
  //   query = query.replaceAll('stptoken', token.toString());

  //   String accessToken = await SharedPreferencesHelper.getAccessToken();
  //   Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};

  //   await _client
  //   .post(getUrlForQuery(query), headers: headers)
  //   .then((result) => result.body)
  //   .then(json.decode)
  //   .then((json){

  //     var error = json['errors'];
  //     if (error != null) {
  //       print(error[0]['message']);
  //       Navigator.of(context).push(MaterialPageRoute<Null>(
  //         builder: (BuildContext context) {
  //           return BookingFailedPage(booking: widget.booking);
  //         },
  //       ));
  //       setState(() {
  //         loading = false;
  //       });

  //     } else {
  //       Navigator.of(context).push(MaterialPageRoute<Null>(
  //         builder: (BuildContext context) {
  //           return BookingSuccessPage(booking: widget.booking);
  //         },
  //       ));
  //     }

  //   });

  // }

  Widget bookingHoursOverview({Booking booking}) {
    return getDateTime(widget.booking);
  }
 Widget getDateTime(Booking booking){
    Map<String, List<BookingHour>> map = BookingHour.groupByDate(booking.bookingHours);
     List<Widget> widgetList = [];
     widgetList.add(Text('DATE',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,)));
        widgetList.add(Text(DateFormat('EEEE, MMMM, dd , yyyy').format(booking.bookingHours.first.dateTime),style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.w400),));
        widgetList.add( SizedBox(height: 10,));
        widgetList.add(Text('TIME(S)',style:TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),);
      booking.bookingHours.forEach((x) {
      var tm = BookingHour.getHour(x.dateTime, context);
      var tm2 = BookingHour.getHour(x.dateTime.add(Duration(hours:1)), context);
        
        
       widgetList.add( 
           Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
                 SizedBox(height: 3,),
                 Container(
                      constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width - 100),
                      child: Text("$tm - $tm2",style:TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.w400, ),),
                    ),
                  SizedBox(height: 3,)
             ],
            
       ));
       
    });
   
    widgetList.add( SizedBox(height: 10,));
        var total =    booking.bookingHours.length > 1 
            ?Text('Total ${booking.bookingHours.length} hours',style:TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),)
              :Text('Total ${booking.bookingHours.length} hour',style:TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),);
        widgetList.add(total);    
  return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widgetList == null ? [Container()] : widgetList
    );
 }
  // TODO add coupon code
//  void _showCouponDialog(BuildContext context){
//    showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        return AlertDialog(
//          title: Text('Coupon Code'),
//          content: SingleChildScrollView(
//            child: ListBody(
//              children: <Widget>[
//                Text('To be implemented'),
//              ],
//            ),
//          ),
//          actions: <Widget>[
//            FlatButton(
//              child: Text('OK'),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }
}

class DateRange{
  final DateTime start;
  final DateTime end;

  DateRange(this.start, this.end);
}
