import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:mt_ui/data/Response/user_modia_link_response.dart';
import 'package:mt_ui/data/auth_helper.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/user_page/profile_edit_page.dart';
import 'package:mt_ui/pages/user_page/widgets/add_media_dialog_content.dart';
import 'package:mt_ui/pages/user_page/widgets/username_widget.dart';
import 'package:mt_ui/res/images.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/utils.dart';
import 'package:mt_ui/widgets/loading_indicators.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/providers/user_provider.dart';
import 'package:mt_ui/blocs/user_bloc.dart';

import '../home_page.dart';
import '../welcome_page.dart';
import 'widgets/addMediaLinks.dart';
import 'widgets/media_list_view.dart';
import 'widgets/webView.dart';

class ProfileViewPage extends StatefulWidget {
  ProfileViewPage({Key key, this.openProfilePage}) : super(key: key);
  final Function(ProfilPageType, User) openProfilePage;
  @override
  State<StatefulWidget> createState() {
    return _ProfileViewPageState();
  }
}

class _ProfileViewPageState extends State<ProfileViewPage> {
  int currentPageIndex = HomePage.PAGE_PROFILE;
  //  HomePageState state = HomePageState();
  User user;
  List<UserMedia> links;
  List<UserMedia> inspirationLinks = [];
  List<UserMedia> portFolioLinks = [];
  bool loading = true;
  // String youTubeLink = '', soundCloudLink = '', yttThumbnail = '', scthumbnail = '';

  @override
  void initState() {
    userBloc.getMediaLinks();

    print("Profile Screen--------------------");

    super.initState();
  }

  goToPage(int index) {
    setState(() {
      currentPageIndex = index;
    });
    print(index);
    //widget.pageController.jumpTo(index.toDouble());
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return UserProvider(
        userBloc: UserBloc(),
        child: Scaffold(body: FutureBuilder(builder: (context, snapshot) {
          return StreamBuilder<List<Map<String, dynamic>>>(
              stream: UserProvider.of(context).results,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(child: CircularProgressIndicator());
                }
                user = snapshot.data[0]["user"];
                links = snapshot.data[1]["links"];
                if (links != null && links.length > 0) {
                  if (links.any((x) => x.section.contains('Portfolio'))) {
                    portFolioLinks =
                        links.where((x) => x.section == "Portfolio").toList() ??
                            [];
                  }
                  if (links.any((x) => x.section.contains('Inspiration'))) {
                    inspirationLinks = links
                            .where((x) => x.section == "Inspiration")
                            .toList() ??
                        [];
                  }
                }

                return CustomScrollView(
                  slivers: <Widget>[
                    SliverAppBar(
                      floating: true,
                      pinned: true,
                      backgroundColor: Colors.blue,
                      leading: IconButton(
                        onPressed: () {
                          widget.openProfilePage(
                              ProfilPageType.ProfilePage, user);
                        },
                        icon: Icon(
                          Icons.keyboard_arrow_left,
                          color: Colors.white,
                        ),
                      ),
                      title: Text(
                        'Profile',
                        style: TextStyle(color: Colors.white),
                      ),
                      centerTitle: true,
                      actions: <Widget>[
                        // IconButton(
                        //   onPressed: () {
                        //     Utils.share("Please download the Space booking app from here – https://www.musictraveler.com/");
                        //     //  Utils.shareToSocial('https://play.google.com/store/apps/details?id=com.musictraveler.mtui');
                        //   },
                        //   icon: Icon(FeatherIcons.share2, color: Colors.white),
                        // ),
                      ],
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          _photoRow(context, user),
                          _headerSection(context, user),

                          // _buildSocialLinks(user),

                          SizedBox(height: 16),
                        ],
                      ),
                    ),
                    _sectionHeader("Portfolio", context),
                    _videoSection("Portfolio", MtImages.wind, 'Portfolio',
                        portFolioLinks, context),
                    portFolioLinks == null || portFolioLinks.length == 0
                        ? SliverToBoxAdapter(child: Container())
                        : addLinksSection(
                            image: null,
                            type: 'Portfolio',
                            header:
                                "Show host what you play by linking to your own recordings on Youtube or SoundCloud!"),
                    _sectionHeader("Inspiration", context),
                    _videoSection("Inspiration", MtImages.inspiration,
                        'Inspiration', inspirationLinks, context),
                    inspirationLinks == null || inspirationLinks.length == 0
                        ? SliverToBoxAdapter(child: Container())
                        : addLinksSection(
                            image: null,
                            type: 'Inspiration',
                            header:
                                "Show host what you play by linking to your own recordings on Youtube or SoundCloud!"),
                  ],
                );
              });
        })));
  }

  Widget addLinksSection({String image, String header, String type}) {
    return SliverToBoxAdapter(
        child: AddMediaLinks(
      image: image,
      header: header,
      type: type,
      onLinkAdded: (link) async {
        await userBloc.submitReview(link: link, type: type).then((val) {
          setState(() {
            print("REfreshUI");
          });
        });
      },
    ));
  }

  Widget _photoRow(BuildContext context, User user) {
    print( MtConstants.baseMediaUrl+ user.avatarUrl);
    return Container(
        alignment: Alignment.bottomCenter,
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
              height: 95,
              width: MediaQuery.of(context).size.width,
              color: Colors.blue,
            ),
            Container(
              margin: EdgeInsets.only(top: 30),
              height: 150,
              width: 150,
              decoration: BoxDecoration(shape: BoxShape.circle),
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.grey,
                ),
                child:
                    // Icon(Icons.person,color: Colors.white,),
                    MyCard(
                  borderRadiusValue: 100,
                  shadow: Shadow.none,
                  child: AspectRatio(
                      aspectRatio: 1,
                      child: UsernameWidget(
                        name: user.name,
                        radius: 75,
                        avatarUrl: user.avatarUrl,
                        backGroundColor: MtColors.blue25,
                        textStyle: TextStyle(
                            fontSize: 40, fontWeight: FontWeight.bold),
                      )),
                ),
              ),
            ),
            Container(
                alignment: Alignment.bottomRight,
                padding: EdgeInsets.only(right: 20),
                height: 150,
                child: IconButton(
                  onPressed: () {
                    widget.openProfilePage(ProfilPageType.ProfileEdit, user);
                    // Navigator.push(context, MaterialPageRoute(
                    //             builder: (context) => ProfileEditPage(user: user),
                    //           ),
                    //         );
                  },
                  icon: Icon(
                    FeatherIcons.edit2,
                    color: Colors.blue,
                  ),
                ))
          ],
        ));
  }

  Widget _buildSocialLinks(User user) {
    List<Widget> items = List();
    bool atLeastOneIsAvailable = false;

    if (user.soundCloudUrl != null) {
      items.add(_socialLink(
          "SoundCloud", FontAwesomeIcons.soundcloud, user.soundCloudUrl));
      atLeastOneIsAvailable = true;
    }

    if (user.facebookUrl != null) {
      items.add(
          _socialLink("Facebook", FontAwesomeIcons.facebook, user.facebookUrl));
      atLeastOneIsAvailable = true;
    }

    if (user.youtubeUrl != null) {
      items.add(
          _socialLink("Youtube", FontAwesomeIcons.youtube, user.youtubeUrl));
      atLeastOneIsAvailable = true;
    }

    if (user.websiteUrl != null) {
      items.add(_socialLink("My website", Icons.public, user.websiteUrl));
      atLeastOneIsAvailable = true;
    }

    // return empty container if no urls are available
    if (!atLeastOneIsAvailable) {
      return Container();
    }

    return Column(
      children: <Widget>[
        Divider(),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Wrap(
            alignment: WrapAlignment.center,
            spacing: 16,
            runSpacing: 16,
            children: items,
          ),
        ),
      ],
    );
  }

  Widget _videoSection(
    String header,
    String image,
    String type,
    List<UserMedia> list,
    context,
  ) {
    if (list == null || list.length == 0) {
      return addLinksSection(image: image, type: type, header: header);
    }
    var urls =
        Iterable.generate(list.length, (index) => list[index].url).toList();
    return MediaListView(urls: urls);
  }

  TextStyle paragraphStyle() => TextStyle(
        height: 1.25,
      );

  Widget _sectionHeader(String title, context) {
    return SliverToBoxAdapter(
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: Text(title,
                style: Theme.of(context).textTheme.subhead.copyWith(
                    color: Colors.black, fontWeight: FontWeight.bold))));
  }

  Column _headerSection(BuildContext context, User user) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 22),
        // Name
        Text(user.name,
            style: Theme.of(context).textTheme.headline.copyWith(
                fontWeight: FontWeight.bold,
                color: Colors.black54,
                fontSize: 30)),
      user.location == null || user.location.isEmpty ? SizedBox() :                
        SizedBox(height: 8),
        // TODO how to construct this from data? (think of perhaps translating the app)
        // user.bio == null || user.bio.isEmpty
        //     ? Container()
        //     : Text(
        //         user.bio,
        //         style: Theme.of(context).textTheme.subhead.copyWith(
        //               color: Colors.black54,
        //             ),
        //         textAlign: TextAlign.center,
        //       ),

        // SizedBox(height: 10),
        user.location == null || user.location.isEmpty ? SizedBox() :
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Spacer(),
            Icon(
              Icons.location_on,
              color: Colors.black54,
            ),
            SizedBox(width: 10),
            Text(user.location),
            Spacer()
          ],
        ),
        user.education == null || user.education.isEmpty ? SizedBox() :
        SizedBox(height: 10),
        user.education == null || user.education.isEmpty ? SizedBox() : 
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Spacer(),
            Icon(
              Icons.school,
              color: Colors.black54,
            ),
            SizedBox(width: 10),
            Text(user.education),
            Spacer()
          ],
        ),
         user.bio == null || user.bio.isEmpty ? SizedBox() :
        SizedBox(height: 10),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceAround,
        //   children: <Widget>[
        //     Spacer(),
        //     Icon(
        //       Icons.work,
        //       color: Colors.black54,
        //     ),
        //     SizedBox(width: 10),
        //     Text(user.occupation),
        //     Spacer()
        //   ],
        // ),
        // SizedBox(height: 10),
        user.bio == null || user.bio.isEmpty ? SizedBox() :
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Text(
            user.bio,
            style: paragraphStyle(),
            textAlign: TextAlign.center,
          ),
        ),

        Padding(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: Divider(
            height: 40,
          ),
        )
      ],
    );
  }

  Widget _socialLink(
    String label,
    IconData icon,
    String url,
  ) {
    return InkResponse(
      radius: 48,
      onTap: () => Utils.launchURL(url),
      child: Container(
        child: Column(
          children: <Widget>[
            Icon(
              icon,
              color: Colors.black54,
              size: 28,
            ),
            SizedBox(height: 10),
            Text(
              label,
              style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.bold,
                fontSize: 13,
              ),
            )
          ],
        ),
      ),
    );
  }
}
