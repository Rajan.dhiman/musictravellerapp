import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/pages/user_page/widgets/add_media_dialog_content.dart';

class AddMediaLinks extends StatefulWidget {
  final String header;
  final String description;
  final String type;
  final String image;
  final Function(String) onLinkAdded;
  AddMediaLinks({Key key, this.header, this.image, this.description, this.type, this.onLinkAdded}) : super(key: key);

  @override
  _AddMediaLinksState createState() => _AddMediaLinksState();
}

class _AddMediaLinksState extends State<AddMediaLinks> {
  Widget _dottedContainer() {
    return DottedBorder(
      borderType: BorderType.RRect,
      radius: Radius.circular(12),
      padding: EdgeInsets.all(6),
      color: Colors.black45,
      child: InkWell(
        onTap: onAddLinkPressed,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          child: Container(
            height: 150,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Stack(
                  alignment: Alignment.centerLeft,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 25),
                      width: 3,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.black45,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    ),
                    Container(
                      width: 50,
                      height: 3,
                      decoration: BoxDecoration(
                          color: Colors.black45,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Text('Embed link',
                    style: Theme.of(context).textTheme.headline.copyWith(
                          color: Colors.black45,
                        ))
              ],
            ),
          ),
        ),
      ),
    );
  }
  onAddLinkPressed()async{
   var link = await showDialog(
      context: context,
      child:Dialog(
        child: AddMediaDialogContent(),
      ) 
      );
    if(link!= null && link.isNotEmpty){
      widget.onLinkAdded(link);
    }
    print(link);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          widget.image == null
              ? SizedBox()
              : SizedBox(
                  height: 20,
                ),
          widget.image == null
              ? SizedBox()
              : Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    widget.image,
                    height: 100,
                  ),
                ),
          widget.image == null
              ? SizedBox()
              : SizedBox(
                  height: 20,
                ),
          widget.image == null
              ? SizedBox()
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Show host what you play by linking to your own recordings on Youtube or SoundCloud!',
                      style: TextStyle(
                          color: Colors.black38,
                          fontSize: 16,
                          fontFamily: 'Lato',
                          fontWeight: FontWeight.w700),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
          SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: _dottedContainer(),
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }
}
