import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/pages/user_page/widgets/webView.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/utils.dart';
import 'package:mt_ui/widgets/my_card.dart';

class MediaListView extends StatefulWidget {
  final List<String> urls;

  // when true, the widget will stay alive in ListViews
  // when it would otherwise have been recycled.
  final bool keepAlive;

  // if not provided, a PLAY icon will show
  final Widget overlayChild;

  // if not provided, the link will launch when pressed
  final Function onPressed;

  const MediaListView({
    Key key,
    this.urls,
    this.keepAlive = true,
    this.overlayChild,
    this.onPressed,
  }) : super(key: key);

  @override
  _MediaListViewState createState() => _MediaListViewState();
}

class _MediaListViewState extends State<MediaListView>
    with AutomaticKeepAliveClientMixin {
  // widget stays alive when you scroll it off the screen - otherwise images would load again.
  @override
  bool get wantKeepAlive => widget.keepAlive;

  @override
  Widget build(BuildContext context) {
    // DD. DO we need this super()?
    super.build(context);

    return _videosList(widget.urls);
  }

  Widget _videosList(List<String> urls) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return buildVideoThumbnail(urls[index]);
        },
        childCount: urls.length,
      ),
    );
  }

  Widget buildVideoThumbnail(String url) {
    return FutureBuilder(
      future: Utils.getMediaInfo(url),
      builder: (context, snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          width: 213,
          child: MyCard(
            onPressed: () {
              if (widget.onPressed != null) {
                widget.onPressed(url);
              } else {
                // Utils.launchURL(url);
                if (url != null && url.isNotEmpty) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => WebViewPage(link: url),
                    ),
                  );
                }
              }
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    SizedBox(
                      // height: 120,
                      // width: 213,
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl:
                            snapshot.hasData ? snapshot.data.thumbnailUrl : "",
                        placeholder: (context, string) => Container(
                          height: 150,
                          color: MtColors.EEEEEE,
                        ),
                      ),
                    ),
                    Container(
                      child: widget.overlayChild ??
                          MyCard(
                            borderRadiusValue: 100,
                            padding: EdgeInsets.all(8),
                            child: Icon(Icons.play_arrow,
                                color: Colors.grey[800], size: 32),
                          ),
                    ),

                    // loading indicator
                    Container(
                      child: !snapshot.hasData
                          ? Positioned(
                              bottom: 0,
                              left: 0,
                              right: 0,
                              child: SizedBox(
                                  height: 2,
                                  child: Opacity(
                                      opacity: 0.5,
                                      child: LinearProgressIndicator())))
                          : Container(),
                    ),
                  ],
                ),
                Container(
                    padding: EdgeInsets.all(16),
                    child: snapshot.hasData
                        ? Text(snapshot.data.title,
                            softWrap: false,
                            overflow: TextOverflow.fade,
                            style:
                                TextStyle(color: Colors.black54, fontSize: 16))
                        : Container(
                            height: 16,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: MtColors.EEEEEE,
                              borderRadius: BorderRadius.circular(4),
                            ),
                          ))
              ],
            ),
          ),
        );
      },
    );
  }
}
