import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mt_ui/blocs/user_bloc.dart';
import 'package:mt_ui/data/auth_helper.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/favourites_page.dart';
import 'package:mt_ui/pages/user_page/profile_view_page.dart';
import 'package:mt_ui/pages/welcome_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/utils.dart';
import 'package:mt_ui/widgets/loading_indicators.dart';
import 'package:mt_ui/widgets/login_view.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:package_info/package_info.dart';
import 'dart:convert';
import 'dart:math' as math;
import 'package:shared_preferences/shared_preferences.dart';

import '../../res/mt_styles.dart';
import '../home_page.dart';
import 'widgets/username_widget.dart';

class UserPage extends StatefulWidget {
  UserPage({Key key, this.openProfilePage}) : super(key: key);
 final Function(ProfilPageType,User) openProfilePage;
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage>
    {
  bool isSignedIn = false;
  MtUser mtUser;
  // FirebaseUser fireBaseUser;
  Widget userSection;
  String appVersion = '';
  String appEnviornment = '';
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {


    print("Profile Tab--------------------");
     getAppBuildNumber();
    super.initState();
    // set user section to loading state
     userSection = _userSectionLoading();
      checkEnviormnent();
      // load user
      _loadUser();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getUserDetail();
     
    });
    
  }

  void getUserDetail()async{
    if(mounted)
     {
        await userBloc.getMediaLinks().then((_){
          setState(() {
            _loadUser() ;
            print("Profile updated");
          });
        });
       
     } 
  }
  checkEnviormnent() async {
    String sessionEnv =  await SharedPreferencesHelper.getBaseUrl();
      if(sessionEnv == "production"){
       
      }else if(sessionEnv == "staging"){
         appEnviornment = 'Staging';
      }else{
         appEnviornment = '';
      }
  }

  @override
  Widget build(BuildContext context) {
    // DD. DO we need this super()?
    // super.build(context); 
  //  _loadUser();
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          automaticallyImplyLeading: false,
          title: Text("Profile", style: MtTextStyle.appBarTitle),
          pinned: false,
          floating: true,
          centerTitle: true,
        ),
        SliverList(
          delegate: SliverChildListDelegate([
           Container(
            //  height: MediaQuery.of(context).size.height - 143,
             child: Column(
              children: <Widget>[
                 _content(),
                 Container(
                      padding: EdgeInsets.symmetric(horizontal:20,vertical:10),
                      alignment: Alignment.bottomCenter,
                      child: Text('App version $appVersion',style: TextStyle(color:Colors.black54),),
                    ),

                     Container(
                      padding: EdgeInsets.symmetric(horizontal:20,vertical:10),
                      alignment: Alignment.bottomCenter,
                      child: Text(appEnviornment,style: TextStyle(color:Colors.black54),),
                    ),

                     
              ],
           ),
           )
             
          ]),
         
        ),
        //  SliverFooter(
        //     child: Container(
        //       padding: EdgeInsets.symmetric(horizontal:20,vertical:10),
        //       alignment: Alignment.bottomCenter,
        //       child: Text('App version $appVersion',style: TextStyle(color:Colors.black54),),
        //     ),
        //   )
      ],
    );
  }

  

  void _loadUser() {

    SharedPreferencesHelper.getUserData().then((String userData) {
      // });

      // FirebaseAuth.instance.currentUser().then((user){
      var user = json.decode(userData);
      // if signed in
      if (user['isAuth'] == 'true') {
        print("signed in ");

        // update user object
        // fireBaseUser = user;
        // mtUser = MtUser(
        //   name: fireBaseUser.displayName ?? null,
        //   avatarUrl: fireBaseUser.photoUrl ?? "",
        // );

        mtUser = MtUser(
          name: user['name'],
          email: user['email'],
          avatarUrl: user['avatar'],
        );

        setState(() {
          userSection = _userContent();
          isSignedIn = true;
           print("User signed in");
        });

        // if not signed in
      } else {
        print("[User_page.dart] User not signed in");

        setState(() {
          userSection = _userSectionNotSignedIn();
        });
      }
    });
  }

  Widget _userSectionLoading() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: MyCard(
        shadow: Shadow.none,
        backgroundColor: MtColors.F5F5F5,
        child: Container(
          height: 270,
          child: Center(
            child: InstrumentsIndicator(
              color: Colors.black12,
            ),
          ),
        ),
      ),
    );
  }

  Widget _userSectionNotSignedIn() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: LoginView(
        onSignedIn: () {
          _loadUser();
        },
      ),
    );
  }

  Widget _content() {
    
    return Column(
      children: <Widget>[
        Container(
          // constraints: BoxConstraints(maxWidth: 600),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              userSection,
             
              Divider(),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: MyCard(
                  onPressed: () => Utils.launchURL("https://www.musictraveler.com/host"),
                  child: ListTile(
                    leading: Icon(Icons.music_note, color: MtColors.blue,),
                    title: Text("Make money hosting", style: TextStyle(
                        color: MtColors.blue,
                        fontWeight: FontWeight.bold
                    )),
                    trailing: Icon(Icons.arrow_forward, ),
                  ),
                ),
              ),

              // Divider(),

// TODO uncomment. Commented by DD 2019.09.01
//              ListTile(
//                leading: Icon(Icons.settings),
//                title: Text("App settings"),
//                onTap: (){
//                  Navigator.push(context,
//                      MaterialPageRoute(builder: (context) => AppSettingsPage())
//                  );
//                },
//              ),
              ListTile(
                leading: Icon(Icons.help),
                title: Text("Help"),
                onTap: () {
                  Utils.launchURL("https://app.musictraveler.com/en/help/");
                },
              ),
              ListTile(
                leading: Icon(Icons.verified_user),
                title: Text("Privacy Policy"),
                onTap: () {
                  Utils.launchURL("https://www.musictraveler.com/privacy");
                },
              ),
              ListTile(
                leading: Icon(Icons.description),
                title: Text("Terms of service"),
                onTap: () {
                  Utils.launchURL("https://www.musictraveler.com/terms");
                },
              ),
              SizedBox(height: 70,),
              isSignedIn ? _signOutButton()
              : Container(),
              SizedBox(height:20),
              
            ],
          ),
        )
      ],
    );
  }
  Widget _signOutButton(){
   
        return Container(
             margin: EdgeInsets.symmetric(horizontal: 30),
             child: MyCard(
                          onPressed: () async {
                            //  print('SignOut button clicked');
                           
                setState(() {
                  loadingSignOut = true;
                });
                SharedPreferencesHelper.setLoginThroughFacebook(false);
                await AuthHelper().signOut();
                // SharedPreferencesHelper.setUserData('', '', '', '', false, '', '', '');
            //            redirect to Welcome page with login options
                Navigator.pushReplacement( 
                context, MaterialPageRoute(builder: (context) => WelcomePage()),);
            },
            backgroundColor: Colors.pink,
            borderRadiusValue: 100,
            shadow: Shadow.large,
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Center(
              child: Text('LOG OUT', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold,),)
        ),
      ),
     );
   
  }
  Widget _userContent() {
    print(mtUser.name);
    return Column(
      children: <Widget>[
        userCard(),
        /// Commented on 6 Jan
        // ListTile(
        //   leading: Icon(Icons.favorite),
        //   title: Text("My favourites"),
        //   onTap: () {
        //     Navigator.push(context,
        //         MaterialPageRoute(builder: (context) => FavouritesPage()));
        //   },
        // ),

        // TODO do we need this form? Check with other Account form
//        ListTile(
//          leading: Icon(Icons.person),
//          title: Text("My account"),
//          onTap: (){
//            Navigator.push(context,
//                MaterialPageRoute(builder: (context) => AccountSettingsPage())
//            );
//          },
//        ),
 /// Commented on 6 Jan
//         ListTile(
//           leading: loadingSignOut
//               ? Padding(
//                   padding: EdgeInsets.all(4),
//                   child: CircularProgressIndicator(),
//                 )
//               : Icon(Icons.exit_to_app),
//           title: Text("Sign out"),
//           onTap: () async {
//             setState(() {
//               loadingSignOut = true;
//             });

//             await AuthHelper().signOut();

//             _loadUser();

//             // Unset Access Token
//             SharedPreferences prefs = await SharedPreferences.getInstance();
//             prefs.remove('accessToken');

//             setState(() {
//               loadingSignOut = false;
//             });

//             SharedPreferencesHelper.setUserData(
//                 '', '', '', '', false, '', '', '');

// //            redirect to Welcome page with login options
//             Navigator.of(context).push(MaterialPageRoute<Null>(
//               builder: (BuildContext context) {
//                 return WelcomePage();
//               },
//             ));
//           },
//         ),
      ],
    );
  }

  bool loadingSignOut = false;

  Widget userCard() {
    return Padding(
      padding: EdgeInsets.all(16),
      child: MyCard(
        borderRadiusValue: 160,
        onPressed: () {
         setState(() {
            widget.openProfilePage(ProfilPageType.ProfileView,null);
         });
          // Navigator.of(context).push(MaterialPageRoute<Null>(
          //   builder: (BuildContext context) => ProfileViewPage(pageController:widget.pageController),
          // ));
        },
        padding: EdgeInsets.all(5),
        child: Row(
          children: <Widget>[
            // Profile pic
            Flexible(
              flex: 2,
              child: AspectRatio(
                aspectRatio:1/1,
                child: UsernameWidget(
                      name: mtUser.name,
                      avatarUrl: mtUser.avatarUrl ,
                      radius: 75,
                      backGroundColor: MtColors.blue25,
                      textStyle: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                    )
                //  ClipRRect(
                //   borderRadius: BorderRadius.circular(100),
                //   child: CachedNetworkImage(
                //     fit: BoxFit.cover,
                //     placeholder: (context, url) => Container(
                //       color: MtColors.EEEEEE,
                //       child: Icon(Icons.person, color: Colors.white, size: 32),
                //     ),
                //     imageUrl: mtUser.avatarUrl,
                //   ),
                // ),
              ),
            ),

            Flexible(
              flex: 6,
              child: Row(
                children: <Widget>[
                  SizedBox(width: 16),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          (mtUser.name == null || mtUser.name == "")
                              ? "My Profile"
                              : mtUser.name,
                          style: Theme.of(context).textTheme.headline.copyWith(fontWeight: FontWeight.bold),
                          softWrap: false,
                          overflow: TextOverflow.fade,
                        ),
                        SizedBox(height: 15,),
                        // Displaying user's email instead of msg
                        Text("View and edit profile",
                            style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.blue)),
                      ],
                    ),
                  ),
                  // SizedBox(width: 16),
                  // Icon(Icons.arrow_forward),
                  // SizedBox(width: 16),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void getAppBuildNumber()async{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    appVersion = "${packageInfo.version} (${packageInfo.buildNumber})";
  }
}
class SliverFooter extends SingleChildRenderObjectWidget {
  /// Creates a sliver that fills the remaining space in the viewport.
  const SliverFooter({
    Key key,
    Widget child,
  }) : super(key: key, child: child);

  @override
  RenderSliverFooter createRenderObject(BuildContext context) => new RenderSliverFooter();
}

class RenderSliverFooter extends RenderSliverSingleBoxAdapter {
  /// Creates a [RenderSliver] that wraps a [RenderBox] which is sized to fit
  /// the remaining space in the viewport.
  RenderSliverFooter({
    RenderBox child,
  }) : super(child: child);

  @override
  void performLayout() {
    final extent = constraints.remainingPaintExtent - math.min(constraints.overlap, 0.0);
    var childGrowthSize = .0; // added
    if (child != null) {
       // changed maxExtent from 'extent' to double.infinity
      child.layout(constraints.asBoxConstraints(minExtent: extent, maxExtent: double.infinity), parentUsesSize: true);
      childGrowthSize = constraints.axis == Axis.vertical ? child.size.height : child.size.width; // added
    }
    final paintedChildSize = calculatePaintOffset(constraints, from: 0.0, to: extent);
    assert(paintedChildSize.isFinite);
    assert(paintedChildSize >= 0.0);
    geometry = new SliverGeometry(
      // used to be this : scrollExtent: constraints.viewportMainAxisExtent,
      scrollExtent: math.max(extent, childGrowthSize),
      paintExtent: paintedChildSize,
      maxPaintExtent: paintedChildSize,
      hasVisualOverflow: extent > constraints.remainingPaintExtent || constraints.scrollOffset > 0.0,
    );
    if (child != null) {
      setChildParentData(child, constraints, geometry);
    }
  }
}