import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mt_ui/blocs/user_bloc.dart';
import 'package:mt_ui/data/dummy_data.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/user_page/widgets/username_widget.dart';
import 'package:mt_ui/res/images.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/loading_indicators.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:intl/intl.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'dart:io';

import '../home_page.dart';

class ProfileEditPage extends StatefulWidget{
  final User user;
  final Function(ProfilPageType,User) openProfilePage;
  ProfileEditPage({Key key,this.user, this.openProfilePage}) : super(key: key);
  @override
  _ProfileEditPageState createState() {
    return new _ProfileEditPageState();
  }
}

class _ProfileEditPageState extends State<ProfileEditPage> {

  final _key = new GlobalKey<ScaffoldState>();
  // final MtUser user = DummyData.getDummyUser();
  // final User user = widget.user;
  bool isProfileTypePublic = true;
  TextEditingController name;
  TextEditingController username;
  TextEditingController email;
  TextEditingController gender;
  TextEditingController phone;
  TextEditingController dob;
  TextEditingController occupation;
  TextEditingController location;
  TextEditingController education;
  TextEditingController bio;
  TextEditingController soundCloudUrl;
  TextEditingController facebookUrl;
  TextEditingController youtubeUrl;
  TextEditingController websiteUrl;
  TextEditingController avatarUrl;
  TextEditingController address;
  TextEditingController city;
  TextEditingController state;
  TextEditingController country;
  TextEditingController postalCode;
  TextEditingController telephone;
  TextEditingController skillLevel;
  TextEditingController school;
  TextEditingController graduated;
  // TextEditingController studyWith;
  TextEditingController ownedInstruments;
  TextEditingController playedInstruments;
  TextEditingController instruction;
  static DateFormat formatter = new DateFormat('yyyy-MM-dd');
  @override
  void initState() {
    
    name = TextEditingController(text: widget.user.name);
    username = TextEditingController(text: widget.user.userName);
    email = TextEditingController(text: widget.user.email);
    phone = TextEditingController(text: widget.user.phoneNumber);
    gender = TextEditingController(text: widget.user.gender );
    dob = TextEditingController();
    occupation = TextEditingController(text: widget.user.occupation);
    location = TextEditingController(text: widget.user.location);
    education = TextEditingController(text: widget.user.education);
    bio = TextEditingController(text: widget.user.bio);
    facebookUrl = TextEditingController(text: widget.user.facebookUrl);
    soundCloudUrl = TextEditingController(text: widget.user.soundCloudUrl);
    youtubeUrl = TextEditingController(text: widget.user.youtubeUrl);
    websiteUrl = TextEditingController(text: widget.user.websiteUrl);
    avatarUrl = TextEditingController(text: widget.user.avatarUrl);
    address = TextEditingController(text: widget.user.address);
    city = TextEditingController(text: widget.user.city);
    state = TextEditingController(text: widget.user.state);
    country= TextEditingController();
    postalCode= TextEditingController(text: widget.user.postalCode);
    telephone= TextEditingController(text: widget.user.phoneNumber);
    skillLevel= TextEditingController(text: widget.user.skill);
    school= TextEditingController(text: widget.user.school);
    graduated= TextEditingController(text: widget.user.graduation);
    // studyWith= TextEditingController();
    ownedInstruments= TextEditingController(text: widget.user.instrumentsOwned);
    playedInstruments= TextEditingController(text: widget.user.instrumentsPlayed);
    instruction = TextEditingController(text: widget.user.instruction);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color(0xff242424), // navigation bar color
      statusBarColor: Colors.blue,
    ));
    super.initState(); 
    setState(() {
    //   name.text          = widget.user.name;
    //   email.text         = widget.user.email;
    //   phone.text         = widget.user.phoneNumber;
    //   gender.text        = widget.user.gender;
    //   occupation.text    = widget.user.occupation;
    //   location.text      = widget.user.location;
    //   education.text     = widget.user.education;
    //   bio.text           = widget.user.bio;
    //   facebookUrl.text   = widget.user.facebookUrl;
    //   soundCloudUrl.text = widget.user.soundCloudUrl;
    //   youtubeUrl.text    = widget.user.youtubeUrl;
    //   websiteUrl.text    = widget.user.websiteUrl;
    //   avatarUrl.text     = widget.user.avatarUrl;
    //   skillLevel.text    = widget.user.skill;
    //   graduated.text     = widget.user.graduation;
    
    
    // address.text = widget.user.address;
    
    
    // city.text = widget.user.city;
    // state.text =widget.user.state;
    
    // // postalCode.text= TextEditingController();
    // telephone= TextEditingController();
    // skillLevel= TextEditingController();
    // school= TextEditingController();
    // graduated= TextEditingController();
    // // studyWith= TextEditingController();
    // ownedInstruments= TextEditingController();
    // playedInstruments= TextEditingController();
    // instruction = TextEditingController();
      
      if(widget.user.dob != null){
          dob.text =  formatter.format(DateTime.parse(widget.user.dob));
      }
      dob.text           = widget.user.dob;
      if(widget.user.countryCode != null){
       var countryApi  = MtConstants.countryList[widget.user.countryCode];
       country.text = countryApi;
      }
      
    });
    print('Name $name');



  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      body: CustomScrollView(
        slivers: <Widget>[

          SliverAppBar(
            title: Text("Profile editing", style: MtTextStyle.appBarTitle.copyWith(color: Colors.white)),
            centerTitle: true,
            pinned: true,
             backgroundColor: Colors.blue,
              leading: IconButton(
                 onPressed: (){widget.openProfilePage(ProfilPageType.ProfileView,null);},
                 icon: Icon(Icons.keyboard_arrow_left,color: Colors.white,),
               ),
            actions: <Widget>[
              // TODO make auto-save instead
              FlatButton(
                child: Text("Save".toUpperCase(), style: MtTextStyle.bold.copyWith(color: Colors.white)),
                onPressed: (){
                  _save();
                },
              ),
            ],
          ),

          SliverList(
            delegate: SliverChildListDelegate([
              Column(
                children: <Widget>[
                  Container(
                    constraints: BoxConstraints(
                      maxWidth: 600
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

//TODO uncomment. Commented by DD 2019.09.01
                        SizedBox(height: 16),
                    _pictureSection(),
                        SizedBox(height: 16),
                        _sectionHeader("MANDATORY", context),
                        
                        Container(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            children: <Widget>[
                              _createTextFieldGeneral("Name*", name),
                              SizedBox(height: 16),
                              _createTextFieldGeneral("Username*", username),
                              SizedBox(height: 16),
                              _createTextFieldGeneral("Email address*", email),
                            ],
                          ),
                        ),  
                      _rowWidget(  _birthDateField(context, dob), _dropdownWidget(gender,choiceList: MtConstants.GENDER_CHOICES)),
                      SizedBox(height: 30),
                      _sectionHeader("OPTIONAL", context),
                       Container(padding: EdgeInsets.all(16),child: _createTextFieldGeneral("Address", address),),
                        _rowWidget( _createTextFieldGeneral("City", city),   _createTextFieldGeneral("State", state),),
                        SizedBox(height: 16),
                        _rowWidget( _dropdownWidget(country, text:"Country", choiceList: MtConstants.countryList.values.toList()),   _createTextFieldGeneral("Postal Code", postalCode),),
                        SizedBox(height: 16),
                        _rowWidget( _createTextFieldGeneral("Telephone", phone),   _dropdownWidget(skillLevel, text:"Skill level", choiceList: MtConstants.SKILL_LEVELS_CHOICES),),
                        // SizedBox(height: 16),
                        Container(
                         height: 50,
                         margin: EdgeInsets.only(left: 20,right: 20,top:20),
                         width: MediaQuery.of(context).size.width ,
                         child:  _dropdownWidget(graduated, text:"Graduated", choiceList: MtConstants.GRADUATION_CHOICES),),
                       
                        Container(padding: EdgeInsets.all(16),child: _createTextFieldGeneral("Music school/university", school),),
                        /// instruction and study/studied with is same vaulue in db
                        Container(padding: EdgeInsets.all(16),child: _createTextFieldGeneral("Study/Studied with", instruction),),
                        Container(padding: EdgeInsets.all(16),child: _createTextFieldGeneral("Website", websiteUrl),),
                        Container(padding: EdgeInsets.all(16),child: _createTextFieldGeneral("Facebook", facebookUrl),),
                        Container(padding: EdgeInsets.all(16),child: _createTextFieldGeneral("Youtube", youtubeUrl),),
                        Container(padding: EdgeInsets.all(16),child: _createTextFieldGeneral("Soundcloud", soundCloudUrl)),
                        Container(padding: EdgeInsets.all(16),child: _createTextFieldGeneral("Occupation", occupation),),
                        // Padding(
                        //   padding: const EdgeInsets.all(16.0),
                        //   child: _sectionHeader("Follow me here", context),
                        // ),

                        Container(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            children: <Widget>[
                              _createTextField("Instruments owned", ownedInstruments, 4, 200,  false),
                              _createTextField("Played instruments", playedInstruments, 4, 200,  false),
                              _createTextField("My bio", bio, 4, 200,  false),
                            ],
                          ),
                        ),
//TODO uncomment. Commented by DD 2019.09.01
//                        SizedBox(height: 16),
//
//                        _mediaSection(MEDIA_TYPE_RECORDINGS),
//                        SizedBox(height: 16),
//
//                        _mediaSection(MEDIA_TYPE_INSPIRATION),

                      ],
                    ),
                  )
                ],
              ),
            ]),
          )
        ],
      )
    );
  }

  static const int MEDIA_TYPE_RECORDINGS = 1;
  static const int MEDIA_TYPE_INSPIRATION = 2;
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery, imageQuality: 20);

    setState(() {
      _image = image;
    });
      // await userBloc.uploadImage(_image, userId: widget.user.id, context: context);
     
  }


  Widget _pictureSection(){
    // print(MtConstants.baseMediaUrl+user.avatarUrl);
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
             margin: EdgeInsets.only(top: 0),
              height: 150,
              width: 150,
              decoration: BoxDecoration(
                shape: BoxShape.circle
              ),
              child: Container(
                  decoration: BoxDecoration(
                  shape: BoxShape.circle,color: Colors.grey,
                ),
                child: Stack(
                  alignment: Alignment.bottomRight,
                  children: <Widget>[
                   UsernameWidget(
                    name: widget.user.name,
                    radius: 75,
                    fileImage: _image,
                    avatarUrl: widget.user.avatarUrl,
                    backGroundColor: MtColors.blue25,
                    textStyle: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                  ),
                    MyCard(
                      onPressed: ()async{
                        getImage();
                      },
                       borderRadiusValue: 40,
                       shadow: Shadow.soft,
                      padding: EdgeInsets.all(10),
                      child: Icon(FeatherIcons.edit2,color: Colors.blue,),
                    ),
                  ],
                ),
              ),
          ),
        //   Row(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: <Widget>[
        //     Text('Profile Type', style:Theme.of(context).textTheme.headline.copyWith(color: Colors.black54)),
        //     InkWell(
        //       onTap: (){
        //         setState(() {
        //           print('Profile type changed');
        //           isProfileTypePublic =! isProfileTypePublic;
        //         });
        //       },
        //       child: Image.asset( isProfileTypePublic ? MtImages.public_profile : MtImages.private_profile, height: 70,),
        //     ),
        //   ],),
        //  Text('*Private profile only display username and avatar to visitors.',style:Theme.of(context).textTheme.caption.copyWith(fontSize: 14), textAlign: TextAlign.center,)
     ],),
    );
  }
  Widget _rowWidget(Widget child1,Widget child2){
    return  Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
                   height: 50,
                   padding: EdgeInsets.only(left: 20,right: 5),
                   width: MediaQuery.of(context).size.width *.5,
                   child:   child1,
            ),   
            Container(
                   height: 50,
                    padding: EdgeInsets.only(right: 20,left: 5),
                   width: MediaQuery.of(context).size.width *.5,
                   child:  child2,
            ),
          ],
        );
  }
  Future<bool> confirmDelete() {
    return showDialog(
      context: context,
      builder: (context){
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          title: Text('Remove video?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'.toUpperCase(), style: TextStyle(color: Colors.black38)),
              onPressed: () {
                Navigator.pop(context, false);
              },
            ),
            FlatButton(
              child: Text('Remove'.toUpperCase(), style: MtTextStyle.bold),
              onPressed: () {
                Navigator.pop(context, true);
              },
            ),
          ],
        );
      }
    );
  }

  Widget _createTextField(String label, TextEditingController controller, int maxLines, int maxLength,  bool capitalizeFirst){
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: MtColors.F5F5F5,
      ),
      child: TextField(
        // focusNode: FocusNode(),
        // controller: TextEditingController(text: value),
        // https://github.com/flutter/flutter/issues/11416
        controller: controller,//new TextEditingController.fromValue(new TextEditingValue(text: value ?? '',selection: new TextSelection.collapsed(offset:value == null ? 0 : value.length))),
        textCapitalization: capitalizeFirst ? TextCapitalization.sentences : TextCapitalization.none,
        maxLines: maxLines,
        // maxLength: maxLength,
        textAlign: TextAlign.start,
        cursorColor: Color(0xffffff),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
          // labelText: label,
          hintText: label,
          labelStyle: TextStyle(color: Colors.black87),
          border: OutlineInputBorder(
           borderSide: BorderSide(width: 1, style: BorderStyle.none),
           borderRadius: const BorderRadius.all(
             const Radius.circular(25.0),
           ),
         ),
        ),
        onChanged: (val){
          // print(val);
          setState(() {
            
            // switch(label) {
            //   case 'Occupation': occupation = val; break;
            //   case 'Location': location = val; break;
            //   case 'Education': education = val; break;
            //   case 'About me': bio = val; break;
            //   case 'Facebook': facebookUrl = val; break;
            //   case 'Youtube': youtubeUrl = val; break;
            //   case 'Website': websiteUrl = val; break;
            //   case 'SoudCloud': soundCloudUrl = val; break;
            // }
          });
        },
      ),
    );
  }
  int whoCanViewValue = 0;

  void onWhoCanViewChanged(int newValue){
    whoCanViewValue = newValue;
    setState(() {});
  }

  Widget _sectionHeader(String title, context) {
    return Row(
      children: <Widget>[
       Expanded(child: Padding(padding: EdgeInsets.symmetric(horizontal: 20),child:  Divider(),),),
         Text(title,
        style: Theme.of(context)
            .textTheme
            .subhead
            .copyWith(color: Colors.black54, fontWeight: FontWeight.bold)),
        Expanded(child: Padding(padding: EdgeInsets.symmetric(horizontal: 20),child:  Divider(),),),
      ],
    );
  }

  DateTime birthDate = DateTime.parse("1919-03-17");
  Widget _birthDateField(context, TextEditingController controller){

    // var controller = controller != null ? TextEditingController(
    //   text: DateFormat.yMMMMd().format(
    //     DateTime.parse(controller.text)
    //   )
    // ) : null;

    return GestureDetector(
      onTap: () async {
        DateTime selectedDate = await showDatePicker(
          initialDatePickerMode: DatePickerMode.year,
          context: context,
          initialDate: DateTime(1980),
          firstDate: DateTime(1900),
          lastDate: DateTime.now(),
          builder: (BuildContext context, Widget child) {
            return child;
          },
        );

        if(selectedDate != null) {
          String formattedDate = DateFormat('yyyy-MM-dd').format(selectedDate);
          // print(formattedDate);
          setState(() {
            dob.text = formattedDate;
          });

          // TODO update user birthday on API
        } else {
          return;
        }

      },
      child: _textFieldHolder(AbsorbPointer(
        absorbing: true,
        child: TextField(
          focusNode: FocusNode(),
          textCapitalization: TextCapitalization.words,
          maxLines: 1,
          controller: controller,
          textAlign: TextAlign.start,
          decoration:_inputDecoration('Date of birth*'),
        ),
      ),
      ),
    );
  }

  // String gender = "Male";
  Widget _dropdownWidget(TextEditingController controller, {String text = "Gender", List<String> choiceList = MtConstants.GENDER_CHOICES}) {
    // genderValue = null;
    // print(genderValue);
    return  Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black87),
        borderRadius: BorderRadius.all(Radius.circular(50)),
        color: MtColors.F9F9F9,
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          hint: Text(text),
          isDense: true,
          items: choiceList.map((String value) {
            return new DropdownMenuItem<String>(
              value: value,
              child: Container(
                width:105,

                child: Text(value, overflow: TextOverflow.fade,),
              )
            );
          }).toList(),
          onChanged: (newValue) {
            setState(() {
              controller.text = newValue;
            });
          },
          value:controller != null ? controller.text.length > 0 ? controller.text : null : null,
        ),
      ),
    );
   
  }

  Widget _createTextFieldGeneral(String label, TextEditingController controller){
   return Container(
     margin: EdgeInsets.symmetric(vertical: 0),
     child: TextField(
        // focusNode: FocusNode(),
        textCapitalization: TextCapitalization.words,
        // controller: _controller,
        // https://github.com/flutter/flutter/issues/11416
        controller:controller,// new TextEditingController.fromValue(new TextEditingValue(text: value ?? '',selection: new TextSelection.collapsed(offset:value != null ? value.length : 0))),
        textAlign: TextAlign.start,
        decoration:_inputDecoration(label),
         onChanged: (val){
          // print(val);
          setState(() {
            switch(label) {
              case 'Name': name.text = controller.text; break;
              case 'Phone number': phone.text = controller.text; break;
            }
          });
        },
      ),
   );
  }
  InputDecoration _inputDecoration(String hint) {
    return InputDecoration(
      contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      hintText: hint,
      labelStyle: TextStyle(color: Colors.black26),
      border: OutlineInputBorder(
        borderSide: BorderSide(width: 1, style: BorderStyle.none),
        borderRadius: const BorderRadius.all(
          const Radius.circular(25.0),
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(width:1, style: BorderStyle.solid,color: Colors.blue),
        borderRadius: const BorderRadius.all(
          const Radius.circular(25.0),
        ),
      ) ,
      filled: true,
      fillColor: MtColors.F9F9F9,
    );
  }
  Widget _textFieldHolder(Widget child){
    return Container(
      // margin: EdgeInsets.only(bottom: 8),
      // decoration: BoxDecoration(
      //   borderRadius: BorderRadius.circular(8),
      //   color: Color(0xFFf9f9f9),
      //   // border: Border.all(color: MtColors.EEEEEE),
      // ),
      child: child
    );
  }

  _save() async {
    if(!_validation()){
      return;
    }
    String res;
    final Client _client = Client();
    if(dob.text != null && dob.text.isEmpty){
      dob.text = formatter.format(DateTime.parse(dob.text));
    }
    String countryCode = '';
     MtConstants.countryList.forEach((key,value){
        if(value == country.text){
          countryCode = key;
        }
    });
    var query = '''mutation {
      profileUpdate (
      
        email:            "${email.text}",
        birthDate:        "${dob.text ?? ''}", 
        postalCode:       "${postalCode.text ?? ''}",
        address:          "${address.text}",
        state:            "${state.text}",
        city:             "${city.text ?? ''}", 
        facebook:         "${facebookUrl.text ?? ''}", 
        gender:           "${gender.text ?? ''}",
        username:         "${username.text ?? ''}", 
        name:             "${name.text ?? ''}", 
        occupation:       "${occupation.text ?? ''}", 
        phone:            "${phone.text ?? ''}", 
        school:           "${school.text ?? ''}", 
        soundcloud:       "${soundCloudUrl.text ?? ''}", 
        website:          "${websiteUrl.text ?? ''}", 
        youtube:          "${youtubeUrl.text ?? ''}",
        isHost:           true,
        isActive:         true,
        isPublic:         true,
        country :          "$countryCode",
        skillLevel:        "${skillLevel.text}",
        graduated :        "${graduated.text}",
        bio:              "${bio.text ?? ''}",
        instrumentsPlayed: "${playedInstruments.text ?? ''}",
        instrumentsOwned : "${ownedInstruments.text ?? ''}"
        instruction      : "${instruction.text}"
        
        ) {
        user {
          id
          username
          name
          email
          bio
          dob
          city
          facebook
          gender
          occupation
          phone
          school
          soundcloud
          website
          youtube
          country
          skillLevel
          graduated
        }
      }
    }''';
   
    // print('query: $query');

    // var map = {
    //        'isActive':         true,
    //        'isPublic':         true,
    //        'isHost'
    // };

   

   try{
          if(_image != null){
           final status = await userBloc.uploadImage(_image, userId: widget.user.id, context: context);
           if(status){
             log("Image uploaded on server");
           }
           else{
             log("Image upload", error:"Image is not uploaded");
             
            //  showSnack("Profile pic upload fail");
             return;
           }
          }
         Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});
         String accessToken = await SharedPreferencesHelper.getAccessToken();
         Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};
         await _client
          .post(getUrlForQuery(query), headers: headers)
          .then((result) => result.body)
          .then(json.decode)
          .then((json) {
            print(json);
            if (json['errors'] != null) {
              res = 'error';
            } else {
              res = 'success';
            }
            // Navigator.pop(context);
            return res;
          }).then((res) {
            print(res);
            var txt;
            switch(res) {
              case 'error': txt = 'Saving failed'; break;
              case 'success': txt = 'Profile updated sucessfully'; break;
            }
            // _key.currentState.showSnackBar(SnackBar(content: Text("To be implemented")));
            _key.currentState.showSnackBar(SnackBar(content: Text(txt)));
            // widget.openProfilePage(ProfilPageType.ProfileView,null);
          }).catchError((err){
             print('Error in saving profile:- $err');
          });
      }
      catch (error){
        print('Error in saving profile:- $error');
      }
  }

  bool _validation(){
    if(email.text == null || email.text.isEmpty){
        showSnack('Email field is required');
      return false;
    }
    if(name.text == null || name.text.isEmpty){
        showSnack('Name field is required');
      return false;
    }
    if(dob.text == null || dob.text.isEmpty){
        showSnack('Date of birth field is required');
      return false;
    }
     if(gender.text == null || gender.text.isEmpty){
        showSnack('Gender field is required');
      return false;
    }
    return true;
  }

  showSnack(String txt){
     _key.currentState.showSnackBar(SnackBar(content: Text(txt)));
  }
}

