import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/my_card.dart';

class AccountSettingsPage extends StatefulWidget{

  @override
  _AccountSettingsPageState createState() => _AccountSettingsPageState();
}

class _AccountSettingsPageState extends State<AccountSettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[

          SliverAppBar(
            pinned: false,
            floating: true,
            title: Text("Account", style: MtTextStyle.appBarTitle),
            centerTitle: true,
          ),

          SliverList(
            delegate: SliverChildListDelegate([

              // column in container in column - otherwise maxWidth doesn't work...
              Column(
                children: <Widget>[
                  Container(
                    constraints: BoxConstraints(
                      maxWidth: 600,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: _sectionHeader("General", context),
                        ),

                        MyCard(
                          shadow: Shadow.soft,
                          borderRadiusValue: 0,
                          padding: EdgeInsets.all(16),
                          child: Column(
                            children: <Widget>[

                              _createTextField("Name", "Nat King Cole"),
                              _createTextField("Email address", "natkingcole@gmail.com"),
                              _createPasswordField("Password", "12345678", context),
                              _createTextField("Phone number", "0044 23 32 24 43"),
                              _genderField(),
                              _birthDateField(context),

                            ],
                          ),
                        ),

                        SizedBox(height: 16),

                        // TODO (MR) uncomment (commented on 2019.08.31 before release)

                        // Padding(
                        //   padding: const EdgeInsets.all(16.0),
                        //   child: _sectionHeader("Social accounts", context),
                        // ),

                        // MyCard(
                        //   shadow: Shadow.soft,
                        //   borderRadiusValue: 0,
                        //   padding: EdgeInsets.all(16),
                        //   child: Column(
                        //     children: <Widget>[

                        //       _socialAccount("Facebook", FontAwesomeIcons.facebook, MtColors.facebookBlue),
                        //       _socialAccountAdd("Google", FontAwesomeIcons.google, MtColors.googleRed),

                        //     ],
                        //   ),
                        // ),

                        // SizedBox(height: 16),

                      ],
                    ),
                  ),
                ],
              ),

            ]),
          )
        ],
      )
    );
  }

  DateTime birthDate = DateTime.parse("1919-03-17");
  Widget _birthDateField(context){

    return GestureDetector(
      onTap: () async {

        DateTime selectedDate = await showDatePicker(
          initialDatePickerMode: DatePickerMode.year,
          context: context,
          initialDate: DateTime(1980),
          firstDate: DateTime(1900),
          lastDate: DateTime.now(),
          builder: (BuildContext context, Widget child) {
            return child;
          },
        );

        if(selectedDate != null) {
          setState(() {
            birthDate = selectedDate;
          });

          // TODO update user birthday on API
        }

      },
      child: _textFieldHolder(AbsorbPointer(
        absorbing: true,
        child: TextField(
          focusNode: FocusNode(),
          textCapitalization: TextCapitalization.words,
          maxLines: 1,
          controller: TextEditingController(text: DateFormat.yMMMMd().format(birthDate)),
          textAlign: TextAlign.start,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
            labelText: "Birth date",
            labelStyle: TextStyle(color: Colors.black87),
            border: InputBorder.none,
          ),
        ),
      ),
      ),
    );
  }

  String gender = "Male";
  Widget _genderField() {
    return _textFieldHolder(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: InputDecorator(
          decoration: InputDecoration(
            labelText: 'Gender',
            labelStyle: TextStyle(color: Colors.black87),
            border: InputBorder.none,
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              isDense: true,
              items: <String>['Male', 'Female', 'Other'].map((String value) {
                return new DropdownMenuItem<String>(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  gender = newValue;
                });
              },
              value: gender,
            ),
          ),
        ),
      ));
  }

  Widget _createTextField(String label, String value){
    return _textFieldHolder(TextField(
        focusNode: FocusNode(),
        textCapitalization: TextCapitalization.words,
        controller: TextEditingController(text: value),
        textAlign: TextAlign.start,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
          labelText: label,
          // prefixIcon: Icon(prefixIcon, color: Colors.black54),
          labelStyle: TextStyle(color: Colors.black87),
          border: InputBorder.none,
        ),
      ),
    );
  }

  Widget _createPasswordField(String label, String value, context){
    return GestureDetector(
      onTap: (){
        // TODO open dialog for changing password
        // this could also be done simply with firebaserUser.sendPasswordResetEmail();
        Scaffold.of(context).showSnackBar(SnackBar(content: Text("TODO: open dialog to change password")));
      },
      child: AbsorbPointer(
        absorbing: true,
        child: _textFieldHolder(TextField(
          obscureText: true,
          textCapitalization: TextCapitalization.words,
          controller: TextEditingController(text: value),
          textAlign: TextAlign.start,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
            labelText: label,
            labelStyle: TextStyle(color: Colors.black87),
            border: InputBorder.none,
          ),
        ),
        ),
      ),
    );
  }

  Widget _textFieldHolder(Widget child){
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Color(0xFFf9f9f9),
        // border: Border.all(color: MtColors.EEEEEE),
      ),
      child: child
    );
  }

  Widget _sectionHeader(String title, context) {
    return Text(title,
        style: Theme.of(context)
            .textTheme
            .subhead
            .copyWith(color: Colors.black54, fontWeight: FontWeight.bold));
  }
}
