import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/my_card.dart';

class AppSettingsPage extends StatefulWidget{
  @override
  _AppSettingsPageState createState() => _AppSettingsPageState();
}

class _AppSettingsPageState extends State<AppSettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[

          SliverAppBar(
            title: Text("App Settings", style: MtTextStyle.appBarTitle),
            centerTitle: true,
            pinned: false,
            floating: true,
          ),

          SliverList(
            delegate: SliverChildListDelegate([

              // column in container in column - otherwise maxWidth doesn't work...
              Column(
                children: <Widget>[
                  Container(
                    constraints: BoxConstraints(
                      maxWidth: 600
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: _sectionHeader("General", context),
                        ),

                        MyCard(
                          shadow: Shadow.soft,
                          borderRadiusValue: 0,
                          padding: EdgeInsets.all(16.0),
                          child: Column(
                            children: <Widget>[

                              _currencySetting(),

                              _notificationsSetting(),

                            ],
                          ),
                        ),

                      ],
                    ),
                  ),
                ],
              ),

            ]),
          ),

        ],
      ),
    );
  }

  bool pushNotifications = true;
  ListTile _notificationsSetting() {
    return ListTile(
      title: Text("Push notifications"),
      subtitle: Text("Get notified about your bookings"),
      trailing: Switch(
        value: pushNotifications,
        onChanged: (newValue){
          setState(() {
            pushNotifications = newValue;
          });
        },
      ),
    );
  }

  String currency = Currencies.userPreferredCurrency;
  ListTile _currencySetting() {
    return ListTile(
      // leading: Icon(FontAwesomeIcons.coins),
      title: Text("Display currency"),
      trailing: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          isDense: true,
          items: Currencies.toList().map((String value) {
            return new DropdownMenuItem<String>(
              value: value,
              child: new Text(value),
            );
          }).toList(),
          onChanged: (newValue) {
            setState(() {
              currency = newValue;
            });
          },
          value: currency,
        ),
      ),
    );
  }

  Widget _sectionHeader(String title, context) {
    return Text(title,
        style: Theme.of(context)
            .textTheme
            .subhead
            .copyWith(color: Colors.black54, fontWeight: FontWeight.bold));
  }
}