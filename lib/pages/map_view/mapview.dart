import 'dart:typed_data';
import 'dart:ui';

import 'package:mt_ui/pages/map_view/widget/mapAppbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/widgets/markerGenerator.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/space_card_room.dart';
import 'package:mt_ui/widgets/space_location_widget.dart';

class MapViewPage extends StatefulWidget {
  MapViewPage({Key key,this.roomsList}) : super(key: key);
  final List<ListingRoom> roomsList;
  @override
  _MapViewPageState createState() => _MapViewPageState();
}

class _MapViewPageState extends State<MapViewPage> {
 
  GoogleMapController mapController;
  GoogleMap map;
   Set<Marker> markers = Set<Marker>();
   Uint8List markerUint8List;
   int counter = 0;
   ListingRoom selectedRoom;
   @override  void initState() {    
    super.initState();  
    
    print('RoomList length : ${widget.roomsList.length}') ;
    initMarkers(0);    
   }
   initMarkers(int selectedRoomIndex){
     if(widget.roomsList != null && widget.roomsList.length > 0){
       selectedRoom = widget.roomsList[selectedRoomIndex];
        var widgetList = widget.roomsList.map((x){
          // print(x.location.coordinates.latitude);
          var key = GlobalKey();
          return paintBoundry(x,key);
        }).toList();  

    MarkerGenerator(widgetList, (bitmaps) {    
      print('BitmapLength: ${bitmaps.length}')  ;
        setState(() {   
         markers.addAll(getMarkers(bitmaps));   
          counter += 1;  
        });    
    }).generate(context);
     }
   }
  @override
  Widget build(BuildContext context) {
    // Space location
    LatLng location = LatLng(widget.roomsList.first.location.coordinates.latitude,widget.roomsList.first.location.coordinates.longitude);
    return Scaffold(
      // appBar:  MapViewAppBar(),
       body: Stack(
          children: <Widget>[
           GoogleMap(
              onMapCreated: _onMapCreated,
              rotateGesturesEnabled: false,
              scrollGesturesEnabled: true,
              zoomGesturesEnabled: false,
              initialCameraPosition: CameraPosition(
                target: location,
                zoom: 14.0,
              ),
              markers: markers,
            ),
            Positioned(
              top: 50,
              left: 20,
              child:Container(
                width: MediaQuery.of(context).size.width - 40,
                height: 45,
                child:MyCard(
                  borderRadiusValue: 30,                  
                  child:  Container(
                    height: 45,
                    child: Row(
                      children: <Widget>[
                       IconButton(
                          onPressed: (){},
                          icon: Icon(Icons.place,color: Colors.grey.shade700),
                         
                          ),
                         Expanded(
                           child: Container(
                             child: Text('${selectedRoom.location.city}, ${selectedRoom.location.country},'),
                           ),
                         ),
                          IconButton(
                          onPressed: (){Navigator.pop(context);},
                          icon: Icon(Icons.list,color: Colors.blue),
                         )
                        ],
                    ),
                   ),
                )
              )
            ),
            Positioned(
              bottom: 20,
              left: 10,
              child:Container(
                width: MediaQuery.of(context).size.width - 20,
                child:  SpaceCardRoom(
                 space: selectedRoom,
                 listViewMode: true,
                ),
              )
            )
          ],
        ),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    print('_onMapCreated initilised');
    setState(() {
      mapController = controller;
    });
  }
  final markerKey = List<GlobalKey>();
 
  Widget  paintBoundry(ListingRoom room, GlobalKey key){
    // print('paintBountry initilised');
    bool isSelcted  = false;
    if(selectedRoom != null && selectedRoom == room){
      isSelcted = true;
      print("MArker selected: ${room.price}");
    }
    return  Container( 
     padding: EdgeInsets.all(20),
     child: Stack(
       alignment: Alignment.center,
       children: <Widget>[
         Transform.rotate(
           angle: .8,
           child: Container(
              height: 80,
              width: 80,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight:  Radius.circular(40),
                  bottomLeft:  Radius.circular(40),
                ),
                 color: isSelcted ? Colors.blue : Colors.blue
              ),
          ),
         ),
         CircleAvatar(
           radius: 35,
           backgroundColor: isSelcted ? Colors.blue.shade100 : Colors.white,
           child: Text('€${room.price.toString()}',style: TextStyle(fontWeight: FontWeight.bold),),
           ),
       ],
     ));
  }
 
  List<Marker> getMarkers(List<Uint8List> markerUint8List, ){
    var markers = markerUint8List.map((x){
       return  Marker(
        onTap: (){
           setState(() {
            initMarkers(markerUint8List.indexOf(x));
           });
         },
        markerId: MarkerId(widget.roomsList[markerUint8List.indexOf(x)].id.toString()),
        icon:  BitmapDescriptor.fromBytes(x),//BitmapDescriptor.fromAsset('assets/graphics/markers/ic_place_blue_64.png'),
        position: LatLng(widget.roomsList[markerUint8List.indexOf(x)].location.coordinates.latitude,
          widget.roomsList[markerUint8List.indexOf(x)].location.coordinates.longitude),
      );
     }).toList();
     return markers;
  }
}
