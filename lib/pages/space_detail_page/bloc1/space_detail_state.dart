import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';

abstract class SpaceDetailState extends Equatable {
  const SpaceDetailState();

  @override
  List<Object> get props => [];
}

class Loading extends SpaceDetailState {}

class Loaded extends SpaceDetailState {
  final Room room;
  final String type;

  const Loaded({@required this.room, this.type});

  @override
  List<Object> get props => [room];

  @override
  String toString() => 'Loaded { items: ${room.name} }';
}

class Failure extends SpaceDetailState {}
