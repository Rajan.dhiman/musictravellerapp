import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mt_ui/resource/repository.dart';
import 'bloc.dart';

class SpaceDetailBloc extends Bloc<SpaceDetailEvent, SpaceDetailState> {
  final Repository repository;

  SpaceDetailBloc({this.repository});

  @override
  SpaceDetailState get initialState => Loading();

  @override
  Stream<SpaceDetailState> mapEventToState(SpaceDetailEvent event) async* {
    if (event is Fetch) {
      try {
        print("Fetch Event");
        yield Loading();
        final room = await repository.fetchSpaceDetail(url: getSearchUrl(event));
        yield Loaded(room: room, type: event.roomId);
      } catch (_) {
        yield Failure();
      }
    }
  }

  String getSearchUrl(Fetch fetch){
     var query = '''query{
      room (id: ${fetch.roomId}){
        place{
          city
          country
          address
          coordinates{
            latitude
            longitude
          }
        }
        rating
        id
        name
        description
        rating
        sizeMeasurement
        placeId
        userId
        capacity
        title
        hasAc
        hasWindow
        cancellationPolicy
        rules
        soundproof
        size
        totalReviews
        averageRating
        listingPrice{
          price
          currency
        }
        staticSchedule{
          price
          priceCurrency
          day
          hour
          timeSlots
          priceLoc
        }
        instruments{
          id
          manufacturer
          instrumentModel
          productionYear
          instrumentId
          roomId
          additionalInfo
          instrument {
            id
            name
          }
        }
        reviews{
          id
          created
          rating
          text
          title
          userId
          user{
            id
            email
            name
            username
            isHost
            occupation
            bio
            isPublic
            avatar
          }
        }
        images{
          id
          objectId
          attachmentFile
          contentTypeId
          order
        }
        isFavorite
      }
    }''';
    return query;
  }
}
