import 'package:equatable/equatable.dart';

abstract class SpaceDetailEvent extends Equatable{
  const SpaceDetailEvent();

  @override
  List<Object> get props => [];
}
class Fetch extends SpaceDetailEvent {
  final String roomId;
 

  Fetch({this.roomId});
   @override
  List<Object> get props => [roomId];

  @override
  String toString() => 'Fetch Space detail id: $roomId space';
}
