
import 'package:flutter/material.dart';
import 'package:mt_ui/widgets/space_picture.dart';
import 'package:photo_view/photo_view.dart';

class ImageViewPge extends StatefulWidget {
  final String imagePath;
  const ImageViewPge({Key key, this.imagePath}) : super(key: key);
  _ImageViewPgeState createState() => _ImageViewPgeState();
}

class _ImageViewPgeState extends State<ImageViewPge> {
  TextEditingController _textEditingController;
  bool isToolAvailable = true;
  @override
  void initState() {
    super.initState();
  }

  Widget _body() {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Container(
            color: Colors.white,
            constraints: BoxConstraints(maxHeight:MediaQuery.of(context).size.height),
            child: InkWell(
              onTap: (){
                setState(() {
                   isToolAvailable = !isToolAvailable;
                });
              },
               child:_imageFeed(widget.imagePath)
            )
          ),
        ),
        // !isToolAvailable ? Container() :
        Align(
            alignment: Alignment.topLeft,
            child: SafeArea(
              child: Container(
                  width: 50,
                  height: 50,
                  alignment: Alignment.topLeft,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white
                  ),
                  child: Wrap(children: <Widget>[BackButton(
                    color: Colors.black,
                  ),],)
              ),
            )
        )]);
  }
  
  Widget _imageFeed(String _image) {
    print(widget.imagePath,);
    return _image == null
        ? Container()
        : Container(
            alignment: Alignment.center,
            child: Container(
                child: PhotoView(
                  backgroundDecoration: BoxDecoration(
                    color: Colors.white
                  ),
                  imageProvider: NetworkImage(widget.imagePath,),
                  loadFailedChild: Center(child: Text('Image loading failed',style: Theme.of(context).textTheme.caption,),),
                )
            ));
  }

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _body());
  }
}