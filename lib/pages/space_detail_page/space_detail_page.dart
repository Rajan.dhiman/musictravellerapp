import 'dart:async';
import 'dart:developer';
import 'package:mt_ui/pages/Auth_page/email_login_page.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/data/dummy_data.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/booking_flow/make_booking_page/check_booking_availability_page.dart';
import 'package:mt_ui/pages/space_detail_page/widgets/covid_page.dart';
import 'package:mt_ui/pages/space_detail_page/widgets/review_item.dart';
import 'package:mt_ui/res/images.dart';
import 'package:mt_ui/utils.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/widgets/favorite_button_room.dart';
import 'package:mt_ui/widgets/location_view_dialog.dart';
import 'package:mt_ui/pages/space_detail_page/widgets/photo_page_view.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/space_location_widget.dart';
import 'package:mt_ui/widgets/icon_with_text.dart';
import 'package:mt_ui/widgets/star_rating.dart';

import 'dart:convert';
import 'package:http/http.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'dart:io';

import 'widgets/reviews_page.dart';

class SpaceDetailPage extends StatefulWidget {
  final Space space;
  final int spaceId;
   bool isFromCheckBookingPage;

   SpaceDetailPage({Key key, this.space, this.spaceId, this.isFromCheckBookingPage = false}) : super(key: key);

  @override
  State<StatefulWidget> createState() => SpaceDetailPageState();
}
class SpaceDetailPageState extends State<SpaceDetailPage>{
   Room room;
   ScrollController controller;
    String appEnviornment = '';
  @override
  void initState() {
    MtConstants.changeUrl();
    checkEnviormnent();
    controller = ScrollController(); 
    _fetchRoom(widget.spaceId);
    _checkLoginStatus();
    super.initState();
     
  }
   bool isLoggedIn = false;
  void _checkLoginStatus()async{
   SharedPreferencesHelper.getAccessToken().then((String res){
      setState(() {
        isLoggedIn = res != null && res.isNotEmpty && res != "null";
      });
    });
    // SharedPreferencesHelper.getLogout().then((bool res){
    //   setState(() {
    //     isLoggedIn = res;
    //   });
    // });
  }


   checkEnviormnent() async {
     String sessionEnv =  await SharedPreferencesHelper.getBaseUrl() ?? "production";
     appEnviornment = sessionEnv;
  }
  @override
  void dispose() { 
    controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {


   
    // Analytics tracking
    // trackSpaceView();
      
    // _fetchRoom(spaceId);

    // return FutureBuilder<dynamic>(
    //   future: _fetchRoom(spaceId),
    //   builder: (context, snapshot) {
    //     if (snapshot.hasData) {}
    //   }
    // );

//    Widget content = Container(child: Text('data'),);

    return Scaffold(
      backgroundColor: Colors.white,
      body: room != null ?
           SafeArea(
            top: true,
            bottom: false,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: CustomScrollView(
                    controller: controller,
                    slivers: <Widget>[
                      // App bar
                      SliverAppBar(
                        title: Text("Details", style: MtTextStyle.appBarTitle),
                        centerTitle: true,
                        floating: true,
                        pinned: true,
                        snap: true,
                      ),

                      PhotoPageView(
                        space: room,
                      ),

                  // main content
                      SliverList(
                        delegate: SliverChildListDelegate(
                          [

                             _titleSection(context, room),

                            _descriptionSection(context, room),
                            Divider(),

                            _instrumentsSection(context, room),
                            Divider(),

                            // TODO amenities and location

                            _amenitiesSection(context, room),
                            Divider(),

                            // // TODO find a way to preload the map - lags on first scroll
                            _locationSection(context, room),

                            Divider(),

                            // // Rules section
                            _textSection("Spaces Rules",
                                room.rules != null && room.rules.isNotEmpty
                                    ? room.rules
                                    : "No rules",
                                context),
                            Divider(),

                            _cancellationSection(context, room),
                            Divider(),

                            // // Noise section // TODO pull from API
                            _textSection("Noise Level",
                                "Generally nothing should disturb you", context),

                            Divider(),

                           Row(
                             children: <Widget>[
                               SizedBox(width:20),
                               
                               _sectionHeader("Reviews",context),
                               SizedBox(width:10),
                                StarRating(
                                  size: 14,
                                  rating: room.averageRating.floorToDouble(),
                                  defaultColor: Colors.black12,
                                ),
                                SizedBox(width:10),
                                Text("${room.reviews?.length ?? 0} review(s)")
                             ],
                           ),
                           SizedBox(width:10,height:10),
                          ],
                        ),
                      ),
                      
                      _reviewsSection(context, room),
                    ]
                  )
                ),
                _bottomBar(context, room),
              ]
            )
          )
       : Scaffold(
            backgroundColor: Colors.white,
            body: SafeArea(
              top: true,
              bottom: false,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: CustomScrollView(
                      slivers: <Widget>[
                        // App bar
                        SliverAppBar(
                          title: Text("Details", style: MtTextStyle.appBarTitle),
                          centerTitle: true,
                          floating: true,
                          pinned: true,
                          snap: true,
                        ),

                        SliverToBoxAdapter(
                          child: SizedBox(
                            height: 40,
                          )
                        ),

                        SliverToBoxAdapter(
                          child: SizedBox(
                            height: MediaQuery.of(context).size.height,
                            child: Align(
                                alignment: Alignment.topCenter,
                                child: CircularProgressIndicator()
                            ),
                          )
                        )
                      ]
                    )
                  )
                ]
              )
            )
          )
        
      
      
    

    // return Scaffold(
    //   backgroundColor: Colors.white,
    //   body: SafeArea(
    //     top: true,
    //     bottom: false,
    //     child: Column(
    //       children: <Widget>[

    //       ]
    //     )
    //   )
    // );

    // return Scaffold(
    //   backgroundColor: Colors.white,
    //   body: SafeArea(
    //     top: true,
    //     bottom: false,
    //     child: Column(
    //       children: <Widget>[
    //         Expanded(
    //           child: CustomScrollView(
    //             slivers: <Widget>[
    //               // App bar
    //               SliverAppBar(
    //                 title: Text("Details", style: MtTextStyle.appBarTitle),
    //                 centerTitle: true,
    //                 floating: true,
    //                 pinned: false,
    //                 snap: true,
    //               ),

    //               // PhotoPageView(
    //               //   space: space,
    //               // ),

    //               // // main content
    //               // SliverList(
    //               //   delegate: SliverChildListDelegate(
    //               //     [

    //               //       _titleSection(context),

    //               //       _descriptionSection(context),
    //               //       Divider(),

    //               //       _instrumentsSection(context),
    //               //       Divider(),

    //               //       _amenitiesSection(context),
    //               //       Divider(),

    //               //       // TODO find a way to preload the map - lags on first scroll
    //               //       _locationSection(context),

    //               //       Divider(),

    //               //       // Rules section
    //               //       _textSection("Spaces Rules",
    //               //           space.rules != null && space.rules.isNotEmpty
    //               //               ? space.rules
    //               //               : "No rules",
    //               //           context),
    //               //       Divider(),

    //               //       _cancellationSection(context),
    //               //       Divider(),

    //               //       // Noise section // TODO pull from API
    //               //       _textSection("Noise Level",
    //               //           "Generally nothing should disturb you", context),

    //               //       Divider(),
    //               //     ],
    //               //   ),
    //               // ),

    //               // // Reviews Header
    //               // SliverToBoxAdapter(
    //               //   child: Padding(
    //               //     padding: const EdgeInsets.all(16),
    //               //     child: Row(
    //               //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //               //       children: <Widget>[
    //               //         Expanded(child: _sectionHeader("Rewiews", context)),
    //               //         StarRating(
    //               //           size: 14,
    //               //           rating: space.rating.toDouble(),
    //               //           defaultColor: Colors.black12,
    //               //         ),
    //               //         SizedBox(
    //               //           width: 16,
    //               //         ),
    //               //         // todo pull from API
    //               //         // Text("32 reviews".toUpperCase(),
    //               //         // TODO morphology
    //               //         Text(space.reviews.length.toString() + " reviews".toUpperCase(),
    //               //             style: TextStyle(
    //               //                 fontWeight: FontWeight.bold,
    //               //                 color: Colors.black38,
    //               //                 fontSize: 11))
    //               //       ],
    //               //     ),
    //               //   ),
    //               // ),
    //               // _reviewsSection(context),
    //             ],
    //           ),
    //         ),
    //         // _bottomBar(context),
    //       ],
    //     ),
    //   ),
    // );
    );
  }

  // Track page view
  Future<void> trackSpaceView() async{
    await FirebaseAnalytics().logViewItem(
      itemId: room.spaceId,
      itemName: room.name,
      itemCategory: "Viewed space",
    );
  }

  Widget _bottomBar(context, Room space) {
    
    return Material(
        child: Container(
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          offset: Offset(0, -3),
          color: MtColors.black7,
          blurRadius: 9,
        )
      ]),
      padding: EdgeInsets.all(8.0),
      child: SafeArea(
        top: false,
        bottom: true,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(width: 8),

            // price
            Row(
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
              children: <Widget>[
                Text(
                  space.getDisplayMinPrice(),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
                Text(
                  "/hour",
                  style: TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                  ),
                ),
              ],
            ),

            // Spacing
            Expanded(
              flex: 1,
              child: SizedBox(),
            ),

            // Button
            RaisedButton(
              elevation: 0,
              color: MtColors.blue,
              child: Text( !isLoggedIn ? "Please Sign In"
                :  "Check Availability".toUpperCase(),
                style: Theme.of(context).textTheme.button.copyWith(
                    color: Colors.white,
                    letterSpacing: 0.8,
                    fontWeight: FontWeight.bold),
              ),
              onPressed: () async{
               if(!isLoggedIn){
                  await SharedPreferencesHelper.saveRoomId(widget.spaceId);
                 Navigator.of(context).push(
                  MaterialPageRoute<Null>(
                       builder: (BuildContext context) {
                         return EmailLoginPage(signUp: false,isFromCheckBookingPage: true,);
                        
                       },
                     ),
                    // ModalRoute.withName('/')
                     
                  );
                   
               }
               else{


                  print("appEnviornment $appEnviornment");
                 if(appEnviornment ==  "production"){
                   Navigator.of(context).push(
                    MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return CovidPage();
                        },
                      ),
                     
                  );
                 }else if(appEnviornment ==  "statging"){
                    Navigator.of(context).push(MaterialPageRoute<Null>(
                    builder: (BuildContext context) {
                      return CheckBookingAvailabilityPage(
                        space: space,
                      );
                    },
                    fullscreenDialog: false ));
                 }else {
                    Navigator.of(context).push(MaterialPageRoute<Null>(
                    builder: (BuildContext context) {
                      return CheckBookingAvailabilityPage(
                        space: space,
                      );
                    },
                    fullscreenDialog: false ));
                 }
               }
              },
              padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 20.0),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
            ),
          ],
        ),
      ),
    ));
  }

  Widget _titleSection(context,Room space) {
    return Container(
      padding: EdgeInsets.only(left:20.0,right:20,bottom: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                IconWithText(
                  text: "${space.location.city}, ${space.location.country}",
                  icon: Icons.place,
                ),

                SizedBox(height: 8.0),

                Text(space.name,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          color: Colors.black87,
                          fontWeight: FontWeight.bold,
                        )),

                SizedBox(height: 10),

                // Stars // TODO pull from object
                GestureDetector(
                onTap: (){
                   controller.animateTo(
                     controller.position.maxScrollExtent, 
                     duration: Duration(milliseconds: 500), 
                     curve: Curves.linear
                   );
                  },
                  child:Row(
                  children: [
                    StarRating(
                      size: 16,
                      rating: space.averageRating.toDouble(),
                      defaultColor: Colors.black12,
                    ),
                    SizedBox(width: 8, height: 35,),
                    // TODO pull from API
                    // Text("32 reviews".toUpperCase(),
                    // TODO morphology
                    Text(space.reviews.length.toString() + " reviews".toUpperCase(),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black38,
                            fontSize: 11))
                  ],
                )
                ),
                
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 22.0),
            // aligned to top, so it doesn't get pushed down, when text length increases. Padding to make it in line with a one-line title.
            child: FavoriteButtonRoom(
              space: space,
            ),
          )
        ],
      ),
    );
  }

  Widget _descriptionSection(context, space) => 
   Container(
        padding: EdgeInsets.all(16.0),
        child: Text(space.description,
            style: Theme.of(context).textTheme.body1.copyWith(fontSize: 18)),
      );

  Widget _instrumentsSection(context, space) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _sectionHeader('Instruments', context),
          SizedBox(height: 16.0),
          Wrap(
            spacing: 8.0,
            runSpacing: 8.0,
            children: _instruments(context, space),
          )
        ],
      ),
    );
  }

  List<Widget> _instruments(context, space) {
    List<Widget> widgets = List<Widget>();

    // Create instrument widgets and add to list
    space.instruments.forEach((instrument) => widgets.add(
      Container(
        constraints: BoxConstraints(
          maxWidth: 200,
        ),
        child: MyCard(
          padding: EdgeInsets.all(16),
          shadow: Shadow.none,
          boxBorder: Border.all(color: MtColors.EEEEEE),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text(
                Utils.capitalize(instrument.type),
                style: MtTextStyle.bold,
              ),
              // TODO check for nulls
              Container(
                child:
                    (instrument.manufacturer != null && instrument.manufacturer != ""
                          || instrument.model != null && instrument.model != "" )
                    ? Text(Utils.capitalize(instrument.manufacturer + " " + instrument.model))
                    : SizedBox(),
                    // Text(
                    //   Utils.capitalize(
                    //     instrument.model ?? "Unknown model"
                    //   )
                    // )
              ),

              Container(
                child: (instrument.year != null && instrument.year != 0)
                ? Text("From ${instrument.year}")
                : SizedBox(),
              ),

              Container(
                child: (instrument.detail != null && instrument.detail != "")
                ? Container(
                  padding: EdgeInsets.only(top: 8.0),
                    child: Text(instrument.detail,
                    style: TextStyle(fontStyle: FontStyle.italic, color: Colors.black54, fontSize: 14),
                  )
                )
                : SizedBox(),
              ),

            ],
          ),
        ),
      )
    ));

    // Display text if space has no instruments
    if (widgets.length == 0) {
      widgets.add(
          Text("No instruments", style: Theme.of(context).textTheme.body1));
    }

    return widgets;
  }

  // TODO fetch from API
  Widget _amenitiesSection(context, space) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _sectionHeader('Amenities', context),
          SizedBox(height: 16.0),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Wrap(
              spacing: 32,
              runSpacing: 32,
              children: <Widget>[
                // TODO check for nulls
                _amenityWithImage(MtImages.room, space.amenities.roomSize.toString() + space.amenities.sizeMeasurement.toString() + "\u00B2", "This space is 14m2"),
                // _amenity(FeatherIcons.home, "14" + space.amenities.sizeMeasurement.toString() + "2", "This space is 14m2"),
                _amenityWithImage(MtImages.wifi, "Wifi", "This space has Wifi"),
                _amenityWithImage(MtImages.user, space.amenities.capacity.toString() + " people", "This space fits up to 5 people"),
                space.amenities.hasWindow  ? _amenityWithImage(MtImages.window,  "Window" ,"This space has a window")
                : _amenityWithImage(MtImages.window,  "None" ,"This space has no window"),
                space.amenities.hasAc ? _amenity(Icons.ac_unit,"AC" , "This space has air conditioning")
                : Container()

              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _amenity(IconData icon, String label, String tooltip) {
    return Container(
      child: Tooltip(
        message: tooltip,
        child: Column(
          children: <Widget>[
            Icon(
              icon,
              color: Colors.black54,
              size: 30,
            ),
            SizedBox(height: 16),
            Text(
              label,
              style:
              TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
  Widget _amenityWithImage(String image, String label, String tooltip) {
    return Container(
      child: Tooltip(
        message: tooltip,
        child: Column(
          children: <Widget>[
            Image.asset(image,height: 30,color: Colors.black54,),
            SizedBox(height: 16),
            Text(
              label,
              style:
              TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }

  Widget _cancellationSection(context, space){

    String title = "Cancellation Policy";
    Widget content;

      String hours = "";
    if(space.cancellationPolicy != Space.POLICY_CUSTOM){

      switch(space.cancellationPolicy){
        case Space.POLICY_24_HOURS:
          hours = "24";
          break;
        case Space.POLICY_48_HOURS:
          hours = "48";
          break;
        case Space.POLICY_72_HOURS:
          hours = "72";
          break;
      }

      hours = space.cancellationPolicy.toString();

      content = ListTile(
        contentPadding: EdgeInsets.zero,
        // trailing: CircleAvatar(
        //   backgroundColor: MtColors.F5F5F5,
        //   foregroundColor: Colors.black87,
        //   child: Text(hours + "h"),
        // ),
        title: Text("If the booking is cancelled $hours hours or more before the starting time of the booking, you will receive a full refund."),
      );
    }else{

      // TODO: 1. this is null even when filled, 2. find a way to open the PDF
      if(space.customCancellationPolicyPdfUrl != null){
        content = RaisedButton(
          padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
          color: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          onPressed: (){
            Utils.launchURL(space.customCancellationPolicyPdfUrl);
          },
          child: Text("See policy".toUpperCase(), style: MtTextStyle.button),
        );
      }else{
        content = Text("No cancellation policy", style: Theme.of(context).textTheme.body1.copyWith(fontSize: 18));
      }
    }

    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              _sectionHeader(title, context),
              SizedBox(width: 10,),
              CircleAvatar(
                backgroundColor: MtColors.F5F5F5,
                foregroundColor: Colors.black87,
                child: Text(hours + "h"),
              ),
            ],
          ),
          SizedBox(height: 16),
          content,
        ],
      ),
    );
  }

  Widget _locationSection(context, space) {
     var shortestSide = MediaQuery.of(context).size.shortestSide;
    var isMobileLayout = shortestSide < 600;
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _sectionHeader("Location", context),

              IconWithText(
                shrinkWrap: true,
                text: "${space.location.city}, ${space.location.country}",
                icon: Icons.place,
                fontSize: 14,
                iconSize: 12,
                textColor: Colors.black38,
              ),

            ],
          ),

          SizedBox(height: 16),
          
          // Map
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute<Null>(
              builder: (BuildContext context) {
                return LocationViewDialog(
                  space: space,
                );
              },
              fullscreenDialog: false));
            },
            child: AbsorbPointer(
              child: Container(
                // height: MediaQuery.of(context).size.height / 3,
                height: isMobileLayout ?  MediaQuery.of(context).size.height / 3 :  MediaQuery.of(context).size.height / 2.4,
                width: MediaQuery.of(context).size.width - 20,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: _googleMapWidget(context,space)
                ),
              ),
            ),
          ),


          SizedBox(height: 16),
          Text("Exact address will be visible once your booking is confirmed",
              style: TextStyle(
                  color: Colors.black38,
                  fontSize: 14,
                  fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }
  // Uri renderUrl;
  Widget _googleMapWidget(BuildContext context,Room space){
    if(Platform.isAndroid){
      return SpaceLocationWidget(
          space: space,
          enableControls: false,
        );
    }
    else if(Platform.isIOS){
        return Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              
             width: MediaQuery.of(context).size.width ,
              child:Image.network(_buildUrl(context, space), fit: BoxFit.fill),
            ),
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color:MtColors.blue75),
                color: MtColors.blue75.withAlpha(50)
              ),
            )
          ],
        );
    }
    else{
      return Container();
    }
  }

  String _buildUrl(BuildContext context,Room space,) {
     var shortestSide = MediaQuery.of(context).size.shortestSide;
    var isMobileLayout = shortestSide < 600;
   
    int defaultWidth = (MediaQuery.of(context).size.width * 2.5).toInt();
    defaultWidth = isMobileLayout ? 620 : 1900;
    print(defaultWidth);
    int defaultHeight = 400;
     Map<String, String> defaultLocation = {
       "latitude": space.location.latitude.toString(),
       "longitude": space.location.longitude.toString()
     };
    var baseUri = new Uri(
        scheme: 'https',
        host: 'maps.googleapis.com',
        port: 443,
        path: '/maps/api/staticmap',
        queryParameters: {
          'size': '${defaultWidth}x$defaultHeight',
          'center': '${defaultLocation['latitude']},${defaultLocation['longitude']}',
          'zoom': '12',
          'key':'AIzaSyB_kIX5UrOzY9KC14LVNRAIsZCkx3xBXeA',
        });
         var url = baseUri.toString() + '&markers=color:blue%7Clabel:SS%7c${defaultLocation['latitude']},${defaultLocation['longitude']}';
         print(url);
        return url;
  }
  Widget _sectionHeader(String title, context) {
    return Text(title,
        style: Theme.of(context)
            .textTheme
            .subhead
            .copyWith(color: Colors.black54, fontWeight: FontWeight.bold));
  }

  Widget _textSection(String title, String body, context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _sectionHeader(title, context),
          SizedBox(height: 16.0),
          Text(body,
              style: Theme.of(context).textTheme.body1.copyWith(fontSize: 18)),
        ],
      ),
    );
  }

  MtUser getUserById(List<MtUser> users, int id) {
    for (var user in users) {
      if (user.id == id) {
        return user;
      }
    }
    return null;
  }



 Future<void>  _fetchRoom(int roomId) async {
  
    final Client _client = Client();
  
    var query = '''query{
      room (id: $roomId){
        place{
          city
          country
          address
          coordinates{
            latitude
            longitude
          }
        }
        rating
        id
        name
        description
        rating
        sizeMeasurement
        placeId
        userId
        capacity
        title
        hasAc
        hasWindow
        cancellationPolicy
        rules
        soundproof
        size
        totalReviews
        averageRating
        listingPrice{
          price
          currency
        }
        staticSchedule{
          price
          priceCurrency
          day
          hour
          timeSlots
          priceLoc
        }
        instruments{
          id
          manufacturer
          instrumentModel
          productionYear
          instrumentId
          roomId
          additionalInfo
          instrument {
            id
            name
          }
        }
        reviews{
          id
          created
          rating
          text
          title
          userId
          user{
            id
            email
            name
            username
            isHost
            occupation
            bio
            isPublic
            avatar
          }
        }
        images{
          id
          objectId
          attachmentFile
          contentTypeId
          order
        }
        isFavorite
      }
    }''';
  
   // print('space_detail_page -> query: $query');
  
    Uri getUrlForQuery(String query) => Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken};
  
      var currencyPrice = await SharedPreferencesHelper.getCurrencyRate();
      var currencyPref = await SharedPreferencesHelper.getCurrencyPreference();
  
   await _client
    .get(getUrlForQuery(query), headers: headers)
    .then((result) => result.body)
    .then(json.decode)
    .then((json) {
      
      //print('json -> json: $json');
       setState(() {
         room =  Room.fromAPIJson(json,curencyPrefrence: currencyPref,rate:currencyPrice);
      });
      
  //  if(widget.isFromCheckBookingPage){
  //    widget.isFromCheckBookingPage = false;
  //    Navigator.push(context, MaterialPageRoute(
  //     //  settings: const RouteSettings(name: '/space-page'),
  //      builder: (_) {
  //        return CheckBookingAvailabilityPage(
  //          space: Room.fromAPIJson(json,curencyPrefrence: currencyPref,rate:currencyPrice)
  //         );
  //      },
  //    ));
  //     room =  Room.fromAPIJson(json,curencyPrefrence: currencyPref,rate:currencyPrice);
  //   }
  //   else{
  //     setState(() {
  //        room =  Room.fromAPIJson(json,curencyPrefrence: currencyPref,rate:currencyPrice);
  //     });
  //   }
    });
  
  }
}

Widget _reviewsSection(context, Room space) {
  List<Review> reviews = space.reviews;
  if(space.reviews != null && space.reviews.isNotEmpty){
    space.reviews.sort((x,y) => DateTime.parse(x.date).compareTo(DateTime.parse(y.date)));
    reviews = space.reviews.reversed.toList();
  }

    // TODO fetch reviews from API
    // DummyData.getDummyReviews();
    // List<MtUser> users = DummyData.getDummyUsers();

    return SliverPadding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            if (index < 3) {
              var review = reviews[index];
              return ReviewItem(
                // review: reviews[index],
                // user: getUserById(users, reviews[index].userId),
                review: review,
                user: MtUser(name: review.username, avatarUrl:review.avatarUrl, email: review.email )//getUserById(users, reviews[index].userId),
              );
            }
             else if (index == 3) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 16),
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return ReviewsPage(
                            reviews: reviews,
                            users: reviews.map((x){
                                return  MtUser(name: x.username, avatarUrl:x.avatarUrl, email: x.email );
                            }).toList(),
                          );
                        },
                        fullscreenDialog: true));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text("See all reviews",
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.bold)),
                      SizedBox(width: 8),
                      Icon(
                        Icons.arrow_forward,
                        color: Colors.black54,
                        size: 18,
                      )
                    ],
                  ),
                ),
              );
            }
          },
          childCount: reviews.length,
        ),
      ),
    );
  }

  MtUser getUserById(List<MtUser> users, int id) {
    for (var user in users) {
      if (user.id == id) {
        return user;
      }
    }
    return null;
  }