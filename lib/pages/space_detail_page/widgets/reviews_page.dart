import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/pages/space_detail_page/widgets/review_item.dart';
import 'package:mt_ui/res/mt_styles.dart';

/*
Maybe use this for dates instead: https://pub.dartlang.org/packages/flutter_calendar
 */

class ReviewsPage extends StatefulWidget {
  final List<Review> reviews;
  final List<MtUser> users;

  const ReviewsPage({this.reviews, this.users});

  @override
  ReviewsPageState createState() => new ReviewsPageState();
}

class ReviewsPageState extends State<ReviewsPage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[

          SliverAppBar(
            title: Text("Reviews", style: MtTextStyle.appBarTitle),
            centerTitle: true,
            floating: true,
            snap: true,
            pinned: true,
          ),

      

          SliverPadding(
            padding: EdgeInsets.only(left: 16, top: 16, right: 16),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return ReviewItem(
                    review: widget.reviews[index],
                    user: getUserById(widget.users, widget.reviews[index].userId),
                  );
                },
                childCount: widget.reviews.length,
              ),
            ),
          ),

        ],
      ),
    );
  }

  MtUser getUserById(List<MtUser> users, int id){
    for (var user in users) {
      if(user.id == id){
        return user;
      }
    }
    return null;
  }

}
