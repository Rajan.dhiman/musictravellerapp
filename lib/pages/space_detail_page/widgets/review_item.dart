import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/star_rating.dart';

class ReviewItem extends StatelessWidget{

  // final Review review;
  final Review review;
  final MtUser user;

  const ReviewItem({this.review, this.user});

  @override
  Widget build(BuildContext context) {
     print(review.avatarUrl);
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: MyCard(
        shadow: Shadow.soft,
        padding: EdgeInsets.all(16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // picture
            Container(
              width: 48,
              child: GestureDetector(
                onTap: () { 
                  // Scaffold.of(context).showSnackBar(SnackBar(content: Text("TODO: Go to user profile")));
                }, // TODO link to user profile
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: AspectRatio(
                    aspectRatio: 1/1,
                    child: CachedNetworkImage(
                      // imageUrl: user.avatarUrl ?? "",
                      imageUrl: MtConstants.baseProfileUrl + review.avatarUrl ?? "",
                      fit: BoxFit.cover,
                      placeholder: (context, url) => Container(
                        color: Colors.white,
                      ),
                      errorWidget: (context, url, error) => Container(
                        decoration: BoxDecoration(
                            color: MtColors.F5F5F5
                        ),
                        child: Center(
                            child: Icon(Icons.person, color: Colors.black54,)
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),

            SizedBox(width: 16),

            Expanded(
              child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        runSpacing: 4,
                        children: <Widget>[
                          // Text("${user.name}", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black54),),
                          Text("${review.username}", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black54),),
                          Text(" rated it ", style: TextStyle(color: Colors.black54),),
                          StarRating(
                            size: 14,
                            rating: review.rating.floorToDouble(),
                            defaultColor: Colors.black12,
                          ),
                        ],
                      ),

                      _reviewTitle(),
                      _reviewText(),

                      SizedBox(height: 8),
                      Text(review.getFormattedDate(), style: TextStyle(fontSize: 12, color: Colors.black38, fontWeight: FontWeight.bold),),

                    ],
                  )
              ),
            ),

          ],
        ),
      ),
    );
  }

  Widget _reviewText(){
    if(review.message == null){
      return Container();
    }else{
      return Container(
        margin: EdgeInsets.only(top: 4.0),
        child: Text(review.message, style: TextStyle(fontSize: 18, color: Colors.black87),),
      );
    }


  }


    Widget _reviewTitle(){
    if(review.title == null){
      return Container();
    }else{
      return Container(
        margin: EdgeInsets.only(top: 4.0),
        child: Text(review.title, style: TextStyle(fontSize: 18, color: Colors.black87),),
      );
    }


  }

}