import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mt_ui/res/mt_styles.dart';


class CovidPage extends StatefulWidget {
  
  const CovidPage();

  @override
  CovidPageState createState() => new CovidPageState();
}

class CovidPageState extends State<CovidPage> {


@override
  initState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color(0xff242424), // navigation bar color
      statusBarColor: Colors.blue,
    ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[

          SliverAppBar(
            title: Text("Important COVID-19 Update", style: MtTextStyle.appBarTitle.copyWith(color: Colors.white)),
            centerTitle: true,
            pinned: true,
            backgroundColor: Colors.blue,
          ),

          SliverList(
            delegate: SliverChildListDelegate([
              Column(
                children: <Widget>[
                  Container(
                    constraints: BoxConstraints(
                      maxWidth: 600
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                      _descriptionSection(context, "Dear Music Travelers"),

                      _descriptionSection(context, "Our commitment to our customers remains unwavering through this trying period of the global coronavirus (COVID-19) pandemic. We continue to monitor the latest news on the virus as it unfolds. If you have bookings and would like to make a cancellation, there will be no fee to do so. You can cancel your bookings online, and shall soon thereafter receive a refund of the payments you made. If you do not receive your refund within 7 business days, please contact us through support@musictraveler.com for assistance."),
               

                     _descriptionSection(context,"Our team is actively working on ways to contribute and support all the musicians, venues and hosts during these difficult times and would welcome your ideas and suggestions. With your assistance and support, we will soon come up with a new plan of how to help everyone in the creative industry."),
                     _descriptionSection(context,"Best wishes and keep on creating."),
                     _descriptionSection(context,"The Music Traveler Team"),
                        

                      ],
                    ),
                  )
                ],
              ),
            ]),
          )
        ],
      )
    );
  }



   Widget _descriptionSection(context, textString) => 
   Container(
        padding: EdgeInsets.all(16.0),
        child: Text(textString,
            style: Theme.of(context).textTheme.body1.copyWith(fontSize: 18)),
      );



}
