import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/pages/space_detail_page/widgets/page_indicator.dart';
import 'package:mt_ui/widgets/space_picture.dart';
import 'dart:math';

import '../imageViewPage.dart';

class PhotoPageView extends StatefulWidget {
  final Room space;

  const PhotoPageView({@required this.space});

  @override
  _MyPageState createState() => _MyPageState();
}


class _MyPageState extends State<PhotoPageView> {
  PageController controller = PageController();


  @override
  Widget build(BuildContext context) {
    var rng = new Random().nextInt(1000000);

    // Return empty widget if space has no pictures
    if(widget.space.images == null || widget.space.images.length < 1){
      return SliverToBoxAdapter(child: SizedBox());
    }

    return SliverToBoxAdapter(
      child: AspectRatio(
        aspectRatio: 4/3,
        child: Stack(
          children: <Widget>[

            PageView.builder(
              controller: controller,
              itemBuilder: (context, position) {
                return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: InkWell(
                    onTap: (){
                       Navigator.of(context).push(MaterialPageRoute<Null>(
                         builder: (BuildContext context) {
                           return ImageViewPge( imagePath: widget.space.images[position].url,);
                     },
                      fullscreenDialog: true));
                    },
                    child: SpacePicture(
                      imageUrl: widget.space.images[position].url,
                      heroTag: "${widget.space.images[position].url}_$rng"
                     ),
                  )
                );
              },
              itemCount: widget.space.images.length, // Can be null
            ),

            _controls(),
           Align(
             alignment: Alignment.bottomCenter,
             child:  Container(
               height: 30,
               decoration: BoxDecoration(
                 color: Colors.white,
                 borderRadius: BorderRadius.only(topLeft:Radius.circular(20),topRight: Radius.circular(20))
               ),
             ),
           )
          ],
        ),
      ),
    );
  }

  Widget _controls(){

    // Return no controls if we only have one picture
    if(widget.space.images.length == 1){
      return SizedBox();
    }

    return Align(
      // bottom: 3,
      // right: 0,
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 80,
            child: DotsIndicator(
                controller: controller,
                itemCount: widget.space.images.length,
                onPageSelected: (int page) {
                  controller.animateToPage(
                    page,
                    duration: Duration(milliseconds: 750),
                    curve: Curves.ease,
                  );
                },
              ),
          ),
      // Row(
      //   children: <Widget>[

      //     // dots
          

      //     SizedBox(width: 16),

      //     // next button
      //     // Container(
      //     //   decoration: BoxDecoration(
      //     //     borderRadius: BorderRadius.only(topLeft: Radius.circular(8)),
      //     //     color: Colors.white,
      //     //   ),
      //     //   height: 48,
      //     //   width: 64,
      //     //   child: FlatButton(
      //     //     // padding: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
      //     //     padding: EdgeInsets.zero,
      //     //     child: Icon(Icons.arrow_forward, color: Colors.black54,),
      //     //     onPressed: (){
      //     //       controller.nextPage(duration: Duration(milliseconds: 750), curve: Curves.ease);
      //     //     }
      //     //   ),
      //     // ),
      //   ],
      // ),
    );

  }

}

