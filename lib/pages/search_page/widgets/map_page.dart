import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/pages/space_detail_page/space_detail_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/icon_with_text.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/search_bar.dart';
// import 'package:geolocator/geolocator.dart';


class MapPage extends StatefulWidget{
  final List<Space> spaces;

  const MapPage({Key key, this.spaces}) : super(key: key);

  @override
  MapPageState createState() => MapPageState();
}

class MapPageState extends State<MapPage> {

  GoogleMapController mapController;
  GoogleMap map;

  // keep track of what marker belongs to what space
  Map<Marker, Space> markerMap = Map<Marker, Space>();

  PageController _controller;
  int currentPage = 0;
  double currentPageValue = 0.0;
  double scrollOffset = 0.0;

  @override
  Widget build(BuildContext context) {

    markerMap = _getMarkers();
    map = GoogleMap(
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: LatLng(
          widget.spaces[0].location.latitude,
          widget.spaces[0].location.longitude,
        ),
        zoom: 12.0,
      ),
      markers: Set<Marker>.of(markerMap.keys),
    );

    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Colors.white,
        elevation: 0,
        title: SearchBar(
          // currentCity: "Berlin", // TODO display current city
          onResult: (String result) {
            updateMap(result);
          },
        ),
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[

          Padding(
            padding: const EdgeInsets.only(bottom: 150),
            child: map,
          ),

          // Top gradient (IgnorePointer to allow map interaction through gradient)
//          Positioned(
//            top: 0,
//            left: 0,
//            right: 0,
//            child: IgnorePointer(
//              child: SafeArea(
//                top: true,
//                child: Container(
//                  height: 100,
//                  decoration: BoxDecoration(
//                    gradient: LinearGradient(
//                      begin: Alignment.bottomCenter,
//                      end: Alignment.topCenter,
//                      colors: [Colors.white.withOpacity(0.0), Colors.white.withOpacity(0.75), Colors.white.withOpacity(1)],
//                    ),
//                  ),
//                ),
//              ),
//            ),
//          ),

          // Transparent app bar
//          Positioned(
//            top: 0, left: 0, right: 0,
//            child: AppBar(
//              // backgroundColor: Colors.white,
//              elevation: 0,
//              title: SearchBar(
//                currentCity: "Berlin",
//                onResult: (String result) {
//                  updateMap(result);
//                },
//              ),
//            ),
//          ),

          // Bottom gradient (IgnorePointer to allow map interaction through gradient)
          Positioned(
            left: 0,
            right: 0,
            bottom: 150,
            child: IgnorePointer(
              child: Container(
                width: double.infinity,
                // height: MediaQuery.of(context).size.height / 2,
                height: 150,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.white.withOpacity(0.0), Colors.white.withOpacity(1)],
                  ),
                ),
              ),
            ),
          ),

          // Spaces horizontal list
          _spacePageView(),

        ],
      )
    );
  }

  void updateMap(String city) async{

    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text("TODO"),
    ));

    // todo find a way to update spaces/markers

    /* move map to the searched city
    List<Placemark> placemarks = await Geolocator().placemarkFromAddress(city);
    if(placemarks.length > 0) {

      // take the first one
      LatLng latLng = LatLng(placemarks[0].position.latitude, placemarks[0].position.longitude);

      // move map to it
      mapController.animateCamera(CameraUpdate.newLatLng(latLng));
    }
    */
  }

  Widget _spacePageView(){

    _controller = PageController(
        keepPage: true,
        viewportFraction: 0.9
    );

    _controller.addListener(() {
      setState(() {
        currentPageValue = _controller.page;
        scrollOffset = _controller.offset;
      });
    });

    return SafeArea(
      bottom: true,
      child: Container(
        height: 210,
        width: double.infinity,
        child: PageView.builder(
          scrollDirection: Axis.vertical,
          physics: BouncingScrollPhysics(),
          controller: _controller,
          onPageChanged: _onPageChanged,
          itemCount: widget.spaces.length,
          itemBuilder: (context, index){

            // Item
            Widget item = _mapSpaceCard(widget.spaces[index]);

            // swiping away
            if (index < currentPageValue) {

              // progress will be 1 at the start and 0 and the end of the swipe
              double progress = index + 1 - currentPageValue;
              if(progress < 0) progress = 0;
              if(progress > 1) progress = 1;

              // Scale down
              const double scaleDownFactor = 0.15;
              double scale = 1 - (scaleDownFactor * (1 - progress));

              // Translate
              const double translateDistance = 170;
              double translation = translateDistance * (1 - progress);

              return Transform.translate(
                offset: Offset(0, translation),
                child: Transform.scale(
                  scale: scale,
                  child: Opacity(
                    opacity: progress,
                    child: item
                  ),
                ),
              );
            }

            // current page
            else if (index == currentPageValue) {
              return Opacity(
                  opacity: 1,
                  child: item
              );
            }

            // Swiping into view
            else if (index == currentPageValue.floor() + 1){
              return Opacity(
                opacity: 1,
                child: item
              );
            }

            // Next item (peeking at the bottom)
            else if (index == currentPageValue.floor() + 2){
              return Opacity(
                  opacity: 1,
                  child: item
              );
            }

            // Page not in view (bellow)
            else {
              return Opacity(
                  opacity: 0,
                  child: item
              );
            }

            // return _mapSpaceCard(widget.spaces[index]);

          },
        ),
      ),
    );
  }

  void _onPageChanged(index){
    if(autoUpdateMap){
      setState(() {
        currentPage = index;
        // go to position of marker at index of list
        mapController.animateCamera(CameraUpdate.newLatLng(markerMap.keys.toList()[index].position));
      });
    }
  }

  Widget _mapSpaceCard(Space space){

    Widget _picture;
    if(space.images.length > 0){
      _picture = CachedNetworkImage(
        fit: BoxFit.cover,
        imageUrl: space.images[0].url,
        placeholder: (context, url) => Container(
          color: MtColors.EEEEEE,
        ),
      );
    }else{
      _picture = Container(
        color: MtColors.EEEEEE,
      );
    }

    String heroTag = '';
    if(space.images.length > 0){
      heroTag = "${space.images[0].url}_0";
    }

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      child: MyCard(
        onPressed: () => _onSpaceCardTap(space),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            Container(
              child: Hero(
                tag: heroTag,
                child: AspectRatio(
                  aspectRatio: 3/4,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: _picture,
                  ),
                ),
              ),
            ),

            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Text(space.name,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.title.copyWith(
                        color: Colors.black87,
                        fontWeight: FontWeight.bold,
                      ),
                    ),

                    SizedBox(height: 12),

                    IconWithText(
                      text: "${space.location.city}, ${space.location.country}",
                      icon: Icons.place,
                      iconSpacing: 8,
                      singleLine: true,
                    ),

                    SizedBox(height: 6),

                    IconWithText(
                      text: space.getDisplayInstruments() ?? "No instruments",
                      icon: Icons.music_note,
                      iconSpacing: 8,
                      singleLine: true,
                    ),

                    SizedBox(height: 12),

                    Text(space.getDisplayMinPrice() + " per hour",
                      style: TextStyle(
                        fontSize: 18,
                        color: MtColors.blue,
                        fontWeight: FontWeight.bold,
                      ),
                    ),

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onSpaceCardTap(space){
    Navigator.push(context, MaterialPageRoute(builder: (_) {
      return SpaceDetailPage(
        space: space,
      );
    }));
  }

  void _onMapCreated(GoogleMapController controller) {
      mapController = controller;
  }

  Space _getSpaceOfMarker(MarkerId markerId){
    return markerMap.entries.firstWhere((entry) => entry.value.spaceId == markerId.value, orElse: () => null).value;
  }

  bool autoUpdateMap = true;

  // Called when tapping a map marker
  void _onMarkerTapped(MarkerId markerId) {

    // Temporarily disable code in onPageChanged that pans the map to the current space.
    // Map update automatically when you click a marker.
    autoUpdateMap = false;
    Future.delayed(Duration(milliseconds: 500), () => autoUpdateMap = true);

    // Get space that belongs to the marker, find that space in the PageView
    // and animate to that page.
    setState(() {

      Space space = _getSpaceOfMarker(markerId);
      for(int i = 0; i < widget.spaces.length; i++){
        if(widget.spaces[i].spaceId == space.spaceId){
         _controller.animateToPage(i, duration: Duration(milliseconds: 500), curve: Curves.ease);
         return;
        }
      }

    });
  }

  Map<Marker, Space> _getMarkers(){
    Map<Marker, Space> map = Map<Marker, Space>();
    for(int i = 0; i < widget.spaces.length; i++){

      Space space = widget.spaces[i];

      // TODO add custom marker with price tag (not yet supported by library (24 april, 2019))
      // TODO add different marker for selected marker if(currentPage == i)
      MarkerId markerId = MarkerId(space.spaceId);
      Marker marker = Marker(
        markerId: markerId,
        position: LatLng(
          space.location.latitude,
          space.location.longitude,
        ),
        icon: BitmapDescriptor.defaultMarker,
        onTap: () => _onMarkerTapped(markerId),
      );

      // add marker to id map
      map.putIfAbsent(marker, () => space);
    }

    return map;
  }
}
