import 'package:flutter/material.dart';
import 'package:mt_ui/pages/search_page/widgets/filter_pages/base_filter_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/date_picker.dart';

class DateFilterPage extends StatefulWidget {

  final DateTime initialDate;

  const DateFilterPage({Key key, this.initialDate}) : super(key: key);

  @override
  _DateFilterPageState createState() => _DateFilterPageState();
}

class _DateFilterPageState extends State<DateFilterPage> {

  bool showApplyButton = false;
  DateTime selectedDate;


  @override
  void initState() {
    super.initState();

    // Apply initial date if provided
    if(widget.initialDate != DateTime(0)){
      selectedDate = widget.initialDate;
    }

  }

  @override
  Widget build(BuildContext context) {

    return BaseFilterPage(
      appBarTitle: "Date",
      showResetButton: (selectedDate != null),
      onResetPressed: (){
        // pass back null to reset filter
        Navigator.pop(context, DateTime(0));
      },
      showApplyButton: showApplyButton,
      onApplyPressed: (){
        // pass back selected date time
        Navigator.pop(context, selectedDate);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          // Quick buttons (Today and tomorrow)
          MyCard(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
            child: Container(
              width: double.infinity,
              child: Wrap(
                spacing: 8,
                children: <Widget>[
                  RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                    color: MtColors.blue10,
                    textColor: MtColors.blue,
                    elevation: 0,
                    onPressed: (){
                      // pass back Today
                      Navigator.pop(context, DateTime.now());
                    },
                    child: Text("Today", style: TextStyle(fontWeight: FontWeight.bold),),
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                    elevation: 0,
                    color: MtColors.blue10,
                    textColor: MtColors.blue,
                    onPressed: (){
                      // pass back Tomorrow
                      Navigator.pop(context, DateTime.now().add(Duration(days: 1)));
                    },
                    child: Text("Tomorrow", style: TextStyle(fontWeight: FontWeight.bold),),
                  ),
                ],
              ),
            ),
          ),

          SizedBox(height: 16),

          MyDatePicker(
            firstDate: DateTime.now().subtract(Duration(days: 1)),
            initialDate: selectedDate ?? DateTime.now(),
            lastDate: DateTime.now()
                .add(Duration(days: 90)), // allow booking 90 days ahead
            initialDatePickerMode: DatePickerMode.day,
            onChanged: (DateTime newDate) {

              setState(() {
                selectedDate = newDate;
                showApplyButton = true;
              });

            },
          ),
        ],
      ),
    );

  }
}
