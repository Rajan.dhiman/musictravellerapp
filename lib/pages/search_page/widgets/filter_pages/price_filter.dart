import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/pages/search_page/widgets/filter_pages/base_filter_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as slider;

class PriceFilterPage extends StatefulWidget {
  /// Current PriceRange
  final PriceRange priceRange;

  const PriceFilterPage({Key key, this.priceRange}) : super(key: key);

  @override
  PriceFilterPageState createState() => PriceFilterPageState();
}

class PriceFilterPageState extends State<PriceFilterPage> {
  double lower;
  double higher;

  bool showApplyButton = false;

  @override
  void initState() {
    super.initState();

    // init with the values passed in
    lower = widget.priceRange.lower ?? widget.priceRange.absMin;
    higher = widget.priceRange.higher ?? widget.priceRange.absMax;
  }

  @override
  Widget build(BuildContext context) {
    TextStyle indicatorStyle = TextStyle(
      fontSize: 40,
      fontWeight: FontWeight.w300,
      color: Colors.black54,
    );

    return BaseFilterPage(
      appBarTitle: "Price",
      showResetButton: widget.priceRange.isActive() || showApplyButton,
      onResetPressed: (){
        // return empty PriceRange to reset filter
        Navigator.pop(context, PriceRange());
      },
      showApplyButton: showApplyButton,
      onApplyPressed: (){

        // If lower is the lowest and highest is the highest, disable the filter.
        if (lower == widget.priceRange.absMin &&
            higher == widget.priceRange.absMax) {
          Navigator.pop(context, PriceRange());
          return;
        }

        // Pop Page and return priceRange
        Navigator.pop(
            context,
            PriceRange(
              lower: lower,
              higher: higher,
            ));
      },
      child: Column(
        children: <Widget>[
          SizedBox(height: 64),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(getPriceText(), style: indicatorStyle),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: SliderTheme(
              data: SliderTheme.of(context).copyWith(
                overlayColor: MtColors.blue25,
                activeTickMarkColor: Colors.blue,
                activeTrackColor: MtColors.blue,
                inactiveTrackColor: MtColors.blue10,
                thumbColor: MtColors.blue,
                thumbShape: RoundSliderThumbShape(enabledThumbRadius: 10),
              ),
              child: slider.RangeSlider(
                min: widget.priceRange.absMin,
                max: widget.priceRange.absMax,
                divisions:
                (widget.priceRange.absMax - widget.priceRange.absMin)
                    .round(),
                lowerValue: lower,
                upperValue: higher,
                showValueIndicator: false,
                valueIndicatorMaxDecimals: 0,
                onChanged: (double newLower, double newHigher) {
                  setState(() {
                    showApplyButton = true;
                    lower = newLower;
                    higher = newHigher;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  String getPriceText() {
    String priceStart = lower.round().toInt().toString();
    String priceEnd = higher.round().toInt().toString();
    String currencySymbol = Currencies.getCurrencySymbol(Currencies.userPreferredCurrency);

    if (higher == widget.priceRange.absMax) {
      priceEnd = priceEnd + "+";
    }

    return "$currencySymbol$priceStart - $currencySymbol$priceEnd";
  }
}

class PriceRange {
  final double lower, higher;

  PriceRange({
    this.lower,
    this.higher,
  });

  static PriceRange parse(String value){
    return PriceRange(
      lower: double.parse(value.substring(0, value.indexOf(","))),
      higher: double.parse(value.substring(value.indexOf(",")+1)),
    );
  }

  String toString(){
    return "$lower,$higher";
  }

  // Min and max prices
  double get absMin => 3;
  double get absMax => 150;

  /// returns true if a range has been added
  bool isActive() {
    return (lower != null && higher != null);
  }

  /// returns true if higher is the abs highest
  bool higherIsAbs(){
    return (higher == absMax);
  }

  /// Return presentable string of price range.
  /// Returns null if empty
  String getPriceRangeDisplay() {
    if (!isActive()) {
      return null;
    }

    String currencySymbol =
        Currencies.getCurrencySymbol(Currencies.userPreferredCurrency);

    // If only lower price
    if (lower != absMin && higher == absMax) {
      return "${lower.toInt()}$currencySymbol+";
    }

    // If only higher price
    else if (lower == absMin && higher != absMax) {
      return "<${higher.toInt()}$currencySymbol";
    }

    // if range
    else if (lower != absMin && higher != absMax) {
      return "$currencySymbol${lower.toInt()} - $currencySymbol${higher.toInt()}";
    } else {
      return null;
    }
  }
}
