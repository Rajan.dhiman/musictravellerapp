import 'package:flutter/material.dart';
import 'package:mt_ui/res/mt_styles.dart';

class BaseFilterPage extends StatefulWidget {
  final Widget child;
  final String appBarTitle;
  final bool showResetButton;
  final Function onResetPressed;
  final bool showApplyButton;
  final Function onApplyPressed;

  const BaseFilterPage({
    Key key,
    this.child,
    this.appBarTitle,
    this.showResetButton,
    this.onResetPressed,
    this.onApplyPressed,
    this.showApplyButton,
  }) : super(key: key);

  @override
  _BaseFilterPageState createState() => _BaseFilterPageState();
}

class _BaseFilterPageState extends State<BaseFilterPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: Colors.white,
                floating: true,
                snap: true,
                centerTitle: true,
                title: Text(widget.appBarTitle, style: MtTextStyle.appBarTitle,),
                actions: [
                  AnimatedOpacity(
                    opacity: widget.showResetButton ? 1 : 0,
                    duration: Duration(milliseconds: 350),
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 350),
                      curve: Curves.easeInOut,
                      transform: Matrix4.translationValues(
                          widget.showResetButton ? 0 : 64,
                          0,
                          0), // slide to bottom
                      color: Colors.white,
                      child: FlatButton(
                        child: Text('RESET'),
                        onPressed: () {
                          widget.onResetPressed();
                        },
                      ),
                    ),
                  ),
                ],
              ),
              SliverList(delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.only(bottom: 64),
                  child: widget.child,
                ),
              ])),
            ],
          ),

          // Apply button
          AnimatedPositioned(
            duration: Duration(milliseconds: 350),
            curve: Curves.easeInOut,
            bottom: widget.showApplyButton ? 16 : -50,
            right: 16,
            child: RaisedButton(
              onPressed: () => widget.onApplyPressed(),
              color: MtColors.blue,
              textColor: Colors.white,
              elevation: 5,
              padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 40.0),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(100)),
              child: Text(
                "Apply".toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.bold, letterSpacing: 0.9),
              ),
            ),
          )
        ],
      ),
    );
  }
}