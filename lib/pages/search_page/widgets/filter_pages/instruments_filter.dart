import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/utils.dart';
import 'package:mt_ui/data/dummy_data.dart';
import 'package:mt_ui/pages/search_page/widgets/filter_pages/base_filter_page.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/my_card.dart';

class InstrumentsFilterPage extends StatefulWidget {
  /// The already selected instruments
  final List<String> value;

  const InstrumentsFilterPage({Key key, this.value}) : super(key: key);

  @override
  InstrumentsFilterPageState createState() => InstrumentsFilterPageState();
}


class InstrumentsFilterPageState extends State<InstrumentsFilterPage> {
  // will be populated by selected instruments
  List<String> selectedInstruments = List();

  List<Instrument> allInstruments = List();

  // List of all instruments populated from API
  List<String> instruments = List();

  bool showApplyButton = false;

  @override
  void initState() {
    super.initState();

    // Pull in instruments from Dummy object // TODO fetch from API
    instruments.addAll(DummyData.instruments);

    allInstruments = DummyData.getAllInstruments();
    print("Selected instrument: ${widget.value.length}");
    // Copy existing value
    selectedInstruments.addAll(widget.value);
  }
  Widget _body(){
    return Container(
      height: MediaQuery.of(context).size.height-100,
      child:  Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal:2.5),
            child: MyCard(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(),
                    ),
                    Expanded(
                      flex: 6,
                      child:   Center(child:  Text('SELECT INSTRUMENTS', style: MtTextStyle.appBarTitle.copyWith(fontSize: 18),),)
                    ),
                    Expanded(
                      flex: 1,
                      child:  InkWell(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        onTap: (){ Navigator.pop(context, List<String>());},
                        child: Container(
                          height: 25,
                          width: 25,
                          decoration: BoxDecoration(
                           border: Border.all(color: Colors.black87),
                           shape: BoxShape.circle
                          ),
                          child:  Icon(Icons.close),
                        ),
                      ),
                    ),
                    
                  ],),
                  SizedBox(height: 10,),
                  Material(
                    // need Material wrapper for the TextField
                    color: Colors.transparent, // get rid of Materials background color
                    child: Container(
                      margin: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: MtColors.F5F5F5),
                      child: TextField(
                        onChanged: (value) {
                          // update list based on text
                          setState(() {
                            instruments = DummyData.instruments
                                .where((string) => string
                                .toLowerCase()
                                .contains(value.toLowerCase()))
                                .toList();
                          });
                        },
                        style: TextStyle(
                            fontSize: 18, color: Colors.black87),
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.search,
                              color: Colors.black38),
                          hintText: "Search instruments",
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 5),
            child: Text('${selectedInstruments.length} selected items',style: Theme.of(context).textTheme.caption,),
          ),
          // GridView.builder(
          //          padding: EdgeInsets.all(20),
          //         //  controller: scrollController,
          //          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          //            crossAxisCount: 2,
          //            childAspectRatio: 1.55,
          //            crossAxisSpacing: 10,
          //            mainAxisSpacing: 10,
          //          ),
          //          itemCount: 6,
          //          itemBuilder: (context, index) =>  _instrument(instruments[index]),
          //        ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(top: 0,bottom: 60, left: 5,right: 5),
              child: SingleChildScrollView(
                child: Wrap(
                   children: _buildChoiceList(instruments),
                ),
              ),
            )
          ),
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
   return  Scaffold(
     backgroundColor: Colors.transparent,
     
      body:Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(bottom: 3,top:3),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white
        ),
        child:  Stack(
        children: <Widget>[
          _body(),
           Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height:40,
              width: MediaQuery.of(context).size.width *.9,
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              alignment: Alignment.center,
              child: Row(
                children: <Widget>[
                  Expanded(child: SizedBox()),
                   MyCard(
                     borderRadiusValue: 40,
                     child:
                      Row( 
                           children: <Widget>[
                           InkWell(
                              onTap: (){
                                setState(() {
                                   selectedInstruments = List.from(instruments);
                                });
                              },
                              child: Container(
                                padding:EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                                child:Text('All',style: Theme.of(context).textTheme.headline.copyWith(fontSize: 20,color: MtColors.blue),textAlign:TextAlign.center,),
                              ),
                            ),
                            InkWell(
                              onTap: (){
                                setState(() {
                                   selectedInstruments.clear();
                                });
                              },
                              child: Container(
                                padding:EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                                child:  Text('Reset',style: Theme.of(context).textTheme.headline.copyWith(fontSize: 20,color: MtColors.blue),textAlign:TextAlign.center,),
                              ),
                            ),
                             MaterialButton(
                               color:MtColors.blue,
                               padding: EdgeInsets.only(bottom: 5),
                               shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                               onPressed: (){Navigator.pop(context, selectedInstruments);},
                               child:Center(child:Text('Apply',style: Theme.of(context).textTheme.headline.copyWith(fontSize: 20,color:Colors.white),textAlign:TextAlign.center,),)
                             )
                           ],
                         ),
                   ),
                   Expanded(child: SizedBox()),
                ],
              )
              
            )
          )
        ],
      ),
      ),
    );
  }

  Widget _instrumentList() {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(), // disable scrolling because parent ListView does it
      itemCount: instruments.length,
      padding: EdgeInsets.zero,
      itemBuilder: (context, index) {

        String thisInstrument = instruments[index].toLowerCase();
        bool isSelected = selectedInstruments.contains(thisInstrument);
        return CheckboxListTile(
          title: Text(
            Utils.capitalize(thisInstrument),
            style: TextStyle(
              color: isSelected ? MtColors.blue : Colors.black87,
              fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
            ),
          ),
          value: isSelected,
          onChanged: (bool checked) {
            // Add instrument to list of selected instruments on checked
            if (checked) {
              if (!selectedInstruments.contains(thisInstrument)) {
                selectedInstruments.add(thisInstrument);
              }
            } else {
              // selectedInstruments.remove(thisInstrument);
              selectedInstruments
                  .removeWhere((instrument) => instrument == thisInstrument);
            }

            // user changed something - show apply button
            showApplyButton = true;

            // Update UI
            setState(() {});
          },
        );
      },
    );
  }
 
  List< Widget >_buildChoiceList(List<String> instrumentsList) {
   List<Widget> choices = List();
    instrumentsList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: ChoiceChip(
          backgroundColor: Colors.grey.shade100,
          selectedColor: Colors.blue,

          label: selectedInstruments.contains(item) ?  Text('$item \u2713') :  Text('$item +'),
          //  Row(children: <Widget>[
          //   Icon(Icons.check,color: Colors.black54,),
          //   Text('item'),
          // ],),
          selected: selectedInstruments.contains(item),
          onSelected: (selected) {
            setState(() {
              selectedInstruments.contains(item)
                  ? selectedInstruments.remove(item)
                  : selectedInstruments.add(item);
              onSelectionChanged(selectedInstruments);
            });
          },
        ),
      ));
    });
    var wrap = instrumentsList == null  || instrumentsList.length == 0 ?Container()
       :  Wrap(children: choices,);
     
    return choices;
  }
  
  Widget _instrument(String text){
    return Container(
        padding: const EdgeInsets.all(2.0),
        child: ChoiceChip(
          backgroundColor: Colors.grey.shade100,
          selectedColor: Colors.blue,
          label: Row(
            children: <Widget>[
              Text(text),
              SizedBox(width: 10,),
              Icon(Icons.check)
            ],
          ),
          selected: selectedInstruments.contains(text),
          onSelected: (selected) {
            setState(() {
              selectedInstruments.contains(text)
                  ? selectedInstruments.remove(text)
                  : selectedInstruments.add(text);
              onSelectionChanged(selectedInstruments);
            });
          },
        ),
      );
  }
  
  onSelectionChanged(List<String> list){

  }
}
