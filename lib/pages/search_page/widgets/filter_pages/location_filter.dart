import 'dart:collection';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mt_ui/blocs/city_suggestions_bloc.dart';
import 'package:mt_ui/data/Response/location_response.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/res/images.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/loading_indicators.dart';
import 'package:mt_ui/widgets/my_card.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:mt_ui/widgets/progress_overlay.dart';

import '../../../home_page.dart';

class LocationFilterPage extends StatefulWidget {
  final Function(int) onBottomNavIconPressed ;

  const LocationFilterPage({Key key, this.onBottomNavIconPressed}) : super(key: key);
  @override
  LocationFilterPageState createState() => LocationFilterPageState();
}

class LocationFilterPageState extends State<LocationFilterPage> {
  final CitySuggestionsBloc citySuggestionsBloc = CitySuggestionsBloc();
  CountryLocationModel locationModel;
  TextField _textField;
  TextEditingController _textFieldController;
  String query = "";
  List<String> suggestions = [];
  List<String> _lastSearchCity = [];
  BuildContext mContext;
  final Client _client = Client();
  List<Widget> searchData;
  HashMap<String, String> cityDecoder = new HashMap();
  double _lastSearchSectionHeight = 0;
  List<String> countryName = [];
  @override
  void initState() {
    super.initState();
       _textFieldController = TextEditingController();
    _getLastSearchCityName();
    searchData = [];
    _buildCityFilter();
    getMyLocation();
    SharedPreferencesHelper.getSearchFilters().then((List<String> value) {
      if (value != null) {
        this.setState(() {
          print(value);
         if (value[0] != null && value[0] != "") {
            _textFieldController.text = value[0];
        }
        });
      }
      
    });
  }
  void getMyLocation() async{
    // Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then((value){
    //     print(value);
    //    var url = citySuggestionsBloc.nearByCities(value.latitude,value.longitude);
    //    print(url);
    //     // var nearbyCities = ('http://api.geonames.org/findNearbyPlaceNameJSON?lat='.$latitude.'&lng='.$longitude.'&style='.$responseStyle.'&cities='.$citySize.'&radius='.$radius.'&maxRows='.$maxRows.'&username='.$username, true));

    // });
  }
  void _getLastSearchCityName()async{
     await SharedPreferencesHelper.getLastSearchedCities().then((val){
       if(val == null){
         return;
       }
       _lastSearchCity = val;
       print('val is not null');
       if(val != null && val.length > 3){
          _lastSearchSectionHeight = 100 + (3 * 54.0);
         _lastSearchCity = val.reversed.take(3).toList();
       }
       else{
          _lastSearchSectionHeight = 100 + (val.length  *54.0);
       }
       
     });
  }
  @override
  Widget build(BuildContext context) {
    mContext = context;
     print("initialized");
     initCityDecoder();
    ThemeData bottomNavBarTheme = Theme.of(context).copyWith(
        canvasColor: Colors.white,
        primaryColor: MtColors.blue,
        // sets the inactive color of the `BottomNavigationBar`
        textTheme: Theme.of(context).textTheme.copyWith(
                caption: TextStyle(
              color: Colors.black54,
            )));

    TextStyle tabLabelStyle = TextStyle(
      fontWeight: FontWeight.bold,
    );
    return Material(
       

    // set current page (init page is set in main.dart)
    // currentPage = pages[currentPageIndex];

    // build layout
    child: Scaffold(
      bottomNavigationBar: Theme(
        data: bottomNavBarTheme,
        child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: 0,
            onTap:(val){
              Navigator.pop(context);
              widget.onBottomNavIconPressed(val);
            },
            items: [
              BottomNavigationBarItem(
                icon: Icon(FeatherIcons.search),
                title: Text("Search", style: tabLabelStyle),
              ),
              // TODO uncomment (DD)
//               BottomNavigationBarItem(
//                 icon: Icon(FeatherIcons.compass),
//                 title: Text("Explore", style: tabLabelStyle),
//               ),
              BottomNavigationBarItem(
                icon: Icon(FeatherIcons.heart),
                title: Text("Favourites", style: tabLabelStyle),
              ),
              BottomNavigationBarItem(
                icon: Icon(FeatherIcons.bookmark),
                title: Text("Bookings", style: tabLabelStyle),
              ),
              BottomNavigationBarItem(
                icon: Icon(FeatherIcons.user),
                title: Text("Profile", style: tabLabelStyle),
              )
            ]),
      ),
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
          top: true,
          bottom: true,
          child:CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                automaticallyImplyLeading: false,
                pinned: true,
                backgroundColor: Colors.white,
                title: Container(
                   height: 50,
                   width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(left: 10,top: 10,bottom: 10),
                    child: MyCard(
                        shadow: Shadow.soft,
                        borderRadiusValue: 80,
                         child: Hero(
                           tag: "searchbar",
                           child:Padding(
                             padding: EdgeInsets.only(right: 10),
                             child:  _searchBar(),
                           )
                      ),),
                ),
                expandedHeight:_lastSearchSectionHeight,
                floating: false,
                snap: false,
                stretch: false,
                flexibleSpace:FlexibleSpaceBar(
                   centerTitle: true,
                   background:Container(
                     margin: EdgeInsets.only(top: 60,left: 0),
                     child: Column(
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: <Widget>[
                        //  ListTile(
                        //    onTap: (){},
                        //    leading: Icon(Icons.my_location,color: Colors.grey.shade600,),
                        //    title: Text('Nearby',style:Theme.of(context).textTheme.caption.copyWith(fontSize: 18),),
                        //  ),
                      _lastSearchCityWidget()
                      ],),
                   )
                ),
                actions: <Widget>[
                //  InkWell(
                //    borderRadius: BorderRadius.all(Radius.circular(50)),
                //    onTap: (){
                //      print('Done');
                //     //  getMyLocation();
                //      Navigator.of(context).pop(_textFieldController.text);
                //    },
                //   child:  Padding(
                //     padding: EdgeInsets.symmetric(horizontal: 10),
                //     child: Center(child: Text('Done', style: Theme.of(context).textTheme.headline.copyWith(fontSize: 20, color: MtColors.blue),),),
                //   )
                //  ),
                ],
              ),
              _buildSuggestions(),
           ],
          )
        ),
      ),
    );
  }
  Widget _lastSearchCityWidget(){
    if(_lastSearchCity == null || _lastSearchCity.length < 1){
      return Container();
    }
    /// [Last Search List]
    return Column(
      children: _lastSearchCity.map((x){
          return Column(
            children: <Widget>[
              InkWell(
                onTap: (){
                   _onCitySelected(x);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Icon(Icons.history),
                      ),
                      Expanded(
                        flex: 8,
                        child: Container(child: Text(x,style: Theme.of(context).textTheme.headline.copyWith(fontSize:16),overflow: TextOverflow.ellipsis,),),
                      )
                    ],
                   ),
                )
              ),
              Padding(child: Divider(height: 10,),padding: EdgeInsets.symmetric(horizontal: 20))
            ],
          );
      }).toList()
    );
  }
  Widget _searchBar() {
   _textField = TextField(
     textCapitalization: TextCapitalization.words,
     focusNode: FocusNode(),
     controller: _textFieldController,
     onChanged: _onTextChanged,
     onSubmitted:_onEnterClicked, // called when user presses ENTER on the keyboard
     style: TextStyle(fontSize: 22, color: Colors.black54, fontWeight: FontWeight.bold),
     decoration: InputDecoration(
       hintText: 'Search city',
       prefixIcon: Icon(FeatherIcons.mapPin, size: 20, color: Colors.black38),
       border: InputBorder.none,
      //  suffixIcon:InkWell(
      //    onTap: (){
      //      _textFieldController.text = '';
      //    },
      //    child:Icon(Icons.close,color:Colors.black54)
      //  )
     ),
   );

    return Material(
      // need Material wrapper for the TextField
      color: Colors.transparent, // get rid of Materials background color
      child: Container(
        child: _textField,
        decoration: BoxDecoration(
            border:
                Border(bottom: BorderSide(color: MtColors.EEEEEE, width: 2.0))),
      ),
    );
  }

  // add new query on text change
 void _onTextChanged(String value) {
   if(value == null || value.isEmpty){
     return ;
   }
   query = _textFieldController.text;
   citySuggestionsBloc.query.add(query);
   citySuggestionsBloc.getSuggestions(citySuggestionsBloc.constructQuery(_textFieldController.text)).then((value){
   if(value.length >0)
       setState(() {
         if(!_lastSearchCity.any((x)=>x == value[0])){
             _lastSearchCity.add(value[0]);
              
           _lastSearchCity = _lastSearchCity.reversed.toList();
            if(_lastSearchCity.length >= 3){
               _lastSearchCity = _lastSearchCity.take(3).toList();
                _lastSearchSectionHeight = 100 + (3 * 48.0);
            }
            else{
               _lastSearchSectionHeight = 100 + (_lastSearchCity.length * 48.0);
            }
         }
      });
   });
 }

  void _onCitySelected(String value){
    // update text field value
   _textFieldController.text = value;

    // Save to prefs the searched city
    if (value != "Anywhere") {
      SharedPreferencesHelper.setLastSearchedCity(value);
    }
    
    // TODO add tracking

    // return results
    Navigator.pop(mContext, value);
  }

 void _onEnterClicked(String value) {
   // Select top suggestion if any
   print(value);
   if (suggestions != null && suggestions.length != 0) {
     _onCitySelected(suggestions[0]);
   }
   citySuggestionsBloc.getSuggestions(citySuggestionsBloc.constructQuery(value)).then((value){
     print(value);
   });
 }
  
  // _fetchPage(int pageNumber, int pageSize) async {
  _buildCityFilter() async {
    List<Widget> countriesAndCities = new List();
    var query = '''
      query {
        searchDropdownData {
          countries {
            code
            cities {
              id
              name
              statusCode
            }
          }
        }
      }
    ''';

    log('query: $query');

    Uri getUrlForQuery(String query) => Uri.http(
        MtConstants.baseUrl, "/graphql", {"query": query});
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
    };

    try{
        await _client
        .get(getUrlForQuery(query), headers: headers)
        .then((result) {
          var body = result.body;
          return body;
        })
        .then(json.decode)
        .then((json) {
             setState(() {
             });
             locationModel = CountryLocationModel.fromJson(json);
             print('Data added');
         });
    }catch(error){
        print('[Erorr] in location_filter.dart:- $error');
    }
 }
  bool addbool(int index) {
    return false;
  }
 List<Widget> _countryList(){
    List<Widget> countriesAndCities = new List();
    if(locationModel != null){
      countriesAndCities.add(
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
          child:  Text('Choose a city',style: Theme.of(context).textTheme.headline.copyWith(fontSize:20),),
        )
      );
    }
    countriesAndCities.add(Padding(child: Divider(),padding: EdgeInsets.symmetric(horizontal: 20),));
    locationModel.data.searchDropdownData.getOrderedCountry().forEach((country){
      List<LocationCard> cards = List();
       country.getOrderedCityList().forEach((city) {
            if (city.statusCode<= 3 && city.statusCode != 0) {
              cards.add(LocationCard(
                  name: "${city.name}, ${country.code}",
                  imageUrl: "#",
                  newCity: city.statusCode == 2 || city.statusCode == 3));
            }
          });
      var widget1 = AnimatedContainer(
              duration: Duration(milliseconds: 300),
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
              margin: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(country.isCitiesVisible? 0 : 30),),
                  // boxShadow: <BoxShadow>[ 
                  //      BoxShadow(  offset: Offset(0, 0),blurRadius: 15,color: MtColors.black7,),
                  //    ],
                  // color: Colors.white
                ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                 InkWell(
                   onTap: (){
                     setState(() {
                       country.isCitiesVisible = !country.isCitiesVisible;
                      });
                   },
                   child:  Row(
                    children: <Widget>[
                      CircleAvatar(
                        radius: 20,
                        child: Image.asset(MtImages.getCountryImage(cityDecoder[country.code]))
                        ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: 30),
                          child: Text(country.name,style: Theme.of(context).textTheme.headline.copyWith(fontSize:18, fontWeight: FontWeight.bold),),)
                      ),
                      Icon(country.isCitiesVisible ?Icons.keyboard_arrow_up : Icons.keyboard_arrow_down),
                    ],
                  ),
                 ),
                  AnimatedContainer(
                    duration: Duration(milliseconds: 500),
                     child: _locationsList(cards,visibility:country.isCitiesVisible),
                   
                  )],
              ),
            );
            countriesAndCities.add(widget1);
            countriesAndCities.add(Padding(child: Divider(),padding: EdgeInsets.symmetric(horizontal: 20),));
    });
  
    return countriesAndCities;
 }
 Widget _buildSuggestions() {
    return StreamBuilder<List<String>>(
      stream: citySuggestionsBloc.results,
      builder: (context, snapshot) {
        // Show popular cities and recent searches, before user starts typing
        // print(jsonData);
        if (locationModel != null && locationModel.data != null) {
          return 
          SliverList(
            delegate: SliverChildListDelegate(
              //  searchData,
              _countryList()
            
            ),
          );
        }
       else{
           _buildCityFilter();
        return 
          SliverList(
            delegate: SliverChildListDelegate(
             [
                Container(
                  height: MediaQuery.of(context).size.height - 150,
                  child: Center(
                    child: InstrumentsIndicator(color: Colors.black54,)
                  ),
                )
             ]
            ),
          );
       }
        // if loading (only first load)
        // if (searchData.length == 0) {
        //   _buildCityFilter();
        //   return Center(
        //     child: Opacity(opacity: 0.25, child: CircularProgressIndicator()),
        //   );
        // }

        // Show suggestions
      //   return ListView.builder(
      //     shrinkWrap: true,
      //     itemCount: snapshot.data.length,
      //     itemBuilder: (BuildContext context, int index) {
      //       String suggestion = snapshot.data[index];
      //       return ListTile(
      //         onTap: () {
      //           _onCitySelected(suggestion);
      //         },
      //         // fire icon when showing popular cities
      //         leading: Icon(Icons.location_city),
      //         // Make the query part of the suggestion bold
      //         title: RichText(
      //           text: TextSpan(
      //               text: suggestion.substring(0, query.length),
      //               style: TextStyle(
      //                   fontSize: 16,
      //                   color: Colors.black87,
      //                   fontWeight: FontWeight.bold),
      //               children: [
      //                 TextSpan(
      //                   text: suggestion.substring(query.length),
      //                   style: TextStyle(
      //                       color: Colors.black54,
      //                       fontSize: 16,
      //                       fontWeight: FontWeight.normal),
      //                 )
      //               ]),
      //         ),
      //       );
      //     },
      //   );
       },
    );
  }

  Widget _chip(String text, Color color){
    return Container(
      padding: EdgeInsets.only(left: 10,right: 10,bottom: 3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        color: color
      ),
      child: Text(text,style:TextStyle(color: Colors.white,fontSize: 12, fontWeight: FontWeight.bold)),
    );
  }
  Widget _locationsList(List<LocationCard> items,{bool visibility = false}) {
    if(!visibility){
      // print('City hide ${items[0].name}');
      return Container();
    }
     var listWidget  = items.map((x){
      return ListTile(
         onTap: (){
            print(x.name);
            _onCitySelected(x.name);
         },
          leading: Container(width: 40,height: 40,),
          title:  Row(
             children: <Widget>[
               Text(x.name,style: Theme.of(context).textTheme.headline.copyWith(fontSize:16),textAlign: TextAlign.center),
               SizedBox(width: 20,),
               x.newCity ? _chip('New', Colors.pink)
               : x.comingSoon ? _chip('New', Colors.grey.shade700,)
               : Container()
             ],
           ),
       );
       }).toList();
    //  listWidget.add(Container(child: Divider(),));
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            children: listWidget,
          ),
          // Divider()
        ],
        );
}

  // Only show first part of city name, i.e. until the first comma
  String _getPopularLocationDisplayName(String value) {
    if (value.contains(",")) {
      return value.substring(0, value.indexOf(","));
    }

    return value;
  }

//  Widget _buildRecentSearches(int indexStart, int indexEnd) {
//    return FutureBuilder(
//      future: SharedPreferencesHelper.getLastSearchedCities(),
//      builder: (context, snapshot) {
//        if (!snapshot.hasData) {
//          return Container();
//        }
//
//        // reverse order (most recent at the top)
//        List<String> cities = snapshot.data.reversed.toList();
//
//        // remove duplicates
//        cities = cities.toSet().toList();
//
//        // remove start of list if specified
//        if (indexStart != null) {
//          cities.removeRange(0, indexStart);
//        }
//
//        // remove end of list if specified
//        if (indexEnd != null) {
//          cities.removeRange(indexEnd, cities.length);
//        }
//
//        return ListView.builder(
//            padding: EdgeInsets.zero,
//            shrinkWrap: true,
//            itemCount: cities.length < 3 ? cities.length : 3, // max length 3
//            itemBuilder: (context, index) {
//              return ListTile(
//                onTap: () => _onCitySelected(cities[index]),
//                leading: Icon(Icons.access_time),
//                title: Text(cities[index]),
//              );
//            });
//      },
//    );
//  }
 void initCityDecoder(){
   cityDecoder["AT"] = "Austria";
    cityDecoder["DE"] = "Germany";
    cityDecoder["CH"] = "Switzerland";
    cityDecoder["US"] = "United States";
    cityDecoder["ES"] = "Spain";
    cityDecoder["IT"] = "Italy";
    cityDecoder["BE"] = "Belgium";
    cityDecoder["PT"] = "Portugal";
    cityDecoder["DK"] = "Denmark";
    cityDecoder["LU"] = "Luxembourg";
    cityDecoder["PL"] = "Poland";
    cityDecoder["JP"] = "Japan";
    cityDecoder["CA"] = "Canada";
    cityDecoder["KR"] = "Korea";
    cityDecoder["NL"] = "Netherlands";
    cityDecoder["FI"] = "Finland";
    cityDecoder["MT"] = "Malta";
    cityDecoder["IS"] = "Iceland";
    cityDecoder["IE"] = "Ireland";
    cityDecoder["FR"] = "France";
    cityDecoder["HK"] = "Hong Kong";
    cityDecoder["SE"] = "Sweden";
    cityDecoder["NO"] = "Norway";
    cityDecoder["GB"] = "Great Britain";
 }
}

class LocationCard {
  final String name;
  final String imageUrl;
  final bool comingSoon;
  final bool pilotCity;
  final bool newCity;

  LocationCard({
    @required this.name,
    @required this.imageUrl,
    this.comingSoon = false,
    this.pilotCity = false,
    this.newCity = false,
  });
}
