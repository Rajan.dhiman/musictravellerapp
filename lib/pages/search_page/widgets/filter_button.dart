import 'package:flutter/material.dart';
import 'package:mt_ui/utils.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/icon_with_text.dart';

class FilterButton extends StatefulWidget{

  final VoidCallback onPressed;
  final String text;
  final IconData activeIcon;
  final bool isActive;

  final String assetImage;
  final Color assetImageColor;
  final double assetImageSize;

  const FilterButton({
    Key key,
    @required this.text,
    @required this.activeIcon,
    @required this.onPressed,
    @required this.isActive,
     this.assetImage, 
     this.assetImageColor, 
     this.assetImageSize,
  }) : super(key: key);

  @override
  FilterButtonState createState() => FilterButtonState();
}

class FilterButtonState extends State<FilterButton> with TickerProviderStateMixin{

  @override
  Widget build(BuildContext context) {

    Widget text = Text(
      Utils.capitalize(widget.text),
      style: TextStyle(
          color: widget.isActive ? MtColors.blue : Colors.black54,
          fontSize: 15,
          fontWeight: FontWeight.bold),
    );

    return RawMaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
      onPressed: widget.onPressed,
      fillColor: widget.isActive ? MtColors.blue10 : MtColors.F5F5F5,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      elevation: 0,
      child: AnimatedSize(
        vsync: this,
        duration: Duration(milliseconds: 350),
        child: widget.isActive ? _activeText(widget.text, widget.activeIcon) : text,
      ),
    );
  }

  Widget _activeText(String activeTitle, IconData activeIcon){
    // if(activeIcon == null){
    //   return Text(Utils.capitalize(activeTitle),style:TextStyle(color: MtColors.blue),);
    // }
    return IconWithText(
      icon: activeIcon,
      text: Utils.capitalize(activeTitle),
      textColor: MtColors.blue,
      iconSpacing: 8,
      shrinkWrap: true,
      assetImage:widget.assetImage,
      assetImageSize: widget.assetImageSize,
      assetImageColor: widget.assetImageColor,
    );
  }
}