import 'dart:developer';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:mt_ui/data/currencyConversionHelper.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/pages/map_view/widget/mapAppbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mt_ui/blocs/spaces_bloc.dart';
import 'package:mt_ui/data/dummy_data.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:mt_ui/pages/search_page/widgets/filter_button.dart';
import 'package:mt_ui/pages/search_page/widgets/filter_pages/instruments_filter.dart';
import 'package:mt_ui/pages/search_page/widgets/filter_pages/price_filter.dart';
import 'package:mt_ui/pages/space_detail_page/space_detail_page.dart';
import 'package:mt_ui/providers/spaces_provider.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/res/images.dart';
import 'package:mt_ui/res/mt_styles.dart';
import 'package:mt_ui/widgets/icon_with_text.dart';
import 'package:mt_ui/widgets/loading_indicators.dart';
import 'package:mt_ui/widgets/my_card.dart';
import 'package:mt_ui/widgets/progress_overlay.dart';
import 'package:mt_ui/widgets/search_bar.dart';
import 'package:mt_ui/widgets/space_card_room.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'dart:io';

// Page widget
class SearchPage extends StatefulWidget {
  final String cityFilter;
  final bool isFromCheckBookingPage;
  final Function(int) onBottomNavIconPressed ;
  SearchPage({Key key, this.cityFilter, this.onBottomNavIconPressed, this.isFromCheckBookingPage = false}) : super(key: key);

  @override
  SearchPageState createState() {
    return new SearchPageState();
  }
}

class SearchPageState extends State<SearchPage> {
   Client _client;

  // @override bool get wantKeepAlive => true;

  ScrollController _controller;
  final _rooms = <ListingRoom>[];
  var service = new CurrencyConvertService();
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<Space> spaces;
  int resultsCount = -1;
  int currentPage = 0;
  bool dollerSelected = false;
  bool euroSelected = false;
  bool poundSelected = false;

  // View mode
  bool listViewMode = true;

  bool loading = true;
  bool fetchPage = false;

  // Show grid view when screen is wider than 500px
  static const double GRID_MIN_WIDTH = 500;

  // Sorting
  SortingOptions sortBy = SortingOptions.spaceName;

  // Filters
  String cityFilter;
  List<String> instrumentFilter = List();
  PriceRange priceFilter = PriceRange();
  DateTime dateFilter;// = DateTime(0); // DateTime(0) means filter not enabled

  @override
  void initState() {
    _client = Client();
    WidgetsBinding.instance
        .addPostFrameCallback((_)  { 
          if(widget.isFromCheckBookingPage){
             SharedPreferencesHelper.getRoomId().then((value){
               if(value == null){
                 return;
               }
               SharedPreferencesHelper.removeRoomId();
               
                 Navigator.push(context, MaterialPageRoute(
                   // add route name so we can pop back to this page from booking success page
                   settings: const RouteSettings(name: '/space-page'),
                   builder: (_) {
                     return SpaceDetailPage(
                      isFromCheckBookingPage: widget.isFromCheckBookingPage,
                      spaceId: value,
                     );
                   },
                 ));
                 
             });
            
          }
        });
    // fetchSelectedCurrency();
    _initiliseApiUtilities();
    _controller = _controller = ScrollController()
      ..addListener(() {
        if (_controller.position.pixels ==
            _controller.position.maxScrollExtent) {
          // setState(() {
            currentPage = currentPage + 1;
            // loading = true;
            fetchPage = true;
          // });
          _fetchPage(currentPage);
        }
      });
    super.initState();

    cityFilter = widget.cityFilter != null &&  widget.cityFilter.isNotEmpty ?  widget.cityFilter : "Vienna, AT";

    // apply last used search filters
    SharedPreferencesHelper.getSearchFilters().then((List<String> value) {
      if (value != null) {
        this.setState(() {
          print(value);
         applyLastFilters(value);
        });
      }
      
      
       _fetchPage(0);
      
    });
  } 
   @override
   void dispose() {
     _client.close(); 
     _controller.dispose();
     super.dispose();
   }
  void fetchSelectedCurrency(){
    SharedPreferencesHelper.getCurrencyPreference().then((value){
       switch (value) {
         case "USD":
            dollerSelected = true; 
            euroSelected = false; 
            poundSelected = false;
         break;
         case "EUR": 
            euroSelected = true; 
            poundSelected = false;
            dollerSelected = false;
           break;
         case "GBP":
          poundSelected = true; 
          euroSelected = false; 
          dollerSelected = false;
          break;
         default:
       }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
//      //TODO DD uncomment map button
//      floatingActionButton: FloatingActionButton(
//        heroTag: "map_fab",
//        child: Icon(Icons.place),
//        onPressed: _onFabPressed,
//      ),
      // appBar: MapViewAppBar(),
      body: CustomScrollView(controller: _controller, slivers: <Widget>[
        SliverAppBar(
          automaticallyImplyLeading: false,
          floating: true,
          pinned: false,
          snap: true,
          backgroundColor: Color(0xffFEFEFE),
          title: SearchBar(
            onBottomNavIconPressed:(val){
                  widget.onBottomNavIconPressed(val);
            },
            currentCity: cityFilter,
            newItems: _rooms,
            onResult: (String result) {
              if(result.isEmpty){
                cityFilter = null;
                
                _fetchPage(0);
                
                saveLastFilters();
              }
              else{
                  // update city filter
                cityFilter = result;
                // save latest filters
                saveLastFilters();
                // Fetch spaces again

                if(cityFilter != null && cityFilter.contains("New York")){
                  /// If city name id New York then
                  /// 1. Get currency rate in usd
                  /// 2. Fetch space list containing New york
                  /// 3. Convert space price in usd
                  /// 4. Display space list on screen
                  setUSDforNewYorkAndLoadSpace();
                }
                else{
                  /// If city is name is not New New York then
                  /// 1. Get currency rate in Euro
                  /// 2. Fetch space list contianing `cityFilter` name
                  /// 3. Convert space price in Euro
                  /// 4. Display space list on screen
                   setEuroCurrencyAndFetchFilterList();
                }
                // _reload();
              }
            
              
            },
          ),
          bottom: PreferredSize(
            preferredSize: Size(double.infinity, (88.0 + 16.0)),
            child: _filtersSection(context),
          ),
        ),
        loading ? SliverFillRemaining(
          child: InstrumentsIndicator(color: Colors.black26),
        )
        : SliverPadding(
            padding: EdgeInsets.only(left: 16, top: 16, right: 16, bottom: 16),
            sliver: SliverList(
              delegate: SliverChildListDelegate(
                loading ? [
                  Center(
                   child: InstrumentsIndicator(color: Colors.black54,),
                 )
               ]
              : !loading && _rooms.length == 0 ? 
              [ Center(
                 child: Padding(
                   padding: EdgeInsets.all(32.0),
                   child: MyCard(
                     padding: EdgeInsets.all(48),
                     child: Center(
                       child: Text(
                         "no search results",
                       ),
                     ),
                   ),
                 )
               )]
              : _rooms != null && _rooms.length > 0 ?
                _rooms.map((space) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 16.0),
                      child: SpaceCardRoom(
                        space: space,
                        listViewMode: listViewMode,
                      ),
                    );
                  }).toList()
                  : [SizedBox()],
                addAutomaticKeepAlives: false,
              ),
            
              // delegate: SliverChildBuilderDelegate(
              //   (BuildContext context, int pageNumber) {
              //    if (pageNumber > 0) return null;
              //    return   _buildPage();
              //    }),
            ),
          )
      ]),
    );
  }
   List<ListingRoom> newItems = [];
   List<int> instrumentFilterInt = new List();
    List<int> instrumentIds = DummyData.instrumentIds;
    List<String> instrumentStrings = DummyData.instruments
        .map((instrument) => instrument.toLowerCase())
        .toList();
  Widget _buildPage() {
    if(loading){
        return Center(
          child: InstrumentsIndicator(color: Colors.black54,),
        );
    }
    else if (!loading && _rooms.length == 0) {
      return Center(
        child: Padding(
          padding: EdgeInsets.all(32.0),
          child: MyCard(
            padding: EdgeInsets.all(48),
            child: Center(
              child: Text(
                "no search results",
              ),
            ),
          ),
        )
      );
    }

    return ListView(
        shrinkWrap: true,
        primary: false,
        addAutomaticKeepAlives: false,
        children: _rooms.map((space) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: SpaceCardRoom(
              space: space,
              listViewMode: listViewMode,
            ),
          );
        }).toList());
  }
  String accessToken ,currencyPref;
  double currencyPrice;
  Map<String, String> headers ;
  _initiliseApiUtilities()async{
     accessToken = await SharedPreferencesHelper.getAccessToken() ?? '';
     headers = {
      HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
    };
     currencyPrice = await SharedPreferencesHelper.getCurrencyRate();
     currencyPref = await SharedPreferencesHelper.getCurrencyPreference();
  }
  int roomcount = 0;
   void setUSDforNewYorkAndLoadSpace(){
      dollerSelected = false;
      euroSelected = false;
      poundSelected = false;
      /// get currency rate and reload api
      // callCurencyChangeApi();
      SharedPreferencesHelper.initCurrencyPreferences(currency:'USD');
      _reload();
   }
   void setEuroCurrencyAndFetchFilterList(){
      dollerSelected = false;
      euroSelected = false;
      poundSelected = false;
      /// get currency rate and reload api
      SharedPreferencesHelper.initCurrencyPreferences(currency:'EUR');
      _reload();
      // callCurencyChangeApi();
   }
  _fetchPage(int pageNumber) async {
    if(pageNumber > 1 && roomcount <= pageNumber * 10 && roomcount == _rooms.length){
      log("Data Overloaded page number $pageNumber Total item:= $roomcount" );
     return ; 
    }

    
    setState(() {
      if(pageNumber < 1){
        loading = true;
      }
    });
    

    int priceFrom =
        (this.priceFilter.lower == null) ? 0 : this.priceFilter.lower.toInt();
    int priceTo = (this.priceFilter.higher == null)
        ? 1000000
        : this.priceFilter.higher.toInt();
    int sortBy = (this.sortBy.index == null) ? 2 : this.sortBy.index.toInt();
    instrumentFilterInt.clear();
    if(instrumentFilter != null && instrumentFilter.isNotEmpty){
       instrumentFilterInt.clear();
       this.instrumentFilter.forEach((instrument) {
         print('instrument:- $instrument');
         var index = instrumentStrings.indexOf(instrument.toLowerCase());
         print('Search at index:- $index');
         print('Find index of:- ${instrumentStrings[index]} at index:- $index');

         instrumentFilterInt
             .add(instrumentIds[index]);
       });
    }
   
    
    var query = '''
      query {
        searchCount (page:$pageNumber, pageSize:10, filter: {priceFrom: $priceFrom, priceTo: $priceTo, instrumentId:$instrumentFilterInt, cityId:"${this.cityFilter != null && this.cityFilter != "Anywhere" ? this.cityFilter.split(',')[0] : null}"}, sort:$sortBy) {
          rooms{
            id
            title
            place{
              country
              city
              coordinates {
                longitude
                latitude
              }
            }
            instruments {
              instrument {
                id
                name
              }
            }
            coverImage {
              id
              objectId
              attachmentFile
              order
            }
            listingPrice {
              price
              currency
            }
            isFavorite
          },
          count
        }
      }
    ''';

    log('query: $query');
     print( MtConstants.baseUrl);
    Uri getUrlForQuery(String query) => Uri.http(
        MtConstants.baseUrl, "/graphql", {"query": query});

         print("baseUrl  ${MtConstants.baseUrl}");
    // String accessToken = await SharedPreferencesHelper.getAccessToken() ?? '';
    // Map<String, String> headers = {
    //   HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
    // };
    var currencyPrice = await SharedPreferencesHelper.getCurrencyRate();
    var currencyPref = await SharedPreferencesHelper.getCurrencyPreference();
    var url  = getUrlForQuery(query);
    try{
       await _client
        .get(url, headers: headers)
        .then((result) => result.body)
        .then(json.decode)
        .then((json) {
          if(json != null){
              if(json['data']['searchCount'] != null){
                roomcount = json['data']['searchCount']['count'];
                json['data']['searchCount']['rooms'].forEach((room) {
                  _rooms.add(ListingRoom.fromAPIJson(room, curencyPrefrence: currencyPref, rate: currencyPrice));
                  // print(newItems.last.location.city);
                });
                resultsCount = json['data']['searchCount']['count'];
                 setState(() {
                  //  _rooms.addAll(newItems);
                   loading = false;
                   if(_rooms.length >0){
                    //  resultsCount = _rooms.length;
                   }     
                   else{
                     resultsCount = -1;
                   }
                   loading = false;
                 });
                }
           }
          //  setState(() {
          //    loading = false;
          //  });
         });
        
    }catch(error){
      setState(() {
        loading = false;
      });
       log('[Erorr] in search_page.dart:', error: "$error");
    }
  }

  Widget _filtersSection(context) {
    // Widget resultsCountWidget = Container();
    // if(resultsCount == -1){
    //   resultsCountWidget = Wrap(
    //     children: [
    //       SizedBox(
    //         child: Padding(
    //           padding: const EdgeInsets.all(4),
    //           child: CircularProgressIndicator(
    //             strokeWidth: 1,
    //           ),
    //         ),
    //         height: 20,
    //         width: 20,
    //       ),
    //       SizedBox(width: 4),
    //       Text("Loading...", style: Theme.of(context).textTheme.subtitle.copyWith(fontWeight: FontWeight.bold, color: Colors.black54)),
    //     ],
    //   );
    // }else{
    //   resultsCountWidget = Text("$resultsCount spaces found", style: Theme.of(context).textTheme.subtitle.copyWith(fontWeight: FontWeight.bold, color: Colors.black54));
    // }
   var title = resultsCount < 1 ? 'Explore' : '$resultsCount Spaces';
    return Container(
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(color: MtColors.EEEEEE, width: 1),
      )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 40,
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 16),
              scrollDirection: Axis.horizontal,
              children: _filters(),
            ),
          ),
          Container(
            height: 48,
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // resultsCountWidget,

                // Spacer
                
                Text(title,style: TextStyle(color:Colors.black54,fontWeight: FontWeight.bold,fontSize: 12),),
                Expanded(child: Container()),

                // sort button
                _sortButton(),

                // view mode button
                // _currencyConvertButton(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _reload() {
    setState(() {
      _rooms.clear();
      currentPage = 0;
    });
    _fetchPage(currentPage);
  }

  void applyLastFilters(List<String> value) {
    // City
    if (value[0] != null && value[0] != "") {
      cityFilter = value[0];
    }

    // date
    if (value != null && value.length > 1 && value[1] != null && value[1].isNotEmpty && value[1] != "") {
      dateFilter = DateTime.parse(value[1]);
    }

    // price
    if (value != null && value.length > 2 && value[2] != null && value[2] != "") {
    // priceFilter = PriceRange.parse(value[2]);
    }

    // instruments
    if (value != null && value.length > 3 &&  value[3] != null && value[3] != "") {
      List<String> instruments =
          value[3].replaceAll("[", "").replaceAll("]", "").split(", ");

      if (instruments.isNotEmpty) {
        instrumentFilter = instruments;
      }
    }
  }

  void saveLastFilters() {
    List<String> filters = List();
    filters.add(cityFilter ?? ""); // city
   // filters.add(dateFilter != DateTime(0) ? dateFilter.toString() : ""); // date
    filters.add(priceFilter.isActive() ? priceFilter.toString() : ""); // price
    filters.add(instrumentFilter.isNotEmpty
        ? instrumentFilter.toString()
        : ""); // instruments
    SharedPreferencesHelper.setSearchFilters(filters);
  }

  Widget _sortButton() {
    return PopupMenuButton<SortingOptions>(
      tooltip: "Sort by...",
      icon: Icon(
        Icons.sort,
        color: Colors.black54,
        //size: 19
      ),
      onSelected: (SortingOptions result) {
        print(result);
        setState(() {
          sortBy = result;
        });
        // save latest filters
        saveLastFilters();
        _reload();
      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<SortingOptions>>[
        const PopupMenuItem<SortingOptions>(
          enabled: false,
          child: ListTile(
            title: Text("Sort by...", style: MtTextStyle.bold),
          ),
        ),
        const PopupMenuDivider(),
        _sortItem(SortingOptions.spaceName, "Space name"),
        _sortItem(SortingOptions.priceIncreasing, "Price (low to high)"),
        _sortItem(SortingOptions.priceDecreasing, "Price (high to low)"),
      ],
    );
  }

  // Widget _currencyConvertButton() {
  //   return Container(
  //     height: 30,
  //     width: 60,
  //     padding: EdgeInsets.symmetric(horizontal: 2),
  //     alignment: Alignment.centerLeft,
  //     decoration: BoxDecoration(
  //       borderRadius: BorderRadius.all(Radius.circular(20)),
  //       color: Colors.grey.shade400
  //     ),
  //     child: Stack(
  //       alignment: Alignment.centerLeft,
  //       children: <Widget>[
  //         AnimatedContainer(
  //           duration: Duration(milliseconds: 500),
  //           child: CircleAvatar(
  //             radius: 13,
  //             backgroundColor: Colors.white,
  //             child: Text('\$',style: TextStyle(color: Colors.grey.shade400),),
  //           ),
  //         ),
  //         Align(
  //           alignment: Alignment.centerRight,
  //           child: Padding(
  //             padding: EdgeInsets.only(right: 3),
  //             child: Text('EUR',style: TextStyle(color: Colors.white, fontSize: 12),),
  //           ),
  //         )
  //       ],
  //     )
  //   );
  //   // return Tooltip(
  //   //   message: "Switch view",
  //   //   child: IconButton(
  //   //     onPressed: () {
  //   //       setState(() {
  //   //         listViewMode = !listViewMode;
  //   //       });

  //   //       // save in prefs
  //   //       SharedPreferencesHelper.setSearchPageListMode(listViewMode);
  //   //     },
  //   //     icon: Icon(
  //   //       _getViewModeIcon(),
  //   //       color: Colors.black54,
  //   //       size: 20,
  //   //     ),
  //   //   ),
  //   // );
  // }

  Widget _sortItem(SortingOptions value, String label) {
    return PopupMenuItem<SortingOptions>(
      value: value,
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        leading: Radio(
          value: value,
          groupValue: sortBy,
          onChanged:
              (value) {
                 print(value);
                  setState(() {
                    sortBy = value;
                  });
                  // save latest filters
                  saveLastFilters();
                  _reload();
                  Navigator.pop(context);
              }, // value changed in PopupMenuButton callback above
        ),
        title: Text(label),
      ),
    );
  }

  // IconData _getViewModeIcon() {
  //   if (listViewMode) {
  //     if (MediaQuery.of(context).size.width > GRID_MIN_WIDTH) {
  //       return FontAwesomeIcons.thLarge;
  //     } else {
  //       return Icons.view_agenda;
  //     }
  //   } else {
  //     return FontAwesomeIcons.thList;
  //   }
  // }

  String _instrumentsFilterText() {
   
    if (instrumentFilter.length == 0) {
      return "Instruments";
    } else if (instrumentFilter.length == 1) {
      return instrumentFilter
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    } else {
      return "${instrumentFilter.length} instruments";
    }
  }

  List<Widget> _filters() {
    return [
      // TODO uncomment
      // Date filter
      // Container(
      //   margin: EdgeInsets.only(right: 8),
      //   child: FilterButton(
      //     text: _dateFilterText() ?? "Date",
      //     activeIcon: Icons.date_range,
      //     isActive: (_dateFilterText() != null),
      //     onPressed: () async {

      //       // Open filter edit page and wait for result
      //       final DateTime selectedDate = await Navigator.push(context,
      //         MaterialPageRoute(
      //           builder: (context) => DateFilterPage(initialDate: dateFilter),
      //           fullscreenDialog: true,
      //         ),
      //       );

      //       // Result is null when not saving changes from filter (e.g. back button)
      //       if(selectedDate == null)
      //         return;

      //       // Update filter
      //       setState(() {
      //         dateFilter = selectedDate;
      //       });

      //       // save latest filters
      //       saveLastFilters();

      //       // Make new query // TODO add API query here
      //       SpacesProvider.of(context).query.add(SpaceBloc.defaultQuery);
      //     },
      //   ),
      // ),

      // Price filter
      Container(
        margin: EdgeInsets.only(right: 8),
        child: FilterButton(
        
          text: dateFilter == null ? 'Date' : DateFormat('MMM dd').format(dateFilter),//priceFilter.getPriceRangeDisplay() ?? 'price',
          activeIcon: null,
          isActive: dateFilter != null,
          onPressed: () async {
            // Open filter edit page and wait for result
            // final result = await Navigator.push(
            //   context,
            //   MaterialPageRoute(
            //     builder: (context) => PriceFilterPage(priceRange: priceFilter),
            //     fullscreenDialog: true
            //   ),
            // );
               print('open calender');
               showDialog(
                 context: context,
                 builder: (context)=> _calender()
               );
          },
        ),
      ),

      // Instruments filter
      Container(
        margin: EdgeInsets.only(right: 8),
        child: FilterButton(
          text: _instrumentsFilterText(),
          activeIcon: Icons.music_note,
          isActive: instrumentFilter.isNotEmpty,
          onPressed: () async {
            await showDialog(
              context: context,
              builder: (BuildContext context) {
                return Dialog(
                  elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  child:Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(40))
                    ),
                    height: MediaQuery.of(context).size.height * .8,
                    width:  MediaQuery.of(context).size.width ,
                    child:   InstrumentsFilterPage(value: instrumentFilter),
                  )
                );
              }
            ).then((result){
             // Result is null when not saving changes from filter (e.g. back button)
              if (result == null) return;

              // Update filter
              setState(() {
                instrumentFilter = result;
              });
            });
            // save latest filters
            saveLastFilters();
            _reload();
            // Make new query // TODO add API query here
            SpacesProvider.of(context).query.add(SpaceBloc.defaultQuery);
          },
        ),
      ),

      // Currency filter
      Container(
        margin: EdgeInsets.only(right: 8),
        child: FilterButton(
          text: 'Currency',
          assetImage: dollerSelected ? MtImages.doller : poundSelected ? MtImages.pound : euroSelected ? MtImages.euro : null,
          assetImageColor: MtColors.blue,
          activeIcon: null,
          isActive: dollerSelected ? true : poundSelected ? true : euroSelected ? true : false,
          onPressed: () async {
            await showDialog(
              context: context,
              useRootNavigator: true,
              builder: (BuildContext context) {
                return Dialog(
                  elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  child:
                  Container(
                     decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(40))
                    ),
                    height: 180,
                    width:  MediaQuery.of(context).size.width ,
                    child:  Container(
                       padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20))
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          MyCard(
                              shadow: Shadow.none,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(16, 0, 16, 8),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Container(),
                                      ),
                                      Expanded(
                                        flex: 7,
                                        child:   Center(child:  Text('SELECT CURRENCY', style: MtTextStyle.appBarTitle.copyWith(fontSize: 18),),)
                                      ),
                                      Expanded(
                                        flex:
                                         1,
                                        child:  InkWell(
                                          borderRadius: BorderRadius.all(Radius.circular(30)),
                                          onTap: (){ Navigator.pop(context, List<String>());},
                                          child: Container(
                                            height: 25,
                                            width: 25,
                                            decoration: BoxDecoration(
                                             border: Border.all(color: Colors.black87),
                                             shape: BoxShape.circle
                                            ),
                                            child:  Icon(Icons.close),
                                          ),
                                        ),
                                      ),
                                      
                                    ],),
                                  ],
                                ),
                              ),
                            ),
                         Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              _currency(MtImages.doller,"Dollar",dollerSelected,1),
                              _currency(MtImages.euro,"Euro",euroSelected,2),
                            ],
                          ),
                         _currency(MtImages.pound,"Pound",poundSelected,3),
                        ],
                      ),
                    ),
                  )
                );
              }
            );
            // .then((result){
            //  // Result is null when not saving changes from filter (e.g. back button)
            //   if (result == null) return;

            //   // Update filter
            //   setState(() {
            //     instrumentFilter = result;
            //   });
            // });
            // save latest filters
            // saveLastFilters();
            // _reload();
            // Make new query // TODO add API query here
            SpacesProvider.of(context).query.add(SpaceBloc.defaultQuery);
          },
        ),
      ),
    ];
  }
  Widget _currency(String image,String text, bool selected, int index){
    return  InkWell(
      onTap: (){
        Navigator.pop(context);
        setState(() {
           dollerSelected = index == 1 ? true : false;
           euroSelected = index == 2 ? true : false;
           poundSelected = index == 3 ? true : false;
        });
        callCurencyChangeApi();
      },
     
      child:  Container(
        width: (MediaQuery.of(context).size.width - 50) / 3,
        padding: EdgeInsets.symmetric(horizontal: 10,vertical:5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
         border: Border.all(color: selected ?  Colors.transparent : Colors.black54),
         color: selected ? MtColors.blue : Colors.transparent
        ),
        child: IconWithText(
          textColor:  selected ? Colors.white : Colors.black54,
          text: text,
          assetImage: image,
          assetImageSize: 27,
          assetImageColor: selected ? Colors.white : Colors.black54,
        )
       ,
      )
    
     );
  }
  Widget _calender(){
    // var height = MediaQuery.of(context).size.height;
    // print('Height:- $height');
    // if(height > 830){
    //   height  = height * .6;
    // }
    // else if(height > 800){
    //   height  = height * .58;
    // }
    // else{
    //   height  = height * .69;
    // }
    return Container(
      height:400,
      alignment: Alignment.center,
       child: Container(
       width: MediaQuery.of(context).size.width * .95,
       height:470,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15),)
        ),
          child:Container(
            child: Stack(
              children: <Widget>[
                 Column(
                    children: <Widget>[
                    SizedBox(height: 30),
                       Material(
                          color: Colors.transparent,
                          child:  CalendarCarousel<Event>(
                           onDayPressed: (DateTime date, List<Event> events) {
                            setState(() {
                               dateFilter = date;
                            });
                           },
                           onDayLongPressed: (DateTime date, ) {
                            setState(() {
                               dateFilter = date;
                            });
                           },
                           weekendTextStyle: TextStyle(
                             color: Colors.black,
                           ),
                           weekdayTextStyle: TextStyle(
                             color: Colors.grey,
                             fontWeight: FontWeight.bold
                           ),
                           headerTextStyle:  TextStyle(
                             color: Colors.black,
                             fontSize: 20,
                             fontWeight: FontWeight.bold
                           ),
                           thisMonthDayBorderColor: Colors.transparent,
                             customDayBuilder: (
                             bool isSelectable,
                             int index,
                             bool isSelectedDay,
                             bool isToday,
                             bool isPrevMonthDay,
                             TextStyle textStyle,
                             bool isNextMonthDay,
                             bool isThisMonthDay,
                             DateTime day,
                           ) {
                             if(isToday && dateFilter != day){
                                return Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.black)
                                  ),
                                  child: Center(
                                    child: Text(day.day.toString()),
                                  ),
                               );
                             }
                             else if(dateFilter != null && dateFilter == day){
                              
                                return Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    // border: Border.all(color: Colors.black),
                                    color: MtColors.blue 
                                  ),
                                  child: Center(
                                    child: Text(day.day.toString(),style: TextStyle(color: Colors.white ),),
                                  ),
                               );
                             }
                             else if(day.isBefore(DateTime.now()) || day.isAfter( DateTime.now().add(Duration(days: 90)))){
                               return  Container(  
                                  height: 40,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    // border: Border.all(color: Colors.black),
                                    color:  Colors.white,
                                  ),
                                  child: Center(
                                    child: Text(day.day.toString(),style: TextStyle(color:  Colors.black45),),
                                  ),
                               );
                             }
                             else{
                               return  Container(  
                                  height: 40,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    // border: Border.all(color: Colors.black),
                                    color:  Colors.white,
                                  ),
                                  child: Center(
                                    child: Text(day.day.toString(),style: TextStyle(color:  Colors.black),),
                                  ),
                               );
                             }
                           },
                          selectedDayBorderColor:  Colors.transparent,
                          selectedDayButtonColor:  Colors.transparent,
                          todayButtonColor: Colors.transparent,
                          todayBorderColor: Colors.transparent,
                          
                           weekFormat: false,
                           height: 420.0,
                           maxSelectedDate:  DateTime.now().add(Duration(days: 90)),
                           minSelectedDate: DateTime.now().add(Duration(days: -1)),
                           selectedDateTime: dateFilter,
                           daysHaveCircularBorder: false, /// null for not rendering any border, true for circular border, false for rectangular border
                         ),
                      ),
                    ],
                  ),
                  Align(
                        alignment: Alignment.topCenter,
                        child: Material(
                          color: Colors.white,
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            child:Text('SELECT DATE',style: TextStyle(color: Colors.black54,fontSize: 18,   ),),
                          )
                        )
                  ),
                 Align(
                   alignment: Alignment.topRight,
                   child: Material(
                     color: Colors.transparent,
                     borderRadius: BorderRadius.circular(30),
                     child: InkWell(
                       onTap: (){
                         setState(() {
                           dateFilter = null;
                         });
                         Navigator.pop(context);
                       },
                       child: Container(
                         margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                         decoration: BoxDecoration(
                           shape: BoxShape.circle,
                           border: Border.all(color: Colors.black,width: 2)
                         ),
                         child: CircleAvatar(
                           radius: 12,
                           backgroundColor: Colors.transparent,
                           child: Icon(Icons.close,color: Colors.black,),
                         ),
                       )
                     ),
                   )
                 ),
                 Positioned(
                   bottom: 20,
                   left: MediaQuery.of(context).size.width * .2,
                   child:   Container(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            MyCard(
                              padding: EdgeInsets.only(left: 20),
                              borderRadiusValue: 40,
                              child: Row(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      MaterialButton(
                                        // color:MtColors.blue,
                                        padding: EdgeInsets.only(bottom: 5),
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                                        onPressed: (){
                                          setState(() {
                                              dateFilter = DateTime.now();
                                              spaceBLoc.setdateFilter = dateFilter;
                                              Navigator.pop(context);
                                            });
                                             saveLastFilters();
                                                _reload();
                                        },
                                        child:Center(child:Text('TODAY',style: Theme.of(context).textTheme.headline.copyWith(fontSize: 20,color: MtColors.blue),textAlign:TextAlign.center,),)
                                      ),
                                     GestureDetector(
                                       onTap: (){
                                          setState(() {
                                             if(dateFilter != null){
                                               spaceBLoc.setdateFilter = dateFilter;
                                                 saveLastFilters();
                                                _reload();
                                             }
                                              Navigator.pop(context);
                                            });
                                        },
                                       child: Container(
                                        padding: EdgeInsets.symmetric(vertical: 12,horizontal: 20),
                                        decoration: BoxDecoration(
                                          color: MtColors.blue,
                                          borderRadius: BorderRadius.circular(40)
                                        ),
                                        child: Center(child:Text('APPLY',style: Theme.of(context).textTheme.headline.copyWith(fontSize: 20,color:Colors.white),textAlign:TextAlign.center,),)
                                      ,)
                                     )
                                      //  MaterialButton(
                                      //   color:MtColors.blue,
                                      //   padding: EdgeInsets.only(bottom: 5),
                                      //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                                      //   onPressed: (){
        
                                      //   },
                                      //   child:Center(child:Text('APPLY',style: Theme.of(context).textTheme.headline.copyWith(fontSize: 20,color:Colors.white),textAlign:TextAlign.center,),)
                                      // )
                                    ],
                                  ),
                            ),
                           ],
                        ),
                      )
                  ,
                 )
              ],
            ),
          )
         ),
    );
  }
  
  void callCurencyChangeApi()async{
    if(poundSelected){
        print('Pound is selected');
        SharedPreferencesHelper.initCurrencyPreferences(currency:'GBP');
     }
     else if(dollerSelected){ 
       SharedPreferencesHelper.initCurrencyPreferences(currency:'USD');
       print('Doller is selected');
     }
     else if(euroSelected){
       SharedPreferencesHelper.initCurrencyPreferences(currency:'EUR');
       print('Euro is selected');
    }
    service.getRates().then((value){
      if(value){
        print('Reload api after currency change');
        _reload();
      }
    });
  }

  
}

enum SortingOptions {
  spaceName,
  priceDecreasing,
  priceIncreasing,
  newest,
}

// class KeepAliveFutureBuilder extends StatefulWidget {
//   final Future future;
//   final AsyncWidgetBuilder builder;

//   KeepAliveFutureBuilder({this.future, this.builder});

//   @override
//   _KeepAliveFutureBuilderState createState() => _KeepAliveFutureBuilderState();
// }

// class _KeepAliveFutureBuilderState extends State<KeepAliveFutureBuilder>
//     with AutomaticKeepAliveClientMixin {
//   @override
//   Widget build(BuildContext context) {

//     // DD. DO we need this super()?
//     super.build(context);

//     return FutureBuilder(
//       future: widget.future,
//       builder: widget.builder,
//     );
//   }

//   @override
//   bool get wantKeepAlive => true;
// }
