import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mt_ui/resource/repository.dart';
import 'bloc.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final Repository repository;

  SearchBloc({this.repository});

  @override
  SearchState get initialState => Loading();

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is Fetch) {
      try {
        yield Loading();
        final items = await repository.fetchAllSpace(url: getSearchUrl(event));
        yield Loaded(items: items, type: event.type);
      } catch (_) {
        yield Failure();
      }
    }
  }

  String getSearchUrl(Fetch fetch){
     var query = '''
      query {
        searchCount (page:${fetch.pageNumber}, pageSize:10, filter: {priceFrom: ${fetch.priceFrom}, priceTo: ${fetch.priceTo}, instrumentId:${fetch.instrumentFilterInt}, cityId:"${fetch.cityFilter != null && fetch.cityFilter != "Anywhere" ? fetch.cityFilter.split(',')[0] : null}"}, sort:${fetch.sortBy}) {
          rooms{
            id
            title
            place{
              country
              city
              coordinates {
                longitude
                latitude
              }
            }
            instruments {
              instrument {
                id
                name
              }
            }
            coverImage {
              id
              objectId
              attachmentFile
              order
            }
            listingPrice {
              price
              currency
            }
            isFavorite
          },
          count
        }
      }
    ''';
    return query;
  }
}
