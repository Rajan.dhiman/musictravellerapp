import 'package:equatable/equatable.dart';

abstract class SearchEvent extends Equatable{
  const SearchEvent();

  @override
  List<Object> get props => [];
}
class Fetch extends SearchEvent {
  final String type;
  final int pageNumber;
  final int priceFrom;
  final int priceTo;
  final List<int> instrumentFilterInt;
  final String cityFilter;
  final int sortBy;

  Fetch({this.pageNumber, this.priceFrom, this.priceTo, this.instrumentFilterInt , this.cityFilter, this.sortBy, this.type});
   @override
  List<Object> get props => [type];

  @override
  String toString() => 'Fetch $type space';
}
