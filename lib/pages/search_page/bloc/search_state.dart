import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

class Loading extends SearchState {}

class Loaded extends SearchState {
  final List<ListingRoom> items;
  final String type;

  const Loaded({@required this.items, this.type});

  @override
  List<Object> get props => [items];

  @override
  String toString() => 'Loaded { items: ${items.length} }';
}

class Failure extends SearchState {}
