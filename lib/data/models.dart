import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mt_ui/helpers/constants.dart';
import 'package:mt_ui/utils.dart';

class ListingRoom {
  final String name;
  final String currency;
  final int id;
  final double price;
  final ListingRoomLocation location;
  final ListingSpaceImage image;
  final List<ListingRoomInstrument> instruments;
  final bool isFavorite;

  ListingRoom(
      {this.name,
      this.id,
      this.price,
      this.location,
      this.image,
      this.instruments,
      this.currency,
      this.isFavorite});

  /// [Currencyconversion]
  factory ListingRoom.fromAPIJson(Map<String, dynamic> json, {String curencyPrefrence,double rate}) {
    var apiCurrency = json['listingPrice'] != null
            ? json['listingPrice']['currency']
            : '';
    var apiPrice = json['listingPrice'] != null ? json['listingPrice']['price'] : 0  as double;
   
    
    if(curencyPrefrence != null && rate != null&& apiCurrency != curencyPrefrence){
      apiCurrency = curencyPrefrence;
      double mod = pow(10.0, 2); 
      apiPrice =  ((apiPrice * rate * mod).round().toDouble() / mod); 
    }

    return ListingRoom(
        name: json['title'],
        id: json['id'],  
        price: apiPrice,
        currency:apiCurrency,
        location: json['place'] == null
            ? null
            : ListingRoomLocation.fromAPIJson(json['place']),
        image: json['coverImage'] == null
            ? null
            : ListingSpaceImage.fromAPIJson(json['coverImage']),
        instruments: (json["instruments"] as List)
            .map<ListingRoomInstrument>(
                (json) => ListingRoomInstrument.fromAPIJson(json))
            .toList(),
        isFavorite: json['isFavorite']);
  
 
}
  String getDisplayMinPrice() {
    // return this.location.getCurrencySymbol() + price.toString();
    // return "\$" + price.toString();
    return currency + price.toString();
  }

  String getDisplayInstruments() {
    if (instruments == null) {
      return null;
    } else if (instruments.isEmpty) {
      return null;
    }

    // create list of instrument types (or model if type is null)
    List<String> instrumentsString = List();
    instruments.forEach((instrument) {
      instrumentsString.add(instrument.name);
    });

    if (instrumentsString.isEmpty) {
      return null;
    }

    // Join to string and capitalize
    return Utils.capitalize((instrumentsString.join(", ")));
  }
}

class ListingRoomLocation {
  final String country, city;
  final Coordinates coordinates;
  ListingRoomLocation({this.country, this.city,this.coordinates});

  factory ListingRoomLocation.fromAPIJson(Map<String, dynamic> json) {
    return ListingRoomLocation(
      country: json['country'] as String,
      city: json['city'] as String,
      coordinates: Coordinates.fromAPIJson(json)
    );
  }
}

class Coordinates{
  final  double longitude;
  final  double latitude;

  Coordinates({this.longitude, this.latitude});

  factory Coordinates.fromAPIJson(Map<String, dynamic> json) {
    // print('Latlong$json');
    return Coordinates(
       latitude: json['coordinates']['latitude'],
        longitude: json['coordinates']['longitude'],
    );
  }
}

class ListingRoomInstrument {
  final String id, name;

  ListingRoomInstrument({
    this.id,
    this.name,
  });

  factory ListingRoomInstrument.fromAPIJson(Map<String, dynamic> json) {
    return ListingRoomInstrument(
      id: json['instrument']['id'].toString(),
      name: json['instrument']['name'] as String,
    );
  }
}

class ListingSpaceImage {
  final String url;
  final int order;

  ListingSpaceImage({this.url, this.order});

  factory ListingSpaceImage.fromAPIJson(Map<String, dynamic> json) {
    return ListingSpaceImage(
      url: MtConstants.baseMediaUrl +
          json['attachmentFile'],
      order: json['order'] as int,
    );
  }
}

class Room {
  final String name,
      description,
      rules,
      spaceId,
      roomId,
      currency,
      customCancellationPolicyPdfUrl;
  final Location location;
  final Amenities amenities;
  final List<Review> reviews;
  final List<String> date;
  final List<Instrument> instruments;
  final List<SpaceImage> images;
  final List<MtUser> users;
  final int rating, capacity,totalReviews, cancellationPolicy, soundproof;
   final double averageRating;
  // final List<RoomBookingHour> bookingHours;
  final double priceDefault, size;
  List<String> dateDefault;
  final double price;
  final bool isFavorite;

  static const int POLICY_24_HOURS = 0;
  static const int POLICY_48_HOURS = 1;
  static const int POLICY_72_HOURS = 2;
  static const int POLICY_CUSTOM = 3;

  Room( {
    this.spaceId,
    this.roomId,
    this.name,
    this.description,
    this.rules,
    this.location,
    this.instruments,
    this.images,
    this.rating,
    this.size,
    this.capacity,
    this.currency,
    this.amenities,
    this.cancellationPolicy,
    this.customCancellationPolicyPdfUrl,
    this.date,
    this.priceDefault = 0,
    this.dateDefault,
    this.soundproof,
    this.reviews,
    this.users,
    this.price,
    this.isFavorite,
    this.totalReviews, 
    this.averageRating,
    // this.bookingHours,
  });

  factory Room.fromAPIJson(Map<String, dynamic> jsonIncome,{String curencyPrefrence,double rate}) {
    double priceDefault;
    List<String> dateDefault;
    // in case of room data from booked spaces
    var rooms = jsonIncome['rooms'] ?? jsonIncome;
    // if (!jsonIncome.containsKey('rooms')) {
    //   // json['reviews'] = [
    //   //   {
    //   //     'id': 1,
    //   //     'created': new DateTime.now(),
    //   //     'rating': 4,
    //   //     'text': 'test comment',
    //   //     'roomId': 1,
    //   //     'userId': 1,
    //   //     'title': 'test title',
    //   //     'user': {
    //   //       'id': 0,
    //   //       'email':'email',
    //   //       'username': 'username',
    //   //       'occupation': 'occupation',
    //   //       'bio': 'bio'
    //   //     }
    //   //   }  
    //   // ];
    // };

    final List<Map<String, dynamic>> roomsScheduleList = rooms['data']['room']
            ['staticSchedule']
        .toList()
        .cast<Map<String, dynamic>>();
    if (roomsScheduleList.length > 0) {
      priceDefault = roomsScheduleList
          .map<double>((room) => room["price"])
          .toList()
          .reduce(min)
          .toDouble();
      dateDefault =
          roomsScheduleList.map<String>((date) => date['date']).toList();
    } else {
      priceDefault = 0.0;
      dateDefault = [];
    }

     var json = jsonIncome['data'];

     /// [Currencyconversion]
     var apiCurrency = json['room']['listingPrice']['currency'];
    var apiPrice = json['room']['listingPrice']['price']  as double;
   

    if(curencyPrefrence != null && rate != null&& apiCurrency != curencyPrefrence){
      apiCurrency = curencyPrefrence;
      double mod = pow(10.0, 2); 
      apiPrice =  ((apiPrice * rate * mod).round().toDouble() / mod); 
      priceDefault = apiPrice;
    }

   
    return Room(
      name: json['room']['name'] as String,
      spaceId: json['room']['id'].toString(),
      roomId: json['room']['id'].toString(),
      description: json['room']['description'] as String,
      location: Location.fromAPIJson(json['room']['place'],apicurrency: apiCurrency),
      instruments: (json['room']["instruments"] as List)
          .map<Instrument>((json) => Instrument.fromAPIJson(json))
          .toList(),
      images: (json['room']['images'] as List)
          .map<SpaceImage>((json) => SpaceImage.fromAPIJson(json))
          .toList(),
      rules: json['room']['rules'] as String,
      rating: json['room']['rating'] as int,
      size: json['room']['size'],
      capacity: json['room']['capacity'],
      priceDefault: priceDefault,
      cancellationPolicy: json['room']['cancellationPolicy'],
      customCancellationPolicyPdfUrl: "json['cancellationPolicyUrl'] as String",
      dateDefault: dateDefault,
      amenities: Amenities.fromAPIJson(json['room']),
      soundproof: json['room']['soundproof'],
      reviews: (json['room']['reviews'] as List)
          .map<Review>((json) => Review.fromAPIJson(json))
          .toList(),
      price: apiPrice,
      currency: apiCurrency,
      isFavorite: json['room']['isFavorite'],
      totalReviews: json['room']['totalReviews'],
      averageRating: json['room']['averageRating']
      // users: (json['users'] as List).map<MtUser>((json) => MtUser.fromAPIJson(json)).toList(),
      // bookingHours: (
      //   json.containsKey('rooms') ? json['rooms']['todaySchedule'] : json['todaySchedule'] as List
      // ).map<RoomBookingHour>((json) => RoomBookingHour.fromAPIJson(json)).toList(),
    );
  }

  String getDisplayMinPrice() {
    var price = this.location.getCurrencySymbol() + priceDefault.toString();
    return price;
  }

  String getDisplayInstruments() {
    if (instruments == null) {
      return null;
    } else if (instruments.isEmpty) {
      return null;
    }

    // create list of instrument types (or model if type is null)
    List<String> instrumentsString = List();
    instruments.forEach((instrument) {
      if (instrument.type != null) {
        instrumentsString.add(instrument.type);
      } else if (instrument.model != null) {
        instrumentsString.add(instrument.model);
      }
    });

    if (instrumentsString.isEmpty) {
      return null;
    }

    // Join to string and capitalize
    return Utils.capitalize((instrumentsString.join(", ")));
  }
}

class Amenities {
  final bool hasAc, hasWindow;
  final int capacity;
  final int roomSize;
  final String sizeMeasurement;

  Amenities(
      {this.hasAc,
      this.hasWindow,
      this.capacity,
      this.roomSize,
      this.sizeMeasurement});

  factory Amenities.fromAPIJson(Map<String, dynamic> json) {
    return Amenities(
        roomSize: json['size'].round() as int,
        sizeMeasurement: json['sizeMeasurement'] as String,
        capacity: json['capacity'] as int,
        hasAc: json['hasAc'] as bool,
        hasWindow: json['hasWindow'] as bool);
  }
}

class SpaceImage {
  final String url;
  final int order;

  SpaceImage({this.url, this.order});

  factory SpaceImage.fromAPIJson(Map<String, dynamic> json) {
    return SpaceImage(
      url: MtConstants.baseMediaUrl +
          json['attachmentFile'],
      order: json['order'] as int,
    );
  }
}

class Instrument {
  final int year, order;
  final String id, type, manufacturer, model, detail;

  Instrument(
      {this.id,
      this.year,
      this.type,
      this.manufacturer,
      this.model,
      this.detail,
      this.order});

  factory Instrument.fromAPIJson(Map<String, dynamic> json) {
    return Instrument(
      id: json['id'].toString(),
      year: json['productionYear'] as int,
      type: json['instrument']['name'],
      manufacturer: json['manufacturer'] as String,
      model: json['instrumentModel'] as String,
      detail: json['additionalInfo'] as String,
      order: 1,
    );
  }
}

class Currencies {
  static const String CURRENCY_EUR = "EUR";
  static const String CURRENCY_USD = "USD";
  static const String CURRENCY_GBP = "GBP";

  // TODO turn this into a user changeable preference or fetch from API and
  //  also need to figure out how to convert currencies
  static const userPreferredCurrency = CURRENCY_EUR;

  static List<String> toList() {
    return [CURRENCY_EUR, CURRENCY_USD];
  }

  static String getCurrencySymbol(String currency) {
    if (currency == Currencies.CURRENCY_EUR) return "€";
    if (currency == Currencies.CURRENCY_USD) return "\$";
    if (currency == Currencies.CURRENCY_GBP) return "£";
    return currency;
  }
}

// Location model
class Location {
  final String country, city, zip, streetAddress1, streetAddress2;
  final double latitude, longitude;
  final String currency;

  Location(
      {this.country,
      this.city,
      this.zip,
      this.streetAddress1,
      this.streetAddress2,
      this.currency,
      this.latitude,
      this.longitude});

  factory Location.fromAPIJson(Map<String, dynamic> json,{String apicurrency}) {
    // String currency;

    //check for
    // var schedule = json.containsKey('rooms') ? json['rooms']['staticSchedule'] : json['staticSchedule'];
    // if (!json.containsKey('rooms')) json['location'] = {
    //   "type": "POINT",
    //   "location": [
    //       13.049217699999986,
    //       52.3992161
    //     ]
    // };
    // final List<Map<String, dynamic>> roomsScheduleList = schedule.toList().cast<Map<String, dynamic>>();
    // if(roomsScheduleList.length > 0) {
    //   currency = roomsScheduleList.map((schedule) => schedule['priceCurrency']).toList()[0];
    // } else {
    //   currency = "EUR";
    // }
    if(apicurrency == null || apicurrency.isEmpty){
       apicurrency = "EUR";
       print("Currency is set to EUR");
    }

    return Location(
      currency: apicurrency,
      country: json['country'] as String,
      city: json['city'] as String,
      streetAddress1: json['address'] as String,
      latitude: json['coordinates']['latitude'],
      longitude: json['coordinates']['longitude'],
      // currency: json['rooms']['schedule'].map((schedule) => schedule['priceCurrency']).toList()[0],
      // isActive: json['lon'] as bool, // TODO add to local model and show/hide spaces depending on it
    );
  }

  String getCurrencySymbol() {
    // print(this.currency);
    return Currencies.getCurrencySymbol(this.currency);
  }
 
  String getFullAddressDisplay() {
    if (streetAddress2 != null)
      return "$streetAddress1, $streetAddress2, $zip, $city, $country";
    return "$streetAddress1,  $zip, $city, $country";
  }
}

class Revieww {
  final int userId, spaceId;
  final int rating;
  final String message, date;

  Revieww(this.userId, this.spaceId, this.rating, this.message, this.date);
}

class Review {
  final int userId, spaceId, rating;
  final String message, title, email, occupation, bio, username, avatarUrl;

  // final DateTime date;
  final String date;

  Review({
    this.userId,
    this.spaceId,
    this.rating,
    this.message,
    this.date,
    this.title,
    this.occupation,
    this.bio,
    this.email,
    this.username,
    this.avatarUrl,
  });

  factory Review.fromAPIJson(Map<String, dynamic> json) {
    return Review(
      userId: json['userId'],
      spaceId: json['id'],
      rating: json['rating'],
      message: json['text'],
      date: json['created'],
      title: json['title'],
      email: json['user']['email'],
      occupation: json['user']['occupation'],
      bio: json['user']['bio'],
      username: json['user']['name'] != null ? json['user']['name'] : json['user']['username'],
      avatarUrl: json['user']['avatarUrl'] ?? json['user']['avatar'],
    );
  }
  
  String getFormattedDate(){
    return   DateFormat("hh:mm a - dd MMM yyyy ").format(DateTime.parse(date)).toString();
  }
}

class MtUser {
  final int id;
  final String name;
  final String email;
  final String avatarUrl;
  final String occupation;
  final String location;
  final String education;
  final String bio;
  final String soundCloudUrl;
  final String facebookUrl;
  final String youtubeUrl;
  final String websiteUrl;
  final List<String> recordingUrls;
  final List<String> inspirationUrls;
  final String city;
  final String state;
  final String country;
  final String postalCode;
  final String telephone;
  final String school;
  final String skillLevel;
  final String graduated;
  final String studyWith;
  final String ownedInstruments;
  final String playedInstruments;

  MtUser({
    this.id,
    this.name,
    this.email,
    this.avatarUrl,
    this.occupation,
    this.location,
    this.education,
    this.bio,
    this.soundCloudUrl,
    this.facebookUrl,
    this.youtubeUrl,
    this.websiteUrl,
    this.recordingUrls,
    this.inspirationUrls,
    this.city,
    this.country,
    this.graduated,
    this.ownedInstruments,
    this.playedInstruments,
    this.postalCode,
    this.school,
    this.skillLevel,
    this.state,
    this.studyWith,
    this.telephone
  });

  factory MtUser.fromAPIJson(Map<String, dynamic> json) {
    return MtUser(
      id: json['id'],
      name: json['username'],
      email: json['email'],
      avatarUrl: '',
      occupation: json['occupation'],
      location: '',
      education: '',
      bio: '',
      soundCloudUrl: '',
      facebookUrl: '',
      youtubeUrl: '',
      websiteUrl: '',
      recordingUrls: [''],
      inspirationUrls: [''],
    );
  }
}

class User {
  final String id;
  final String name;
  final String userName;
  final String email;
  final String avatarUrl;
  final String occupation;
 
  final String education;
  final String bio;
  final String soundCloudUrl;
  final String facebookUrl;
  final String youtubeUrl;
  final String websiteUrl;
  final bool isHost;
  final bool isPublic;
  final String city;
  final String school;
  final String gender;
  final String phoneNumber;
  final String dob;
  final String soundcloud;
  final String youtube;
  final String facebook;
  final String avatar;
  final List<String> recordingUrls;
  final List<String> inspirationUrls;
  final String countryCode;
  final String skill;
  final String graduation;
  final String instruction;

  String youtubeThumbnaiil;
  String soundCloudThumbnaiil;
  final String instrumentsPlayed;
  final String instrumentsOwned;
  final String location;
  final String state;
  final String address;
  final String postalCode;

  User({
    this.id,
    this.name,
    this.userName,
    this.email,
    this.avatarUrl,
    this.occupation,
    this.location,
    this.education,
    this.bio,
    this.soundCloudUrl,
    this.facebookUrl,
    this.youtubeUrl,
    this.websiteUrl,
    this.recordingUrls,
    this.inspirationUrls,
    this.isHost,
    this.isPublic,
    this.city,
    this.school,
    this.gender,
    this.phoneNumber,
    this.dob,
    this.youtube,
    this.soundcloud,
    this.facebook,
    this.avatar,
    this.countryCode, 
    this.skill, 
    this.graduation,
    this.soundCloudThumbnaiil,
    this.youtubeThumbnaiil,
    this.instruction,
    this.address,
    this.state,
    this.postalCode,
    this.instrumentsPlayed,
    this.instrumentsOwned
  });

  factory User.fromAPIJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      name: json['name'],
      userName: json['username'] != null ? json['username'] : '',
      email: json['email'],
      avatarUrl: json['avatar'] != null ? json['avatar'] : '',
      occupation: json['occupation'] != null ? json['occupation'] : '',
      location: json['city'] != null ? json['city'] : '',
      education: json['school'] != null ? json['school'] : '',
      school: json['school'] != null ? json['school'] : '',
      bio: json['bio'] != null ? json['bio'] : '',
      soundCloudUrl: json['soundcloud'] != null ? json['soundcloud'] : null,
      facebookUrl: json['facebook'] != null ? json['facebook'] : null,
      youtubeUrl: json['youtube'] != null ? json['youtube'] : null,
      websiteUrl: json['website'] != null ? json['website'] : null,
      recordingUrls: [''],
      inspirationUrls: [''],
      
      isPublic: json['isPublic'] != null ? json['isPublic'] : false,
      isHost: json['isHost'] != null ? json['isHost'] : false,
      gender: json['gender'] != null ? json['gender'] : null,
      phoneNumber: json['phone'] != null ? json['phone'] : null,
      dob: json['dob'] != null ? json['dob'] : null,
      countryCode: json['country'] != null ? json['country'] : null,
      graduation: json['graduated'] != null ? json['graduated'] : null,
      skill: json['skillLevel'] != null ? json['skillLevel'] : null,
      youtubeThumbnaiil: json['youtube'] != null ? json['youtube'] : null,
      soundCloudThumbnaiil:  json['soundcloud'] != null ? json['soundcloud'] : null,
      instruction: json['instruction'] != null ? json['instruction'] : null,
      address: json['address'] != null ? json['address'] : null,
      city: json['city'] != null ? json['city'] : null,
      state: json['state'] != null ? json['state'] : null,
      postalCode: json['postalCode'] != null ? json['postalCode'] : null,
      instrumentsPlayed:json['instrumentsPlayed'] != null ? json['instrumentsPlayed'] : null,
      instrumentsOwned:json['instrumentsOwned'] != null ? json['instrumentsOwned'] : null,
    );
  }
}

///use to show [Bookings] on Bookings page
Room bookingSpace(Map<String, dynamic> json) {
  return Room(
    spaceId: json['id'].toString(),
    name: json['name'],
    images: !json.containsKey('coverImage') ? null :
    [
      SpaceImage(
        order: 0,
        url: 'https://s3.amazonaws.com/musictraveler.herokuapp.com/media/' +
            json['coverImage']['attachmentFile'],
      ),
    ],
    location: Location(
      city: "Vienna",
      country: "Austria",
      streetAddress1: "Annagasse 1/10",
      zip: "1010",
      latitude: 48.204321,
      longitude: 16.3719837,
      currency: "EUR",
    ),
  );
}

class Booking implements Comparable<Booking> {
  final List<BookingHour> bookingHours;
  final Room space;
  int partySize, id, roomId;
  final int status;
  ///[NEw added]
   final double price;
  ///
//  DD. before type was bool
  final int isApprovedApi;
  final bool isPaid;
  final String currency;

  Booking(
      {this.space,
      this.partySize,
      this.bookingHours,
      this.status,
      this.roomId,
      this.isApprovedApi,
      this.isPaid,
      this.currency,
      this.price});

  factory Booking.fromAPIJson(Map<String, dynamic> json) {
    // print(bookingSpace(json['room']));
    return Booking(
      // space: Space.fromAPIJson(json['room']),
      space: bookingSpace(json['room']),
      // todo replace static number
      partySize: 1,
      currency : json['roomPriceCurrency'],
      price : json['price'],
      isPaid: json['isPaid'],
      // todo replace static number
      status: json["isApproved"] ? 2 : 1,
      isApprovedApi : json["isApproved"] ? 2 : 1,
      bookingHours: [
        BookingHour(
          dateTime: DateTime.parse(json['start']),
          start: DateTime.parse(json['start']),
          end: DateTime.parse(json['end']),
          price: json['roomPrice'],
          isAvailable: true,
        )
      ],
    );
  }

  static const int STATUS_PENDING = 1;
  static const int STATUS_APPROVED = 2;
  static const int STATUS_CANCELLED = 3;
  static const int STATUS_REJECTED = 4;

  bool isPending() => (status == STATUS_PENDING);

  bool isApproved() => (status == STATUS_APPROVED);

  bool isCancelled() => (status == STATUS_CANCELLED);

  bool isRejected() => (status == STATUS_REJECTED);

  /// returns true if booking is in the past
  bool isPast() {
    BookingHour lastBookingHour = bookingHours[bookingHours.length - 1];
    return (lastBookingHour.dateTime.compareTo(DateTime.now()) < 0);
  }

  // TODO pull this from API
  static const double INSURANCE_FEE = 1;

  double getTotalSpaceFee() {
    double totalPrice = 0;

    for (BookingHour b in bookingHours) {
      totalPrice = totalPrice + b.price;
    }

    return totalPrice;
  }

  double getInsuranceFee() => INSURANCE_FEE;

  String getInsuranceFeeDisplay() {
     
      var price =  space.location.getCurrencySymbol()  + Utils.convertToTwoDecimals(getInsuranceFee());
      return price;
  }

  /// returns string of total price for selected hours without trailing .0
  String getTotalSpaceFeeDisplay() {
    String totalPrice = getTotalSpaceFee().toString();
    if (totalPrice.endsWith(".0")) {
      totalPrice = totalPrice.substring(0, totalPrice.length - 2);
    }
    if(totalPrice.contains('.')){
      var dd= totalPrice.split('.')[1].length;
      var endIndex = dd > 2 ? totalPrice.indexOf('.') + 3 : totalPrice.indexOf('.') + dd + 1;
      print(totalPrice);
      totalPrice= totalPrice.substring(0,endIndex);
    }
      var price = space.location.getCurrencySymbol() + totalPrice;
    return price;
  }

  String getTotalSpaceFeeDisplayTwoDecimals() {
    String totalPrice = Utils.convertToTwoDecimals(getTotalSpaceFee());
    if(totalPrice == ".00"){
      totalPrice = "0.00";
    }
      var price = space.location.getCurrencySymbol() + totalPrice;
      return price;
  }

  /// returns string of total price for the space + insurance
  String getTotalPriceDisplayTwoDecimals() {
    String totalPrice =
        Utils.convertToTwoDecimals(getTotalSpaceFee() + getInsuranceFee());
      var price = space.location.getCurrencySymbol() + totalPrice;
    return price;
  }

  @override
  int compareTo(Booking other) {
    // Allows to call .sort(); on a list of Booking to sort them by date
    return bookingHours[0].dateTime.compareTo(other.bookingHours[0].dateTime);
  }
}

class BookingHour implements Comparable<BookingHour> {
  final DateTime dateTime, start, end;
  final double price;
  bool isAvailable = false;

  BookingHour(
      {this.dateTime, this.price, this.start, this.end, this.isAvailable});

  // Get Get weekday
  static String getDayShort(DateTime date) =>
      new DateFormat("EEEE").format(date).substring(0, 3);

  // Get Date number
  static String getDate(DateTime date) => new DateFormat("d").format(date);

  // Get date with month (Feb 12)
  static String getDateAndMonth(DateTime date) =>
      new DateFormat("MMMd").format(date);

  // Get hour in locale format (13:00 or 1 pm)
  static String getHour(DateTime date, BuildContext context) =>
      MediaQuery.of(context).alwaysUse24HourFormat
          ? DateFormat("Hm").format(date) // 24h
          : DateFormat("j").format(date); // 12h

  // get hour in 24h format as int
  static int getHour24(DateTime date, BuildContext context) =>
      DateFormat("Hm").format(date) as int;

  static String getHourSpanString(
      List<BookingHour> hours, BuildContext context) {
    // sort chronologically - original order is order of user selection
    hours.sort();

    List<String> hourList = List<String>();
    for (BookingHour bookingHour in hours) {
      String hour = getHour(bookingHour.dateTime, context);
      String nextHour = getHour(bookingHour.dateTime.add(Duration(hours: 1)), context);
      hourList.add(hour + " - " + nextHour);
    }

    String string = hourList.join(", ");

    // 24h Regex to condense hour list (14:00 - 15:00, 15:00 - 16:00 --> 14:00 - 16:00)
    var repeatedTime24RegEx = RegExp(r"- (\d\d:\d\d), \1 ");
    string = string.replaceAll(repeatedTime24RegEx, "");

    // AM/PM Regex to condense hour list, (14 PM - 15 PM, 15 PM - 16 PM --> 14 PM - 16 PM)
    var repeatedTimeAMPMRegEx = RegExp(r"- ([1-9][0-9]* [AP][M]), \1 ");
    // string = string.replaceAll(repeatedTimeAMPMRegEx, "");

    return string;
  }

  /// Creates a map, where the key is the date (as string) and the value
  /// is a list of BookingHours on that date.
  static Map<String, List<BookingHour>> groupByDate(List<BookingHour> hours) {
    Map<String, List<BookingHour>> map = Map<String, List<BookingHour>>();
    for (BookingHour hour in hours) {
      String date = BookingHour.getDateAndMonth(hour.dateTime);
      if (!map.containsKey(date)) {
        List<BookingHour> list = new List<BookingHour>();
        list.add(hour);
        map.putIfAbsent(date, () => list);
      } else {
        map.update(date, (list) {
          list.add(hour);
          return list;
        });
      }
    }
    return map;
  }

  @override
  int compareTo(BookingHour other) {
    // Allows to call .sort(); on a list of BookingHours to sort them by date/time
    return dateTime.compareTo(other.dateTime);
  }
}

class RoomBookingHour {
  final double price;
  final String priceCurrency;
  final int day;
  final int hour;

  RoomBookingHour({
    this.price,
    this.priceCurrency,
    this.day,
    this.hour,
  });

  /// [Currencyconversion]
  factory RoomBookingHour.fromAPIJson(Map<String, dynamic> json,{String curencyPrefrence, double rate}) {
    
    String apiCurrency = json['priceCurrency'];
    double apiPrice = json['price'];
  
     if(curencyPrefrence != null && rate != null&& apiCurrency != curencyPrefrence){
      apiCurrency = curencyPrefrence;
      double mod = pow(10.0, 2); 
      apiPrice =  ((apiPrice * rate * mod).round().toDouble() / mod); 
    }
    return RoomBookingHour(
      price: apiPrice,
      priceCurrency: apiCurrency,
      day: json['day'],
      hour: json['hour'],
    );
  }
}

class RoomBookingHourBooked {
  final String date;
  final int day;
  final List reservedHours;

  RoomBookingHourBooked({
    this.day,
    this.date,
    this.reservedHours,
  });

  factory RoomBookingHourBooked.fromAPIJson(Map<String, dynamic> json) {
    return RoomBookingHourBooked(
      reservedHours: json['reservedHours'],
      day: json['day'],
      date: json['date'],
    );
  }
}

// class Space {
//   final String name, description, rules, spaceId, roomId, currency, customCancellationPolicyPdfUrl;
//   final Location location;
//   final Amenities amenities;
//   final List<Review> reviews;
//   final List<String> date;
//   final List<Instrument> instruments;
//   final List<SpaceImage> images;
//   final List<MtUser> users;
//   final int rating, capacity, cancellationPolicy, soundproof;
//   // final List<RoomBookingHour> bookingHours;
//   double priceDefault, size;
//   List<String> dateDefault;

//   static const int POLICY_24_HOURS = 0;
//   static const int POLICY_48_HOURS = 1;
//   static const int POLICY_72_HOURS = 2;
//   static const int POLICY_CUSTOM = 3;

//   Space({
//     this.spaceId,
//     this.roomId,
//     this.name,
//     this.description,
//     this.rules,
//     this.location,
//     this.instruments,
//     this.images,
//     this.rating,
//     this.size,
//     this.capacity,
//     this.currency = "EUR",
//     this.amenities,
//     this.cancellationPolicy,
//     this.customCancellationPolicyPdfUrl,
//     this.date,
//     this.priceDefault = 0,
//     this.dateDefault,
//     this.soundproof,
//     this.reviews,
//     this.users,
//     // this.bookingHours,
//   });

//   factory Space.fromAPIJson(Map<String, dynamic> json) {

//     double priceDefault;
//     List<String> dateDefault;
//     // in case of room data from booked spaces
//     var rooms = json['rooms'] ?? json;
//     if (!json.containsKey('rooms')) {
//       json['reviews'] = [
//         {
//           'id': 1,
//           'created': new DateTime.now(),
//           'rating': 4,  
//           'text': 'test comment',
//           'roomId': 1,
//           'userId': 1,
//           'title': 'test title',
//           'user': {
//             'id': 0,
//             'email':'email',
//             'username': 'username',
//             'occupation': 'occupation',
//             'bio': 'bio'
//           }
//         }
//       ];
//     };

//     final List<Map<String, dynamic>> roomsScheduleList = rooms['staticSchedule'].toList().cast<Map<String, dynamic>>();
//     if(roomsScheduleList.length > 0) {
//       priceDefault = roomsScheduleList.map<double>((room) => room["price"]).toList().reduce(min).toDouble();
//       dateDefault = roomsScheduleList.map<String>((date) => date['date']).toList();
//     } else {
//       priceDefault = 0.0;
//       dateDefault = [];
//     }

//     return Space(
//         name: json['room']['name'] as String,
//         spaceId: json['room']['id'].toString(),
//         roomId: json['room']['id'].toString(),
//         description: json['room']['description'] as String,
//         location: Location.fromAPIJson(json['room']['place']),
//         instruments: (
//           json['room']["instruments"] as List
//         ).map<Instrument>((json) => Instrument.fromAPIJson(json)).toList(),
//         images: (
//           json['room']['images'] as List
//         ).map<SpaceImage>((json) => SpaceImage.fromAPIJson(json)).toList(),
//         rules: json['room']['rules'] as String,
//         rating: json['room']['rating'] as int,
//         size: json['room']['size'],
//         capacity: json['room']['capacity'],
//         priceDefault: priceDefault,
//         cancellationPolicy: json['room']['cancellationPolicy'],
//         customCancellationPolicyPdfUrl: "json['cancellationPolicyUrl'] as String",
//         dateDefault: dateDefault,
//         amenities: Amenities.fromAPIJson(json['room']),
//         soundproof: json['room']['soundproof'],
//         reviews: (json['room']['reviews'] as List).map<Review>((json) => Review.fromAPIJson(json)).toList(),
//         // users: (json['users'] as List).map<MtUser>((json) => MtUser.fromAPIJson(json)).toList(),
//         // bookingHours: (
//         //   json.containsKey('rooms') ? json['rooms']['todaySchedule'] : json['todaySchedule'] as List
//         // ).map<RoomBookingHour>((json) => RoomBookingHour.fromAPIJson(json)).toList(),
//     );
//   }

//   String getDisplayMinPrice(){
//     return this.location.getCurrencySymbol() + priceDefault.toString();
//   }

//   String getDisplayInstruments() {
//     if(instruments == null){
//       return null;
//     } else if (instruments.isEmpty) {
//       return null;
//     }

//     // create list of instrument types (or model if type is null)
//     List<String> instrumentsString = List();
//     instruments.forEach((instrument){
//       if(instrument.type != null) {
//         instrumentsString.add(instrument.type);
//       }else if(instrument.model != null){
//         instrumentsString.add(instrument.model);
//       }
//     });

//     if(instrumentsString.isEmpty){
//       return null;
//     }

//     // Join to string and capitalize
//     return Utils.capitalize((instrumentsString.join(", ")));
//   }

// }

class Space {
  final String name,
      description,
      rules,
      spaceId,
      roomId,
      currency,
      customCancellationPolicyPdfUrl;
  final Location location;
  final Amenities amenities;
  final List<Review> reviews;
  final List<String> date;
  final List<Instrument> instruments;
  final List<SpaceImage> images;
  final List<MtUser> users;
  final int rating, capacity, cancellationPolicy, soundproof;

  // final List<RoomBookingHour> bookingHours;
  double priceDefault, size;
  List<String> dateDefault;

  static const int POLICY_24_HOURS = 0;
  static const int POLICY_48_HOURS = 1;
  static const int POLICY_72_HOURS = 2;
  static const int POLICY_CUSTOM = 3;

  Space({
    this.spaceId,
    this.roomId,
    this.name,
    this.description,
    this.rules,
    this.location,
    this.instruments,
    this.images,
    this.rating,
    this.size,
    this.capacity,
    this.currency = "EUR",
    this.amenities,
    this.cancellationPolicy,
    this.customCancellationPolicyPdfUrl,
    this.date,
    this.priceDefault = 0,
    this.dateDefault,
    this.soundproof,
    this.reviews,
    this.users,
    // this.bookingHours,
  });

  factory Space.fromAPIJson(Map<String, dynamic> json) {
    double priceDefault;
    List<String> dateDefault;
    // in case of room data from booked spaces
    var rooms = json['rooms'] ?? json;
    if (!json.containsKey('rooms')) {
      json['reviews'] = [
        {
          'id': 1,
          'created': new DateTime.now(),
          'rating': 4,
          'text': 'test comment',
          'roomId': 1,
          'userId': 1,
          'title': 'test title',
          'user': {
            'id': 0,
            'email': 'email',
            'username': 'username',
            'occupation': 'occupation',
            'bio': 'bio'
          }
        }
      ];
    }

    final List<Map<String, dynamic>> roomsScheduleList =
        rooms['staticSchedule'].toList().cast<Map<String, dynamic>>();
    if (roomsScheduleList.length > 0) {
      priceDefault = roomsScheduleList
          .map<double>((room) => room["price"])
          .toList()
          .reduce(min)
          .toDouble();
      dateDefault =
          roomsScheduleList.map<String>((date) => date['date']).toList();
    } else {
      priceDefault = 0.0;
      dateDefault = [];
    }

    return Space(
      name: json.containsKey('rooms')
          ? json['rooms']['name']
          : json['name'] as String,
      spaceId: json['id'].toString(),
      roomId: json.containsKey('rooms')
          ? json['rooms']['id'].toString()
          : json['id'] as String,
      description: json.containsKey('rooms')
          ? json['rooms']['description']
          : json['description'] as String,
      location: Location.fromAPIJson(json),
      instruments: (json.containsKey('rooms')
              ? json['rooms']["instruments"]
              : json["instruments"] as List)
          .map<Instrument>((json) => Instrument.fromAPIJson(json))
          .toList(),
      images: (json.containsKey('rooms')
              ? json["rooms"]["images"]
              : json['images'] as List)
          .map<SpaceImage>((json) => SpaceImage.fromAPIJson(json))
          .toList(),
      rules: json.containsKey('rooms')
          ? json['rooms']['rules']
          : json['rules'] as String,
      rating: json.containsKey('rooms')
          ? json['rooms']['rating']
          : json['rating'] as int,
      size: json.containsKey('rooms') ? json['rooms']['size'] : json['size'],
      capacity: json.containsKey('rooms')
          ? json['rooms']['capacity']
          : json['capacity'],
      priceDefault: priceDefault,
      cancellationPolicy: json.containsKey('rooms')
          ? json['rooms']['cancellationPolicy']
          : json['cancellationPolicy'],
      customCancellationPolicyPdfUrl: "json['cancellationPolicyUrl'] as String",
      dateDefault: dateDefault,
      amenities: Amenities.fromAPIJson(
          json.containsKey('rooms') ? json['rooms'] : json),
      soundproof: json.containsKey('rooms')
          ? json['rooms']['soundproof']
          : json['soundproof'],
      reviews: (json.containsKey('rooms')
              ? json["rooms"]["reviews"]
              : json['reviews'] as List)
          .map<Review>((json) => Review.fromAPIJson(json))
          .toList(),
      // users: (json['users'] as List).map<MtUser>((json) => MtUser.fromAPIJson(json)).toList(),
      // bookingHours: (
      //   json.containsKey('rooms') ? json['rooms']['todaySchedule'] : json['todaySchedule'] as List
      // ).map<RoomBookingHour>((json) => RoomBookingHour.fromAPIJson(json)).toList(),
    );
  }

  String getDisplayMinPrice() {
    var price = this.location.getCurrencySymbol()  +  priceDefault.toString();
    return price;
  }

  String getDisplayInstruments() {
    if (instruments == null) {
      return null;
    } else if (instruments.isEmpty) {
      return null;
    }

    // create list of instrument types (or model if type is null)
    List<String> instrumentsString = List();
    instruments.forEach((instrument) {
      if (instrument.type != null) {
        instrumentsString.add(instrument.type);
      } else if (instrument.model != null) {
        instrumentsString.add(instrument.model);
      }
    });

    if (instrumentsString.isEmpty) {
      return null;
    }

    // Join to string and capitalize
    return Utils.capitalize((instrumentsString.join(", ")));
  }
}
