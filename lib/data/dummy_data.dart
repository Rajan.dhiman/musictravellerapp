import 'package:flutter/material.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/res/instruments_icons.dart';

class DummyData{

  static const List<String> instruments = [
    "Grand piano",
    "Upright piano",
    "E-piano",
    "Stage piano",
    "Two pianos",
    "Keyboards",
    "Organ",
    "Harpsichord",
    "Harp",
    "Drum set",
    "Congas",
    "Timpani",
    "Marimba",
    "Vibraphone",
    "Xylophone",
    "Glockenspiel",
    "Gongs",
    "Bongo drums",
    "Classical Guitar",
    "Acoustic Guitar",
    "Western Guitar",
    "Electric Guitar",
    "Guitar Amplifier",
    "Electric Bass",
    "Bass Amplifier",
    "Loop Machine",
    "PA System",
    "Violin",
    "Viola",
    "Cello",
    "Double bass",
    "Ukulele",
    "Latin Percussion",
    "Banjo",
    "Sitar",
    "Zither",
    "Balalaika",
    "Accordion",
  ];
  static const List<int> instrumentIds = [
    1,
    61,
    34,
    10,
    71,
    62,
    20,
    19,
    36,
    9,
    69,
    63,
    33,
    32,
    30,
    41,
    48,
    59,
    65,
    3,
    64,
    4,
    11,
    66,
    67,
    35,
    72,
    2,
    5,
    6,
    8,
    68,
    70,
    60,
    43,
    53,
    55,
    57,
  ];

  static List<Instrument> getAllInstruments(){
    List<Instrument> instruments = List<Instrument>();
    instruments.add(Instrument(
        id: "1",
        type: "Grand Piano",
        order: 1
    ));
    instruments.add(Instrument(
        id: "2",
        type: "Violin",
        order: 25
    ));
    instruments.add(Instrument(
        id: "3",
        type: "Acoustic Guitar",
        order: 18
    ));
    instruments.add(Instrument(
        id: "4",
        type: "Electric Guitar",
        order: 20
    ));
    instruments.add(Instrument(
        id: "5",
        type: "Viola",
        order: 26
    ));
    instruments.add(Instrument(
        id: "6",
        type: "Cello",
        order: 27
    ));
    instruments.add(Instrument(
        id: "6",
        type: "Cello",
        order: 27
    ));
    return instruments;
  }

  // not used for anything atm
  static const Map<String, IconData> instrumentIcons = const {
    "Grand piano": Instruments.piano,
    "Upright piano": Instruments.piano_upright,
    "E-piano": null,
    "Stage piano": null,
    "Two pianos": null,
    "Keyboards": null,
    "Organ": null,
    "Harpsichord": null,
    "Harp": Instruments.harp,
    "Drum set": Instruments.drum_set,
    "Congas": Instruments.conga,
    "Timpani": null,
    "Marimba": null,
    "Vibraphone": null,
    "Xylophone": Instruments.xylophone,
    "Glockenspiel": null,
    "Gongs": null,
    "Bongo drums": null,
    "Classical Guitar": Instruments.acoustic_guitar,
    "Acoustic Guitar": null,
    "Western Guitar": null,
    "Electric Guitar": Instruments.electric_guitar,
    "Guitar Amplifier": Instruments.amp,
    "Electric Bass": Instruments.bass_guitar,
    "Bass Amplifier": Instruments.amp,
    "Loop Machine": null,
    "PA System": null,
    "Violin": Instruments.violin,
    "Viola": null,
    "Cello": Instruments.violoncello,
    "Double bass": Instruments.contrabass,
    "Ukulele": null,
    "Latin Percussion": null,
    "Banjo": Instruments.banjo,
    "Sitar": null,
    "Zither": null,
    "Balalaika": null,
    "Accordion": Instruments.accordion,
  };

  static const String loremIpsum1 = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est";

  static MtUser getDummyUser(){
    return MtUser(
      id: 1,
      name: "Nat King Cole",
      avatarUrl: "https://tse3.mm.bing.net/th?id=OIP.BtnnMb9JzYyCyWaaXl41OAHaGL&pid=Api",
      occupation: "Musician",
      location: "Santa Monica, California",
      education: "Pangea Music University",
      bio: DummyData.loremIpsum1,
      soundCloudUrl: "https://soundcloud.com/natkingcole",
      youtubeUrl: "https://www.youtube.com/channel/UCjP351k141Rq881npFWU2-g",
      facebookUrl: "https://www.facebook.com/natkingcole/",
      websiteUrl: "https://www.natkingcole.com/",
      inspirationUrls: ["https://www.youtube.com/watch?v=D9Cs_zb4q14",
                          "https://www.youtube.com/watch?v=qDQpZT3GhDg",
                            "https://www.youtube.com/watch?v=5m2HN2y0yV8"],
      recordingUrls: ["https://www.youtube.com/watch?v=qJ-BuRs9Hng",
                        "https://www.youtube.com/watch?v=JFyuOEovTOE",
                          "https://soundcloud.com/natkingcole/polka-dots-and-moonbeams"],
    );
  }

  static List<Revieww> getDummyReviews(){
    List<Revieww> reviews = List<Revieww>();
    reviews.add(Revieww(12, 132, 5,
        "Wonderful space! Well tuned, soft and sensitive piano.", "12 august"));
    reviews.add(Revieww(
        13,
        132,
        3,
        "So so, I've had better, but still worth the price. Location is good.",
        "14 august"));
    reviews.add(Revieww(
        14,
        132,
        5,
        "Fantastic! Best space I've found around here. Host is very kind. Would recommend.",
        "19 august"));
    reviews.add(Revieww(
        15,
        132,
        5,
        "I'm too shy to have a picture, but the space is pretty cool still, you know.",
        "22 august"));
    reviews.add(Revieww(
        15,
        132,
        5,
        "I'm too shy to have a picture, but the space is pretty cool still, you know.",
        "22 august"));
    reviews.add(Revieww(12, 132, 5, null, "22 august"));
    return reviews;
  }

  static List<MtUser> getDummyUsers(){
    List<MtUser> users = List<MtUser>();
    users.add(MtUser(
        id: 12,
        name: "Bill Evans",
        avatarUrl: "https://media.npr.org/assets/music/specials/piano_jazz_30/artists/evans-0226346d0fbb14857978305dfc1d3ce8967e3fb4-s800-c85.jpg"
    ));
    users.add(MtUser(
        id: 13,
        name: "Jimie Hendrix",
        avatarUrl: "https://i.guim.co.uk/img/static/sys-images/Guardian/Pix/pictures/2013/12/10/1386678719846/Jimi-Hendrix-009.jpg?width=300&quality=85&auto=format&fit=max&s=7658e62926d7e0818afae3c41b558a7c"
    ));
    users.add(MtUser(
        id: 14,
        name: "Bobby McFerin",
        avatarUrl: "https://media.npr.org/assets/img/2013/07/03/b_mcferrin_wide-04fe6d64a593789ea5b63fb1025fb61a09d9ba7c-s800-c85.jpg"
    ));
    users.add(MtUser(
        id: 15,
        name: "Jakob H."
    ));
    return users;
  }

}
