import 'dart:convert';

import 'package:http/http.dart';
import 'package:mt_ui/data/rates.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'currency_response_model.dart';

class CurrencyConvertService {
  
  String baseurl = "https://api.exchangerate-api.com/v4/latest/";
  String baseFixerUrl = "http://data.fixer.io/api/latest?access_key=e0ce1d818ec4edb69fda3f80bd0f8462";
  // String apiKey = "defff1f9da263b5ebd3017ac";

  // String get rates {
  //   return url + "bulk/" + apiKey + "/";
  // }

  // String get convertion {
  //   return url + "pair/" + apiKey + "/";
  // }

  Future<bool> getRates1() async {
    try{
        final currencyParam = await  SharedPreferencesHelper.getCurrencyPreference();// preferences.getString("currencyParam") ?? '';
        var url =  baseurl + "EUR";//currencyParam;
        final response = await get(url);
        final map = json.decode(response.body);
    
        if (map["rates"] != null) {
          final ratesJSON = map["rates"];
          final ratesObject = new Rates.fromJson(ratesJSON);
         var status  = await saveToLocal(ratesObject.toJson(),currencyParam);
         return status;
        }
        else {
          final error = new ApiError.fromJson(map);
          print(error);
           return false;
        }
    }catch(error){
      print('[FILE] CurrencyConvertService.dart');
      print('Error in call currency conversion api :- $error');
    }
    
  }

  Future<bool> saveToLocal(Map<String,dynamic> map, String currencyParam) async {
    var rate = double.parse(map[currencyParam.toUpperCase()].toString());
    var status  = await  SharedPreferencesHelper.setCurrencyRate(rate,currencyPref: currencyParam);
    return status;
  }
    /// [Currencyconversion]
  // Future<bool> getFixerRates() async {
    Future<bool> getRates() async {
    try{
        final currencyParam = await  SharedPreferencesHelper.getCurrencyPreference();// preferences.getString("currencyParam") ?? '';
        var url = baseFixerUrl + "&base=" + "EUR";//currencyParam;// baseurl + "EUR";/
        final response = await get(url);
        final map = json.decode(response.body);
    
        if (map["success"] == true) {
          final ratesJSON = map["rates"];
          
          final ratesObject = new CurrencyResponse.fromJson(map);
         var status  = await saveToLocal(ratesObject.rates,currencyParam);
         return status;
        }
        else {
          final error = new ApiError.fromJson(map);
          print(error);
           return false;
        }
    }catch(error){
      print('[FILE] CurrencyConvertService.dart');
      print('Error in call currency conversion api :- $error');
    }
    
  }

}