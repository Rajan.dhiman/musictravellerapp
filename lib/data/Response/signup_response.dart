import 'dart:convert';

class SignupResponse {
    List<SignupResponseElement> errors;
    Data data;

    SignupResponse({
        this.errors,
        this.data,
    });

    factory SignupResponse.fromRawJson(String str) => SignupResponse.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory SignupResponse.fromJson(Map<String, dynamic> json) => SignupResponse(
        errors: json["errors"] == null ? null : List<SignupResponseElement>.from(json["errors"].map((x) => SignupResponseElement.fromJson(x))),
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "errors": errors == null ? null : List<dynamic>.from(errors.map((x) => x.toJson())),
        "data": data == null ? null : data.toJson(),
    };
}

class Data {
    dynamic register;

    Data({
        this.register,
    });

    factory Data.fromRawJson(String str) => Data.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        register: json["register"],
    );

    Map<String, dynamic> toJson() => {
        "register": register,
    };
}

class SignupResponseElement {
    String message;

    SignupResponseElement({
        this.message,
    });

    factory SignupResponseElement.fromRawJson(String str) => SignupResponseElement.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory SignupResponseElement.fromJson(Map<String, dynamic> json) => SignupResponseElement(
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
    };
}

