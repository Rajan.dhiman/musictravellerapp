
import 'dart:convert';

import 'package:mt_ui/helpers/constants.dart';

class CountryLocationModel {
    Data data;

    CountryLocationModel({
        this.data,
    });

    factory CountryLocationModel.fromRawJson(String str) => CountryLocationModel.fromJson(json.decode(str));


    factory CountryLocationModel.fromJson(Map<String, dynamic> json) => CountryLocationModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
    );

   
}

class Data {
    SearchDropdownData searchDropdownData;

    Data({
        this.searchDropdownData,
    });

    factory Data.fromRawJson(String str) => Data.fromJson(json.decode(str));


    factory Data.fromJson(Map<String, dynamic> json) => Data(
        searchDropdownData: json["searchDropdownData"] == null ? null : SearchDropdownData.fromJson(json["searchDropdownData"]),
    );

}

class SearchDropdownData {
    List<Country> countries;
    
    SearchDropdownData({
        this.countries,
    });

    factory SearchDropdownData.fromRawJson(String str) => SearchDropdownData.fromJson(json.decode(str));


    factory SearchDropdownData.fromJson(Map<String, dynamic> json) => SearchDropdownData(
        countries: json["countries"] == null ? null : List<Country>.from(json["countries"].map((x) => Country.fromJson(x))),
    );
   List<Country> getOrderedCountry(){
      countries.sort((x,y) => x.name.compareTo(y.name));
     return countries;
   }
}

class Country {
    String code;
    String name;
    bool isCitiesVisible;
    List<City> cities;

    Country({
        this.code,
        this.cities,
        this.name,
        this.isCitiesVisible 
    });

    factory Country.fromRawJson(String str) => Country.fromJson(json.decode(str));


    factory Country.fromJson(Map<String, dynamic> json) => Country(
        code: json["code"] == null ? null : json["code"],
        name: MtConstants.getCountryName(json["code"] ),
        isCitiesVisible :false,
        cities: json["cities"] == null ? null : List<City>.from(json["cities"].map((x) => City.fromJson(x))),
    );

   List<City> getOrderedCityList(){
      cities.sort((x,y) => x.name.compareTo(y.name));
     return cities;
   }
}

class City {
    int id;
    String name;
    int statusCode;

    City({
        this.id,
        this.name,
        this.statusCode,
    });

    factory City.fromRawJson(String str) => City.fromJson(json.decode(str));


    factory City.fromJson(Map<String, dynamic> json) => City(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        statusCode: json["statusCode"] == null ? null : json["statusCode"],
    );

   
}
