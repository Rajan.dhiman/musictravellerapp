// To parse this JSON data, do
//
//     final userMediaResponse = userMediaResponseFromJson(jsonString);

import 'dart:convert';

import 'package:mt_ui/data/models.dart';

class UserMediaResponse {
    Data data;

    UserMediaResponse({
        this.data,
    });

    factory UserMediaResponse.fromRawJson(String str) => UserMediaResponse.fromJson(json.decode(str));

    // String toRawJson() => json.encode(toJson());

    factory UserMediaResponse.fromJson(Map<String, dynamic> json) => UserMediaResponse(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
    );

    // Map<String, dynamic> toJson() => {
    //     "data": data == null ? null : data.toJson(),
    // };
}

class Data {
    List<UserMedia> userMedia;

    Data({
        this.userMedia,
    });

    factory Data.fromRawJson(String str) => Data.fromJson(json.decode(str));

    // String toRawJson() => json.encode(toJson());

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        userMedia: json["userMedia"] == null ? null : List<UserMedia>.from(json["userMedia"].map((x) => UserMedia.fromJson(x))),
    );

    // Map<String, dynamic> toJson() => {
    //     "userMedia": userMedia == null ? null : List<dynamic>.from(userMedia.map((x) => x.toJson())),
    // };
}

class UserMedia {
    String id;
    String type;
    String section;
    String url;
    bool isPrivate;
    User user;

    UserMedia({
        this.id,
        this.type,
        this.section,
        this.url,
        this.isPrivate,
        this.user,
    });

    factory UserMedia.fromRawJson(String str) => UserMedia.fromJson(json.decode(str));

    // String toRawJson() => json.encode(toJson());

    factory UserMedia.fromJson(Map<String, dynamic> json) => UserMedia(
        id: json["id"] == null ? null : json["id"],
        type: json["type"] == null ? null : json["type"],
        section: json["section"] == null ? null : json["section"],
        url: json["url"] == null ? null : json["url"],
        isPrivate: json["isPrivate"] == null ? null : json["isPrivate"],
        user: json["user"] == null ? null : User.fromAPIJson(json["user"]),
    );

    // Map<String, dynamic> toJson() => {
    //     "id": id == null ? null : id,
    //     "type": type == null ? null : type,
    //     "section": section == null ? null : section,
    //     "url": url == null ? null : url,
    //     "isPrivate": isPrivate == null ? null : isPrivate,
    //     "user": user == null ? null : user.toJson(),
    // };
}

// class User {
//     String name;

//     User({
//         this.name,
//     });

//     factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

//     String toRawJson() => json.encode(toJson());

//     factory User.fromJson(Map<String, dynamic> json) => User(
//         name: json["name"] == null ? null : json["name"],
//     );

//     Map<String, dynamic> toJson() => {
//         "name": name == null ? null : name,
//     };
// }
