
import 'dart:convert';

class ProfilePicResponse {
    String image;
    String message;

    ProfilePicResponse({
        this.image,
        this.message,
    });

    factory ProfilePicResponse.fromRawJson(String str) => ProfilePicResponse.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory ProfilePicResponse.fromJson(Map<String, dynamic> json) => ProfilePicResponse(
        image: json["image"] == null ? null : json["image"],
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "image": image == null ? null : image,
        "message": message == null ? null : message,
    };
}
