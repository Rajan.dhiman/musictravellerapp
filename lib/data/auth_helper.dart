import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'dart:async';
import 'dart:io' show HttpHeaders, Platform;
import 'package:http/http.dart' as http;

import 'package:mt_ui/helpers/constants.dart';

import 'Response/signup_response.dart';

class AuthHelper {
  Uri getUrlForQuery(String query) => Uri.http(
      MtConstants.baseUrl, "/graphql", {"query": query});
  final Client _client = Client();
  
  Future<bool> getAccessToken()async{
    var token =await  SharedPreferencesHelper.getRefreshToken();
    if(token != null && token.isNotEmpty){
         var query = '''mutation {
           refreshToken(
             refreshToken: "$token"
            ){
              newToken
              success
              error
            }
         }''';
          log('query: $query');

       var isTokenReceived = await _client
            .post(getUrlForQuery(query))
            .then((result) {
               var body =result.body;
               print("[refreshToekn] $body");
               return body;
            })
            .then(json.decode)
            .then((json) {
              if(json['data'] == null || json['data']['refreshToken'] == null){
                return false;
              }
              else{
                 var errors = json['data']['refreshToken']['errors'];
                 var success = json['data']['refreshToken']['success'];
                 if (errors == null &&  success == "True") {
                    String acesstoken = json['data']['refreshToken']['newToken'];
                    SharedPreferencesHelper.setUserLogin(acesstoken, token);
                   return true;
                 } else {
                   return false;
                 }
              }
        });
        return isTokenReceived;
    }
    else{
      print('[Storage] Access token not found in storage');
      return Future.value(false);
    }
  }
  Future<AuthResult> connectWithGoogle() async {
    // Sign in with Google
    GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    // googleUser will be null if user didn't select an account from the account dialog
    if (googleUser == null) {
      return AuthResult(errorMessage: "Sign In failed 01");
    }

    GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    if (googleAuth == null) {
      return AuthResult(errorMessage: "Sign In failed 02");
    }

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    try {
      FirebaseUser user =
          (await FirebaseAuth.instance.signInWithCredential(credential)).user;

      if (user != null) {
        // sign in success
        print("Sign in sucess");

        return AuthResult(user: user);
      } else {
        // sign in failed
        return AuthResult(errorMessage: "Sign In failed 03");
      }
    } catch (exception) {
      // ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL
      if (exception
          .toString()
          .contains("ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL")) {
        print(exception.toString());
        return AuthResult(
          errorMessage:
              "Looks like you previously signed in with a different provider. Try sign in with facebook or email.",
        );
      }

      // ERROR_INVALID_CREDENTIAL
      if (exception.toString().contains("ERROR_INVALID_CREDENTIAL")) {
        print(exception.toString());
        return AuthResult(
          errorMessage: "Sign In failed: invalid credential",
        );
      }

      // ERROR_USER_DISABLED
      if (exception.toString().contains("ERROR_USER_DISABLED")) {
        print(exception.toString());
        return AuthResult(
          errorMessage: "This user is disabled",
        );
      }
    }

    print("failed to log in with google");
    return AuthResult(errorMessage: "Sign In failed 04");
  }

  Future<AuthResult> connectWithFacebook() async {
    final facebookLogin = FacebookLogin();
    //facebookLogin.loginBehavior = FacebookLoginBehavior.nativeOnly;
    facebookLogin.loginBehavior = FacebookLoginBehavior.nativeWithFallback;
    if (facebookLogin == null) {
      return null;
    }

    try {
      final result = await facebookLogin.logIn(['email','public_profile']);

      if (result.status == FacebookLoginStatus.cancelledByUser) {
        // cancelled
        print("cancelled by user");
      } else if (result.status == FacebookLoginStatus.error) {
        // error
        print("error signing in");
        print(result.errorMessage);
        return AuthResult(
          errorMessage: "Sign In failed: " + result.errorMessage,
        );
      } else if (result.status == FacebookLoginStatus.loggedIn) {

        print("Facebook Response $result");
        AuthCredential credential = FacebookAuthProvider.getCredential(
            accessToken: result.accessToken.token);

        if (credential == null) {
          print("failed to get credential");
          return AuthResult(
            errorMessage: "Sign In failed",
          );
        }

        FirebaseUser user =
            (await FirebaseAuth.instance.signInWithCredential(credential)).user;
        if (user != null) {
          // success!!
          print("facebook login success");

          await fbConnect(
            user
          );
          return AuthResult(user: user);
        } else {
          print("firebase user is null");
          return AuthResult(
            errorMessage: "Sign In failed",
          );
        }
      }
    } catch (exception) {
      // ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL
      if (exception
          .toString()
          .contains("ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL")) {
        print(exception.toString());
        return AuthResult(
          errorMessage:
              "Looks like you previously signed in with a different provider. Try sign in with Google or email.",
        );
      }

      // ERROR_INVALID_CREDENTIAL
      if (exception.toString().contains("ERROR_INVALID_CREDENTIAL")) {
        print(exception.toString());
        return AuthResult(
          errorMessage: "Sign In failed: invalid credential",
        );
      }

      // ERROR_USER_DISABLED
      if (exception.toString().contains("ERROR_USER_DISABLED")) {
        print(exception.toString());
        return AuthResult(
          errorMessage: "This user is disabled",
        );
      }

      print("unknown error while signing in with facebook");
      print(exception);
      return AuthResult(
        errorMessage: "Unknown error: " + exception.toString(),
      );
    }

    print("failed to log in with facebook");
    return AuthResult(
      errorMessage: "Sign In failed",
    );
  }

  Future<AuthResult> createAccountWithEmail(
      String email, String password) async {
    try {
      final FirebaseUser user = (await FirebaseAuth.instance
              .createUserWithEmailAndPassword(email: email, password: password))
          .user;

      if (user != null) {
        // send email verification
        user.sendEmailVerification();

        // user get's automatically signed in, but they should
        // verify email first, so sign out
        await FirebaseAuth.instance.signOut();

        // TODO create user on API

        return AuthResult(user: user);
      } else {
        return AuthResult(errorMessage: "Sign In failed");
      }
    } catch (exception) {
      // ERROR_EMAIL_ALREADY_IN_USE
      if (exception.toString().contains("ERROR_EMAIL_ALREADY_IN_USE")) {
        return AuthResult(
          errorMessage: "A user with this email address already exists",
        );
      }

      // ERROR_WEAK_PASSWORD
      if (exception.toString().contains("ERROR_WEAK_PASSWORD")) {
        return AuthResult(
          errorMessage: "Your password is not strong enough",
        );
      }

      // ERROR_INVALID_EMAIL
      if (exception.toString().contains("ERROR_INVALID_EMAIL")) {
        return AuthResult(
          errorMessage: "The email address appears invalid",
        );
      }

      // something else went wrong
      print(exception);
      return AuthResult(
        errorMessage: "Unknown error: " + exception.toString(),
      );
    }
  }

  // Sign out from google, facebook, or just firebase (email)
  Future<Null> signOut() async {
    print('********************  User initiate logout *******************');
    GoogleSignIn googleSignIn = GoogleSignIn();
    FacebookLogin facebookLogin = FacebookLogin();
    if (await googleSignIn.isSignedIn()) {
      await FirebaseAuth.instance.signOut();
      await googleSignIn.disconnect();
      await googleSignIn.signOut();
    } else if (await facebookLogin.isLoggedIn) {
      await facebookLogin.logOut();
      await FirebaseAuth.instance.signOut();
    } else {
      await FirebaseAuth.instance.signOut();
    }
    SharedPreferencesHelper.setLogout(true);
  }

  Future<String> fbConnect(FirebaseUser user) async {
    String email = user.email;
    String name = user.displayName;
    String uid = user.uid;
    String avatar = user.photoUrl;
     var userAgent = "";

      if (Platform.isIOS) {
          userAgent = "ios";
        }
        if (Platform.isAndroid) {
          userAgent = "android";
        }
    var query = '''mutation {
      fbConnect(email: "$email", name: "$name", uid: "$uid", avatar: "$avatar" , userAgent : "$userAgent") {
        accessToken
        refreshToken
      }
    }''';

    print('query: $query');

    await _client
        .post(getUrlForQuery(query))
        .then((result) => result.body)
        .then(json.decode)
        .then((json) {
      var errors = json['errors'];
      if (errors == null) {
        String accessToken = json['data']['fbConnect']['accessToken'];
        String refreshToken = json['data']['fbConnect']['refreshToken'];
        SharedPreferencesHelper.setUserLogin(accessToken, refreshToken);
        SharedPreferencesHelper.setLoginThroughFacebook(true);
        return "success";
      } else {
        return errors;
      }
    });
    return "success";
  }

  Future<String> signUp(String email, String password, String name) async {
     var userAgent = "";

      if (Platform.isIOS) {
          userAgent = "ios";
        }
        if (Platform.isAndroid) {
          userAgent = "android";
        }
    var query = '''mutation {
      register(password: "$password", email: "$email", name: "$name",  userAgent: "$userAgent") {
       # accessToken
       # refreshToken
        success
       message
      }
    }''';

    log('query: $query');

   var status = await _client
        .post(getUrlForQuery(query))
        .then((result) {
          var val =result.body;
          return val;
        })
        .then(json.decode)
        .then((json) {
      var response = SignupResponse.fromJson(json);
      // var sucess = json['data']['register']['success'] ?? false;
      if (response.data != null && response.data.register != null) {
        // String accessToken = json['data']['register']['accessToken'];
        // String refreshToken = json['data']['register']['refreshToken'];
        // if(true){
        //     SharedPreferencesHelper.saveCredential(email: email,password: password);
        //   }
        // SharedPreferencesHelper.setUserLogin(accessToken, refreshToken);
        SharedPreferencesHelper.setLoginThroughFacebook(false);
        return "success";
      } else {
        return response.errors.first.message;
      }
    });
    return status;
  }

  Future<String> signIn(String email, String password,{bool remember = false}) async {
    String res = "";

    var query = '''mutation {
        auth(password: "$password", username: "$email") {
        accessToken
        refreshToken
        message
      }
    }''';

    log('query: $query');

    print('query: $query');

    await _client
        .post(getUrlForQuery(query))
        .then((result) => result.body)
        .then(json.decode)
        .then((json) {
          print('response: $json');
            String message = json['data']['auth']['message'];
          if (json['data']['auth']['accessToken'] != null) {
            String accessToken = json['data']['auth']['accessToken'];
            String refreshToken = json['data']['auth']['refreshToken'];
            SharedPreferencesHelper.setUserLogin(accessToken, refreshToken);
           
          // SharedPreferencesHelper.setLogout(false);
           SharedPreferencesHelper.setLoginThroughFacebook(false);
           SharedPreferencesHelper.saveCredential(email: email,password: password,remember: remember);
          
            res = null;
          } else {
            res = message;
          }
        });
    return res;
  }

  Future<String> processUserData() async {
    var userDetailsQuery = '''query {
      user{
        id
        email
        username
        name
        isHost
        occupation
        bio
        isPublic
        avatar
        #password
        #phoneNumber
        #gender
        #birthDate
      }
    }''';

    String accessToken = await SharedPreferencesHelper.getAccessToken() ?? '';
    String bearer = 'Bearer ' + accessToken;
    Map<String, String> headers = {HttpHeaders.authorizationHeader: bearer};

    var status  = await _client
        .get(getUrlForQuery(userDetailsQuery), headers: headers)
        .then((result){
          var data =  result.body;
          print(data);
          return data;
        })
        .then(json.decode)
        .then((json) {
      if (json['data']['user'] != null) {
        print(json['data']);
        SharedPreferencesHelper.setUserData(
          'true',
          json['data']['user']['id'],
          json['data']['user']['email'],
          json['data']['user']['username'],
          json['data']['user']['name'],
          json['data']['user']['isHost'],
          json['data']['user']['occupation'],
          json['data']['user']['bio'],
          json['data']['user']['avatar'],
          // json['data']['user']['password'],
          // json['data']['user']['phoneNumber'],
          // json['data']['user']['gender'],
          // json['data']['user']['birthDate'],
        );
        return "success";
      }
      else{
        if(json["errors"] != null && json["errors"].length > 0){
          var error = json["errors"][0]["message"];
          print(error);
          return error ?? "Server error try again";
        }
        else{
          return "fail";
        }
      }
    });
    return status;
  }


  Future<void> getUserData() async {}
}

class AuthResult {
  final FirebaseUser user;
  final String errorMessage;

  AuthResult({this.user, this.errorMessage});
}


