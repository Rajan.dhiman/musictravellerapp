import 'dart:collection';

class Rates {
  double usd;
  double eur;
  double gbp;

  var rates = new LinkedHashMap();

  Rates();

  void initValues() {
    rates['USD'] = new Map.from({'symbol': '\$', 'flag': '🇺🇸', 'definition': "United States Dollar", 'value': usd});
    rates['EUR'] = new Map.from({'symbol': '‎€', 'flag': '🇪🇺', 'definition': "Euro", 'value': eur});
    rates['GBP'] = new Map.from({'symbol': '£', 'flag': '🇬🇧', 'definition': "Pound Sterling", 'value': gbp});
    
  }

  Rates.fromJson(Map<String, dynamic> json):
   usd = (json['USD'] != null) ? json['USD'] + 0.0 : 0.0,
   eur = (json['EUR'] != null) ? json['EUR'] + 0.0 : 0.0,
   gbp = (json['GBP'] != null) ? json['GBP'] + 0.0 : 0.0;
  
  toJson(){
    return {
        "USD" : usd,
        "EUR" : eur,
        "GBP" : gbp,
    };
  }

}

class ApiError {
  final String error;

  ApiError.fromJson(Map<String, dynamic> json):
    error = json['error'];

  Map<String, dynamic> toJson() => {
    'error': error,
  };
}

enum Currency{
  Doller,
  Pound,
  Euro
}
