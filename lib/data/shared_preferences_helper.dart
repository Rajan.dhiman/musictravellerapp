import 'dart:async';
import 'dart:developer';

import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/data/rates.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class SharedPreferencesHelper {

  // Search page list mode ==============================================

  static const String KEY_SEARCH_PAGE_LIST_MODE = "search_page_list_mode";

   var ENV_MODE = "envMode";

  static const bool KEY_SEARCH_PAGE_LIST_MODE_DEFAULT = false;

  static Future<bool> isSearchPageListMode() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(KEY_SEARCH_PAGE_LIST_MODE) ?? KEY_SEARCH_PAGE_LIST_MODE_DEFAULT;
  }

  static Future<bool> setSearchPageListMode(bool isListMode) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(KEY_SEARCH_PAGE_LIST_MODE, isListMode);
  }

  // Search page last city search for ==========================================

  static const String KEY_LAST_SEARCHED_CITIES = "last_searched_cities";

  /// return null if empty
  static Future<List<String>> getLastSearchedCities() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(KEY_LAST_SEARCHED_CITIES);
  }

  static Future<bool> setLastSearchedCitiesList(List<String> cities) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList(KEY_LAST_SEARCHED_CITIES, cities);
  }

  /// ads city to the list of recent searched cities
  static Future<bool> setLastSearchedCity(String city) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> recentCities = await getLastSearchedCities();
    if(recentCities == null){
      recentCities = List();
    }
    recentCities.add(city);
    return prefs.setStringList(KEY_LAST_SEARCHED_CITIES, recentCities);
  }

  // Search Filters - save state  ==========================================

  static const String KEY_SEARCH_FILTERS = "search_filters";

  /// returns filters in this order [city, date, price, instruments], null if nothing
  static Future<List<String>> getSearchFilters() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(KEY_SEARCH_FILTERS);
  }

  /// save filters in this order [city, date, price, instruments]
  static Future<bool> setSearchFilters(List<String> filters) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList(KEY_SEARCH_FILTERS, filters);
  }

  // Favorites ===========================================================

  static const String KEY_FAVORITES = "favorites";

  static Future<bool> isSpaceFavorite(Space space) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> favs = prefs.getStringList(KEY_FAVORITES);

    // return true if space id is in the list of faves
    if(favs != null){
      return (favs.contains(space.spaceId));
    } else {
      return (false);
    }
  }

  static Future<bool> setSpaceFavorite(Space space, bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = space.spaceId;
    List<String> favs = prefs.getStringList(KEY_FAVORITES);

    // if list is null, never created, create and add
    if(favs == null){
      favs = List<String>();
      favs.add(key);
    }

    // Remove key if value is false and list contains key
    if(!value && favs.contains(key)){
      favs.remove(key);

    // Add key if value is true and list does not contain key
    }else if(value && !favs.contains(key)){
      favs.add(key);
    }

    return prefs.setStringList(KEY_FAVORITES, favs);
  }

  static Future<List<String>> getFavouriteSpaces() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> items = prefs.getStringList(KEY_FAVORITES);
    if(items != null){
      return items;
    }else{
      return List<String>();
    }
  }

  // Auth ================================================================

  static Future<void> setUserLogin(String accessToken, String refreshToken) async {
    print('setting setUserLogin , access and refreshtoken');
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString("accessToken", accessToken);
    await pref.setString("refreshToken", refreshToken);
  }
  
  static Future<String> getAccessToken() async {
    print('getting getAccessToken');
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var accessToken = prefs.getString("accessToken");
    return accessToken;
  }
  static Future<String> getRefreshToken() async {
    print('getting getRefreshToken');
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var accessToken = prefs.getString("refreshToken");
    return accessToken;
  }
  
  /// [Facebook Login ?] me ================================================================
  static Future<bool> isLoginThroughFacebook()async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = prefs.getBool('facebookLogin') ?? false;
    return value;
  }
  /// [Facebook Login ?] me ================================================================
  static Future<bool> setLoginThroughFacebook(bool login)async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = prefs.setBool('facebookLogin',login);
    return value;
  }
  // Remember me ================================================================
  static Future<bool> getRememberMe() async {
    // print('getting getCredential');
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = prefs.getBool('remember') ?? false;
    return value;
  }
  static Future<List<String>> getCredential() async {
    print('getting getCredential');
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString('email');
    var password = prefs.getString('password');
    List<String> list;
    if(email != null && password != null){
      list = [];
      list.add(email);
      list.add(password);
    }
    return list;
  }
  static Future<void> saveCredential({String email,String password,bool remember = false}) async {
    print('Credentials save');
    final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('email',email);
      prefs.setString('password',password);
      prefs.setBool('remember',remember);
  }
  // User Data ================================================================

  static Future<void> setUserData(
      String isAuth,
      String id,
      String email,
      String username,
      String name,
      bool isHost,
      String occupation,
      String bio,
      String avatar,
      // String password,
      // String phoneNumber,
      // String gender,
      // String birthDate,
    ) async {
      print('setting setUserData');
      final SharedPreferences pref = await SharedPreferences.getInstance();

      var userDataMap = {};
      userDataMap['isAuth'] = isAuth;
      userDataMap['id'] = id;
      userDataMap['email'] = email;
      userDataMap['username'] = username;
      userDataMap['name'] = name;
      userDataMap['isHost'] = isHost;
      userDataMap['occupation'] = occupation;
      userDataMap['bio'] = bio;
      userDataMap['avatar'] = avatar;
      // userDataMap['password'] = password;
      // userDataMap['phoneNumber'] = phoneNumber;
      // userDataMap['gender'] = gender;
      // userDataMap['birthDate'] = birthDate;

      var userDataMapString = json.encode(userDataMap);
      await pref.setString("userDataMap", userDataMapString);
    }
    
  static Future<String> getUserData() async {
    print('getting getUserData');
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var userDataMap = prefs.getString("userDataMap");
    return userDataMap;
  }
  static Future<void>setLogout(bool value)async{
    print('setting logout $value in db');
    final SharedPreferences pref = await SharedPreferences.getInstance();
      pref.clear();
      print("Local storage clear");
    if(value){
    }
    await pref.setBool("logout", value);
  }
  static Future<bool>getLogout()async{
    final SharedPreferences pref = await SharedPreferences.getInstance();
     var flag =  pref.getBool("logout");
     return flag;
  }
  static Future<void> clearStorage() async {
    log('setting clearStorage');
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.clear();
  }
  static Future<void> setStripeToken(String stripeToken) async {
    print('setting setStripeToken');
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString("accessToken", stripeToken);
  }

   /// intilise currency prefrence 
  static Future<void> initCurrencyPreferences({String currency}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if(currency != null){
        await preferences.setString("currencyParam", currency);
      print("Successfully updated User Defaults currency to $currency");
    }
    else if (!(preferences.getKeys().contains("currencyParam"))) {
      await preferences.setString("currencyParam", 'EUR');
      // print("Successfully Initialized User Defaults currency");
    }
    else {
      // print("User Defaults currency Already Instanciated");
    }
  }
  /// Get selected [currency rate ] 
  static Future<double> getCurrencyRate() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    double rate = preferences.getDouble('rate');
    return rate;
  }
  /// Store selected [currency rate ]
  static Future<bool> setCurrencyRate(double rate,{String currencyPref}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var status  = await preferences.setDouble('rate',rate);
    // print('Currency Rate is stored in LocalDb  Currency :- $currencyPref  Rate:- $rate');
    return status;
  }
  /// Get stored [currency prefrence rate] to call currency conversion api
  static Future<String> getCurrencyPreference() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final currencyParam = preferences.getString("currencyParam") ?? 'EUR';
    return currencyParam;
  }
   /// Store room id  [Required after login  ]
  static Future<bool> saveRoomId(int roomId) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var status  = await preferences.setInt('roomId',roomId);
    print('Room Id is stored in LocalDb   :- $roomId ');
    return status;
  }
  /// Get room ID [currency prefrence rate] 
  static Future<int> getRoomId() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final roomId = preferences.getInt("roomId");
    return roomId;
  }
  /// Get room ID [currency prefrence rate] 
  static Future<void> removeRoomId() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove("roomId");
  }



    /// intilise baseurl prefrence 
  static Future<void> initBaseUrlPreferences({String url}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if(url != null){
        await preferences.setString("baseUrl", url);
      
      print("Successfully updated base url to $url");
    }
    else if (!(preferences.getKeys().contains("baseUrl"))) {
     // await preferences.setString("baseUrl", 'EUR');
     //  print("Successfully Initialized User Defaults currency");
    }
  }


   static Future<String> getBaseUrl() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final baseUrl = preferences.getString("baseUrl");
    return baseUrl;
  }

}
