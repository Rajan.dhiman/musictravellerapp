

import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:mt_ui/helpers/constants.dart';

import '../utils.dart';
import 'models.dart';

class InvoiceResponse {
    Data data;

    InvoiceResponse({
        this.data,
    });

    factory InvoiceResponse.fromRawJson(String str) => InvoiceResponse.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory InvoiceResponse.fromJson(Map<String, dynamic> json,{String curencyPrefrence, double rate}) => InvoiceResponse(
        data: json["data"] == null ? null : Data.fromJson(json["data"],curencyPrefrence:curencyPrefrence,rate:rate),
    );

    Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
    };
}

class Data {
    List<Invoice> invoices;

    Data({
        this.invoices,
    });

    factory Data.fromRawJson(String str) => Data.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory Data.fromJson(Map<String, dynamic> json,{String curencyPrefrence, double rate}) => Data(
        invoices: json["invoices"] == null ? null : List<Invoice>.from(json["invoices"].map((x) => Invoice.fromJson(x,curencyPrefrence:curencyPrefrence,rate:rate))),
    );

    Map<String, dynamic> toJson() => {
        "invoices": invoices == null ? null : List<dynamic>.from(invoices.map((x) => x.toJson())),
    };
}

class Invoice {
    String id;
    int userId;
    int roomId;
    bool isPaid;
    bool isApproved;
    String insuranceRevenueCurrency;
    double insuranceRevenueLoc;
    double price;
    String timezone;
    String priceCurrency;
    int partySize;
    List<Reservation> reservations;
    UserClass user;
    UserClass room;
    String status;
    String isRated;
    /// Extra variable 
    List<BookingHour> bookingHours;

    Invoice({
        this.id,
        this.userId,
        this.roomId,
        this.isPaid,
        this.isApproved,
        this.insuranceRevenueCurrency,
        this.insuranceRevenueLoc,
        this.price,
        this.timezone,
        this.priceCurrency,
        this.reservations,
        this.user,
        this.room,
        this.partySize,
        this.bookingHours,
        this.status,
        this.isRated
    });

    factory Invoice.fromRawJson(String str) => Invoice.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());
     
     /// [Currencyconversion]
    factory Invoice.fromJson(Map<String, dynamic> json,{String curencyPrefrence, double rate}) {
      List<BookingHour> hoursList = [];
      if(json["reservations"] != null){
         List<Reservation>.from(json["reservations"].map((x) => Reservation.fromJson(x, curencyPrefrence:curencyPrefrence, rate:rate ))).forEach((y){
           hoursList.addAll(y.bookingHours);
         });
      }
       String apiCurrency = json['priceCurrency'];
       double apiPrice = json['price'] ?? 1;
       if(curencyPrefrence != null && rate != null && apiCurrency != curencyPrefrence){
         apiCurrency = curencyPrefrence;
         double mod = pow(10.0, 2); 
         apiPrice =  ((apiPrice * rate * mod).round().toDouble() / mod); 
       }
        
      return Invoice(
        id: json["id"] == null ? null : json["id"],
        userId: json["userId"] == null ? null : json["userId"],
        roomId: json["roomId"] == null ? null : json["roomId"],
        isPaid: json["isPaid"] == null ? false : json["isPaid"],
        status: json["status"] == null ? null : json["status"],
        partySize: json["partySize"] == null ? null : json["partySize"],
        isApproved: json["isApproved"] == null ? null : json["isApproved"],
        insuranceRevenueCurrency: json["insuranceRevenueCurrency"] == null ? null : json["insuranceRevenueCurrency"],
        insuranceRevenueLoc: json["insuranceRevenueLoc"] == null ? null : json["insuranceRevenueLoc"],
        price: apiPrice,//json["price"] == null ? null : json["price"],
        timezone: json["timezone"] == null ? null : json["timezone"],
        priceCurrency: apiCurrency ,// json["priceCurrency"] == null ? null : json["priceCurrency"],
        reservations: json["reservations"] == null ? null : List<Reservation>.from(json["reservations"].map((x) => Reservation.fromJson(x,status:json["status"], rate: rate, curencyPrefrence:curencyPrefrence ))),
        user: json["user"] == null ? null : UserClass.fromJson(json["user"]),
        room: json["room"] == null ? null : UserClass.fromJson(json["room"]),
        isRated: json["isRated"],
        /// Extra
        bookingHours :hoursList
      
      );   
    }

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "userId": userId == null ? null : userId,
        "roomId": roomId == null ? null : roomId,
        "isPaid": isPaid == null ? null : isPaid,
        "isApproved": isApproved == null ? null : isApproved,
        "insuranceRevenueCurrency": insuranceRevenueCurrency == null ? null : insuranceRevenueCurrency,
        "insuranceRevenueLoc": insuranceRevenueLoc == null ? null : insuranceRevenueLoc,
        "price": price == null ? null : price,
        "timezone": timezone == null ? null : timezone,
        "priceCurrency": priceCurrency == null ? null : priceCurrency,
        "reservations": reservations == null ? null : List<dynamic>.from(reservations.map((x) => x.toJson())),
        "user": user == null ? null : user.toJson(),
        "room": room == null ? null : room.toJson(),
    };

 String getTotalPriceDisplayTwoDecimals() {
    String totalPrice =
        Utils.convertToTwoDecimals(getTotalSpaceFee() + getInsuranceFee());
    var price   = reservations.first.space.location.getCurrencySymbol() + totalPrice;
    return price;
  }
  double getTotalSpaceFee() {
    double totalPrice = 0;

    for (BookingHour b in bookingHours) {
      totalPrice = totalPrice + b.price;
    }

    return totalPrice;
  }

  static const double INSURANCE_FEE = 1;
  double getInsuranceFee() => INSURANCE_FEE;

  String getInsuranceFeeDisplay() {
    var price  = reservations.first.space.location.getCurrencySymbol() + Utils.convertToTwoDecimals(getInsuranceFee())  ;
    return price;
  }
}

class Reservation implements Comparable<Reservation> {
    String id;
    int roomId;
    double roomPrice;
    String roomPriceCurrency;
    double price;
    DateTime start;
    DateTime end;
    bool isApprovedApi;
    bool isPaid;
    ReservationRoom space;
    final int status;
    int partySize;
    final List<BookingHour> bookingHours;

    String insuranceRevenueCurrency;
    double insuranceRevenueLoc;
    String timezone;
    String priceCurrency;
    
   
    Reservation( {
        this.id,
        this.roomId,
        this.roomPrice,
        this.roomPriceCurrency,
        this.price,
        this.start,
        this.end,
        this.isApprovedApi,
        this.isPaid,
        this.space,
        this.bookingHours,
        this.status,
    });

    factory Reservation.fromRawJson(String str) => Reservation.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory Reservation.fromJson(Map<String, dynamic> json, {String curencyPrefrence, double rate, String status }) {
       String apiCurrency = json['roomPriceCurrency'];
       double apiPrice = json['price'] ?? 1;
       double apiroomPrice = json['roomPrice'] ?? 1;

       /// [Currencyconversion]
       if(curencyPrefrence != null && rate != null && apiCurrency != curencyPrefrence){
         apiCurrency = curencyPrefrence;
         double mod = pow(10.0, 2); 
         apiPrice =  ((apiPrice * rate * mod).round().toDouble() / mod); 
         apiroomPrice =  ((apiroomPrice * rate * mod).round().toDouble() / mod); 
       }

      return Reservation(
        id: json["id"] == null ? null : json["id"],
        roomId: json["roomId"] == null ? null : json["roomId"],
        roomPrice: apiroomPrice,//json["roomPrice"] == null ? null : json["roomPrice"],
        roomPriceCurrency: apiCurrency,//json["roomPriceCurrency"] == null ? null : json["roomPriceCurrency"],
        price: apiPrice,//json["price"] == null ? null : json["price"],
        start: json["start"] == null ? null : DateTime.parse(json["start"]),
        end: json["end"] == null ? null : DateTime.parse(json["end"]),
        isApprovedApi:  status == "Approved" ?  true: false,//json["isApproved"] == null ? null : json["isApproved"],
        isPaid: json["isPaid"] == null ? null : json["isPaid"],
        space: json["room"] == null ? null : ReservationRoom.fromJson(json["room"], currency:apiCurrency),
        status: status == "Pending" ? 1 : status == "Approved" ? 2 : status == "Cancelled" ? 3 : status == "Rejected" ? 4 : 1,
        bookingHours: [
        BookingHour(
          dateTime: DateTime.parse(json['start']),
          start: DateTime.parse(json['start']),
          end: DateTime.parse(json['end']),
          price: apiroomPrice ,//json['roomPrice'],
          isAvailable: true,
        ),
       ],
     );
    }
    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "roomId": roomId == null ? null : roomId,
        "roomPrice": roomPrice == null ? null : roomPrice,
        "roomPriceCurrency": roomPriceCurrency == null ? null : roomPriceCurrency,
        "price": price == null ? null : price,
        "start": start == null ? null : start.toIso8601String(),
        "end": end == null ? null : end.toIso8601String(),
        // "isApproved": getStatus(),
        "isPaid": isPaid == null ? null : isPaid,
        "room": space == null ? null : space.toJson(),
    };

  static const int STATUS_PENDING = 1;
  static const int STATUS_APPROVED = 2;
  static const int STATUS_CANCELLED = 3;
  static const int STATUS_REJECTED = 4;

  bool isPending() => (status  == STATUS_PENDING );

  bool isApproved() => (status == STATUS_APPROVED);

  bool isCancelled() => (status == STATUS_CANCELLED);

  bool isRejected() => (status == STATUS_REJECTED);
  
  bool isPast() {
    BookingHour lastBookingHour = bookingHours[bookingHours.length - 1];
    return (lastBookingHour.dateTime.compareTo(DateTime.now()) < 0);
  }

  // TODO pull this from API
  static const double INSURANCE_FEE = 1;

  double getTotalSpaceFee() {
    double totalPrice = 0;

    for (BookingHour b in bookingHours) {
      totalPrice = totalPrice + b.price;
    }

    return totalPrice;
  }

  double getInsuranceFee() => INSURANCE_FEE;

  String getInsuranceFeeDisplay() {
    var price = space.location.getCurrencySymbol() + Utils.convertToTwoDecimals(getInsuranceFee()) ;
    return price;
  }

  /// returns string of total price for selected hours without trailing .0
  String getTotalSpaceFeeDisplay() {
    String totalPrice = getTotalSpaceFee().toString();
    if (totalPrice.endsWith(".0")) {
      totalPrice = totalPrice.substring(0, totalPrice.length - 2);
    }
    if(totalPrice.contains('.')){
      var dd= totalPrice.split('.')[1].length;
      var endIndex = dd > 2 ? totalPrice.indexOf('.') + 3 : totalPrice.indexOf('.') + dd + 1;
      print(totalPrice);
      totalPrice= totalPrice.substring(0,endIndex);
    }
    var price =  space.location.getCurrencySymbol() + totalPrice ;
    return price;
  }

  String getTotalSpaceFeeDisplayTwoDecimals() {
    String totalPrice = Utils.convertToTwoDecimals(getTotalSpaceFee());
    if(totalPrice == ".00"){
      totalPrice = "0.00";
    }
    var price = space.location.getCurrencySymbol() + totalPrice;
    return price;
  }

  /// returns string of total price for the space + insurance
  String getTotalPriceDisplayTwoDecimals() {
    String totalPrice =
        Utils.convertToTwoDecimals(getTotalSpaceFee() + getInsuranceFee());
    var price =  space.location.getCurrencySymbol() + totalPrice;
    return price;
  }
  @override
  int compareTo(Reservation other) {
    // TODO: implement compareTo
    return null;
  }

}

class ReservationRoom {
    int id;
    String name;
    CoverImage coverImage;
    Location location;
    Host host;
    ReservationRoom({
        this.id,
        this.name,
        this.coverImage,
        this.location, 
        this.host
    });

    factory ReservationRoom.fromRawJson(String str) => ReservationRoom.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory ReservationRoom.fromJson(Map<String, dynamic> json, {String currency}) => ReservationRoom(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        coverImage: json["coverImage"] == null ? null : CoverImage.fromJson(json["coverImage"]),
        location: Location.fromAPIJson(json['place'],apicurrency: currency),
        host : Host.fromJson(json['user'])
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "coverImage": coverImage == null ? null : coverImage.toJson(),
        
    };
}

class CoverImage {
    int id;
    int objectId;
    String attachmentFile;

    CoverImage({
        this.id,
        this.objectId,
        this.attachmentFile,
    });

    factory CoverImage.fromRawJson(String str) => CoverImage.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory CoverImage.fromJson(Map<String, dynamic> json) => CoverImage(
        id: json["id"] == null ? null : json["id"],
        objectId: json["objectId"] == null ? null : json["objectId"],
        attachmentFile: json["attachmentFile"] == null ? null :  MtConstants.baseMediaUrl + json["attachmentFile"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "objectId": objectId == null ? null : objectId,
        "attachmentFile": attachmentFile == null ? null : attachmentFile,
    };
}

class UserClass {
    String name;

    UserClass({
        this.name,
    });

    factory UserClass.fromRawJson(String str) => UserClass.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory UserClass.fromJson(Map<String, dynamic> json) => UserClass(
        name: json["name"] == null ? null : json["name"],
    );

    Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
    };
}


class Host {
  final String name;
  final String phoneNumber;
  final String email;

  Host({this.name, this.phoneNumber, this.email});

   factory Host.fromJson(Map<String, dynamic> json) => Host(
        name: json["name"] == null ? null : json["name"],
        phoneNumber: json["phone"] == null ? null : json["phone"],
        email: json["email"] == null ? null : json["email"],
    );
}