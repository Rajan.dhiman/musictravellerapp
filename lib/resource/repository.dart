import 'package:mt_ui/data/models.dart';
import 'apiProvider.dart';

class Repository {
  final apiProvider = ApiProvider();
  Future<List<ListingRoom>> fetchAllSpace({String url = ''}) => apiProvider.fetchSpaceList(query: url);
  Future<Room> fetchSpaceDetail({String url = ''}) => apiProvider.fetchRoom(query: url);
}