import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:http/http.dart';
import 'package:mt_ui/data/models.dart';
import 'package:mt_ui/data/shared_preferences_helper.dart';
import 'package:mt_ui/helpers/constants.dart';

class ApiProvider {
  Client client = Client();
  Future<List<ListingRoom>> fetchSpaceList({String query}) async {
    List<ListingRoom> listingRoomList = [];
    var currencyPrice = await SharedPreferencesHelper.getCurrencyRate();
    var currencyPref = await SharedPreferencesHelper.getCurrencyPreference();
    // print("entered Url : $url");
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
    };
    print("Asdaasdfasfasfdsfvdsvdvsdvadfvadfvadfv------");
    Uri getUrlForQuery(String query) =>
        Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});
    var url = getUrlForQuery(query);
    try {
      await client
          .get(url, headers: headers)
          .then((result) => result.body)
          .then(json.decode)
          .then((json) {
        if (json != null) {
          json['data']['searchCount']['rooms'].forEach((room) {
            listingRoomList.add(ListingRoom.fromAPIJson(room,
                curencyPrefrence: currencyPref, rate: currencyPrice));
          });
          return listingRoomList;
        }
      });
    } catch (error) {
      print('[Erorr] in fetchSpaceList api:- $error');
    }
  }

  Future<Room> fetchRoom({String query}) async {
    log("Fetch Room detail");
    Uri getUrlForQuery(String query) =>
        Uri.http(MtConstants.baseUrl, "/graphql", {"query": query});
    String accessToken = await SharedPreferencesHelper.getAccessToken();
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
    };

    var currencyPrice = await SharedPreferencesHelper.getCurrencyRate();
    var currencyPref = await SharedPreferencesHelper.getCurrencyPreference();
    final room = await client
        .get(getUrlForQuery(query), headers: headers)
        .then((result) => result.body)
        .then(json.decode)
        .then((json) {
      final room = Room.fromAPIJson(json,
          curencyPrefrence: currencyPref, rate: currencyPrice);
      log("Room data fetched from api");
      return room;
    });

    return room;
  }
}
